      PROGRAM Calculate_Fugacity_Coefficients
      implicit none

      integer    MaxMol
      Parameter (MaxMol = 10)

      double precision R
      Parameter       (R = 8.31446d0)

      integer I,J,NMoltype,Tm,io,N

      double precision a_pure(MaxMol),b_pure(MaxMol),b_mix,
     &                 c_pure(MaxMol),c_mix,U_pure(MaxMol),U_mix,
     &                 V_pure(MaxMol),V_mix,k(MaxMol,MaxMol),
     &                 Tc_pure(MaxMol),Pc_pure(MaxMol),w_pure(MaxMol),
     &                 lnf(MaxMol),am,bm,Amix,Bmix,Z_mix,Z(3),Zmin,Zmax,temp,
     &                 P,T,y(MaxMol),ysum,Tc_read,Pc_read,w_read

      logical LCheck(MaxMol)

      character*6   state
      character*24  Cmoltype(MaxMol),Cmol,Cmol2
      character*100 BRICKDIR

      CALL GETENV("BRICK_DIR",BRICKDIR)

      WRITE(6,*)

 10   WRITE(6,'(A)',ADVANCE="NO") "Number of molecule types in mixture: "
      READ(6,*,iostat=io,err=10) Nmoltype
      WRITE(6,*)
      IF(Nmoltype.LT.1) THEN
         WRITE(6,'(A)') "Number of molecule types should be 1 or more"
         GO TO 10
      END IF

      DO Tm=1,Nmoltype
         WRITE(6,'(A14,i2,A2)',ADVANCE="NO") "Molecule type ", Tm, ": "
         READ(6,*) Cmoltype(Tm)
      END DO

      WRITE(6,*)

  20  WRITE(6,'(A)',ADVANCE="NO") "Temperature (in Kelvin) : "
      READ(6,*,iostat=io,err=20) T
      IF(T.LT.0.0d0) THEN
         WRITE(6,'(A)') "Temperature should be positive."
         GO TO 20
      END IF

  30  WRITE(6,'(A)',ADVANCE="NO") "Pressure (in Pascal)    : "
      READ(6,*,iostat=io,err=30) P
      IF(P.LT.0.0d0) THEN
         WRITE(6,'(A)') "Pressure should be positive."
         GO TO 30
      END IF

      IF(NMoltype.EQ.1) THEN
         y(1) = 1.0d0
      ELSE
         WRITE(6,*)
         ysum = 0.0d0
         WRITE(6,'(A)') "Mole fractions/ratios (do not need to be normalized)"
         DO Tm=1,NMoltype
            N=LEN(TRIM(Cmoltype(I)))
  40        WRITE(6,'(A,A)',ADVANCE="NO") Cmoltype(Tm), ": "
            READ(6,*,iostat=io,err=40) y(Tm)
            IF(y(Tm).LT.0.0d0) THEN
               WRITE(6,'(A)') "Number should be positive."
               GO TO 40
            ELSE
               ysum = ysum + y(Tm)
            END IF
         END DO

C     Normalize mole fractions
         DO Tm=1,NMoltype
            y(Tm)=y(Tm)/ysum
         END DO
      END IF


      IF(NMoltype.EQ.1) THEN
         k(1,1) = 0.0d0
         WRITE(6,*)
      ELSE
         WRITE(6,*)
         WRITE(6,'(A)') "Interaction parameters (k_ij):"
         DO I=1,NMoltype
            N=LEN(TRIM(Cmoltype(I)))
            IF(I.NE.NMoltype) WRITE(6,'(A<N>,A)') Cmoltype(I), " with: "
            DO J=I,NMoltype
               IF(I.EQ.J) THEN
                  k(I,J) = 0.0d0
               ELSE
  50              WRITE(6,'(2x,A)',ADVANCE="NO") Cmoltype(J)
                  READ(6,*,iostat=io,err=50) k(I,J)
                  IF((k(I,J).LT.0.0d0).OR.(k(I,J).GT.1.0d0)) THEN
                     WRITE(6,'(A)') "Parameter should be between 0 and 1."
                     GO TO 50
                  END IF
                  k(J,I) = k(I,J)
               END IF
            END DO
            WRITE(6,*)
         END DO
      END IF

      DO Tm=1,NMoltype
         LCheck(Tm)=.false.
      END DO

      OPEN(71,file=TRIM(BRICKDIR)//"/PARAMETERS/critical_properties")
      DO
         READ(71,*,IOSTAT=io) Cmol, Cmol2, Tc_read, Pc_read, w_read
         IF(io.EQ.0) THEN
            DO Tm=1,NMoltype
               IF((Cmol.EQ.Cmoltype(Tm)).OR.(Cmol2.EQ.Cmoltype(Tm))) THEN
                  Tc_pure(Tm) = Tc_read
                  Pc_pure(Tm) = Pc_read
                  w_pure(Tm)  = w_read
                  IF(LCheck(Tm)) THEN
                     WRITE(6,'(A,A)') "Warning, critical properties are defined more than once for: ", Cmoltype(Tm)
                  END IF
                  LCheck(Tm) = .true.
               END IF
            END DO
         ELSEIF(io.GT.0) THEN
            CYCLE
         ELSE
            EXIT
         END IF
      END DO

      DO Tm=1,NMoltype
         IF(.NOT.LCheck(Tm)) THEN
            WRITE(6,'(A,A)') "Critical properties not found for ", Cmoltype(Tm)
   60       WRITE(6,'(A)',ADVANCE="NO") "T_c = "
            READ(6,*,iostat=io,err=60) Tc_pure(Tm)
            IF(Tc_pure(Tm).LT.0.0d0) THEN
               WRITE(6,'(A)') "Critical temperature should be positive."
               GO TO 60
            END IF
   61       WRITE(6,'(A)',ADVANCE="NO") "P_c = "
            READ(6,*,iostat=io,err=61) Pc_pure(Tm)
            IF(Pc_pure(Tm).LT.0.0d0) THEN
               WRITE(6,'(A)') "Critical pressure should be positive."
               GO TO 61
            END IF
   62       WRITE(6,'(A)',ADVANCE="NO") "w   = "
            READ(6,*,iostat=io,err=62) w_pure(Tm)
            WRITE(6,*)
         END IF
      END DO


      DO I=1,NMoltype
         a_pure(I) = 0.45724d0*R*R*Tc_pure(I)*Tc_pure(I)/Pc_pure(I)
         b_pure(I) = 0.07780d0*R*Tc_pure(I)/Pc_pure(I)
         c_pure(I) = a_pure(I)*(1.0d0+(0.37464d0+1.54226d0*w_pure(I)-0.26992*w_pure(I)*w_pure(I))
     &               *(1.0d0 - dsqrt(T/Tc_pure(I))))**2
         U_pure(I) = c_pure(I)*P/(R*R*T*T)
         V_pure(I) = b_pure(I)*P/(R*T)
      END DO


      b_mix = 0.0d0
      c_mix = 0.0d0
      U_mix = 0.0d0
      V_mix = 0.0d0

      DO I=1,NMoltype
         b_mix = b_mix + y(I)*b_pure(I)
         V_mix = V_mix + y(I)*V_pure(I)
         DO J=1,NMolType
            c_mix = c_mix + y(I)*y(J)*(1.0d0 - k(I,J))*dsqrt(c_pure(I)*c_pure(J))
            U_mix = U_mix + y(I)*y(J)*(1.0d0 - k(I,J))*dsqrt(U_pure(I)*U_pure(J))
         END DO
      END DO

      CALL Solve_CubicEquation(1.0d0,
     &                        -(1.0d0-V_mix),
     &                         U_mix-3.0d0*V_mix*V_mix-2.0d0*V_mix,
     &                        -(U_mix*V_mix-V_mix*V_mix-V_mix*V_mix*V_mix),
     &                         Z(1),Z(2),Z(3))

      IF((Z(2).LT.0.0d0).AND.(Z(3).LT.0.0d0)) THEN
         Z_mix = Z(1)
      ELSE
         Zmin = Z(1)
         Zmax = Z(1)
         DO I=2,3
            IF(Z(I).LT.Zmin) Zmin = Z(I)
            IF(Z(I).GT.Zmax) Zmax = Z(I)
         END DO

   1     WRITE(6,'(A)',ADVANCE='NO') "Liquid or Gas? (l/g): "
         READ(6,*) state
         IF(state(1:1).EQ."l") THEN
            Z_mix = Zmax
         ELSEIF(state(1:1).EQ."g") THEN
            Z_mix = Zmin
         ELSE
            GO TO 1
         END IF

      END IF

      DO I=1,NMoltype
         temp = 0.0d0
         DO J=1,NMoltype
            temp = temp + y(J)*(1.0d0 - k(I,J))*dsqrt(U_pure(I)*U_pure(J))
         END DO

         lnf(I) = (1.0d0/(dsqrt(8.0d0)*V_mix))*(U_mix*V_pure(I)/V_mix - 2.0d0*temp)
     &          *dlog((Z_mix + (1.0d0 + dsqrt(2.0d0))*V_mix)/(Z_mix + (1.0d0 - dsqrt(2.0d0))*V_mix))
     &          + (V_pure(I)/V_mix)*(Z_mix - 1.0d0) - dlog(Z_mix - V_mix)

      END DO

      WRITE(6,*)
      WRITE(6,'(A)') "Fugacity Coefficients                       "
      WRITE(6,'(A)') "--------------------------------------------"
      DO Tm=1,Nmoltype
         WRITE(6,'(A,f15.9)') Cmoltype(Tm), dexp(lnf(Tm))
      END DO
      WRITE(6,'(A)') "--------------------------------------------"
      WRITE(6,'(A)') "Values can be directly used as input."
      WRITE(6,*)

      STOP

      CONTAINS

      SUBROUTINE Solve_CubicEquation(a_in,b_in,c_in,d_in,x1,x2,x3)
      implicit none

C      Solve the cubic equation
C     a_in*x^3 + b_in*x^2 + c_in*x + d_in = 0

      double precision a_in,b_in,c_in,d_in,a,b,c,d,theta,x1,x2,x3,Q,R,S,T,M,TwoPi
      Parameter (TwoPi = 8.0d0*DATAN(1.0d0))

      x1 = 0.0d0
      x2 = 0.0d0
      x3 = 0.0d0

      a = 1.0d0   ! Leading coefficient = 1
      b = b_in/a_in
      c = c_in/a_in
      d = d_in/a_in

      Q = (b*b-3.0d0*c)/9.0d0
      R = (2.0d0*b*b*b - 9.0d0*b*c + 27.0d0*d)/54.0d0

      M = R*R - Q*Q*Q

      IF(M.LE.0.0d0) THEN
         theta = dacos(R/dsqrt(Q*Q*Q))

         x1 = -(2.0d0*dsqrt(Q)*dcos(theta/3.0d0)) - b/3.0d0
         x2 = -(2.0d0*dsqrt(Q)*dcos((theta+TwoPi)/3.0d0)) - b/3.0d0
         x3 = -(2.0d0*dsqrt(Q)*dcos((theta-TwoPi)/3.0d0)) - b/3.0d0

      ELSE

         S = (-R+dsqrt(M))**(1.0d0/3.0d0)
         T = (-R-dsqrt(M))**(1.0d0/3.0d0)

         x1 = S + T - b/3.0d0

      END IF

      END SUBROUTINE

      END
