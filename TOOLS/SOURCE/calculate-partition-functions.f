      PROGRAM calculate_partition_function
      implicit none

      integer Max_Mol,Max_Trot,Max_Tvib
      parameter (Max_Mol  = 20)
      parameter (Max_Trot = 3)
      parameter (Max_Tvib = 40)

      double precision kB,NA,h,R,onepi,twopi,MeterstoAngstrom
      parameter (kB = 1.38064852D-23)
      parameter (NA = 6.022140857D+23)
      parameter (h  = 6.626070040D-34)
      parameter (R  = kB*NA)
      parameter (onepi = 4.0d0*DATAN(1.0d0))
      parameter (twopi = 8.0d0*DATAN(1.0d0))
      parameter (MeterstoAngstrom = -69.0775527898d0)

      integer  I,Tm,Nmoltype,Symmetry_number,Nrottemp,Nvibtemp,io
      double precision T,electronic_degeneracy,electronic_ground_energy,molar_mass,
     &         qtrans,qrot,qvib,qelec,Trot(Max_Trot),Tvib(Max_Tvib),Qpartition(Max_Mol)
      character*24  Cmoltype(Max_Mol)
      character*100 BRICKDIR,parameterfile
      logical Lfile_exist

      CALL GETENV("BRICK_DIR",BRICKDIR)

      WRITE(6,*)

 10   WRITE(6,'(A)',ADVANCE="NO") "Number of molecule types: "
      READ(6,*,iostat=io,err=10) Nmoltype
      WRITE(6,*)
      IF(Nmoltype.LT.1) THEN
         WRITE(6,'(A)') "Number of molecule types should be 1 or more"
         GO TO 10
      END IF

      DO Tm=1,Nmoltype
         WRITE(6,'(A14,i2,A2)',ADVANCE="NO") "Molecule type ", Tm, ": "
         READ(6,*) Cmoltype(Tm)
      END DO

      WRITE(6,*)

  20  WRITE(6,'(A)',ADVANCE="NO") "Temperature (in Kelvin): "
      READ(6,*,iostat=io,err=20) T
      IF(T.LT.0.0d0) THEN
         WRITE(6,'(A)') "Temperature should be positive"
         GO TO 20
      END IF

      WRITE(6,*)

      DO Tm=1,Nmoltype

         parameterfile=TRIM(BRICKDIR) // "/PARAMETERS/PARTITIONFUNCTIONS/" // Cmoltype(Tm)
         INQUIRE(file=parameterfile,EXIST=Lfile_exist)
         IF(.NOT.Lfile_exist) THEN
            WRITE(6,'(A,A)') "Parameters not found for: ", Cmoltype(Tm)
            CYCLE
         END IF

         OPEN(71,file=parameterfile)

         READ(71,*)
         READ(71,*) Symmetry_number, electronic_degeneracy, electronic_ground_energy, molar_mass

         READ(71,*) Nrottemp
         READ(71,*) (Trot(I), I=1,Nrottemp)

         READ(71,*) Nvibtemp
         READ(71,*) (Tvib(I), I=1,Nvibtemp)

         CLOSE(71)

         qtrans = 1.5d0*dlog(twopi*kB*T*molar_mass/(h*h*1000.0d0*NA))
         qelec  = dlog(electronic_degeneracy) + 1000.0d0*electronic_ground_energy/(R*T)

         IF(Nrottemp.EQ.1) THEN
            qrot = dlog(T/(Symmetry_number*Trot(1)))
         ELSEIF(Nrottemp.EQ.3) THEN
            qrot = 0.5d0*dlog(onepi*T*T*T/(Symmetry_number*Symmetry_number*Trot(1)*Trot(2)*Trot(3)))
         ELSE
            WRITE(6,'(A,A)') "1 or 3 rotational temperatures should be given for: ", Cmoltype(Tm)
            CYCLE
         END IF

         qvib = 0.0d0
         DO I=1,Nvibtemp
            qvib = qvib - dlog(1.0d0 - dexp(-Tvib(I)/T))
         END DO

         Qpartition(Tm) = qtrans + qrot + qelec + qvib + MeterstoAngstrom

      END DO
      
      WRITE(6,'(A)') "Chemical potential"
      WRITE(6,'(A)') "--------------------------------------------"
      DO Tm=1,Nmoltype
         WRITE(6,'(A,f9.3,1x,A8)') Cmoltype(Tm), -R*T*Qpartition(Tm)*0.001d0, "[kJ/mol]"
      END DO
      WRITE(6,'(A)') "--------------------------------------------"
      WRITE(6,*)
      

      WRITE(6,'(A)') "Natural logarithm of the partition functions"
      WRITE(6,'(A)') "--------------------------------------------"
      DO Tm=1,Nmoltype
         WRITE(6,'(A,f15.9)') Cmoltype(Tm), Qpartition(Tm)
      END DO
      WRITE(6,'(A)') "--------------------------------------------"
      WRITE(6,'(A)') "Values can be directly used as input."

      WRITE(6,*)

      STOP
      END
