      PROGRAM integrate_dU_dlambda
      implicit none

      integer I,N_LambdaBins,N
      double precision lambda(1001),dUdl(1001),step_size,lambda_value,
     & Y2nd(1001),dUdl_int,SUM,kB,NA,R,mu_excess_kelvin,mu_excess_kJpermole
      parameter (kB = 1.38064852D-23)
      parameter (NA = 6.022140857D+23)
      parameter (R  = kB*NA)
      logical arguments_from_command_line     
      character dummy,answer
      character*256 dUdlfile

      dUdlfile="OUTPUT/CFC/dU_dlambda.out"

      WRITE(6,*)
      WRITE(6,'(A)') "\033[1;33m###############################################################\033[0m"
      WRITE(6,'(A)') "\033[1;33m Integrate dU/dlambda to obtain the excess chemical potential  \033[0m"
      WRITE(6,'(A)') "\033[1;33m###############################################################\033[0m"
      WRITE(6,*)
      WRITE(6,'(A)',ADVANCE="NO") "Use the file 'OUTPUT/CFC/dU_dlambda.out' (y/n): "
      READ(5,*) answer
      IF(answer(1:1).EQ."n".OR.answer(1:1).EQ."N") THEN
         WRITE(6,'(A)',ADVANCE="NO") "Name of the file: "
         READ(5,*) dUdlfile
      END IF
      WRITE(6,*)

C     Read data from the file dU_dlambda.out
      OPEN(1,file=dUdlfile)
      READ(1,'(A,2x,I4)') dummy, N_LambdaBins

      DO I=1,N_LambdaBins
         READ(1,*) lambda(I+1), dUdl(I+1)
      END DO
C     Calculate 2nd derivatives
      CALL spline_der(lambda,dUdl,N_LambdaBins+1,Y2nd)

      N = 10000000  ! Number of points for the interpolation 
      step_size = 1.0d0/dble(N) ! Increase lambda by step_size from 0 to 1

      lambda_value = 0.0d0

C     Interpolate with a spline and integrate using the trapezoidal rule
      SUM = 0.0d0
      CALL splint(lambda,dUdl,Y2nd,N_LambdaBins+1,lambda_value,dUdl_int)
      SUM = SUM + dUdl_int

      DO I=2,N
         lambda_value = lambda_value + step_size
         CALL splint(lambda,dUdl,Y2nd,N_LambdaBins+1,lambda_value,dUdl_int)
         SUM = SUM + 2.0d0*dUdl_int
      END DO

      lambda_value = lambda_value + step_size
      CALL splint(lambda,dUdl,Y2nd,N_LambdaBins+1,lambda_value,dUdl_int)
      SUM = SUM + dUdl_int

C     Convert integral to chemical potential (the factor 0.5 originates from the trapezoidal rule)      
      mu_excess_kelvin    = SUM * step_size * 0.5d0
      mu_excess_kJpermole = SUM * step_size * 0.5d0 * R/1000.0d0

C     Print the result
      WRITE(6,'(A)') "Excess chemical potential"
      WRITE(6,'(A)') "--------------------------------------------"
      WRITE(6,'(f9.3,1x,A3)') mu_excess_kelvin,    "[K]"
      WRITE(6,'(f9.3,1x,A8)') mu_excess_kJpermole, "[kJ/mol]"
      WRITE(6,'(A)') "--------------------------------------------"
      WRITE(6,*)

      END 

      SUBROUTINE spline_der(LS,dUdl,N_LambdaBins,Y2nd)
      IMPLICIT none

      
      DOUBLE PRECISION dUdl(1001),P,Sig,Y2nd(1001),
     &                 U(1001),LS(1001)
      INTEGER I,K,N_LambdaBins

      Y2nd(1)=0.0d0
      U(1)=0.0d0
      
      DO I=2,N_LambdaBins
         Sig=(LS(I)-LS(I-1))/(LS(I+1)-LS(I-1))
         P=Sig*Y2nd(I-1)+2.0d0
         Y2nd(I)=(Sig-1.0d0)/P
         U(I)=(6.0d0*((dUdl(I+1)-dUdl(I))/(LS(I+1)-LS(I))-(dUdl(I)-dUdl(I-1))
     &        /(LS(I)-LS(I-1)))/(LS(I+1)-LS(I-1))-Sig*U(I-1))/P
      END DO

      DO K=N_LambdaBins,1,-1
         Y2nd(K)=Y2nd(K)*Y2nd(K+1)+U(K)
      END DO

      RETURN
      END SUBROUTINE
   
      SUBROUTINE splint(Xa,Ya,Y2a,N_LambdaBins,Lambda,W_Sinterpolated)
      IMPLICIT none
      
      INTEGER N_LambdaBins,K,Khi,Klo,Ibin
      DOUBLE PRECISION Lambda,W_Sinterpolated,Xa(1001),Y2a(1001),Ya(1001),A,B,H

      Klo=1
      Khi=N_LambdaBins
3     CONTINUE
      IF (Khi-Klo.Gt.1) THEN
        K=(Khi+Klo)/2
         IF(Xa(K).Gt.Lambda) THEN
            Khi=K
         ELSE
            Klo=K
         END IF
         GO TO 3
      END IF
      H=Xa(Khi)-Xa(Klo)
      A=(Xa(Khi)-Lambda)/H
      B=(Lambda-Xa(Klo))/H
      W_Sinterpolated=A*Ya(Klo)+B*Ya(Khi)+((A**3-A)*Y2a(Klo)+(B**3-B)*Y2a(Khi))
     &                *(H**2)/6.0d0
      
      END SUBROUTINE
