      program iterative_scheme
      implicit none

      integer Maxcol,Maxrow
      Parameter (Maxcol = 5)
      Parameter (Maxrow = 1000)

      integer io,I,J,K,Ncol,Nrow,Nweightfunctions,arg_int(10)
      double precision Lambda(Maxrow),Weight(Maxrow,Maxcol),plambda(Maxrow,Maxcol),WeightNew(Maxrow,Maxcol),WeightMin
      logical arguments_from_command_line
      character*256 line,weightfile,plambdafile,arg(10)
      character*100 dummy
      character*1 answer

      arguments_from_command_line=.false.
      IF(COMMAND_ARGUMENT_COUNT().GT.1) THEN
         DO I=2,COMMAND_ARGUMENT_COUNT()
            CALL GETARG(I, arg(I))
            READ(arg(I),*) arg_int(I-1)
         END DO
         arguments_from_command_line=.true.
      END IF

      weightfile="OUTPUT/WEIGHTFUNCTIONS/weightfunction.out"
      plambdafile="OUTPUT/CFC/plambda.out"

      WRITE(6,*)
      WRITE(6,'(A)') "\033[1;33m###############################################################\033[0m"
      WRITE(6,'(A)') "\033[1;33m Apply the iterative scheme to obtain a better weightfunction  \033[0m"
      WRITE(6,'(A)') "\033[1;33m###############################################################\033[0m"
      WRITE(6,*)
      IF(.NOT.arguments_from_command_line) THEN
         WRITE(6,'(A)',ADVANCE="NO") "Use the file 'OUTPUT/WEIGHTFUNCTIONS/weightfunction.out' (y/n): "
         READ(5,*) answer
         IF(answer(1:1).EQ."n".OR.answer(1:1).EQ."N") THEN
            WRITE(6,'(A)',ADVANCE="NO") "Name of the file: "
            READ(5,*) weightfile
         END IF
         WRITE(6,'(A)',ADVANCE="NO") "Use the file 'OUTPUT/CFC/plambda.out' (y/n): "
         READ(5,*) answer
         IF(answer(1:1).EQ."n".OR.answer(1:1).EQ."N") THEN
            WRITE(6,'(A)',ADVANCE="NO") "Name of the file: "
            READ(5,*) plambdafile
         END IF
      END IF

      Nweightfunctions=0

      OPEN(7,file=weightfile)
      OPEN(8,file=plambdafile)
      OPEN(9,file="weightfunction.iterative")

  10  CONTINUE
      READ(7,'(A)',END=30) line
      WRITE(9,'(A)') TRIM(line)

      Nweightfunctions = Nweightfunctions + 1

      WRITE(6,*)
      WRITE(6,'(A,i2)') "Weightfunction ", Nweightfunctions
      WRITE(6,'(A)') "\033[1;33m---------------------------------------------------------------\033[0m"

      IF(.NOT.arguments_from_command_line) THEN
  20     WRITE(6,'(A)',ADVANCE="NO") "Number of Boxes or Reaction Steps: "
         READ(6,*,iostat=io,err=20) Ncol
         IF(Ncol.LT.1) THEN
            WRITE(6,'(A)') "Number should be a positive integer"
            GO TO 20
         END IF
      ELSE
         Ncol = arg_int(Nweightfunctions)
         WRITE(6,*) Ncol
      END IF

      J=1
      DO
         READ(7,*,IOSTAT=io) Lambda(J), (Weight(J,K), K=1,Ncol)
         IF(io.EQ.0) THEN
            IF(Lambda(J).LT.0.0d0.OR.Lambda(J).GT.1.0d0) THEN
               WRITE(6,*) "Error Input"
               STOP
            END IF
            READ(8,*,IOSTAT=io) dummy, (dummy, plambda(J,K), K=1,Ncol)
            DO K=1,Ncol
               WeightNew(J,K) = Weight(J,K)-0.5d0*dlog(max(1.0D-8,plambda(J,K)))
            END DO

            J=J+1
            CYCLE
         ELSE
            Nrow=J-1
            WeightMin = WeightNew(1,1)
            DO J=1,Nrow
               DO K=1,Ncol
                  WeightMin = min(WeightMin,WeightNew(J,K))
               END DO
            END DO

            DO J=1,Nrow
               WRITE(9,'(f10.7,99(1x,e20.10e3))') Lambda(J), (WeightNew(J,K)-WeightMin, K=1,Ncol)
            END DO

            WRITE(9,*)
            WRITE(9,*)

            BACKSPACE(7)
            GO TO 10
         END IF

      END DO

  30  CONTINUE
      CLOSE(7)
      CLOSE(8)
      CLOSE(9)

      WRITE(6,*)
      WRITE(6,'(A)') "Finished. Copy weightfunction.iterative to INPUT/weightfunction.in to use in simulations."
      WRITE(6,*)

      STOP
      END
