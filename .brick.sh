brick() {
   if [ $# -eq 0 ]; then
      cd ${BRICK_DIR}

   elif [ $1 == "update" ]; then
      cd ${BRICK_DIR} && git pull

      read -p "Re-compile? Y/n " answer
      if [ ! $answer == 'n' ] || [ -z $answer ] ; then
         brick compile
      fi

   elif [ $1 == "source" ]; then
      cd ${BRICK_DIR}/SOURCE

   elif [ $1 == "pack" ]; then
      tar --exclude OUTPUT/CONFIGURATIONS -czvf package.tar.gz INPUT OUTPUT sim.log

   elif [ $1 == "new" ]; then
      if [ $# -eq 1 ]; then
         echo "Input name of new folder"
      else
         if [ -d $2 ]; then
            echo "Folder already exists"
         else
            mkdir $2
            cd $2
            cp ${BRICK_DIR}/RUN/run .
            mkdir INPUT
            cp ${BRICK_DIR}/TEMPLATES/settings.example INPUT/settings.in
         fi
      fi

   elif [ $1 == "input" ]; then
      ${BRICK_DIR}/TOOLS/create-input.sh

   elif [ $1 == "compile" ]; then
      if [ $# -eq 1 ]; then
##       if ifort exists only compile with it, if not use gfortran, if that also doesn't exist, say that they weren't found MERT 27/07/21
         if command -v ifort > ${BRICK_DIR}/.compiler_info; then
            ${BRICK_DIR}/COMPILE/compile-ifort-optimize
            ${BRICK_DIR}/COMPILE/compile-ifort-debug
            ${BRICK_DIR}/COMPILE/compile-ifort-tools
         elif command -v gfortran > ${BRICK_DIR}/.compiler_info; then
            ${BRICK_DIR}/COMPILE/compile-gfortran-optimize
            ${BRICK_DIR}/COMPILE/compile-gfortran-debug
            ${BRICK_DIR}/COMPILE/compile-gfortran-tools
         else
            echo "Intel Fortran compiler or gfortran could not be found"
         fi
##       if ifort exists only compile with it, if not use gfortran, if that also doesn't exist, say that they weren't found MERT 27/07/21
      elif [ $2 == "-h" ] || [ $2 == "--help" ]; then
         echo "Compile the main code or tools. Options: "
         echo "-i/--intel/--ifort         : use the Intel Fortran Compiler"
         echo "-g/--gnufortran/--gfortran : use the GNU Fortran Compiler"
         echo "-d/--debug                 : compile with debug options"
         echo "-o/--optimize              : compile with optimizations"
         echo "-t/--tools                 : compile the tools (uses ifort)"
      else
         res=1
         while [ "$2" != "" ]; do
            case $2 in
               -i | --intel | --ifort )         res=$((2*res))
                                    ;;
               -g | --gnufortran | --gfortran ) res=$((3*res))
                                    ;;
               -d | --debug )                   res=$((5*res))
                                    ;;
               -o | --optimize )                res=$((7*res))
                                    ;;
               -t | --tools )                   res=$((11*res))
                                    ;;
                * )                             echo "Option unknown: " $2
            esac
            shift
         done

         if [ $(($res % 11)) -eq 0 ]; then
##       if ifort exists only compile with it, if not use gfortran MERT 27/07/21
            if command -v ifort > ${BRICK_DIR}/.compiler_info; then
               ${BRICK_DIR}/COMPILE/compile-ifort-tools
            elif command -v gfortran ${BRICK_DIR/.compiler_info}; then
               ${BRICK_DIR}/COMPILE/compile-gfortran-tools
            fi 
##       if ifort exists only compile with it, if not use gfortran MERT 27/07/21
            res=$(($res / 11))
         fi

         if [ $res -eq 2 ]; then
            ${BRICK_DIR}/COMPILE/compile-ifort-optimize
            ${BRICK_DIR}/COMPILE/compile-ifort-debug
         elif [ $res -eq 3 ]; then
            ${BRICK_DIR}/COMPILE/compile-gfortran-optimize
            ${BRICK_DIR}/COMPILE/compile-gfortran-debug
         elif [ $res -eq 5 ]; then
            ${BRICK_DIR}/COMPILE/compile-ifort-debug
            ${BRICK_DIR}/COMPILE/compile-gfortran-debug
         elif [ $res -eq 7 ]; then
            ${BRICK_DIR}/COMPILE/compile-ifort-optimize
            ${BRICK_DIR}/COMPILE/compile-gfortran-optimize
         elif [ $res -eq 10 ]; then
            ${BRICK_DIR}/COMPILE/compile-ifort-debug
         elif [ $res -eq 14 ]; then
            ${BRICK_DIR}/COMPILE/compile-ifort-optimize
         elif [ $res -eq 15 ]; then
            ${BRICK_DIR}/COMPILE/compile-gfortran-debug
         elif [ $res -eq 21 ]; then
            ${BRICK_DIR}/COMPILE/compile-gfortran-optimize
         fi
      fi

   elif [ $1 == "calculate" ]; then
      if [ $# -eq 1 ]; then
         echo "Not enough arguments, choose what to calculate"
      elif [ $2 == "partition-functions" ]; then
         ${BRICK_DIR}/EXECUTABLES/calculate-partition-functions
      elif [ $2 == "fugacity-coefficients" ]; then
         ${BRICK_DIR}/EXECUTABLES/calculate-fugacity-coefficients
      elif [ $2 == "Ewald-parameters" ]; then
         ${BRICK_DIR}/EXECUTABLES/calculate-ewald-parameters $3
      fi

   elif [ $1 == "integrate" ]; then
      if [ $2 == "dUdl" ]; then
         ${BRICK_DIR}/EXECUTABLES/integrate-dUdl $3
      fi
      
   elif [ $1 == "wolfplot" ]; then
      ${BRICK_DIR}/TOOLS/wolfplot.sh

   elif [ $1 == "smoothen-weightfunction" ]; then
      ${BRICK_DIR}/EXECUTABLES/smoothen-weightfunction $@

   elif [ $1 == "iterative-scheme" ]; then
      ${BRICK_DIR}/EXECUTABLES/iterative-scheme $@

   else
      echo "Command unknown"
   fi
}
