CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC                                                                                             CCC
CCC                                  Initialize Wang Landau                                     CCC
CCC                                                                                             CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      SUBROUTINE Wang_Landau_Initialize
      implicit none

      include "global_variables.inc"
      include "output.inc"
      include "settings.inc"
      include "wang_landau.inc"

      integer Ifrac,Rs,Ib,Ibin,Ireac,Tf,io
      character dummy

      CALL system('mkdir OUTPUT/WEIGHTFUNCTIONS')

CCC   Set weightfunction and histogram to zero
      DO Ifrac=1,MaxFrac
         DO Rs=1,MaxReactionStep
            DO Ib=1,2
               DO Ibin=1,MaxLambdaBin
                  Weight(Ibin,Ib,Rs,Ifrac)     = 0.0d0
                  Weightprev(Ibin,Ib,Rs,Ifrac) = 0.0d0
                  H(Ibin,Ib,Rs,Ifrac)          = 0.0d0
               END DO
            END DO
         END DO
         Fmod(Ifrac) = Fmod_in
      END DO

CCC   Read weightfunction from file
      IF(Lweight) THEN

         WRITE(6,'(A)') "Weightfunction"

         OPEN(200,file="./INPUT/weightfunction.in")

         DO Ifrac=1,N_Frac

            Tf = Type_Frac(Ifrac)

            READ(200,*,IOSTAT=io,err=10) dummy, N_LambdaBin(Ifrac)

            IF(Tf.EQ.1) THEN
               Ib = Box_Frac(Ifrac)
               Rs = 1
               DO Ibin=1,N_LambdaBin(Ifrac)
                  READ(200,*,IOSTAT=io,err=20) dummy, Weight(Ibin,Ib,Rs,Ifrac)
                  Weightprev(Ibin,Ib,Rs,Ifrac) = Weight(Ibin,Ib,Rs,Ifrac)
               END DO
            ELSEIF(Tf.EQ.2) THEN
               Rs = 1
               DO Ibin=1,N_LambdaBin(Ifrac)
                  READ(200,*,IOSTAT=io,err=30) dummy,(Weight(Ibin,Ib,Rs,Ifrac), Ib=1,N_Box)
                  DO Ib=1,N_Box
                     Weightprev(Ibin,Ib,Rs,Ifrac) = Weight(Ibin,Ib,Rs,Ifrac)
                  END DO
               END DO
            ELSEIF(Tf.EQ.3) THEN
               Ireac = Reaction_Frac(Ifrac)
               Ib = Box_Frac(Ifrac)
               DO Ibin=1,N_LambdaBin(Ifrac)
                  READ(200,*,IOSTAT=io,err=40) dummy,(Weight(Ibin,Ib,Rs,Ifrac), Rs=1,N_ReactionStep(Ireac))
                  DO Rs=1,N_ReactionStep(Ireac)
                     Weightprev(Ibin,Ib,Rs,Ifrac) = Weight(Ibin,Ib,Rs,Ifrac)
                  END DO
               END DO
            ELSEIF(Tf.EQ.4) THEN
               Ib = Box_Frac(Ifrac)
               Rs = 1
               DO Ibin=1,N_LambdaBin(Ifrac)
                  READ(200,*,IOSTAT=io,err=50) dummy, Weight(Ibin,Ib,Rs,Ifrac)
                  Weightprev(Ibin,Ib,Rs,Ifrac) = Weight(Ibin,Ib,Rs,Ifrac)
               END DO
            END IF

            IF(Ifrac.NE.N_Frac) THEN
               READ(200,*)
               READ(200,*)
            END IF

         END DO

         CLOSE(200)

         WRITE(6,'(A,A)') OK, "Read from ./INPUT/weightfunction.in"
         WRITE(6,*)

      END IF

      RETURN

CCC   Error handling reading weightfunctions from file.
 10   IF(io.NE.0) THEN
         WRITE(6,'(A,A,i2)') ERROR, "Reading number of bins for weightfunction from file. Ifrac = ", Ifrac
         STOP
      END IF
 20   IF(io.NE.0) THEN
         WRITE(6,'(A,A,i2)') ERROR, "Reading weightfunction from file. Type = NVT/NPT Ensemble, Ifrac = ", Ifrac
         STOP
      END IF
 30   IF(io.NE.0) THEN
         WRITE(6,'(A,A,i2)') ERROR, "Reading weightfunction from file. Type = Gibbs Ensemble, Ifrac = ", Ifrac
         STOP
      END IF
 40   IF(io.NE.0) THEN
         WRITE(6,'(A,A,i2)') ERROR, "Reading weightfunction from file. Type = Reaction Ensemble, Ifrac = ", Ifrac
         STOP
      END IF
 50   IF(io.NE.0) THEN
         WRITE(6,'(A,A,i2)') ERROR, "Reading weightfunction from file. Type = Grand Canonical Ensemble, Ifrac = ", Ifrac
         STOP
      END IF

      END


CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC                                                                                             CCC
CCC                           Update the histogram of a fractional                              CCC
CCC                                                                                             CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      SUBROUTINE Wang_Landau_Update(Ifrac)
      implicit none

      include "global_variables.inc"
      include "settings.inc"
      include "wang_landau.inc"

      integer           Ibin,Rs,Tf,Ib,Ifrac
      double precision  Lf

      IF(Fmod(Ifrac).LT.1.0D-6) RETURN

      Tf = Type_Frac(Ifrac)
      Lf = Lambda_Frac(Ifrac)
      Ib = Box_Frac(Ifrac)

      IF(Tf.EQ.3) THEN
         Rs = ReactionStep_Frac(Ifrac)
      ELSE
         Rs = 1
      END IF

      Ibin = 1 + int(dble(N_LambdaBin(Ifrac))*Lf)
      Weight(Ibin,Ib,Rs,Ifrac) = Weight(Ibin,Ib,Rs,Ifrac) - Fmod(Ifrac)
      H(Ibin,Ib,Rs,Ifrac) = H(Ibin,Ib,Rs,Ifrac) + 1.0d0

      RETURN
      END



CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC                                                                                             CCC
CCC                             Check if the histograms are flat                                CCC
CCC                                                                                             CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      SUBROUTINE Wang_Landau_Check_For_Flatness(Icycle)
      implicit none

      include "global_variables.inc"
      include "output.inc"
      include "settings.inc"
      include "wang_landau.inc"

      integer Icycle,Ib,Rs,Ifrac,Tf,Ibin,Ireac,unit(MaxFrac),Iff
      double precision MinH,MaxH,AvH,MinW
      character*100 weightfunctionfile

      save  unit,Iff
      data  Iff /0/

CCC   Initialize the files (each fractional gets a separate file)
      IF(Iff.EQ.0) THEN
         DO Ifrac=1,N_Frac
            unit(Ifrac) = 200 + Ifrac
            IF(Ifrac.LE.9) THEN
               WRITE(weightfunctionfile,'(A,i1,A)') "./OUTPUT/WEIGHTFUNCTIONS/weightfunction-", Ifrac, ".dat"
            ELSE
               WRITE(weightfunctionfile,'(A,i2,A)') "./OUTPUT/WEIGHTFUNCTIONS/weightfunction-", Ifrac, ".dat"
            END IF
            OPEN(unit=unit(Ifrac),file=weightfunctionfile)
            IF(Type_Frac(Ifrac).NE.3) THEN
               WRITE(unit(Ifrac),'(A,A)') "# Evolution of the weightfunction during equilibration phase for fractional: ",
     &            TRIM(Name_Frac(Ifrac))
            ELSE
               WRITE(unit(Ifrac),'(A,A)') "# Evolution of the weightfunction during equilibration phase for fractional: ",
     &            TRIM(Name_Reaction(Reaction_Frac(Ifrac)))
            END IF
         END DO
         Iff=1
      END IF

CCC   Check for each fractional if the histogram is flat
      DO Ifrac=1,N_Frac
         Tf = Type_Frac(Ifrac)

         IF(Tf.EQ.1) THEN  ! NVT/NPT
            Ib   = Box_Frac(Ifrac)
            Rs   = 1
            MinH = H(1,Ib,Rs,Ifrac)
            MaxH = MinH
            AvH  = 0.0d0

            DO Ibin=1,N_LambdaBin(Ifrac)
               MinH = min(MinH,H(Ibin,Ib,Rs,Ifrac))
               MaxH = max(MaxH,H(Ibin,Ib,Rs,Ifrac))
               AvH  = AvH + H(Ibin,Ib,Rs,Ifrac)
            END DO

            AvH = AvH/dble(N_LambdaBin(Ifrac))

            IF((MaxH-MinH).LT.flatc*AvH) THEN
               MinW = Weight(1,Ib,Rs,Ifrac)
               DO Ibin=1,N_LambdaBin(Ifrac)
                  MinW = min(MinW,Weight(Ibin,Ib,Rs,Ifrac))
               END DO

               DO Ibin=1,N_LambdaBin(Ifrac)
                  Weight(Ibin,Ib,Rs,Ifrac)     = Weight(Ibin,Ib,Rs,Ifrac) - MinW
                  Weightprev(Ibin,Ib,Rs,Ifrac) = Weight(Ibin,Ib,Rs,Ifrac)
               END DO

               WRITE(Unit(Ifrac),'(A,i10)') "# Weightfunction after cycle ", Icycle

               DO Ibin=1,N_LambdaBin(Ifrac)
                  WRITE(unit(Ifrac),'(f10.7,1x,e20.10e3)') dble(Ibin-1)/dble(N_LambdaBin(Ifrac)-1),
     &              Weight(Ibin,Ib,Rs,Ifrac)
               END DO

               WRITE(unit(Ifrac),*)
               WRITE(unit(Ifrac),*)

               Fmod(Ifrac) = Fmod(Ifrac)/Fred

               DO Ibin=1,N_LambdaBin(Ifrac)
                  H(Ibin,Ib,Rs,Ifrac) = 0.0d0
               END DO
            END IF

         ELSEIF(Tf.EQ.2) THEN ! Gibbs Ensemble
            Rs   = 1
            MinH = H(1,1,Rs,Ifrac)
            MaxH = MinH
            AvH  = 0.0d0

            DO Ib=1,N_Box
               DO Ibin=1,N_LambdaBin(Ifrac)
                  MinH = min(MinH,H(Ibin,Ib,Rs,Ifrac))
                  MaxH = max(MaxH,H(Ibin,Ib,Rs,Ifrac))
                  AvH  = AvH + H(Ibin,Ib,Rs,Ifrac)
               END DO
            END DO

            AvH = AvH/dble(N_Box*N_LambdaBin(Ifrac))

            IF((MaxH-MinH).LT.flatc*AvH) THEN
               MinW = Weight(1,1,Rs,Ifrac)
               DO Ib=1,N_Box
                  DO Ibin=1,N_LambdaBin(Ifrac)
                     MinW = min(MinW,Weight(Ibin,Ib,Rs,Ifrac))
                  END DO
               END DO

               DO Ib=1,N_Box
                  DO Ibin=1,N_LambdaBin(Ifrac)
                     Weight(Ibin,Ib,Rs,Ifrac)     = Weight(Ibin,Ib,Rs,Ifrac) - MinW
                     Weightprev(Ibin,Ib,Rs,Ifrac) = Weight(Ibin,Ib,Rs,Ifrac)
                  END DO
               END DO

               WRITE(Unit(Ifrac),'(A,i10)') "# Weightfunction after cycle ", Icycle

               DO Ibin=1,N_LambdaBin(Ifrac)
                  WRITE(unit(Ifrac),'(f10.7,2(1x,e20.10e3))') dble(Ibin-1)/dble(N_LambdaBin(Ifrac)-1),
     &              (Weight(Ibin,Ib,Rs,Ifrac), Ib=1,N_Box)
               END DO

               WRITE(unit(Ifrac),*)
               WRITE(unit(Ifrac),*)

               Fmod(Ifrac) = Fmod(Ifrac)/Fred

               DO Ib=1,N_Box
                  DO Ibin=1,N_LambdaBin(Ifrac)
                     H(Ibin,Ib,Rs,Ifrac) = 0.0d0
                  END DO
               END DO
            END IF

         ELSEIF(Tf.EQ.3) THEN ! Reaction Ensemble
            Ireac = Reaction_Frac(Ifrac)

            IF(Ireac.EQ.0) THEN
               WRITE(6,'(A,A)') ERROR, "Fractional is not for Reactions"
               STOP
            END IF

            Ib   = Box_Frac(Ifrac)
            MinH = H(1,Ib,1,Ifrac)
            MaxH = MinH
            AvH  = 0.0d0

            DO Rs=1,N_ReactionStep(Ireac)
               DO Ibin=1,N_LambdaBin(Ifrac)
                  MinH = min(MinH,H(Ibin,Ib,Rs,Ifrac))
                  MaxH = max(MaxH,H(Ibin,Ib,Rs,Ifrac))
                  AvH  = AvH + H(Ibin,Ib,Rs,Ifrac)
               END DO
            END DO

            AvH = AvH/dble(N_ReactionStep(Ireac)*N_LambdaBin(Ifrac))

            IF((MaxH-MinH).LT.flatc*AvH) THEN
               MinW = Weight(1,Ib,1,Ifrac)
               DO Rs=1,N_ReactionStep(Ireac)
                  DO Ibin=1,N_LambdaBin(Ifrac)
                     MinW = min(MinW,Weight(Ibin,Ib,Rs,Ifrac))
                  END DO
               END DO

               DO Rs=1,N_ReactionStep(Ireac)
                  DO Ibin=1,N_LambdaBin(Ifrac)
                     Weight(Ibin,Ib,Rs,Ifrac)     = Weight(Ibin,Ib,Rs,Ifrac) - MinW
                     Weightprev(Ibin,Ib,RS,Ifrac) = Weight(Ibin,Ib,Rs,Ifrac)
                  END DO
               END DO

               WRITE(Unit(Ifrac),'(A,i10)') "# Weightfunction after cycle ", Icycle

               DO Ibin=1,N_LambdaBin(Ifrac)
                  WRITE(unit(Ifrac),'(f10.7,99(1x,e20.10e3))') dble(Ibin-1)/dble(N_LambdaBin(Ifrac)-1),
     &               (Weight(Ibin,Ib,Rs,Ifrac), Rs=1,N_ReactionStep(Ireac))
               END DO

               WRITE(unit(Ifrac),*)
               WRITE(unit(Ifrac),*)

               Fmod(Ifrac) = Fmod(Ifrac)/Fred

               DO Rs=1,N_ReactionStep(Ireac)
                  DO Ibin=1,N_LambdaBin(Ifrac)
                     H(Ibin,Ib,Rs,Ifrac) = 0.0d0
                  END DO
               END DO
            END IF

         ELSEIF(Tf.EQ.4) THEN ! Grand Canonical Ensemble
            Ib   = Box_Frac(Ifrac)
            Rs   = 1
            MinH = H(1,Ib,Rs,Ifrac)
            MaxH = MinH
            AvH  = 0.0d0

            DO Ibin=1,N_LambdaBin(Ifrac)
               MinH = min(MinH,H(Ibin,Ib,Rs,Ifrac))
               MaxH = max(MaxH,H(Ibin,Ib,Rs,Ifrac))
               AvH  = AvH + H(Ibin,Ib,Rs,Ifrac)
            END DO

            AvH = AvH/dble(N_LambdaBin(Ifrac))

            IF((MaxH-MinH).LT.flatc*AvH) THEN
               MinW = Weight(1,Ib,Rs,Ifrac)
               DO Ibin=1,N_LambdaBin(Ifrac)
                  MinW = min(MinW,Weight(Ibin,Ib,Rs,Ifrac))
               END DO

               DO Ibin=1,N_LambdaBin(Ifrac)
                  Weight(Ibin,Ib,Rs,Ifrac)     = Weight(Ibin,Ib,Rs,Ifrac) - MinW
                  Weightprev(Ibin,Ib,Rs,Ifrac) = Weight(Ibin,Ib,Rs,Ifrac)
               END DO

               WRITE(Unit(Ifrac),'(A,i10)') "# Weightfunction after cycle ", Icycle

               DO Ibin=1,N_LambdaBin(Ifrac)
                  WRITE(unit(Ifrac),'(f10.7,1x,e20.10e3)') dble(Ibin-1)/dble(N_LambdaBin(Ifrac)-1),
     &              Weight(Ibin,Ib,Rs,Ifrac)
               END DO

               WRITE(unit(Ifrac),*)
               WRITE(unit(Ifrac),*)

               Fmod(Ifrac) = Fmod(Ifrac)/Fred

               DO Ibin=1,N_LambdaBin(Ifrac)
                  H(Ibin,Ib,Rs,Ifrac) = 0.0d0
               END DO
            END IF

         END IF

      END DO

      RETURN
      END



CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC                                                                                             CCC
CCC   Finalize the weightfunctions and write to files (use the last one with a flat histogram)  CCC
CCC                                                                                             CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      SUBROUTINE Wang_Landau_Finalize_Weightfunction
      implicit none

      include "global_variables.inc"
      include "settings.inc"
      include "wang_landau.inc"

      integer Ifrac,Tf,Ibin,Ireac,Ib,Rs

      OPEN(200,file="./OUTPUT/WEIGHTFUNCTIONS/weightfunction.out")

      DO Ifrac=1,N_Frac

         Tf = Type_Frac(Ifrac)

         IF(Tf.EQ.1) THEN
            Ib = Box_Frac(Ifrac)
            Rs = 1
            WRITE(200,'(A,i5,A,A24)') "# ", N_LambdaBin(Ifrac), " bins for weightfunction of ", Name_Frac(Ifrac)
         
            DO Ibin=1,N_LambdaBin(Ifrac)
               Weight(Ibin,Ib,Rs,Ifrac) = Weightprev(Ibin,Ib,Rs,Ifrac)
               WRITE(200,'(f10.7,1x,e20.10e3)') dble(Ibin-1)/dble(N_LambdaBin(Ifrac)-1),
     &           Weight(Ibin,Ib,Rs,Ifrac)
            END DO
         ELSEIF(Tf.EQ.2) THEN
            Rs = 1
            WRITE(200,'(A,i5,A,A24)') "# ", N_LambdaBin(Ifrac), " bins for weightfunction of ", Name_Frac(Ifrac)        
            DO Ibin=1,N_LambdaBin(Ifrac)
               DO Ib=1,N_Box
                  Weight(Ibin,Ib,Rs,Ifrac) = Weightprev(Ibin,Ib,Rs,Ifrac)
               END DO
               WRITE(200,'(f10.7,2(1x,e20.10e3))') dble(Ibin-1)/dble(N_LambdaBin(Ifrac)-1),
     &           (Weight(Ibin,Ib,Rs,Ifrac), Ib=1,N_Box)
            END DO
         ELSEIF(Tf.EQ.3) THEN
            Ireac = Reaction_Frac(Ifrac)
            Ib = Box_Frac(Ifrac)
            WRITE(200,'(A,i5,A,A24)') "# ", N_LambdaBin(Ifrac), " bins for weightfunction of ", Name_Reaction(Ireac)       
            DO Rs=1,N_ReactionStep(Ireac)
               DO Ibin=1,N_LambdaBin(Ifrac)
                  Weight(Ibin,Ib,Rs,Ifrac) = Weightprev(Ibin,Ib,Rs,Ifrac)
               END DO
            END DO
            DO Ibin=1,N_LambdaBin(Ifrac)
               WRITE(200,'(f10.7,99(1x,e20.10e3))') dble(Ibin-1)/dble(N_LambdaBin(Ifrac)-1),
     &           (Weight(Ibin,Ib,Rs,Ifrac), Rs=1,N_ReactionStep(Ireac))
            END DO
         ELSEIF(Tf.EQ.4) THEN
            Ib = Box_Frac(Ifrac)
            Rs = 1
            WRITE(200,'(A,i5,A,A24)') "# ", N_LambdaBin(Ifrac), " bins for weightfunction of ", Name_Frac(Ifrac)    
            DO Ibin=1,N_LambdaBin(Ifrac)
               Weight(Ibin,Ib,Rs,Ifrac) = Weightprev(Ibin,Ib,Rs,Ifrac)
               WRITE(200,'(f10.7,1x,e20.10e3)') dble(Ibin-1)/dble(N_LambdaBin(Ifrac)-1),
     &           Weight(Ibin,Ib,Rs,Ifrac)
            END DO
         END IF

         WRITE(200,*)
         WRITE(200,*)

      END DO

      CLOSE(200)

      RETURN
      END
