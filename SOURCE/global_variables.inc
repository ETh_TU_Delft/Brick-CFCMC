C     Global Variables

C     N_MolTotal          = Total Number of Molecules in the Simulation (including Fractional Molecules)
C     N_Box               = Number of Simulation Boxes (1 or 2)
C     N_MolInBox          = Total Number of Molecules in a Box (including Fractional Molecules)
C     I_MolInBox          = Array of Labels of Molecules in a Box

C     N_MolType           = Number of Molecule Types (components)
C     N_AtomType          = Number of Atom Types
C     N_AtomInMolType     = Number of Atoms in a Molecule Type
C     N_BondInMolType     = Number of Bonds in a Molecule Type
C     N_BendingInMolType  = Number of Bendings in a Molecule Type
C     N_TorsionInMolType  = Number of Torsions in a Molecule Type

C     N_Frac              = Total Number of Fractionals in the Simulation
C     N_MolInFrac         = Number of Molecules in a Fractional
C     I_MolInFrac         = Array of Labels of Molecules in a Fractional
C     I_MolTypeInFrac     = Array of Molecule Types in a Fractional
C     N_MolOfMolTypeInFrac = Number of Molecules of a Molecule Type in a Fractional
C     N_LambdaBin         = Number of Lambda bins for a Fractional Group

C     N_Reaction          = Number of Reactions
C     N_ReactionStep      = Number of Steps in a Reaction
C     N_MolInReactionStep = Number of Molecules in a Reaction Step in a Reaction
C     I_MolTypeInReactionStep = Array of Molecule Types in a Reaction Step in a Reaction
C     N_MolOfMolTypeInReactionStep = Number of Molecules of a Molecule Type in a Reaction Step in a Reaction

C     N_MolTypePair       = Number of defined Molecule Type Pairs

C     Nmptpb              = Number of whole Molecules of a given Molecule Type in a Box
C     Imptpb              = Arrays of Labels of Molecules of a given Molecule Type in a Box

C     R_Cluster_2         = Maximum Distance between COMs of Molecules to be part of a Cluster

C     U_LJ_Inter          = Total LJ Intermolecular Energy in a Box
C     U_LJ_Intra          = Total LJ Intramolecular Energy in a Box
C     U_EL_Real           = Total Electrostatic Real Energy in a Box
C     U_EL_Intra          = Total Electrostatic Intramolecular Energy in a Box
C     U_EL_Excl           = Total Electrostatic Exclusion Energy in a Box
C     U_EL_Four           = Total Electrostatic Fourier Energy in a Box
C     U_Bending_Total     = Total Bending Energy in a Box
C     U_Torsion_Total     = Total Torsion energy in a Box
C     U_LJ_Tail           = Total LJ Energy Tailcorrections in a Box
C     U_EL_Self           = Total Electrostatic SelfTerm in a Box
C     U_Total             = Total Energy in a Box

C     L_LJ                = Intermolecular LJ Interaction between Atom Types?
C     L_EL                = Electrostatic Interaction between Atom Types?
C     L_Intra             = Intramolecular Interaction between Atoms in a Molecule Type?
C     L_Frac              = Is this Molecule part of a Fractional?
C     L_LJ_Tail           = Tailcorrections for the LJ Energy?
C     L_LJ_Shift          = Truncated and Shifted LJ Energy?
C     L_IdealGas          = Are Molecules in this Box treated as an Ideal Gas?
C     L_PrintAtom         = Print this Atom in the Configuration?
C     L_Charge            = Does this Atom Type have a Charge?
C     L_ChargeInMolType   = Are there Charges in this Molecule Type?
C     L_No_MC_Moves       = Do we perform MC moves in this Simulation?

C     C_AtomType          = String defining an Atom Type
C     C_BendingType       = String defining a Bending Type
C     C_TorsionType       = String defining a Torsion Type
C     C_AtomPrint         = String containing the Atom Species to Print
C     C_MolType           = String containing the Name of the Molecule Type
C     Name_Frac           = String containing the Name of the Fractional Type
C     Name_Reaction       = String containing a Reaction used for Output
C     C_MolTypePair       = String containing a Pair of Molecule Types

C     Ibox                = In which Box is a Molecule
C     Ibvapor             = Vapor Phase Box

C     N_UmbrellaBinPressure = Number of bins for Umbrella Sampling Pressure
C     N_UmbrellaBinBeta   = Number of bins for Umbrella Sampling Beta
C     P_Umbrella          = Pressure Values for Umbrella Sampling
C     Beta_Umbrella       = Beta Values for Umbrella Sampling

C     TypeMol             = Molecule Type of a Molecule
C     TypeAtom            = Atom Type of an Atom in a Molecule Type
C     TypeBending         = Bending Type of a Bending in a Molecule Type
C     TypeTorsion         = Torsion Type of a Torsion in a Molecule Type

C     Delta_Translation   = Delta for Translation Moves for a Molecule Type in a Box
C     Delta_Rotation      = Delta for Rotation Moves for a Molecule Type in a Box
C     Delta_Lambda        = Delta for Lambda Moves for a Fractional Type in a Box
C     Delta_Volume        = Delta for Volume Change Moves in a Box
C     Delta_ClusterVolume = Delta for Volume Change Cluster Moves in a Box
C     dt_SmartTranslation = Time step for Smart Monte Carlo Translation trial move in a Box
C     Nstep_SmartTranslation = Number of steps in the Smart Monte Carlo Translation trial move in a Box
C     dt_SmartRotation    = Time step for Smart Monte Carlo Rotation trial move in a Box
C     Nstep_SmartRotation = Number of steps in the Smart Monte Carlo Rotation trial move in a Box

C     Box_Frac            = In which Box is a Fractional
C     Type_Frac           = Fractional type of a Fractional
C     Reaction_Frac       = In which Reaction is a Fractional
C     ReactionStep_Frac   = In which Step of a Reaction is a Fractional
C     Lambda_Frac         = Lambda of a Fractional Group
C     LambdaSwitch        = Switching Point between LJ and EL interactions for a Fractional Group

C     Bondlist            = List with Atoms that are bonded in a Molecule Type
C     BendingList         = List with Atoms that define Bendings in a Molecule Type
C     TorsionList         = List with Atoms that define Torsions in a Molecule Type

C     X/Y/Z               = Coordinates of an Atom in a Molecule
C     XCM/YCM/ZCM         = Geometric Center of Mass of a Molecule
C     Xin/Yin/Zin         = Coordinates of an Atom in a Molecule Type (used as initial conformer)

C     Qsum                = Sum of the Charges in a Molecule Type
C     Qsum_2              = Sum of the Square of the Charges in a Molecule Type

C     Qpartition          = Logartihm of the Partition Function of a Molecule Type
C     Fugacity_Coeff      = Fugacity coefficient of a Molecule Type
C     Volume              = Volume of a Box
C     MolarMass           = Molar Mass of a Molecule Type
C     MassOneMolecule     = Mass of one Molecule of a Molecule Type

C     BoxSize             = Size of a Box
C     InvBoxSize          = Inverse Boxsize

C     CounterType         = List of Molecule Types that form a Pair

C     OnePi               = Pi
C     TwoPi               = 2*Pi
C     R4pie               = Prefactor for Electrostatic (Coulomb) Potential
C     Zero                = 1.0D-8
C     R_GasConstant       = The Gas Constant 

C     Temperature         = Imposed Temperature of the system
C     Beta                = 1/Temperature
C     Pressure            = Imposed Pressure on the System
C     Pconv               = Pressure Conversion Factor
C     Mconv               = Mass Conversion Factor (Number Density to kg/m3)
C     Weight              = Weightfunction

      include 'maximum_dimensions.inc'

      integer N_MolType,N_MolTotal,N_AtomType,
     &     N_AtomInMolType(MaxMolType),N_Box,Ntorsiontype,
     &     N_MolInBox(2),Nmptpb(2,MaxMolType),N_Frac,Nbendingtype,
     &     N_TorsionInMolType(MaxMolType),N_BondInMolType(MaxMolType),
     &     N_MolInFrac(MaxFrac),N_MolOfMolTypeInFrac(MaxMolType,MaxFrac),
     &     N_Reaction,N_ReactionStep(MaxReaction),N_MolTypePair,
     &     N_BendingInMolType(MaxMolType)

      integer TypeMol(MaxMol),TypeAtom(MaxMolType,MaxAtom),Type_Frac(MaxFrac),
     &     Ibox(MaxMol),I_MolInBox(2,MaxMol),Box_Frac(MaxFrac),Ibvapor,
     &     TypeTorsion(MaxMolType,MaxTorsionInMolType),
     &     Imptpb(2,MaxMolType,MaxMol),I_MolInFrac(MaxFrac,MaxMolInFrac),
     &     Reaction_Frac(MaxFrac),ReactionStep_Frac(MaxFrac),
     &     N_MolInReactionStep(MaxReaction,MaxReactionStep),
     &     I_MolTypeInReactionStep(MaxReaction,MaxReactionStep,MaxMolInFrac),
     &     TypeBending(MaxMolType,MaxBendingInMolType),
     &     I_MolTypeInFrac(MaxFrac,MaxMolInFrac),
     &     N_MolOfMolTypeInReactionStep(MaxReaction,MaxReactionStep,MaxMolType),
     &     N_LambdaBin(MaxFrac),N_UmbrellaBinPressure(2),N_UmbrellaBinBeta(2),
     &     Nstep_SmartTranslation(2),Nstep_SmartRotation(2)

      integer Bondlist(MaxMolType,MaxBond,2),
     &     BendingList(MaxMolType,MaxBendingInMolType,3),
     &     TorsionList(MaxMolType,MaxTorsionInMolType,4),
     &     CounterType(MaxMolTypePair,2)

      double precision X(MaxMol,MaxAtom),Y(MaxMol,MaxAtom),
     &     Z(MaxMol,MaxAtom),XCM(MaxMol),Volume(2),
     &     YCM(MaxMol),ZCM(MaxMol),Xin(MaxMolType,MaxAtom),
     &     Yin(MaxMolType,MaxAtom),Zin(MaxMolType,MaxAtom),
     &     Qsum(MaxMolType),Qsum_2(MaxMolType),Lambda_Frac(MaxFrac),
     &     Qpartition(MaxMolType),Fugacity_Coeff(MaxMolType),
     &     MolarMass(MaxMolType),P_Umbrella(MaxUmbrellaBin,2),
     &     Beta_Umbrella(MaxUmbrellaBin,2),MassOneMolecule(MaxMolType)

      double precision BoxSize(2),InvBoxSize(2),R_Cluster_2

      double precision U_EL_Self(2),U_LJ_Tail(2)

      double precision dUTail_dl,dUELSelf_dl,dU_dlTotal
      logical L_dUdl


      double precision Delta_Translation(2,MaxMolType),
     &     Delta_Rotation(2,MaxMolType),Delta_Lambda(2,MaxFrac),Delta_Volume(2),
     &     Delta_ClusterVolume(2),dt_SmartTranslation(2),dt_SmartRotation(2)


      double precision U_LJ_Inter(2),U_LJ_Intra(2),U_EL_Real(2),U_EL_Intra(2),
     &     U_Bending_Total(2),U_Torsion_Total(2),U_EL_Excl(2),U_EL_Four(2),
     &     U_Total(2)

      double precision OnePi,TwoPi,R4pie,Temperature,Beta,Pressure,Pconv,Mconv,
     &     Zero,R_GasConstant

      Parameter (R4pie = 167101.0800066561d0) !=k_e*e^2/(k_B*10^-10)
      Parameter (OnePi = 4.0d0*DATAN(1.0d0))
      Parameter (TwoPi = 8.0d0*DATAN(1.0d0))
      Parameter (Zero  = 1.0d-8)
      Parameter (R_GasConstant = 8.3144598d0 )

      double precision Weight(MaxLambdaBin,2,MaxReactionStep,MaxFrac),
     &     d2Wdl2(MaxLambdaBin,2,MaxReactionStep,MaxFrac),
     &     LambdaSwitch(MaxFrac),MidPointBins(MaxLambdaBin,MaxFrac)

      logical L_LJ(MaxAtomType,MaxAtomType),L_EL(MaxAtomType,MaxAtomType),
     &     L_Charge(MaxAtomType),L_ChargeInMolType(MaxMolType),
     &     L_Intra(MaxMolType,MaxAtom,MaxAtom),L_Frac(MaxMol),
     &     L_LJ_Tail,L_LJ_Shift,L_No_MC_Moves,L_IdealGas(2),
     &     L_ImposedPressure,L_PrintAtom(MaxAtomType)

      character*24   C_AtomType(MaxAtomType)
      character*24   C_BendingType(MaxBendingType)
      character*24   C_TorsionType(MaxTorsionType)
      character*24   C_MolType(MaxMolType)
      character*24   Name_Frac(MaxFrac)
      character*24   Name_ReactionStep(MaxReaction,MaxReactionStep)

      character*7    C_AtomPrint(MaxAtomType)

      character*40   C_MolTypePair(MaxMolTypePair)
      character*120  Name_Reaction(MaxReaction)

      Common /GlobalIntegers/ N_MolType,N_MolTotal,N_AtomType,N_AtomInMolType,
     &     N_Box,Ntorsiontype,N_MolInBox,Nmptpb,N_Frac,N_MolInFrac,
     &     N_MolInReactionStep,TypeMol,TypeAtom,Ibox,I_MolInBox,TypeTorsion,
     &     Imptpb,Bondlist,Type_Frac,TorsionList,N_TorsionInMolType,
     &     N_BondInMolType,I_MolInFrac,I_MolTypeInFrac,Box_Frac,N_Reaction,
     &     N_ReactionStep,ReactionStep_Frac,I_MolTypeInReactionStep,
     &     Reaction_Frac,Ibvapor,N_MolTypePair,CounterType,N_BendingInMolType,
     &     Nbendingtype,TypeBending,BendingList,N_MolOfMolTypeInReactionStep,
     &     N_MolOfMolTypeInFrac,N_LambdaBin,N_UmbrellaBinPressure,
     &     N_UmbrellaBinBeta,Nstep_SmartTranslation,Nstep_SmartRotation

      Common /GlobalDoubles/ X,Y,Z,XCM,YCM,ZCM,Xin,Yin,Zin,BoxSize,InvBoxSize,
     &     Temperature,beta,Delta_Translation,Delta_Rotation,Delta_Lambda,
     &     Delta_Volume,Delta_ClusterVolume,dt_SmartTranslation,
     &     dt_SmartRotation,Qsum,Qsum_2,Fugacity_Coeff,U_LJ_Inter,
     &     U_LJ_Intra,U_EL_Excl,U_EL_Real,U_EL_Intra,U_Bending_Total,MidPointBins,
     &     U_Torsion_Total,U_LJ_Tail,U_EL_Self,U_EL_Four,U_Total,Volume,
     &     Pressure,Pconv,Mconv,Weight,Lambda_Frac,Qpartition,MolarMass,
     &     R_Cluster_2,LambdaSwitch,P_Umbrella,Beta_Umbrella,MassOneMolecule,
     &     dU_dlTotal,dUTail_dl,dUELSelf_dl,d2Wdl2

      Common /GlobalLogicals/ L_LJ,L_EL,L_Frac,L_LJ_Tail,L_LJ_Shift,L_IdealGas,
     &     L_Intra,L_PrintAtom,L_Charge,L_ChargeInMolType,L_No_MC_Moves,
     &     L_ImposedPressure,L_dUdl

      Common /GlobalCharacters/ C_AtomType,C_AtomPrint,C_MolType,Name_Frac,
     &     Name_Reaction,C_TorsionType,C_MolTypePair,C_BendingType,
     &     Name_ReactionStep
