      SUBROUTINE VolumeChangeNPT
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "energy.inc"
      include "output.inc"

      integer Imol,Ib,Tm,I,Iatom,Select_Random_Integer
      double precision Boxold,Volold,Volnew,Df,dX,dY,dZ,dE,
     &   E_LJ_InterNew,E_LJ_IntraNew,E_EL_RealNew,E_EL_IntraNew,E_BendingNew,E_TorsionNew,
     &   Eold,Enew,E_EL_FourNew,XCMold(MaxMol),YCMold(MaxMol),ZCMold(MaxMol),
     &   Xold(MaxMol,MaxAtom),Yold(MaxMol,MaxAtom),Zold(MaxMol,MaxAtom),
     &   E_LJ_TailOld,E_EL_SelfOld,E_EL_ExclNew,E_LJ_TailNew,E_EL_SelfNew,Ran_Uniform,
     &   ddU_dl,dUELFour_dl,dU_dlOld,dU_dlNew,dUTail_dlOld,dUTail_dlNew,dUELSelf_dlOld,
     &   dUELSelf_dlNew
      logical L_Overlap_Inter,L_Overlap_Intra,Laccept

C     Volume change in the normal NPT ensemble (with and without a fractional particle)
      Ib = Select_Random_Integer(N_Box)

      Boxold = BoxSize(Ib)
      Volold = Boxold*Boxold*Boxold
      Volnew = Volold + (2.0d0*Ran_Uniform()-1.0d0)*Delta_Volume(Ib)

      IF(Volnew.LE.0.0d0) RETURN

C     Save old configuration
      E_LJ_TailOld = U_LJ_Tail(Ib)
      E_EL_SelfOld = U_EL_Self(Ib)

      Eold  = U_Total(Ib)
      dUTail_dlOld   = dUTail_dl 
      dUELSelf_dlOld = dUELSelf_dl
      dU_dlOld       = dU_dlTotal

      IF(L_Ewald(Ib)) CALL Ewald_Store(Ib,1)

C     Scale box size
      Df             = (Volnew/Volold)**(1.0d0/3.0d0)
      BoxSize(Ib)    = Volnew**(1.0d0/3.0d0)
      InvBoxSize(Ib) = 1.0d0/BoxSize(Ib)

      IF(.NOT.L_IdealGas(Ib)) THEN
         IF(dsqrt(R_Cut_Max_2(Ib)).GT.0.5d0*BoxSize(Ib)) THEN
            WRITE(6,'(A,A,i1)') ERROR, "Volume; Rcut is too large in Box ", Ib
            STOP
         END IF
      END IF

      DO I=1,N_MolInBox(Ib)
         Imol = I_MolInBox(Ib,I)
         Tm   = TypeMol(Imol)

         DO Iatom=1,N_AtomInMolType(Tm)
            Xold(Imol,Iatom) = X(Imol,Iatom)
            Yold(Imol,Iatom) = Y(Imol,Iatom)
            Zold(Imol,Iatom) = Z(Imol,Iatom)
         END DO

         XCMold(Imol) = XCM(Imol)
         YCMold(Imol) = YCM(Imol)
         ZCMold(Imol) = ZCM(Imol)

         XCM(Imol) = XCM(Imol)*Df
         YCM(Imol) = YCM(Imol)*Df
         ZCM(Imol) = ZCM(Imol)*Df

         dX = XCM(Imol) - XCMold(Imol)
         dY = YCM(Imol) - YCMold(Imol)
         dZ = ZCM(Imol) - ZCMold(Imol)

         DO Iatom=1,N_AtomInMolType(Tm)
            X(Imol,Iatom) = X(Imol,Iatom) + dX
            Y(Imol,Iatom) = Y(Imol,Iatom) + dY
            Z(Imol,Iatom) = Z(Imol,Iatom) + dZ
         END DO

      END DO

      CALL Energy_Total(Ib,E_LJ_InterNew,E_LJ_IntraNew,E_EL_RealNew,E_EL_IntraNew,E_EL_ExclNew,
     &                     E_BendingNew,E_TorsionNew,L_Overlap_Inter,L_Overlap_Intra,dU_dlNew)

      IF(L_Overlap_Intra) THEN
         WRITE(6,'(A,A)') ERROR, "Intramolecular Energy Overlap (Volume Change NPT)"
         STOP
      ELSEIF(L_Overlap_Inter) THEN
         Laccept=.false.
         GO TO 1
      END IF

      E_EL_FourNew = 0.0d0
      dUELFour_dl  = 0.0d0
      IF(L_Ewald(Ib)) CALL Ewald_Total(Ib,E_EL_FourNew,dUELFour_dl)

      CALL Energy_Correction(Ib)

      E_LJ_TailNew = U_LJ_Tail(Ib)
      E_EL_SelfNew = U_EL_Self(Ib)

      Enew = E_LJ_InterNew + E_LJ_IntraNew + E_EL_RealNew + E_EL_IntraNew + E_BendingNew
     &     + E_TorsionNew  + E_LJ_TailNew  + E_EL_SelfNew + E_EL_ExclNew  + E_EL_FourNew

      dUTail_dlNew    = dUTail_dl
      dUELSelf_dlNew  = dUELSelf_dl
      dU_dlNew        = dU_dlNew + dUTail_dlNew + dUELSelf_dlNew + dUELFour_dl

      dE = Enew - Eold

      CALL Accept_or_Reject(dexp(-beta*(dE + Pressure*(Volnew - Volold)) +
     &     dble(N_MolInBox(Ib))*dlog(Volnew/Volold)),Laccept)


   1  CONTINUE

      TrialVolume(Ib) = TrialVolume(Ib) + 1.0d0

      IF(Laccept) THEN

         AcceptVolume(Ib) = AcceptVolume(Ib) + 1.0d0

         U_LJ_Inter(Ib) = E_LJ_InterNew
         U_LJ_Intra(Ib) = E_LJ_IntraNew

         U_EL_Real(Ib)  = E_EL_RealNew
         U_EL_Intra(Ib) = E_EL_IntraNew
         U_EL_Excl(Ib)  = E_EL_ExclNew
         U_EL_Four(Ib)  = E_EL_FourNew

         U_Bending_Total(Ib) = E_BendingNew
         U_Torsion_Total(Ib) = E_TorsionNew

         U_Total(Ib) = U_Total(Ib) + dE

         Volume(Ib) = Volnew

         ddU_dl     = dU_dlNew - dU_dlOld
         dU_dlTotal = dU_dlTotal + ddU_dl

      ELSE

         BoxSize(Ib)    = Boxold
         InvBoxSize(Ib) = 1.0d0/BoxSize(Ib)
         U_LJ_Tail(Ib)  = E_LJ_TailOld
         U_EL_Self(Ib)  = E_EL_SelfOld

         dUTail_dl      = dUTail_dlOld
         dUELSelf_dl    = dUELSelf_dlOld

         DO I=1,N_MolInBox(Ib)
            Imol=I_MolInBox(Ib,I)

            DO Iatom=1,N_AtomInMolType(TypeMol(Imol))
               X(Imol,Iatom) = Xold(Imol,Iatom)
               Y(Imol,Iatom) = Yold(Imol,Iatom)
               Z(Imol,Iatom) = Zold(Imol,Iatom)
            END DO

            XCM(Imol) = XCMold(Imol)
            YCM(Imol) = YCMold(Imol)
            ZCM(Imol) = ZCMold(Imol)

         END DO

         IF(L_Ewald(Ib)) CALL Ewald_Store(Ib,2)

      END IF

      RETURN
      END
