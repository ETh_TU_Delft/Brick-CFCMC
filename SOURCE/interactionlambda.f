      SUBROUTINE interactionlambda(Imol,Lambda_LJ,Lambda_EL,Lambda_IN,dlLJ_dlf,dlEL_dlf)
      implicit none

      include "global_variables.inc"
      include "output.inc"
      include "settings.inc"

      integer  Imol,Ifrac,Jfrac,I,J,N
      double precision Lf,Ls,Lambda_LJ,Lambda_EL,Lambda_IN,InvN,dlLJ_dlf,dlEL_dlf


      Jfrac=0
      J=0
      DO Ifrac=1,N_Frac
         DO I=1,N_MolInFrac(Ifrac)
            IF(I_MolInFrac(Ifrac,I).EQ.Imol) THEN
               Jfrac=Ifrac
               J=I
               GO TO 1
            END IF
         END DO
      END DO

  1   CONTINUE

      IF((Jfrac.EQ.0).OR.(J.EQ.0)) THEN
         WRITE(6,'(A,A)') ERROR, "Fractional Molecule not found in Fractional Groups"
         STOP
      ELSEIF(J.GT.N_MolInFrac(Jfrac)) THEN
         WRITE(6,'(A,A)') ERROR, "Fractional Molecule not in Fractional Group"
         STOP
      END IF

      Lf = Lambda_Frac(Jfrac)
      Ls = LambdaSwitch(Jfrac)
      N = N_LambdaBin(Jfrac)

      InvN = 1.0d0/dble(N)
      
C     If L_dUdl is true, use a custom scaling for Lambda_LJ and Lambda_EL. Also calculate 
C     derivative of Lambda_LJ and Lambda_EL with respect to lambda_frac.
C     If L_dUdl is false, use the other scaling scheme with lambda_switch where the first 
C     and last bins are 0 and 1, respectively.
      IF(L_dUdl) THEN
         IF(Lf.LT.0.4d0) THEN
            Lambda_LJ = (20.0d0/9.0d0)*Lf
            Lambda_EL = 0.0d0
            dlLJ_dlf  = 20.0d0/9.0d0
            dlEL_dlf  = 0.0d0
         ELSEIF(Lf.LT.0.5d0) THEN
            Lambda_LJ = 1.0d0-((100.0d0/9.0d0)*((Lf-0.5d0)**2))
            Lambda_EL = 0.0d0
            dlLJ_dlf  = -1.0d0*(200.d0*(Lf-0.5d0))/9.0d0
            dlEL_dlf  = 0.0d0
         ELSEIF(Lf.LT.0.6d0) THEN
            Lambda_LJ = 1.0d0
            Lambda_EL = (100.0d0/9.0d0)*((Lf-0.5d0)**2)
            dlLJ_dlf  = 0.0d0
            dlEL_dlf  = (200.d0*(Lf-0.5d0))/9.0d0
         ELSE
            Lambda_LJ = 1.0d0
            Lambda_EL = (-11.0d0/9.0d0)+(1.0d0+(11.0d0/9.0d0))*Lf
            dlLJ_dlf  = 0.0d0
            dlEL_dlf  = 20.0d0/9.0d0
         END IF
      ELSE
         IF(Lf.LT.InvN) THEN
            Lambda_LJ = 0.0d0
            Lambda_EL = 0.0d0
         ELSEIF(Lf.LT.Ls) THEN
            Lambda_LJ = (Lf-InvN)/(Ls-InvN)
            Lambda_EL = 0.0d0
         ELSEIF(Lf.LT.dble(N-1)*InvN) THEN
            Lambda_LJ = 1.0d0
            Lambda_EL = (Lf-Ls)/(InvN*dble(N-1)-Ls)
         ELSE
            Lambda_LJ = 1.0d0
            Lambda_EL = 1.0d0
         END IF
      END IF
      
CCC   Intramolecular interactions for fractional molecules are never scaled.
      Lambda_IN = 1.0d0

      RETURN
      END
