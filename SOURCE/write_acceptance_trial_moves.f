      SUBROUTINE Write_Acceptance_Trial_Moves
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "trial_moves.inc"

      integer Ib,Tm,Ifrac,Ipair,Ireac,I,Rs
      character*81 bars
      parameter (bars = "---------------------------------------------------------------------------------")

CCC   Write number of trials and aceptances of each move

      OPEN(50,file="./OUTPUT/trial_moves.info")
      WRITE(50,'(A)') "                                            Accept     Trial    Accept     Trial"

      IF(L_Translation) THEN
         WRITE(50,'(A)') bars
         WRITE(50,'(A)') "Translation"
         DO Tm=1,N_MolType
            WRITE(50,'(1x,A20,19x,4(i10))') C_MolType(Tm),
     &        (int(AcceptTranslation(Ib,Tm)), int(TrialTranslation(Ib,Tm)), Ib=1,N_Box)
         END DO
      END IF

      IF(L_Rotation) THEN
         WRITE(50,'(A)') bars
         WRITE(50,'(A)') "Rotation"
         DO Tm=1,N_MolType
            WRITE(50,'(1x,A20,19x,4(i10))') C_MolType(Tm),
     &        (int(AcceptRotation(Ib,Tm)), int(TrialRotation(Ib,Tm)), Ib=1,N_Box)
         END DO
      END IF

      IF(L_Bending) THEN
         WRITE(50,'(A)') bars
         WRITE(50,'(A)') "Bending"
         DO Tm=1,N_MolType
            IF(N_BendingInMolType(Tm).NE.0) THEN
               WRITE(50,'(1x,A,3x,i3)') C_MolType(Tm)
               DO I=1,N_BendingInMolType(Tm)
                  IF(I.LE.9) THEN
                     WRITE(50,'(A9,1x,i1,29x,4(i10))') "  Bending", I,
     &                 (int(AcceptBending(Ib,Tm,I)), int(TrialBending(Ib,Tm,I)), Ib=1,N_Box)
                  ELSE
                     WRITE(50,'(A9,1x,i2,28x,4(i10))') "  Bending", I,
     &                 (int(AcceptBending(Ib,Tm,I)), int(TrialBending(Ib,Tm,I)), Ib=1,N_Box)
                  END IF
               END DO
            END IF
         END DO
      END IF

      IF(L_Torsion) THEN
         WRITE(50,'(A)') bars
         WRITE(50,'(A)') "Torsion"
         DO Tm=1,N_MolType
            IF(N_TorsionInMolType(Tm).NE.0) THEN
               WRITE(50,'(1x,A,3x,i3)') C_MolType(Tm)
               DO I=1,N_TorsionInMolType(Tm)
                  IF(I.LE.9) THEN
                     WRITE(50,'(A9,1x,i1,29x,4(i10))') "  Torsion", I,
     &                 (int(AcceptTorsion(Ib,Tm,I)), int(TrialTorsion(Ib,Tm,I)), Ib=1,N_Box)
                  ELSE
                     WRITE(50,'(A9,1x,i2,28x,4(i10))') "  Torsion", I,
     &                 (int(AcceptTorsion(Ib,Tm,I)), int(TrialTorsion(Ib,Tm,I)), Ib=1,N_Box)
                  END IF
               END DO
            END IF
         END DO
      END IF

      IF(L_PairTranslation) THEN
         WRITE(50,'(A)') bars
         WRITE(50,'(A)') "Pair Translation"
         DO Ipair=1,N_MolTypePair
            WRITE(50,'(1x,A20,19x,4(i10))') C_MolTypePair(Ipair),
     &        (int(AcceptPairTranslation(Ib,Ipair)), int(TrialPairTranslation(Ib,Ipair)), Ib=1,N_Box)
         END DO
      END IF

      IF(L_PairRotation) THEN
         WRITE(50,'(A)') bars
         WRITE(50,'(A)') "Pair Rotation"
         DO Ipair=1,N_MolTypePair
            WRITE(50,'(1x,A20,19x,4(i10))') C_MolTypePair(Ipair),
     &        (int(AcceptPairRotation(Ib,Ipair)), int(TrialPairRotation(Ib,Ipair)), Ib=1,N_Box)
         END DO
      END IF

      IF(L_ClusterTranslation) THEN
         WRITE(50,'(A)') bars
         WRITE(50,'(A)') "Cluster Translation"
         WRITE(50,'(1x,A20,19x,4(i10))') "Total",
     &     (int(AcceptClusterTranslation(Ib)), int(TrialClusterTranslation(Ib)), Ib=1,N_Box)
      END IF

      IF(L_ClusterRotation) THEN
         WRITE(50,'(A)') bars
         WRITE(50,'(A)') "Cluster Rotation"
         WRITE(50,'(1x,A20,19x,4(i10))') "Total",
     &     (int(AcceptClusterRotation(Ib)), int(TrialClusterRotation(Ib)), Ib=1,N_Box)
      END IF

      IF(L_SmartTranslation) THEN
         WRITE(50,'(A)') bars
         WRITE(50,'(A17,23x,4(i10))') "Smart Translation",
     &        (int(AcceptSmartTranslation(Ib)), int(TrialSmartTranslation(Ib)), Ib=1,N_Box)
         WRITE(50,'(A40,2(e20.10))') "Av. acc. displacements (units of length)",
     &        (AvDelta_AcceptSmartTranslation(Ib)/max(TrialSmartTranslation(Ib),1.0d0), Ib=1,N_Box)
      END IF

      IF(L_SmartRotation) THEN
         WRITE(50,'(A)') bars
         WRITE(50,'(A14,26x,4(i10))') "Smart Rotation",
     &        (int(AcceptSmartRotation(Ib)), int(TrialSmartRotation(Ib)), Ib=1,N_Box)
         WRITE(50,'(A28,12x,2(e20.10))') "Av. acc. rotations (degrees)",
     &        (AvDelta_AcceptSmartRotation(Ib)*(180.0d0/OnePi)/max(TrialSmartRotation(Ib),1.0d0), Ib=1,N_Box)
      END IF

      IF(L_LambdaMove) THEN
         WRITE(50,'(A)') bars
         WRITE(50,'(A)') "Lambda Move"
         DO Ifrac=1,N_Frac
            IF(Type_Frac(Ifrac).NE.3) THEN
               Rs = 1
               WRITE(50,'(1x,A20,19x,4(i10))') Name_Frac(Ifrac),
     &           (int(AcceptLambdaMove(Ib,Rs,Ifrac)), int(TrialLambdaMove(Ib,Rs,Ifrac)), Ib=1,N_Box)
            ELSEIF(Type_Frac(Ifrac).EQ.3) THEN
               Ireac = Reaction_Frac(Ifrac)
               WRITE(50,'(1x,A,i2)') "Reaction ", Ireac
               DO Rs=1,N_ReactionStep(Ireac)
                  WRITE(50,'(2x,A20,18x,4(i10))') Name_ReactionStep(Ireac,Rs),
     &              (int(AcceptLambdaMove(Ib,Rs,Ifrac)), int(TrialLambdaMove(Ib,Rs,Ifrac)), Ib=1,N_Box)
               END DO
            END IF
         END DO
      END IF


      IF(L_GCMCLambdaMove) THEN
         WRITE(50,'(A)') bars
         WRITE(50,'(A)') "Lambda Move GCMC"
         DO Ifrac=1,N_Frac
            IF(Type_Frac(Ifrac).NE.4) CYCLE
            WRITE(50,'(1x,A)') Name_Frac(Ifrac)
            WRITE(50,'(2x,A11,27x,4(i10))') "Lambda Move",
     & (int(AcceptGCMCLambdaMoveLambdaMove(Ib,Ifrac)), int(TrialGCMCLambdaMoveLambdaMove(Ib,Ifrac)), Ib=1,N_Box)
            WRITE(50,'(2x,A11,27x,4(i10))') "Deletion   ",
     & (int(AcceptGCMCLambdaMoveDeletion(Ib,Ifrac)), int(TrialGCMCLambdaMoveDeletion(Ib,Ifrac)), Ib=1,N_Box)
            WRITE(50,'(2x,A11,27x,4(i10))') "Insertion  ",
     & (int(AcceptGCMCLambdaMoveInsertion(Ib,Ifrac)), int(TrialGCMCLambdaMoveInsertion(Ib,Ifrac)), Ib=1,N_Box)
         END DO
      END IF

      IF(L_NVPTHybrid) THEN
         WRITE(50,'(A)') bars
         WRITE(50,'(A)') "Fractional Reinsertion"
         DO Ifrac=1,N_Frac
            IF(Type_Frac(Ifrac).NE.1) CYCLE
            WRITE(50,'(1x,A)') Name_Frac(Ifrac)
            DO Tm=1,N_MolType
               IF(N_MolOfMolTypeInFrac(Tm,Ifrac).NE.0) THEN
                  WRITE(50,'(2x,A20,18x,4(i10))') C_MolType(Tm),
     &              (int(AcceptNVPTswap(Ib,Tm,Ifrac)), int(TrialNVPTswap(Ib,Tm,Ifrac)), Ib=1,N_Box)
               END IF
            END DO
         END DO

         WRITE(50,'(A)') bars
         WRITE(50,'(A)') "Fractional Identity Change"
         DO Ifrac=1,N_Frac
            IF(Type_Frac(Ifrac).NE.1) CYCLE
            WRITE(50,'(1x,A)') Name_Frac(Ifrac)
            DO Tm=1,N_MolType
               IF(N_MolOfMolTypeInFrac(Tm,Ifrac).NE.0) THEN
                  WRITE(50,'(2x,A20,18x,4(i10))') C_MolType(Tm),
     &              (int(AcceptNVPTchange(Ib,Tm,Ifrac)), int(TrialNVPTchange(Ib,Tm,Ifrac)), Ib=1,N_Box)
               END IF
            END DO
         END DO
      END IF

      IF(L_GEHybrid) THEN
         WRITE(50,'(A)') bars
         WRITE(50,'(A)') "Swap Fractional GE"
         DO Ifrac=1,N_Frac
            IF(Type_Frac(Ifrac).NE.2) CYCLE
            WRITE(50,'(1x,A20,19x,4(i10))') Name_Frac(Ifrac),
     &        (int(AcceptGEswap(Ib,Ifrac)), int(TrialGEswap(Ib,Ifrac)), Ib=1,N_Box)
         END DO

         WRITE(50,'(A)') bars
         WRITE(50,'(A)') "Fractional Identity Change GE"
         DO Ifrac=1,N_Frac
            IF(Type_Frac(Ifrac).NE.2) CYCLE
            WRITE(50,'(1x,A20,19x,4(i10))') Name_Frac(Ifrac),
     &        (int(AcceptGEchange(Ib,Ifrac)), int(TrialGEchange(Ib,Ifrac)), Ib=1,N_Box)
         END DO
      END IF

      IF(L_RXMCHybrid) THEN
         WRITE(50,'(A)') bars
         WRITE(50,'(A)') "Fractional Reaction RxMC"
         DO Ifrac=1,N_Frac
            IF(Type_Frac(Ifrac).NE.3) CYCLE
            Ireac = Reaction_Frac(Ifrac)
            WRITE(50,'(1x,A,i2)') "Reaction ", Ireac
            DO Rs=1,N_ReactionStep(Ireac)
               WRITE(50,'(2x,A20,18x,4(i10))') Name_ReactionStep(Ireac,Rs),
     &           (int(AcceptRXMCswap(Ib,Rs,Ifrac)), int(TrialRXMCswap(Ib,Rs,Ifrac)), Ib=1,N_Box)
            END DO
         END DO

         WRITE(50,'(A)') bars
         WRITE(50,'(A)') "Identity Change RxMC"
         DO Ifrac=1,N_Frac
            IF(Type_Frac(Ifrac).NE.3) CYCLE
            Ireac = Reaction_Frac(Ifrac)
            WRITE(50,'(1x,A,i2)') "Reaction ", Ireac
            DO Rs=1,N_ReactionStep(Ireac)
               WRITE(50,'(2x,A20,18x,4(i10))') Name_ReactionStep(Ireac,Rs),
     &           (int(AcceptRXMCchange(Ib,Rs,Ifrac)), int(TrialRXMCchange(Ib,Rs,Ifrac)), Ib=1,N_Box)
            END DO
         END DO
      END IF

      IF(L_Volume) THEN
         WRITE(50,'(A)') bars
         WRITE(50,'(A6,34x,4(i10))') "Volume", (int(AcceptVolume(Ib)), int(TrialVolume(Ib)), Ib=1,N_Box)
      END IF

      IF(L_ClusterVolume) THEN
         WRITE(50,'(A14,26x,4(i10))') "Volume Cluster",
     &     (int(AcceptClusterVolume(Ib)), int(TrialClusterVolume(Ib)), Ib=1,N_Box)
      END IF

      CLOSE(50)

      RETURN
      END
