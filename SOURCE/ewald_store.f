      SUBROUTINE Ewald_Store(Ib,Ichoice)
      implicit none

      include "maximum_dimensions.inc"
      include "ewald.inc"

      integer Ib,Ichoice,I

      IF(Ichoice.EQ.1) THEN

         rkcutsq_Stored(Ib) = rkcutsq_old(Ib)

         DO I = 1,N_Kvec(Ib)
            InvK_2_Stored(I,Ib)       = InvK_2(I,Ib)
            CKC_old_Stored(I,Ib)      = CKC_old(I,Ib)
            CKS_old_Stored(I,Ib)      = CKS_old(I,Ib)

            CKC_dUdl_old_Stored(I,1)  = CKC_dUdl_old(I,1)
            CKS_dUdl_old_Stored(I,1)  = CKS_dUdl_old(I,1)

            Ewald_Factor_Stored(I,Ib) = Ewald_Factor(I,Ib)
         END DO

      ELSE

         rkcutsq_old(Ib) = rkcutsq_Stored(Ib)

         DO I = 1,N_Kvec(Ib)
            InvK_2(I,Ib)       = InvK_2_Stored(I,Ib)
            CKC_old(I,Ib)      = CKC_old_Stored(I,Ib)
            CKS_old(I,Ib)      = CKS_old_Stored(I,Ib)

            CKC_dUdl_old(I,1)  = CKC_dUdl_old_Stored(I,1) 
            CKS_dUdl_old(I,1)  = CKS_dUdl_old_Stored(I,1)

            Ewald_Factor(I,Ib) = Ewald_Factor_Stored(I,Ib)
         END DO

      END IF

      RETURN

      END
