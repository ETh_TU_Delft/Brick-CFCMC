      Function Select_Random_Integer(J)
      Implicit None

C     Select A Random Number From The Range 1 To J

      Integer Select_Random_Integer,J
      Double Precision Ran_Uniform

C     Avoid Numerical Roundoff Errors That Cause A Number Larger Than J
      IF(J.EQ.1) THEN
         Select_Random_Integer=1
      ELSE
 1       Continue
         Select_Random_Integer = 1 + Int(Dble(J)*Ran_Uniform())
         If (Select_Random_Integer.Gt.J) Goto 1
      END IF

      Return
      End
