      SUBROUTINE Ewald_Move(Ib,dE_EL_Four,ddUELFour_dl)
      implicit none

      include "global_variables.inc"
      include "energy.inc"
      include "ewald.inc"
      include "output.inc"

C     Calculate the fourier-space energy difference between two configurations

      integer Ib,I,Mmin,Nmin,Mm,Nn,l,m,n,Kmax,
     &        ll,Ivec,Ncharges,J,K,ktype(MaxMol*MaxAtom)

      double precision  Cs,ckc(2),cks(2),qqq(2*MaxMol*MaxAtom),
     &   clm(2*MaxMol*MaxAtom),slm(2*MaxMol*MaxAtom),dE_EL_Four,
     &   elc(2*MaxMol*MaxAtom,0:1),els(2*MaxMol*MaxAtom,0:1),
     &   emc(2*MaxMol*MaxAtom,0:MaxKvec),ems(2*MaxMol*MaxAtom,0:MaxKvec),
     &   enc(2*MaxMol*MaxAtom,0:MaxKvec),ens(2*MaxMol*MaxAtom,0:MaxKvec),
     &   ssx,ssy,ssz,TwoPiOverL,E_FourOld,E_FourNew,
     &   rkx,rky,rkz,rksq
      double precision ckc_dUdl(2),cks_dUdl(2),dUELFour_dlNew,dUELFour_dlOld,ddUELFour_dl,
     &   qqq_unscaled(2*MaxMol*MaxAtom)
      logical L_Scale(2*MaxMol*MaxAtom)

      dE_EL_Four = 0.0d0
      ddUELFour_dl = 0.0d0

      IF(L_IdealGas(Ib)) RETURN

      Kmax       = Kmax_Ewald(Ib)
      TwoPiOverL = Twopi*InvBoxSize(Ib)

C     Kspace term
      I = 0
      DO J = 1,2
         DO K = 1,NKSPACE(Ib,J)
            I        = I + 1
            ktype(I) = J
            elc(I,0) = 1.0D0
            emc(I,0) = 1.0D0
            enc(I,0) = 1.0D0
            els(I,0) = 0.0D0
            ems(I,0) = 0.0D0
            ens(I,0) = 0.0D0
            ssx      = TwoPiOverL*XKSPACE(K,Ib,J)
            ssy      = TwoPiOverL*YKSPACE(K,Ib,J)
            ssz      = TwoPiOverL*ZKSPACE(K,Ib,J)
            elc(I,1) = dcos(ssx)
            emc(I,1) = dcos(ssy)
            enc(I,1) = dcos(ssz)
            els(I,1) = dsin(ssx)
            ems(I,1) = dsin(ssy)
            ens(I,1) = dsin(ssz)
            qqq(I)   = QKSPACE(K,Ib,J)

            qqq_unscaled(I) = QKSPACE_Unscaled(K,Ib,J)
            L_Scale(I)      = L_KSPACE_Scale(K,Ib,J)

         END DO
      END DO

      Ncharges = I

      E_FourOld = 0.0D0
      E_FourNew = 0.0D0

      dUELFour_dlNew = 0.0d0
      dUELFour_dlOld = 0.0d0

      mmin = 0
      nmin = 1
      Ivec = 0

      DO l = 2,Kmax
         DO i = 1,Ncharges

            emc(i,l) = emc(i,l - 1)*emc(i,1) - ems(i,l - 1)*ems(i,1)
            ems(i,l) = ems(i,l - 1)*emc(i,1) + emc(i,l - 1)*ems(i,1)

         END DO
      END DO

      DO l = 2,Kmax
         DO i = 1,Ncharges

            enc(i,l) = enc(i,l - 1)*enc(i,1) - ens(i,l - 1)*ens(i,1)
            ens(i,l) = ens(i,l - 1)*enc(i,1) + enc(i,l - 1)*ens(i,1)

         END DO
      END DO

C    Loop Over K vectors
      DO ll = 0,Kmax

         rkx = dble(ll)*TwoPiOverL

         IF(ll.GE.1) THEN
            DO i = 1,Ncharges
               cs       = elc(i,0)

               elc(i,0) = elc(i,1)*cs - els(i,1)*els(i,0)
               els(i,0) = elc(i,1)*els(i,0) + els(i,1)*cs
            END DO
         END IF

         DO mm = mmin,Kmax

            m = iabs(mm)
            rky = dble(mm)*TwoPiOverL

            IF(mm.GE.0) THEN
               DO i = 1,Ncharges
                  clm(i) = elc(i,0)*emc(i,m) - els(i,0)*ems(i,m)
                  slm(i) = els(i,0)*emc(i,m) + ems(i,m)*elc(i,0)
               END DO
            ELSE
               DO i = 1,Ncharges
                  clm(i) = elc(i,0)*emc(i,m) + els(i,0)*ems(i,m)
                  slm(i) = els(i,0)*emc(i,m) - ems(i,m)*elc(i,0)
               END DO
            END IF

            DO nn = nmin,Kmax

               n = iabs(nn)
               rkz = dble(nn)*TwoPiOverL

               rksq = rkx*rkx+rky*rky+rkz*rkz

               IF(rksq.LT.rkcutsq_old(Ib)) THEN
                  Ivec = Ivec + 1

                  ckc(1) = 0.0D0
                  ckc(2) = 0.0D0

                  cks(1) = 0.0D0
                  cks(2) = 0.0D0

                  ckc_dUdl(1) = 0.0d0
                  ckc_dUdl(2) = 0.0d0
                  cks_dUdl(1) = 0.0d0
                  cks_dUdl(2) = 0.0d0

                  IF (nn.GE.0) THEN
                     DO i = 1,Ncharges
                        ckc(ktype(i)) = ckc(ktype(i)) + qqq(i)*(clm(i)*enc(i,n) - slm(i)*ens(i,n))
                        cks(ktype(i)) = cks(ktype(i)) + qqq(i)*(slm(i)*enc(i,n) + clm(i)*ens(i,n))
C     Calculate non-scaled cos and sin for dU/dlambda calculation
                        IF(L_dUdl.AND.L_Scale(i)) THEN
                           ckc_dUdl(ktype(i)) = ckc_dUdl(ktype(i)) + qqq_unscaled(i)*(clm(i)*enc(i,n) - slm(i)*ens(i,n))
                           cks_dUdl(ktype(i)) = cks_dUdl(ktype(i)) + qqq_unscaled(i)*(slm(i)*enc(i,n) + clm(i)*ens(i,n))
                        END IF

                     END DO
                  ELSE
                     DO i = 1,Ncharges
                        ckc(ktype(i)) = ckc(ktype(i)) + qqq(i)*(clm(i)*enc(i,n) + slm(i)*ens(i,n))
                        cks(ktype(i)) = cks(ktype(i)) + qqq(i)*(slm(i)*enc(i,n) - clm(i)*ens(i,n))
C     Calculate non-scaled cos and sin for dU/dlambda calculation
                        IF(L_dUdl.AND.L_Scale(i)) THEN
                           ckc_dUdl(ktype(i)) = ckc_dUdl(ktype(i)) + qqq_unscaled(i)*(clm(i)*enc(i,n) + slm(i)*ens(i,n))
                           cks_dUdl(ktype(i)) = cks_dUdl(ktype(i)) + qqq_unscaled(i)*(slm(i)*enc(i,n) - clm(i)*ens(i,n))
                        END IF

                     END DO
                  END IF

                  CKC_new(Ivec,Ib) = CKC_old(Ivec,Ib) + ckc(2) - ckc(1)
                  CKS_new(Ivec,Ib) = CKS_old(Ivec,Ib) + cks(2) - cks(1)
                     
C     Calculate for the new configuration
                  CKC_dUdl_new(Ivec,1) = CKC_dUdl_old(Ivec,1) + ckc_dUdl(2) - ckc_dUdl(1)
                  CKS_dUdl_new(Ivec,1) = CKS_dUdl_old(Ivec,1) + cks_dUdl(2) - cks_dUdl(1)

                  E_FourNew = E_FourNew + Ewald_Factor(Ivec,Ib)*(CKC_new(Ivec,Ib)*CKC_new(Ivec,Ib) +
     &                                                           CKS_new(Ivec,Ib)*CKS_new(Ivec,Ib))

                  E_FourOld = E_FourOld + Ewald_Factor(Ivec,Ib)*(CKC_old(Ivec,Ib)*CKC_old(Ivec,Ib) +
     &                                                           CKS_old(Ivec,Ib)*CKS_old(Ivec,Ib))
     
C     Calculate dUELFour_dl for new and old configs
                  IF(L_dUdl) THEN
                     dUELFour_dlNew = dUELFour_dlNew + LKSPACE(2)*2.0d0*Ewald_Factor(Ivec,1)*
     &                                                 (CKC_dUdl_new(Ivec,1)*CKC_new(Ivec,1)+
     &                                                   CKS_dUdl_new(Ivec,1)*CKS_new(Ivec,1))
                     dUELFour_dlOld = dUELFour_dlOld + LKSPACE(1)*2.0d0*Ewald_Factor(Ivec,1)*
     &                                                 (CKC_dUdl_old(Ivec,1)*CKC_old(Ivec,1)+
     &                                                   CKS_dUdl_old(Ivec,1)*CKS_old(Ivec,1))
                  END IF

               END IF

            END DO
            nmin = -Kmax
         END DO
         mmin = -Kmax
      END DO

      IF(Ivec.NE.N_Kvec(Ib)) THEN
         WRITE(6,'(A,A)') ERROR, "Number of counted K-vectors does not match with the number stored."
         STOP
      END IF

      dE_EL_Four = E_FourNew - E_FourOld
      ddUELFour_dl = dUELFour_dlNew - dUELFour_dlOld

      RETURN

      END
