C     Variables for bending
C
C     theta0        = equilibrium bending angle
C     K_bend        = force constant
C     dtheta        = Max change in bending angle
C     Natombendlist = Number of atoms in Iatombendlist
C     Iatombendlist = Labels of atoms that should be rotated for each bending and molecule type

      integer   Natombendlist(MaxMolType,MaxBendingInMolType,2),
     &          Iatombendlist(MaxMolType,MaxBendingInMolType,2,MaxAtom)

      double precision theta0(MaxBendingType),K_bend(MaxBendingType),
     &                 dtheta(MaxBendingType)

      Common /BendingInteger/ Natombendlist,Iatombendlist

      Common /BendingDouble/ theta0,K_bend,dtheta
