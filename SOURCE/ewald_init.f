      SUBROUTINE Ewald_Init
      implicit none

      include "maximum_dimensions.inc"
      include "ewald.inc"

      NKSPACE(1,1) = 0
      NKSPACE(1,2) = 0
      NKSPACE(2,1) = 0
      NKSPACE(2,2) = 0

      RETURN

      END
