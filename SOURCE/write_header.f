      SUBROUTINE WRITE_HEADER(title)
      implicit none

      include "global_variables.inc"

      character(len=*) title

      IF(N_Box.EQ.1) THEN
         CALL WRITE_HEADER_SHORT(title)
      ELSE
         CALL WRITE_HEADER_LONG(title)
      END IF

      RETURN
      END


      SUBROUTINE WRITE_HEADER_SHORT(title)
      implicit none

      include "output.inc"

      character(len=*) title
      character*78     newtitle,title2
      integer          Ntitle,Nfilling,Nunchanged,I,J

      title2 = "   " // title // "   "

      Ntitle   = LEN(title)+6
      Nfilling = LEN(TRIM(hashes(1)))

      Nunchanged = CEILING(0.5d0*dble(Nfilling-Ntitle))

      newtitle = TRIM(hashes(1))

      DO I=1,Ntitle
         J=Nunchanged+I
         newtitle(J:J)=title2(I:I)
      END DO

      WRITE(6,'(A)') TRIM(hashes(1))
      WRITE(6,'(A)') newtitle
      WRITE(6,'(A)') TRIM(hashes(1))

      RETURN
      END



      SUBROUTINE WRITE_HEADER_LONG(title)
      implicit none

      include "output.inc"

      character(len=*) title
      character*121    newtitle,title2
      integer          Ntitle,Nfilling,Nunchanged,I,J

      title2 = "   " // title // "   "

      Ntitle   = LEN(title)+6
      Nfilling = LEN(TRIM(hashes(2)))

      Nunchanged = CEILING(0.5d0*dble(Nfilling-Ntitle))

      newtitle = hashes(2)

      DO I=1,Ntitle
         J=Nunchanged+I
         newtitle(J:J)=title2(I:I)
      END DO

      WRITE(6,'(A)') hashes(2)
      WRITE(6,'(A)') newtitle
      WRITE(6,'(A)') hashes(2)

      RETURN
      END
