C     Common parameters for Kspace calculations
C
C     NKSPACE         = Number of Particles in the calculation
C     N_Kvec          = Number of k-vectors
C     X/Y/ZKSPACE     = Coordinates of the Particles in the Calculation
C     QSPACE          = Charges of the Particles in the Calculation
C     Ewald_Factor    = Prefactor in the Fourier Summation
C     InvK_2          = Inverse Length of Wavevector Squared
C     rkcutsq         = CutOff Length of Wavevector Squared
C     CKC_old/CKS_old = Total Vector for Old Configuration (cosine)
C     CKC_new/CKS_new = Total Vector for New Configuration (sine)

      integer          NKSPACE(2,2),N_Kvec(2)

      double precision XKSPACE(max(2*MaxMolInFrac,MaxMolInCluster)*MaxAtom,2,2),
     &                 YKSPACE(max(2*MaxMolInFrac,MaxMolInCluster)*MaxAtom,2,2),
     &                 ZKSPACE(max(2*MaxMolInFrac,MaxMolInCluster)*MaxAtom,2,2),
     &                 QKSPACE(max(2*MaxMolInFrac,MaxMolInCluster)*MaxAtom,2,2),
     &                 Ewald_Factor((MaxKvec+1)*(2*MaxKvec+1)*(2*MaxKvec+1),2),
     &                 InvK_2((MaxKvec+1)*(2*MaxKvec+1)*(2*MaxKvec+1),2),
     &                 CKC_old((MaxKvec+1)*(2*MaxKvec+1)*(2*MaxKvec+1),2),
     &                 CKS_old((MaxKvec+1)*(2*MaxKvec+1)*(2*MaxKvec+1),2),
     &                 CKC_new((MaxKvec+1)*(2*MaxKvec+1)*(2*MaxKvec+1),2),
     &                 CKS_new((MaxKvec+1)*(2*MaxKvec+1)*(2*MaxKvec+1),2),
     &                 Ewald_Factor_Stored((MaxKvec+1)*(2*MaxKvec+1)*(2*MaxKvec+1),2),
     &                 InvK_2_Stored((MaxKvec+1)*(2*MaxKvec+1)*(2*MaxKvec+1),2),
     &                 CKC_old_Stored((MaxKvec+1)*(2*MaxKvec+1)*(2*MaxKvec+1),2),
     &                 CKS_old_Stored((MaxKvec+1)*(2*MaxKvec+1)*(2*MaxKvec+1),2),
     &                 rkcutsq_Stored(2),rkcutsq_old(2),
     &                 CKC_dUdl_old((MaxKvec+1)*(2*MaxKvec+1)*(2*MaxKvec+1),1),
     &                 CKS_dUdl_old((MaxKvec+1)*(2*MaxKvec+1)*(2*MaxKvec+1),1),
     &                 CKC_dUdl_new((MaxKvec+1)*(2*MaxKvec+1)*(2*MaxKvec+1),1),
     &                 CKS_dUdl_new((MaxKvec+1)*(2*MaxKvec+1)*(2*MaxKvec+1),1),
     &                 LKSPACE(2),
     &                 CKC_dUdl_old_Stored((MaxKvec+1)*(2*MaxKvec+1)*(2*MaxKvec+1),1),
     &                 CKS_dUdl_old_Stored((MaxKvec+1)*(2*MaxKvec+1)*(2*MaxKvec+1),1),
     &                 QKSPACE_Unscaled(max(2*MaxMolInFrac,MaxMolInCluster)*MaxAtom,2,2)
      logical L_KSPACE_Scale(max(2*MaxMolInFrac,MaxMolInCluster)*MaxAtom,2,2)

      COMMON /EwaldInt/    NKSPACE,N_Kvec

      COMMON /EwaldDble/   XKSPACE,YKSPACE,ZKSPACE,QKSPACE,Ewald_Factor,CKC_old,
     &                     CKS_old,CKC_new,CKS_new,InvK_2,Ewald_Factor_Stored,
     &                     CKC_old_Stored,CKS_old_Stored,InvK_2_Stored,
     &                     rkcutsq_old,rkcutsq_Stored,
     &                     CKC_dUdl_old,CKS_dUdl_old,CKC_dUdl_new,CKS_dUdl_new,
     &                     LKSPACE,CKC_dUdl_old_Stored,CKS_dUdl_old_Stored,QKSPACE_Unscaled
      COMMON /EwaldLog/    L_KSPACE_Scale

