      SUBROUTINE Ewald_Total(Ib,E_EL_Four,dUELFour_dl)
      implicit none

      include "global_variables.inc"
      include "energy.inc"
      include "ewald.inc"

C     Calculate the coulomb energy of a configuration
      integer Ib,I,J,It,Tm,Mmin,Nmin,Mm,Nn,l,m,n,Imol,Ncharges,Ivec,
     &         ll,Kmax,Iatom

      double precision E_EL_Four,Cs,
     &         ckc,cks,clm(MaxMol*MaxAtom),slm(MaxMol*MaxAtom),qqq(MaxMol*MaxAtom),
     &         elc(MaxMol*MaxAtom,0:1),els(MaxMol*MaxAtom,0:1),emc(MaxMol*MaxAtom,0:MaxKvec),
     &         ems(MaxMol*MaxAtom,0:MaxKvec),enc(MaxMol*MaxAtom,0:MaxKvec),ens(MaxMol*MaxAtom,0:MaxKvec),
     &         ssx,ssy,ssz,prefactor,Myl,Myc,Myi,rkx,rky,rkz,rksq,TwoPiOverL
      double precision dlLJ_dlf,dlEL_dlf,ckc_dUdl,cks_dUdl,dUELFour_dl,qqqT(MaxMol*MaxAtom)
      logical L_ScaleT(MaxMol*MaxAtom)

      E_EL_Four   = 0.0d0
      dUELFour_dl = 0.0d0

      IF(L_IdealGas(Ib)) RETURN

      Kmax       = Kmax_Ewald(Ib)
      prefactor  = 2.0D0*TwoPi*R4Pie*InvBoxSize(Ib)*InvBoxSize(Ib)*InvBoxSize(Ib)
      TwoPiOverL = TwoPi*InvBoxSize(Ib)

      rkcutsq_old(Ib) = 1.05d0*1.05d0*(Kmax*TwoPiOverL)*(Kmax*TwoPiOverL)

      Ivec = 0
      mmin = 0
      nmin = 1

      I = 0
      DO J = 1,N_MolInBox(Ib)

         Imol = I_MolInBox(Ib,J)
         Tm   = TypeMol(Imol)

         IF(L_frac(Imol)) CALL interactionlambda(Imol,Myl,Myc,Myi,dlLJ_dlf,dlEL_dlf)

         DO Iatom = 1,N_AtomInMolType(Tm)
            It=TypeAtom(Tm,Iatom)
            IF(L_Charge(It)) THEN

               I = I +1

               elc(I,0) = 1.0D0
               emc(I,0) = 1.0D0
               enc(I,0) = 1.0D0
               els(I,0) = 0.0D0
               ems(I,0) = 0.0D0
               ens(I,0) = 0.0D0

               ssx = TwoPiOverL*X(Imol,Iatom)
               ssy = TwoPiOverL*Y(Imol,Iatom)
               ssz = TwoPiOverL*Z(Imol,Iatom)

               elc(I,1) = dcos(ssx)
               emc(I,1) = dcos(ssy)
               enc(I,1) = dcos(ssz)
               els(I,1) = dsin(ssx)
               ems(I,1) = dsin(ssy)
               ens(I,1) = dsin(ssz)
               qqqT(I) = Q(It)
               
               IF(L_frac(Imol)) THEN
                  qqq(I)  = Myc*Q(It)
                  IF(L_dUdl) THEN
                     L_ScaleT(I) = .true.
                     LKSPACE(1) = dlEL_dlf
                     LKSPACE(2) = dlEL_dlf
                  END IF

               ELSE
                  qqq(I)  = Q(It)
                  IF(L_dUdl) L_ScaleT(I) = .false.
               END IF

            END IF

         END DO

      END DO

      Ncharges = I

      mmin = 0
      nmin = 1
      Ivec = 0

      DO l = 2,Kmax
         DO i = 1,Ncharges

            emc(i,l) = emc(i,l - 1)*emc(i,1) - ems(i,l - 1)*ems(i,1)
            ems(i,l) = ems(i,l - 1)*emc(i,1) + emc(i,l - 1)*ems(i,1)

         END DO
      END DO


      DO l = 2,Kmax
         DO i = 1,Ncharges

            enc(i,l) = enc(i,l - 1)*enc(i,1) - ens(i,l - 1)*ens(i,1)
            ens(i,l) = ens(i,l - 1)*enc(i,1) + enc(i,l - 1)*ens(i,1)

         END DO
      END DO


C    Loop Over K Vectors
      DO ll = 0,Kmax

         rkx = dble(ll)*TwoPiOverL

         IF(ll.GE.1) THEN
            DO i = 1,Ncharges
               cs = elc(i,0)

               elc(i,0) = elc(i,1)*cs       - els(i,1)*els(i,0)
               els(i,0) = elc(i,1)*els(i,0) + els(i,1)*cs
            END DO
         END IF

         DO mm = mmin,Kmax

            m = iabs(mm)
            rky = dble(mm)*TwoPiOverL

            IF(mm.GE.0) THEN
               DO i = 1,Ncharges
                  clm(i) = elc(i,0)*emc(i,m) - els(i,0)*ems(i,m)
                  slm(i) = els(i,0)*emc(i,m) + ems(i,m)*elc(i,0)
               END DO
            ELSE
               DO i = 1,Ncharges
                  clm(i) = elc(i,0)*emc(i,m) + els(i,0)*ems(i,m)
                  slm(i) = els(i,0)*emc(i,m) - ems(i,m)*elc(i,0)
               END DO
            END IF

            DO nn = nmin,Kmax

               n = iabs(nn)
               rkz = dble(nn)*TwoPiOverL

               rksq = rkx*rkx+rky*rky+rkz*rkz

               IF(rksq.LT.rkcutsq_old(Ib)) THEN
                  Ivec = Ivec + 1

                  InvK_2(Ivec,Ib) = 1.0d0/rksq
                  Ewald_Factor(Ivec,Ib) = prefactor*dexp(-rksq/(4.0d0*(Alpha_EL(Ib)*Alpha_EL(Ib))))*InvK_2(Ivec,Ib)

                  ckc = 0.0D0
                  cks = 0.0D0

                  ckc_dUdl = 0.0d0
                  cks_dUdl = 0.0d0

                  IF(nn.GE.0) THEN
                     DO i = 1,Ncharges
                        ckc = ckc + qqq(i)*(clm(i)*enc(i,n) - slm(i)*ens(i,n))
                        cks = cks + qqq(i)*(slm(i)*enc(i,n) + clm(i)*ens(i,n))
C     Calculate non-scaled cos and sin for dU/dlambda
                        IF(L_dUdl.AND.L_ScaleT(i)) THEN
                           ckc_dUdl = ckc_dUdl + qqqT(i)*(clm(i)*enc(i,n)-slm(i)*ens(i,n))
                           cks_dUdl = cks_dUdl + qqqT(i)*(slm(i)*enc(i,n)+clm(i)*ens(i,n))
                        END IF

                     END DO
                  ELSE
                     DO i = 1,Ncharges
                        ckc = ckc + qqq(i)*(clm(i)*enc(i,n) + slm(i)*ens(i,n))
                        cks = cks + qqq(i)*(slm(i)*enc(i,n) - clm(i)*ens(i,n))
C     Calculate non-scaled cos and sin for dU/dlambda
                        IF(L_dUdl.AND.L_ScaleT(i)) THEN
                           ckc_dUdl = ckc_dUdl + qqqT(i)*(clm(i)*enc(i,n)+slm(i)*ens(i,n))
                           cks_dUdl = cks_dUdl + qqqT(i)*(slm(i)*enc(i,n)-clm(i)*ens(i,n))
                        END IF

                     END DO
                  END IF

                  CKC_old(Ivec,Ib) = ckc
                  CKS_old(Ivec,Ib) = cks
              
                  CKC_dUdl_old(Ivec,1) = ckc_dUdl
                  CKS_dUdl_old(Ivec,1) = cks_dUdl

                  E_EL_Four = E_EL_Four + Ewald_Factor(Ivec,Ib)*(ckc*ckc+cks*cks)

                  IF(L_dUdl) THEN
                     dUELFour_dl = dUELFour_dl + dlEL_dlf*2.0d0*Ewald_Factor(Ivec,1)*(ckc_dUdl*ckc+cks_dUdl*cks)
                  END IF

               END IF
            END DO
            nmin = -Kmax
         END DO
         mmin = -Kmax
      END DO

      N_Kvec(Ib) = Ivec

      RETURN

      END
