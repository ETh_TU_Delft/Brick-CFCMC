      SUBROUTINE Force_Molecule(Imol,F_x,F_y,F_z,L_Overlap_Inter,L_Overlap_Intra)
      implicit none

      include "global_variables.inc"
      include "energy.inc"

C     Calculate the force on molecule Imol in its box

      integer I,Imol,Jmol,Ib,Tmi,Tmj,ii,jj,It,Jt
      double precision dX,dY,dZ,R2,R,R0,R0c,R6,Is,F_x,F_y,F_z,F0,F1,Lambda_LJi,Lambda_LJj,
     &                 Lambda_ELi,Lambda_ELj,Myl,Myc,Myi,Off,Ofc,dderfc,ROFC,R2OFC,
     &                 dlLJ_dlf,dlEL_dlf
      logical L_Overlap_Inter,L_Overlap_Intra

      F_x = 0.0d0
      F_y = 0.0d0
      F_z = 0.0d0

      L_Overlap_Inter = .false.
      L_Overlap_Intra = .false.

      Ib  = Ibox(Imol)
      Tmi = TypeMol(Imol)

C     In an ideal gas there is no force
      IF(.NOT.L_IdealGas(Ib)) THEN

C     Calculate total force on all atoms
C     Loop over all molecules in the same box and skip Imol
         DO I=1,N_MolInBox(Ib)
            Jmol = I_MolInBox(Ib,I)

            IF(Imol.EQ.Jmol) CYCLE

            Tmj = TypeMol(Jmol)

            IF(L_Frac(Imol).AND.L_Frac(Jmol)) THEN
               CALL interactionlambda(Imol,Lambda_LJi,Lambda_ELi,Myi,dlLJ_dlf,dlEL_dlf)
               CALL interactionlambda(Jmol,Lambda_LJj,Lambda_ELj,Myi,dlLJ_dlf,dlEL_dlf)

               Myl = Lambda_LJi*Lambda_LJj
               Off = Alpha_Offset_LJ*(1.0d0 - Myl)

               Myc = Lambda_ELi*Lambda_ELj
               Ofc = Alpha_Offset_EL*(1.0d0 - Myc)
            ELSEIF(L_Frac(Imol)) THEN
               CALL interactionlambda(Imol,Myl,Myc,Myi,dlLJ_dlf,dlEL_dlf)

               Off = Alpha_Offset_LJ*(1.0d0 - Myl)
               Ofc = Alpha_Offset_EL*(1.0d0 - Myc)

            ELSEIF(L_Frac(Jmol)) THEN
               CALL interactionlambda(Jmol,Myl,Myc,Myi,dlLJ_dlf,dlEL_dlf)

               Off = Alpha_Offset_LJ*(1.0d0 - Myl)
               Ofc = Alpha_Offset_EL*(1.0d0 - Myc)
            END IF

            DO ii = 1,N_AtomInMolType(Tmi)
               DO jj = 1,N_AtomInMolType(Tmj)

                  dX = X(Imol,ii) - X(Jmol,jj)
                  dY = Y(Imol,ii) - Y(Jmol,jj)
                  dZ = Z(Imol,ii) - Z(Jmol,jj)

                  dX = dX - BoxSize(Ib)*Dnint(dX*InvBoxSize(Ib))
                  dY = dY - BoxSize(Ib)*Dnint(dY*InvBoxSize(Ib))
                  dZ = dZ - BoxSize(Ib)*Dnint(dZ*InvBoxSize(Ib))

                  R2 = dX*dX + dY*dY + dZ*dZ

                  IF(R2.LT.R_Cut_Max_2(Ib)) THEN

                     R  = dsqrt(R2)
                     It = TypeAtom(Tmi,ii)
                     Jt = TypeAtom(Tmj,jj)

                     IF(L_Frac(Imol).OR.L_Frac(Jmol)) THEN
                        IF(L_LJ(It,Jt)) THEN
                           IF(R2.LT.R_Cut_LJ_2(Ib)) THEN
                              Is  = 1.0d0/Sigma_2(It,Jt)
                              R0 = 1.0d0/((R2*Is)**(C_LJ*0.5d0) + Off)
                              R0C = R0**(6.0d0/C_LJ)
                              F0 = 48.0d0*Myl*Epsilon(It,Jt)*(Is**(C_LJ*0.5d0))
     &                               *R0*(R**(C_LJ-2.0d0))*(R0C*(R0C-0.5d0))
                              F_x = F_x + F0*dX
                              F_y = F_y + F0*dY
                              F_z = F_z + F0*dZ
                           END IF
                        END IF


                        IF(L_EL(It,Jt)) THEN
                                IF(R2.LT.R_Cut_EL_2(Ib)) THEN
                                       ROFc = R + OFc
                                       R2OFc = ROFc*ROFc
                                       F1 = R4Pie*Myc*Q(It)*Q(Jt)*(Dderfc(Alpha_DSF(Ib)*ROFc)/R2OFc
     &                                    + 2.0d0*Alpha_DSF(Ib)*(dexp(-Alpha_DSF(Ib)*Alpha_DSF(Ib)*R2OFc))
     &                                    /(dsqrt(OnePi)*ROFC)-DSF_Factor(Ib))/(R)

                                       F_x = F_x + F1*dX
                                       F_y = F_y + F1*dY
                                       F_z = F_z + F1*dZ

                                END IF
                        END IF

                     ELSE

                        IF(R2.LT.R_Min_2(It,Jt)) THEN
                           L_Overlap_Inter = .true.
                           RETURN
                        END IF

                        IF(L_LJ(It,Jt)) THEN
                           IF(R2.LT.R_Cut_LJ_2(Ib)) THEN
                              R6 = Sigma_2(It,Jt)/R2
                              R6 = R6*R6*R6
                              F0 = 48.0d0*Epsilon(It,Jt)*R6*(R6-0.5d0)/R2

                              F_x = F_x + F0*dX
                              F_y = F_y + F0*dY
                              F_z = F_z + F0*dZ
                           END IF
                        END IF

                        IF(L_EL(It,Jt)) THEN
                           IF(R2.LT.R_Cut_EL_2(Ib)) THEN
                              F1 = R4Pie*Q(It)*Q(Jt)*(Dderfc(Alpha_DSF(Ib)*R)/R
     &                             +2.0d0*Alpha_DSF(Ib)*(dexp(-Alpha_DSF(Ib)*Alpha_DSF(Ib)*R2))
     &                             /(dsqrt(OnePi))-DSF_Factor(Ib)*R)/(R2)

                              F_x = F_x + F1*dX
                              F_y = F_y + F1*dY
                              F_z = F_z + F1*dZ
                           END IF
                        END IF

                     END IF
                  END IF

               END DO
            END DO
         END DO

      END IF

      RETURN

      END
