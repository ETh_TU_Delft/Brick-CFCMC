      SUBROUTINE Smart_Rotation
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "energy.inc"
      include "ewald.inc"
      include "output.inc"


      integer Imol,Ib,Tm,I,J,Select_Random_Integer,Iatom,N_timestep_small,RR(1),
     &     I_MD_step
      double precision dE,XCMold(MaxMol),YCMold(MaxMol),ZCMold(MaxMol),Xold(MaxMol,MaxAtom),
     &     Yold(MaxMol,MaxAtom),Zold(MaxMol,MaxAtom),M_x,M_y,M_z,E_LJ_InterNew,E_LJ_IntraNew,
     &     E_EL_RealNew,E_EL_IntraNew,E_EL_ExclNew,E_BendingNew,E_TorsionNew,E_EL_FourNew,Eold,
     &     Enew,Ran_Gauss,E_EL_SelfOld,E_EL_SelfNew,E_LJ_TailOld,E_LJ_TailNew,wx(MaxMol),wy(Maxmol),
     &     wz(MaxMol),Krot_old,Krot_new,dKrot,Ix(MaxMol),Iy(MaxMol),Iz(MaxMol),wfac,Kaim,q0(MaxMol),
     &     q1(Maxmol),q2(MaxMol),q3(MaxMol),Phi1,Phi2,Phi3,Pq0t,Pq1t,Pq2t,Pq3t,q0t,q1t,q2t,q3t,
     &     Lx(MaxMol),Ly(MaxMol),Lz(MaxMol),Pq0(MaxMol),Pq1(MaxMol),Pq2(MaxMol),Pq3(MaxMol),
     &     dXb(MaxMol,MaxAtom),dYb(MaxMol,MaxAtom),dZb(MaxMol,MaxAtom),order_R(4),
     &     InertiaMatrix(3,3),r,s,t,U(3),V(3),W(3),RotationMatrixInverse(3,3,MaxMol),cc,ss,qtot,ddt,
     &     RotationMatrix(3,3,MaxMol),Tx,Ty,Tz,Tq0(Maxmol),Tq1(MaxMol),Tq2(MaxMol),Tq3(MaxMol),
     &     Tx_0,Ty_0,Tz_0,Rsq1,Rsq2,Rsq3,Rsq4,dXp(MaxMol,Maxatom),dYp(MaxMol,Maxatom),
     &     dZp(MaxMol,Maxatom),XG,YG,ZG,sum_mass,ux,uy,uz,vx,vy,vz,delta_angle,Avd,
     &     ddU_dl,dUELFour_dl,dU_dlOld,dU_dlNew,dUTail_dlOld,dUTail_dlNew,dUELSelf_dlOld,
     &     dUELSelf_dlNew
      logical L_Overlap_Inter,L_Overlap_Intra,Laccept

      Ib = Select_Random_Integer(N_Box)

      N_timestep_small=10
      Krot_old=0.0d0
      Krot_new=0.0d0

      sum_mass=0.0d0
      XG=0.0d0
      YG=0.0d0
      ZG=0.0d0

C COM of the system (if necessary)

      DO I=1,N_MolInBox(Ib)
         Imol = I_MolInBox(Ib,I)
         Tm = TypeMol(Imol)
         
         XG=XG+XCM(Imol)*MassOneMolecule(Tm)
         YG=YG+YCM(Imol)*MassOneMolecule(Tm)
         ZG=ZG+ZCM(Imol)*MassOneMolecule(Tm)
         sum_mass=sum_mass+MassOneMolecule(Tm)
      END DO

      XG=XG/sum_mass
      YG=YG/sum_mass
      ZG=ZG/sum_mass

      TrialSmartRotation(Ib) = TrialSmartRotation(Ib) + 1.0d0
 
C     Save old configuration

      E_LJ_TailOld = U_LJ_Tail(Ib)
      E_EL_SelfOld = U_EL_Self(Ib)

      Eold  = U_Total(Ib)

      dU_dlOld       = dU_dlTotal
      dUTail_dlOld   = dUTail_dl
      dUELSelf_dlOld = dUELSelf_dl

      IF(L_Ewald(Ib)) CALL Ewald_Store(Ib,1)

C Initialize angular velocities

      DO I=1,N_MolInBox(Ib)
         Imol = I_MolInBox(Ib,I)
         Tm   = TypeMol(Imol)
         IF(N_AtomInMolType(Tm).EQ.1)   CYCLE

         CALL Calculate_Inertia_Matrix(Imol,InertiaMatrix)
         CALL Calculate_Eigenvalues_and_Vectors(InertiaMatrix,r,s,t,U,V,W)

         Ix(Imol) = r
         Iy(Imol) = s
         Iz(Imol) = t

         RotationMatrix(:,1,Imol)=U
         RotationMatrix(:,2,Imol)=V
         RotationMatrix(:,3,Imol)=W

         wx(Imol)=Ran_Gauss()
         wy(Imol)=Ran_Gauss()
         wz(Imol)=Ran_Gauss()
         Krot_old = Krot_old + 0.5d0*(Ix(Imol)*wx(Imol)*wx(Imol)+Iy(Imol)*wy(Imol)
     &             *wy(Imol)+Iz(Imol)*wz(Imol)*wz(Imol))

      END DO

      Kaim = 1.5d0*N_MolInBox(Ib)/beta
      wfac = dsqrt(Kaim/Krot_old)
      Krot_old = 0.0d0

      DO I=1,N_MolInBox(Ib)
         Imol = I_MolInBox(Ib,I)
         Tm   = TypeMol(Imol)

         IF(N_AtomInMolType(Tm).EQ.1)   CYCLE

         wx(I) = wx(I)*wfac
         wy(I) = wy(I)*wfac
         wz(I) = wz(I)*wfac

         Krot_old = Krot_old + 0.5d0*(Ix(Imol)*wx(Imol)*wx(Imol)+Iy(Imol)*wy(Imol)
     &             *wy(Imol)+Iz(Imol)*wz(Imol)*wz(Imol))
      END DO

C Shepherd's method to obtain quaternions from rotation matrix      
      
      DO I=1,N_MolInBox(Ib)
         Imol = I_MolInBox(Ib,I)
         Tm   = TypeMol(Imol)

         IF(N_AtomInMolType(Tm).EQ.1)   CYCLE

         Rsq1 = dsqrt(1.0d0+RotationMatrix(1,1,Imol)+RotationMatrix(2,2,Imol)+RotationMatrix(3,3,Imol))
         Rsq2 = dsqrt(1.0d0+RotationMatrix(1,1,Imol)-RotationMatrix(2,2,Imol)-RotationMatrix(3,3,Imol))
         Rsq3 = dsqrt(1.0d0-RotationMatrix(1,1,Imol)+RotationMatrix(2,2,Imol)-RotationMatrix(3,3,Imol))
         Rsq4 = dsqrt(1.0d0-RotationMatrix(1,1,Imol)-RotationMatrix(2,2,Imol)+RotationMatrix(3,3,Imol))

         order_R(1)=RotationMatrix(1,1,Imol)+RotationMatrix(2,2,Imol)+RotationMatrix(3,3,Imol)
         order_R(2)=RotationMatrix(1,1,Imol)
         order_R(3)=RotationMatrix(2,2,Imol)
         order_R(4)=RotationMatrix(3,3,Imol)

         RR(1)=MAXLOC(order_R,dim=1)

         IF(RR(1).EQ.1) THEN
                q0(Imol) = 0.5d0*Rsq1
                q1(Imol) = 0.5d0*(RotationMatrix(3,2,Imol)-RotationMatrix(2,3,Imol))/Rsq1
                q2(Imol) = 0.5d0*(RotationMatrix(1,3,Imol)-RotationMatrix(3,1,Imol))/Rsq1
                q3(Imol) = 0.5d0*(RotationMatrix(2,1,Imol)-RotationMatrix(1,2,Imol))/Rsq1
         ELSEIF(RR(1).EQ.2) THEN
                q0(Imol) = 0.5d0*(RotationMatrix(3,2,Imol)-RotationMatrix(2,3,Imol))/Rsq2
                q1(Imol) = 0.5d0*Rsq2
                q2(Imol) = 0.5d0*(RotationMatrix(2,1,Imol)+RotationMatrix(1,2,Imol))/Rsq2
                q3(Imol) = 0.5d0*(RotationMatrix(1,3,Imol)+RotationMatrix(3,1,Imol))/Rsq2
         ELSEIF(RR(1).EQ.3) THEN
                q0(Imol) = 0.5d0*(RotationMatrix(1,3,Imol)-RotationMatrix(3,1,Imol))/Rsq3
                q1(Imol) = 0.5d0*(RotationMatrix(2,1,Imol)+RotationMatrix(1,2,Imol))/Rsq3
                q2(Imol) = 0.5d0*Rsq3
                q3(Imol) = 0.5d0*(RotationMatrix(3,2,Imol)+RotationMatrix(2,3,Imol))/Rsq3
         ELSE
                q0(Imol) = 0.5d0*(RotationMatrix(2,1,Imol)-RotationMatrix(1,2,Imol))/Rsq4
                q1(Imol) = 0.5d0*(RotationMatrix(1,3,Imol)+RotationMatrix(3,1,Imol))/Rsq4
                q2(Imol) = 0.5d0*(RotationMatrix(3,2,Imol)+RotationMatrix(2,3,Imol))/Rsq4
                q3(Imol) = 0.5d0*Rsq4
         END IF


         qtot = dsqrt(q0(Imol)**2+q1(Imol)**2+q2(Imol)**2+q3(Imol)**2)
         
         q0(Imol) = q0(Imol)/qtot
         q1(Imol) = q1(Imol)/qtot
         q2(Imol) = q2(Imol)/qtot
         q3(Imol) = q3(Imol)/qtot

C Modify initial rotation matrix

         RotationMatrix(1,1,Imol)=q0(Imol)**2+q1(Imol)**2-q2(Imol)**2-q3(Imol)**2
         RotationMatrix(1,2,Imol)=2.0d0*(q1(Imol)*q2(Imol)-q0(Imol)*q3(Imol))
         RotationMatrix(1,3,Imol)=2.0d0*(q1(Imol)*q3(Imol)+q0(Imol)*q2(Imol))
         RotationMatrix(2,1,Imol)=2.0d0*(q1(Imol)*q2(Imol)+q0(Imol)*q3(Imol))
         RotationMatrix(2,2,Imol)=q0(Imol)**2-q1(Imol)**2+q2(Imol)**2-q3(Imol)**2
         RotationMatrix(2,3,Imol)=2.0*(q2(Imol)*q3(Imol)-q0(Imol)*q1(Imol))
         RotationMatrix(3,1,Imol)=2.0*(q1(Imol)*q3(Imol)-q0(Imol)*q2(Imol))
         RotationMatrix(3,2,Imol)=2.0*(q2(Imol)*q3(Imol)+q0(Imol)*q1(Imol))
         RotationMatrix(3,3,Imol)=q0(Imol)**2-q1(Imol)**2-q2(Imol)**2+q3(Imol)**2


C Store intial coordinates
         
         DO Iatom=1,N_AtomInMolType(Tm)
             Xold(Imol,Iatom) = X(Imol,Iatom)
             Yold(Imol,Iatom) = Y(Imol,Iatom)
             Zold(Imol,Iatom) = Z(Imol,Iatom)

             dXp(Imol,Iatom) = X(Imol,Iatom)-XCM(Imol)
             dYp(Imol,Iatom) = Y(Imol,Iatom)-YCM(Imol)
             dZp(Imol,Iatom) = Z(Imol,Iatom)-ZCM(Imol)

             dXb(Imol,Iatom)=dXp(Imol,Iatom)*RotationMatrix(1,1,Imol)+
     &         dYp(Imol,Iatom)*RotationMatrix(2,1,Imol)+dZp(Imol,Iatom)*RotationMatrix(3,1,Imol)

             dYb(Imol,Iatom)=dXp(Imol,Iatom)*RotationMatrix(1,2,Imol)+
     &         dYp(Imol,Iatom)*RotationMatrix(2,2,Imol)+dZp(Imol,Iatom)*RotationMatrix(3,2,Imol)

             dZb(Imol,Iatom)=dXp(Imol,Iatom)*RotationMatrix(1,3,Imol)+
     &         dYp(Imol,Iatom)*RotationMatrix(2,3,Imol)+dZp(Imol,Iatom)*RotationMatrix(3,3,Imol)

             XCMold(Imol) = XCM(Imol)
             YCMold(Imol) = YCM(Imol)
             ZCMold(Imol) = ZCM(Imol)

         END DO

C     Initialize torques and angular momenta

         CALL Torque_Molecule(Imol,Tx_0,Ty_0,Tz_0,L_Overlap_Inter,L_Overlap_Intra)
         
         IF(L_Overlap_Intra) THEN
             WRITE(6,'(A,A)') ERROR,
     &          "Intramolecular Torque Overlap (Smart Rotation)"
             STOP
         ELSEIF(L_Overlap_Inter) THEN
             Laccept=.false.
             GO TO 1
         END IF

C     Rotate torques to principal frame

         Tx = RotationMatrix(1,1,Imol)*Tx_0 + RotationMatrix(2,1,Imol)*Ty_0 +
     &              RotationMatrix(3,1,Imol)*Tz_0 
         Ty = RotationMatrix(1,2,Imol)*Tx_0 + RotationMatrix(2,2,Imol)*Ty_0 +
     &              RotationMatrix(3,2,Imol)*Tz_0 
         Tz = RotationMatrix(1,3,Imol)*Tx_0 + RotationMatrix(2,3,Imol)*Ty_0 +
     &              RotationMatrix(3,3,Imol)*Tz_0 

C     Quaternion torques 

         Tq0(Imol) = 2.0d0*(-q1(Imol)*Tx-q2(Imol)*Ty-q3(Imol)*Tz)
         Tq1(Imol) = 2.0d0*( q0(Imol)*Tx-q3(Imol)*Ty+q2(Imol)*Tz)
         Tq2(Imol) = 2.0d0*( q3(Imol)*Tx+q0(Imol)*Ty-q1(Imol)*Tz)
         Tq3(Imol) = 2.0d0*(-q2(Imol)*Tx+q1(Imol)*Ty+q0(Imol)*Tz)

C     Angular momenta L=I*omega

         Lx(Imol) = Ix(Imol)*wx(Imol)
         Ly(Imol) = Iy(Imol)*wy(Imol)
         Lz(Imol) = Iz(Imol)*wz(Imol)

C     Initialize conjugate quaternion momenta Pq=2*M*L

         Pq0(Imol) = 2.0d0*(-q1(Imol)*Lx(Imol) -q2(Imol)*Ly(Imol)
     &            -q3(Imol)*Lz(Imol))
         Pq1(Imol) = 2.0d0*( q0(Imol)*Lx(Imol) -q3(Imol)*Ly(Imol)
     &            +q2(Imol)*Lz(Imol))
         Pq2(Imol) = 2.0d0*( q3(Imol)*Lx(Imol) +q0(Imol)*Ly(Imol)
     &            -q1(Imol)*Lz(Imol))
         Pq3(Imol) = 2.0d0*(-q2(Imol)*Lx(Imol) +q1(Imol)*Ly(Imol)
     &            +q0(Imol)*Lz(Imol))

      END DO

      DO I_MD_step=1,Nstep_SmartRotation(Ib)

         Krot_new=0.0d0

         DO I=1,N_MolInBox(Ib)
                Imol = I_MolInBox(Ib,I)
                Tm   = TypeMol(Imol)
                IF(N_AtomInMolType(Tm).EQ.1)   CYCLE

C     Update conjugate momenta to a half timestep        

                Pq0(Imol) = Pq0(Imol) + 0.5d0*dt_SmartRotation(Ib)*Tq0(Imol)
                Pq1(Imol) = Pq1(Imol) + 0.5d0*dt_SmartRotation(Ib)*Tq1(Imol)
                Pq2(Imol) = Pq2(Imol) + 0.5d0*dt_SmartRotation(Ib)*Tq2(Imol)
                Pq3(Imol) = Pq3(Imol) + 0.5d0*dt_SmartRotation(Ib)*Tq3(Imol)

C     Rotate quaternion and momenta

                ddt = dt_SmartRotation(Ib)/(dble(N_timestep_small))
                DO J=1,N_timestep_small
                        phi3 = 0.25d0*(-q3(Imol)*Pq0(Imol) +
     &                       q2(Imol)*Pq1(Imol) - q1(Imol)*Pq2(Imol)
     &                        +q0(Imol)*Pq3(Imol))/Iz(Imol)

                        cc=dcos(phi3*0.5d0*ddt)
                        ss=dsin(phi3*0.5d0*ddt)

                        q0t = cc*q0(Imol) - ss*q3(Imol)
                        q1t = cc*q1(Imol) + ss*q2(Imol)
                        q2t = cc*q2(Imol) - ss*q1(Imol)
                        q3t = cc*q3(Imol) + ss*q0(Imol)

                        Pq0t = cc*Pq0(Imol) - ss*Pq3(Imol)
                        Pq1t = cc*Pq1(Imol) + ss*Pq2(Imol)
                        Pq2t = cc*Pq2(Imol) - ss*Pq1(Imol)
                        Pq3t = cc*Pq3(Imol) + ss*Pq0(Imol)

                        q0(Imol) = q0t
                        q1(Imol) = q1t
                        q2(Imol) = q2t
                        q3(Imol) = q3t

                        Pq0(Imol) = Pq0t
                        Pq1(Imol) = Pq1t
                        Pq2(Imol) = Pq2t
                        Pq3(Imol) = Pq3t

                        phi2 = 0.25d0*(-q2(Imol)*Pq0(Imol) -
     &                       q3(Imol)*Pq1(Imol) + q0(Imol)*Pq2(Imol) 
     &                       + q1(Imol)*Pq3(Imol))/Iy(Imol)

                        cc=dcos(phi2*0.5d0*ddt)
                        ss=dsin(phi2*0.5d0*ddt)

                        q0t = cc*q0(Imol) - ss*q2(Imol)
                        q1t = cc*q1(Imol) - ss*q3(Imol)
                        q2t = cc*q2(Imol) + ss*q0(Imol)
                        q3t = cc*q3(Imol) + ss*q1(Imol)

                        Pq0t = cc*Pq0(Imol) - ss*Pq2(Imol)
                        Pq1t = cc*Pq1(Imol) - ss*Pq3(Imol)
                        Pq2t = cc*Pq2(Imol) + ss*Pq0(Imol)
                        Pq3t = cc*Pq3(Imol) + ss*Pq1(Imol)

                        q0(Imol) = q0t
                        q1(Imol) = q1t
                        q2(Imol) = q2t
                        q3(Imol) = q3t

                        Pq0(Imol) = Pq0t
                        Pq1(Imol) = Pq1t
                        Pq2(Imol) = Pq2t
                        Pq3(Imol) = Pq3t

                        phi1 = 0.25d0*(-q1(Imol)*Pq0(Imol) +
     &                       q0(Imol)*Pq1(Imol) + q3(Imol)*Pq2(Imol)
     &                       - q2(Imol)*Pq3(Imol))/Ix(Imol)

                        cc=dcos(phi1*ddt)
                        ss=dsin(phi1*ddt)

                        q0t = cc*q0(Imol) - ss*q1(Imol)
                        q1t = cc*q1(Imol) + ss*q0(Imol)
                        q2t = cc*q2(Imol) + ss*q3(Imol)
                        q3t = cc*q3(Imol) - ss*q2(Imol)

                        Pq0t = cc*Pq0(Imol) - ss*Pq1(Imol)
                        Pq1t = cc*Pq1(Imol) + ss*Pq0(Imol)
                        Pq2t = cc*Pq2(Imol) + ss*Pq3(Imol)
                        Pq3t = cc*Pq3(Imol) - ss*Pq2(Imol)
        
                        q0(Imol) = q0t
                        q1(Imol) = q1t
                        q2(Imol) = q2t
                        q3(Imol) = q3t

                        Pq0(Imol) = Pq0t
                        Pq1(Imol) = Pq1t
                        Pq2(Imol) = Pq2t
                        Pq3(Imol) = Pq3t

                        phi2 = 0.25d0*(-q2(Imol)*Pq0(Imol) -
     &                      q3(Imol)*Pq1(Imol) + q0(Imol)*Pq2(Imol)
     &                       + q1(Imol)*Pq3(Imol))/Iy(Imol)

                        cc=dcos(phi2*0.5d0*ddt)
                        ss=dsin(phi2*0.5d0*ddt)

                        q0t = cc*q0(Imol) - ss*q2(Imol)
                        q1t = cc*q1(Imol) - ss*q3(Imol)
                        q2t = cc*q2(Imol) + ss*q0(Imol)
                        q3t = cc*q3(Imol) + ss*q1(Imol)

                        Pq0t = cc*Pq0(Imol) - ss*Pq2(Imol)
                        Pq1t = cc*Pq1(Imol) - ss*Pq3(Imol)
                        Pq2t = cc*Pq2(Imol) + ss*Pq0(Imol)
                        Pq3t = cc*Pq3(Imol) + ss*Pq1(Imol)

                        q0(Imol) = q0t
                        q1(Imol) = q1t
                        q2(Imol) = q2t
                        q3(Imol) = q3t

                        Pq0(Imol) = Pq0t
                        Pq1(Imol) = Pq1t
                        Pq2(Imol) = Pq2t
                        Pq3(Imol) = Pq3t

                        phi3 = 0.25d0*(-q3(Imol)*Pq0(Imol) +
     &                       q2(Imol)*Pq1(Imol) - q1(Imol)*Pq2(Imol) 
     &                       + q0(Imol)*Pq3(Imol))/Iz(Imol)

                        cc=dcos(phi3*0.5d0*ddt)
                        ss=dsin(phi3*0.5d0*ddt)

                        q0t = cc*q0(Imol) - ss*q3(Imol)
                        q1t = cc*q1(Imol) + ss*q2(Imol)
                        q2t = cc*q2(Imol) - ss*q1(Imol)
                        q3t = cc*q3(Imol) + ss*q0(Imol)

                        Pq0t = cc*Pq0(Imol) - ss*Pq3(Imol)
                        Pq1t = cc*Pq1(Imol) + ss*Pq2(Imol)
                        Pq2t = cc*Pq2(Imol) - ss*Pq1(Imol)
                        Pq3t = cc*Pq3(Imol) + ss*Pq0(Imol)

                        q0(Imol) = q0t
                        q1(Imol) = q1t
                        q2(Imol) = q2t
                        q3(Imol) = q3t

                        Pq0(Imol) = Pq0t
                        Pq1(Imol) = Pq1t
                        Pq2(Imol) = Pq2t
                        Pq3(Imol) = Pq3t            
                END DO

C  New rotation matrix

                RotationMatrix(1,1,Imol)=q0(Imol)**2+q1(Imol)**2-q2(Imol)**2-q3(Imol)**2
                RotationMatrix(1,2,Imol)=2.0d0*(q1(Imol)*q2(Imol)-q0(Imol)*q3(Imol))
                RotationMatrix(1,3,Imol)=2.0d0*(q1(Imol)*q3(Imol)+q0(Imol)*q2(Imol))
                RotationMatrix(2,1,Imol)=2.0d0*(q1(Imol)*q2(Imol)+q0(Imol)*q3(Imol))
                RotationMatrix(2,2,Imol)=q0(Imol)**2-q1(Imol)**2+q2(Imol)**2-q3(Imol)**2
                RotationMatrix(2,3,Imol)=2.0*(q2(Imol)*q3(Imol)-q0(Imol)*q1(Imol))
                RotationMatrix(3,1,Imol)=2.0*(q1(Imol)*q3(Imol)-q0(Imol)*q2(Imol))
                RotationMatrix(3,2,Imol)=2.0*(q2(Imol)*q3(Imol)+q0(Imol)*q1(Imol))
                RotationMatrix(3,3,Imol)=q0(Imol)**2-q1(Imol)**2-q2(Imol)**2+q3(Imol)**2
    
C     Update angular momenta and velocities to a half timestep

                Lx(Imol)=0.5d0*(-q1(Imol)*Pq0(Imol)+q0(Imol)*Pq1(Imol)+q3(Imol)*Pq2(Imol)
     &                   -q2(Imol)*Pq3(Imol))
                Ly(Imol)=0.5d0*(-q2(Imol)*Pq0(Imol)-q3(Imol)*Pq1(Imol)+q0(Imol)*Pq2(Imol)
     &                   +q1(Imol)*Pq3(Imol))
                Lz(Imol)=0.5d0*(-q3(Imol)*Pq0(Imol)+q2(Imol)*Pq1(Imol)-q1(Imol)*Pq2(Imol)
     &                   +q0(Imol)*Pq3(Imol))
        
                wx(Imol)=Lx(Imol)/Ix(Imol)
                wy(Imol)=Ly(Imol)/Iy(Imol)
                wz(Imol)=Lz(Imol)/Iz(Imol)

C New atomic positions
        
                DO Iatom=1,N_AtomInMolType(Tm)        
                        X(Imol,Iatom)= RotationMatrix(1,1,Imol)*dXb(Imol,Iatom)+ 
     &                   RotationMatrix(1,2,Imol)*dYb(Imol,Iatom)+RotationMatrix(1,3,Imol)
     &                   *dZb(Imol,Iatom)+XCM(Imol)
                        Y(Imol,Iatom)= RotationMatrix(2,1,Imol)*dXb(Imol,Iatom)+
     &                   RotationMatrix(2,2,Imol)*dYb(Imol,Iatom)+RotationMatrix(2,3,Imol)
     &                   *dZb(Imol,Iatom)+YCM(Imol)
                        Z(Imol,Iatom)= RotationMatrix(3,1,Imol)*dXb(Imol,Iatom)+
     &                   RotationMatrix(3,2,Imol)*dYb(Imol,Iatom)+RotationMatrix(3,3,Imol)
     &                   *dZb(Imol,Iatom)+ZCM(Imol)
                END DO
           END DO

           DO I=1,N_MolInBox(Ib)
                 Imol = I_MolInBox(Ib,I)
                 Tm   = TypeMol(Imol) 
                 IF(N_AtomInMolType(Tm).EQ.1)   CYCLE 

C New torques and rotation matrix

                 CALL Torque_Molecule(Imol,Tx_0,Ty_0,Tz_0,L_Overlap_Inter,L_Overlap_Intra)
                 IF(L_Overlap_Intra) THEN
                        WRITE(6,'(A,A)') ERROR, 
     &                  "Intramolecular Torque Overlap (Smart Rotation)"
                        STOP
                 ELSEIF(L_Overlap_Inter) THEN
                        Laccept=.false.
                        GO TO 1
                 END IF

C New torques in body frame 

                 Tx = RotationMatrix(1,1,Imol)*Tx_0+RotationMatrix(2,1,Imol)*Ty_0+
     &                 RotationMatrix(3,1,Imol)*Tz_0 
                 Ty = RotationMatrix(1,2,Imol)*Tx_0+RotationMatrix(2,2,Imol)*Ty_0+
     &                 RotationMatrix(3,2,Imol)*Tz_0 
                 Tz = RotationMatrix(1,3,Imol)*Tx_0+RotationMatrix(2,3,Imol)*Ty_0+
     &                 RotationMatrix(3,3,Imol)*Tz_0 

C New quaternion torques 

                 Tq0(Imol) =2.0d0*(-q1(Imol)*Tx-q2(Imol)*Ty-q3(Imol)*Tz)
                 Tq1(Imol) =2.0d0*( q0(Imol)*Tx-q3(Imol)*Ty+q2(Imol)*Tz)
                 Tq2(Imol) =2.0d0*( q3(Imol)*Tx+q0(Imol)*Ty-q1(Imol)*Tz)
                 Tq3(Imol) =2.0d0*(-q2(Imol)*Tx+q1(Imol)*Ty+q0(Imol)*Tz)

C     Update conjugate momenta to full timestep        
 
                 Pq0(Imol) = Pq0(Imol) + 0.5d0*dt_SmartRotation(Ib)*Tq0(Imol)
                 Pq1(Imol) = Pq1(Imol) + 0.5d0*dt_SmartRotation(Ib)*Tq1(Imol)
                 Pq2(Imol) = Pq2(Imol) + 0.5d0*dt_SmartRotation(Ib)*Tq2(Imol)
                 Pq3(Imol) = Pq3(Imol) + 0.5d0*dt_SmartRotation(Ib)*Tq3(Imol)

C     Update angular momenta and velocities to full timestep

                 Lx(Imol)=0.5d0*(-q1(Imol)*Pq0(Imol)+q0(Imol)*Pq1(Imol)+q3(Imol)*Pq2(Imol)
     &                  -q2(Imol)*Pq3(Imol))
                 Ly(Imol)=0.5d0*(-q2(Imol)*Pq0(Imol)-q3(Imol)*Pq1(Imol)+q0(Imol)*Pq2(Imol)
     &                  +q1(Imol)*Pq3(Imol))
                 Lz(Imol)=0.5d0*(-q3(Imol)*Pq0(Imol)+q2(Imol)*Pq1(Imol)-q1(Imol)*Pq2(Imol)
     &                  +q0(Imol)*Pq3(Imol))

                 wx(Imol)=Lx(Imol)/Ix(Imol)
                 wy(Imol)=Ly(Imol)/Iy(Imol)
                 wz(Imol)=Lz(Imol)/Iz(Imol)

                 Krot_new = Krot_new +0.5d0*(Ix(Imol)*wx(Imol)*wx(Imol)+Iy(Imol)*wy(Imol)
     &              *wy(Imol)+Iz(Imol)*wz(Imol)*wz(Imol))
           END DO 
      END DO

C     Calculate energy of new configuration
      CALL Energy_Total(Ib,E_LJ_InterNew,E_LJ_IntraNew,E_EL_RealNew,E_EL_IntraNew,E_EL_ExclNew,
     &                     E_BendingNew,E_TorsionNew,L_Overlap_Inter,L_Overlap_Intra,dU_dlNew)
      IF(L_Overlap_Intra) THEN
         WRITE(6,'(A,A)') ERROR, 
     &   "Intramolecular Energy Overlap (Smart Rotation)"
         STOP
      ELSEIF(L_Overlap_Inter) THEN
         Laccept=.false.
         WRITE(6,'(A,A)') ERROR, 
     &   "Intermolecular Energy Overlap (SmartRotation)"
         STOP
         GO TO 1
      END IF

      E_EL_FourNew = 0.0d0
      dUELFour_dl  = 0.0d0
      IF(L_Ewald(Ib)) CALL Ewald_Total(Ib,E_EL_FourNew,dUELFour_dl)

      CALL Energy_Correction(Ib)

      E_LJ_TailNew = U_LJ_Tail(Ib)
      E_EL_SelfNew = U_EL_Self(Ib)

      Enew = E_LJ_InterNew + E_LJ_IntraNew + E_EL_RealNew + E_EL_IntraNew + E_BendingNew
     &       + E_TorsionNew  + E_LJ_TailNew  + E_EL_SelfNew + E_EL_ExclNew  + E_EL_FourNew
      
      dUTail_dlNew   = dUTail_dl
      dUELSelf_dlNew = dUELSelf_dl
      dU_dlNew       = dU_dlNew + dUTail_dlNew + dUELSelf_dlNew + dUELFour_dl
      
      dE    = Enew - Eold
      dKrot = Krot_new - Krot_old

      CALL Accept_or_Reject(dexp(-beta*(dE+dKrot)),Laccept)

   1  CONTINUE

      IF(Laccept) THEN
         AcceptSmartRotation(Ib) = AcceptSmartRotation(Ib) + 1.0d0

         U_LJ_Inter(Ib) = E_LJ_InterNew
         U_LJ_Intra(Ib) = E_LJ_IntraNew
         U_EL_Real(Ib)  = E_EL_RealNew
         U_EL_Intra(Ib) = E_EL_IntraNew
         U_EL_Excl(Ib)  = E_EL_ExclNew
         U_EL_Four(Ib)  = E_EL_FourNew
         U_EL_Self(Ib)  = E_EL_SelfNew

         U_Bending_Total(Ib) = E_BendingNew
         U_Torsion_Total(Ib) = E_TorsionNew

         U_Total(Ib) = U_Total(Ib) + dE
         ddU_dl      = dU_dlNew - dU_dlOld
         dU_dlTotal  = dU_dlTotal + ddU_dl

C     Calculate average rotation angles (construct vectors between 1st and 2nd atom and use dotproduct)         
         Avd = 0.0d0
         DO I=1,N_MolInBox(Ib)
            Imol = I_MolInBox(Ib,I)

            ux = Xold(Imol,2) - Xold(Imol,1) 
            uy = Yold(Imol,2) - Yold(Imol,1) 
            uz = Zold(Imol,2) - Zold(Imol,1) 

            vx = X(Imol,2) - X(Imol,1) 
            vy = Y(Imol,2) - Y(Imol,1) 
            vz = Z(Imol,2) - Z(Imol,1) 

            delta_angle = dacos((ux*vx + uy*vy + uz*vz)/(dsqrt(ux*ux+uy*uy+uz*uz)*dsqrt(vx*vx+vy*vy+vz*vz)))

            Avd = Avd + delta_angle
         END DO

         AvDelta_AcceptSmartRotation(Ib) = AvDelta_AcceptSmartRotation(Ib) + Avd/dble(N_MolInBox(Ib))

      ELSE

         U_LJ_Tail(Ib)  = E_LJ_TailOld
         U_EL_Self(Ib)  = E_EL_SelfOld

         dUTail_dl      = dUTail_dlOld
         dUELSelf_dl    = dUELSelf_dlOld

         DO I=1,N_MolInBox(Ib)
            Imol=I_MolInBox(Ib,I)
            Tm = TypeMol(Imol)
            IF(N_AtomInMolType(Tm).EQ.1)   CYCLE

            DO Iatom=1,N_AtomInMolType(TypeMol(Imol))
               X(Imol,Iatom) = Xold(Imol,Iatom)
               Y(Imol,Iatom) = Yold(Imol,Iatom)
               Z(Imol,Iatom) = Zold(Imol,Iatom)
            END DO

            XCM(Imol) = XCMold(Imol)
            YCM(Imol) = YCMold(Imol)
            ZCM(Imol) = ZCMold(Imol)
         END DO

         IF(L_Ewald(Ib)) CALL Ewald_Store(Ib,2)

      END IF



      RETURN
      END
