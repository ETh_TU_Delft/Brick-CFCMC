      SUBROUTINE Smart_Translation
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "energy.inc"
      include "output.inc"

C     Displace molecules based on the force acting on them
      integer Imol,Ib,Tm,I,Select_Random_Integer,Iatom,I_MD_step
      double precision dE,XCMold(MaxMol),YCMold(MaxMol),ZCMold(MaxMol),
     &      Xold(MaxMol,MaxAtom),Yold(MaxMol,MaxAtom),Zold(MaxMol,MaxAtom),Fx,Fy,Fz,
     &      E_LJ_InterNew,E_LJ_IntraNew,E_EL_RealNew,E_EL_IntraNew,E_EL_ExclNew,E_BendingNew,
     &      E_TorsionNew,E_EL_FourNew,Eold,Enew,dX(MaxMol),dY(MaxMol),dZ(MaxMol),Avd,
     &      Ran_Gauss,E_LJ_TailOld,E_LJ_TailNew,E_EL_SelfOld,E_EL_SelfNew,Kold,Knew,dK,Kaim,
     &      vx(MaxMol),vy(MaxMol),vz(MaxMol),vfac,dR(MaxMol),
     &      ddU_dl,dUELFour_dl,dU_dlOld,dU_dlNew,dUTail_dlOld,dUTail_dlNew,dUELSelf_dlOld,
     &      dUELSelf_dlNew
      logical L_Overlap_Inter,L_Overlap_Intra,Laccept

      Ib = Select_Random_Integer(N_Box)
      
      TrialSmartTranslation(Ib) = TrialSmartTranslation(Ib) + 1.0d0

C     Save old configuration
      E_LJ_TailOld = U_LJ_Tail(Ib)
      E_EL_SelfOld = U_EL_Self(Ib)

      Eold = U_Total(Ib)

      dU_dlOld       = dU_dlTotal
      dUTail_dlOld   = dUTail_dl
      dUELSelf_dlOld = dUELSelf_dl

      IF(L_Ewald(Ib)) CALL Ewald_Store(Ib,1)

      DO I=1,N_MolInBox(Ib)
         Imol = I_MolInBox(Ib,I)
         Tm   = TypeMol(Imol)

         DO Iatom=1,N_AtomInMolType(Tm)
            Xold(Imol,Iatom) = X(Imol,Iatom)
            Yold(Imol,Iatom) = Y(Imol,Iatom)
            Zold(Imol,Iatom) = Z(Imol,Iatom)
         END DO

         XCMold(Imol) = XCM(Imol)
         YCMold(Imol) = YCM(Imol)
         ZCMold(Imol) = ZCM(Imol)

         dR(I) = 0.0d0
      END DO

      Kold = 0.0d0

      DO I=1,N_MolInBox(Ib)
         Imol = I_MolInBox(Ib,I)
         Tm   = TypeMol(Imol)

         vx(I) = Ran_Gauss()
         vy(I) = Ran_Gauss()
         vz(I) = Ran_Gauss()

         Kold = Kold + 0.5d0*MassOneMolecule(Tm)*(vx(I)*vx(I)+vy(I)*vy(I)+vz(I)*vz(I))  
      END DO

      Kaim = 1.5d0*dble(N_MolInBox(Ib))/beta
      vfac = dsqrt(Kaim/Kold)

      Kold = 0.0d0

      DO I=1,N_MolInBox(Ib)
         Imol = I_MolInBox(Ib,I)
         Tm   = TypeMol(Imol)

         vx(I) = vx(I)*vfac
         vy(I) = vy(I)*vfac
         vz(I) = vz(I)*vfac

         Kold = Kold + 0.5d0*MassOneMolecule(Tm)*(vx(I)*vx(I)+vy(I)*vy(I)+vz(I)*vz(I))
      END DO

      DO I_MD_step=1,Nstep_SmartTranslation(Ib)

         DO I=1,N_MolInBox(Ib)
            Imol = I_MolInBox(Ib,I)
            Tm   = TypeMol(Imol)
            
            CALL Force_Molecule(Imol,Fx,Fy,Fz,L_Overlap_Inter,L_Overlap_Intra)
            IF(L_Overlap_Intra) THEN
               WRITE(6,'(A,A)') ERROR, "Intramolecular Force Overlap (Smart Translation)"
               STOP
            ELSEIF(L_Overlap_Inter) THEN
               WRITE(6,'(A,A)') ERROR, "Intermolecular Force Overlap (Smart Translation)"
               STOP
            END IF         

            vx(I) = vx(I) + 0.5d0*Fx*dt_SmartTranslation(Ib)/MassOneMolecule(Tm)
            vy(I) = vy(I) + 0.5d0*Fy*dt_SmartTranslation(Ib)/MassOneMolecule(Tm)
            vz(I) = vz(I) + 0.5d0*Fz*dt_SmartTranslation(Ib)/MassOneMolecule(Tm)

            dX(I) = vx(I)*dt_SmartTranslation(Ib)   
            dY(I) = vy(I)*dt_SmartTranslation(Ib)
            dZ(I) = vz(I)*dt_SmartTranslation(Ib)
         END DO

C     Generate next configuration
         DO I=1,N_MolInBox(Ib)
            Imol = I_MolInBox(Ib,I)
            Tm   = TypeMol(Imol)

            DO Iatom=1,N_AtomInMolType(Tm)
               X(Imol,Iatom) = X(Imol,Iatom) + dX(I)
               Y(Imol,Iatom) = Y(Imol,Iatom) + dY(I)
               Z(Imol,Iatom) = Z(Imol,Iatom) + dZ(I)
            END DO

            XCM(Imol) = XCM(Imol) + dX(I)
            YCM(Imol) = YCM(Imol) + dY(I)
            ZCM(Imol) = ZCM(Imol) + dZ(I)

            CALL Place_molecule_back_in_box(Imol)

            dR(I) = dR(I) + dsqrt(dX(I)*dX(I)+dY(I)*dY(I)+dZ(I)*dZ(I))
         END DO

         DO I=1,N_MolInBox(Ib)
            Imol = I_MolInBox(Ib,I)
            Tm = TypeMol(Imol)
         
            CALL Force_Molecule(Imol,Fx,Fy,Fz,L_Overlap_Inter,L_Overlap_Intra)
            IF(L_Overlap_Intra) THEN
               WRITE(6,'(A,A)') ERROR, "Intramolecular Force Overlap (Smart Translation)"
               STOP
            ELSEIF(L_Overlap_Inter) THEN
               Laccept=.false.
               GO TO 1
            END IF
         
            vx(I) = vx(I) + 0.5d0*Fx*dt_SmartTranslation(Ib)/MassOneMolecule(Tm)
            vy(I) = vy(I) + 0.5d0*Fy*dt_SmartTranslation(Ib)/MassOneMolecule(Tm)
            vz(I) = vz(I) + 0.5d0*Fz*dt_SmartTranslation(Ib)/MassOneMolecule(Tm)
         END DO

      END DO

C     Calculate energy of new configuration    
      Knew = 0.0d0
      DO I=1,N_MolInBox(Ib)
         Imol = I_MolInBox(Ib,I)
         Tm = TypeMol(Imol)

         Knew = Knew + 0.5d0*MassOneMolecule(Tm)*(vx(I)*vx(I)+vy(I)*vy(I)+vz(I)*vz(I))
      END DO  

      CALL Energy_Total(Ib,E_LJ_InterNew,E_LJ_IntraNew,E_EL_RealNew,E_EL_IntraNew,E_EL_ExclNew,
     &                     E_BendingNew,E_TorsionNew,L_Overlap_Inter,L_Overlap_Intra,dU_dlNew)

      IF(L_Overlap_Intra) THEN
         WRITE(6,'(A,A)') ERROR, "Intramolecular Energy Overlap (Smart Translation)"
         STOP
      ELSEIF(L_Overlap_Inter) THEN
         Laccept=.false.
         GO TO 1
      END IF

      E_EL_FourNew = 0.0d0
      dUELFour_dl  = 0.0d0
      IF(L_Ewald(Ib)) CALL Ewald_Total(Ib,E_EL_FourNew,dUELFour_dl)

      CALL Energy_Correction(Ib)

      E_LJ_TailNew = U_LJ_Tail(Ib)
      E_EL_SelfNew = U_EL_Self(Ib)

      Enew = E_LJ_InterNew + E_LJ_IntraNew + E_EL_RealNew + E_EL_IntraNew + E_BendingNew
     &     + E_TorsionNew  + E_LJ_TailNew  + E_EL_SelfNew + E_EL_ExclNew + E_EL_FourNew

      dUELSelf_dlNew = dUELSelf_dl
      dUTail_dlNew   = dUTail_dl
      dU_dlNew       = dU_dlNew + dUTail_dlNew + dUELSelf_dlNew + dUELFour_dl

      dE = Enew - Eold 
      dK = Knew - Kold

      CALL Accept_or_Reject(dexp(-beta*(dE+dK)),Laccept)

   1  CONTINUE

      IF(Laccept) THEN
         AcceptSmartTranslation(Ib) = AcceptSmartTranslation(Ib) + 1.0d0

         U_LJ_Inter(Ib) = E_LJ_InterNew
         U_LJ_Intra(Ib) = E_LJ_IntraNew

         U_EL_Real(Ib)  = E_EL_RealNew
         U_EL_Intra(Ib) = E_EL_IntraNew
         U_EL_Excl(Ib)  = E_EL_ExclNew
         U_EL_Four(Ib)  = E_EL_FourNew
         U_EL_Self(Ib)  = E_EL_SelfNew

         U_Bending_Total(Ib) = E_BendingNew
         U_Torsion_Total(Ib) = E_TorsionNew

         U_Total(Ib) = U_Total(Ib) + dE

         ddU_dl = dU_dlNew - dU_dlOld
         dU_dlTotal = dU_dlTotal + ddU_dl

         Avd = 0.0d0
         DO I=1,N_MolInBox(Ib)
            Avd = Avd + dR(I)
         END DO
         AvDelta_AcceptSmartTranslation(Ib) = AvDelta_AcceptSmartTranslation(Ib) + Avd/dble(N_MolInBox(Ib))

      ELSE

         U_LJ_Tail(Ib)  = E_LJ_TailOld
         U_EL_Self(Ib)  = E_EL_SelfOld

         dUTail_dl      = dUTail_dlOld
         dUELSelf_dl    = dUELSelf_dlOld

         DO I=1,N_MolInBox(Ib)
            Imol=I_MolInBox(Ib,I)

            DO Iatom=1,N_AtomInMolType(TypeMol(Imol))
               X(Imol,Iatom) = Xold(Imol,Iatom)
               Y(Imol,Iatom) = Yold(Imol,Iatom)
               Z(Imol,Iatom) = Zold(Imol,Iatom)
            END DO

            XCM(Imol) = XCMold(Imol)
            YCM(Imol) = YCMold(Imol)
            ZCM(Imol) = ZCMold(Imol)
         END DO

         IF(L_Ewald(Ib)) CALL Ewald_Store(Ib,2)

      END IF

      RETURN
      END
