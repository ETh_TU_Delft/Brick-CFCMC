      SUBROUTINE Write_Restart(Icycle)
      implicit none

      include "global_variables.inc"
      include "output.inc"

      integer  Ib,Tm,Imol,I,Iatom,Ifrac,Icycle
      character*10 N1

CCC   Write current configuration and delta's to file
C     This file is a special one: it can be used as an input file

      OPEN(91,file="./OUTPUT/restart.out")

      WRITE(91,'(A,i10)') "Simulation state at cycle: ", Icycle
      WRITE(91,'(A)') hashes(1)
      WRITE(91,*)

      WRITE(N1,'(i6)') N_MolTotal
      WRITE(91,'(A,A)') "Number-of-molecules: ", adjustl(TRIM(N1))
      WRITE(91,*)
      WRITE(91,'(A,2f12.6)') "Box-size(s): ", (BoxSize(Ib), Ib=1,N_Box)

      WRITE(91,*)
      WRITE(91,'(A)') bars(1)
      WRITE(91,*)

      WRITE(91,'(A)') "Deltas"

      WRITE(91,'(A)') "Translation per molecule type"
      DO Ib=1,N_Box
         WRITE(91,'(a4,i1,1x,99e20.10e3)') "Box ", Ib, (Delta_Translation(Ib,Tm), Tm=1,N_MolType)
      END DO

      WRITE(91,*)

      WRITE(91,'(A)') "Rotation per molecule type"
      DO Ib=1,N_Box
         WRITE(91,'(a4,i1,1x,99e20.10e3)') "Box ", Ib, (Delta_Rotation(Ib,Tm)*180.0d0/OnePi, Tm=1,N_MolType)
      END DO

      WRITE(91,*)
      WRITE(91,'(A)') "Volume    Delta_Volume"
      DO Ib=1,N_Box
         WRITE(91,'(a4,i1,3e20.10e3)') "Box ", Ib, Delta_Volume(Ib)
      END DO

      WRITE(91,*)
      WRITE(91,'(A)') bars(1)
      WRITE(91,*)

      DO Ib=1,N_Box
      WRITE(91,'(A,i1)') "Molecules in box ", Ib
      WRITE(91,'(A)') hashes(1)
         DO Tm=1,N_MolType
            WRITE(91,'(A,i2)') "Moleculetype ", Tm
            WRITE(N1,'(i6)') Nmptpb(Ib,Tm)
            WRITE(91,'(A,A)')  "Number-of-molecules: ", adjustl(TRIM(N1))
            WRITE(91,'(A)') "Atomtype         X                  Y                   Z"
            DO I=1,Nmptpb(Ib,Tm)
               Imol = Imptpb(Ib,Tm,I)
               DO Iatom=1,N_AtomInMolType(Tm)
                  WRITE(91,'(i3,3f20.10)')
     &                     TypeAtom(Tm,Iatom), X(Imol,Iatom), Y(Imol,Iatom), Z(Imol,Iatom)
               END DO
            END DO
            WRITE(91,*)
         END DO
         WRITE(91,*)
      END DO

      WRITE(N1,'(i2)') N_Frac
      WRITE(91,'(A,A)') "Number-of-fractionals: ", N1
      WRITE(91,'(A)') hashes(1)

      DO Ifrac=1,N_Frac
         WRITE(91,'(A,i2)')   "Fractional-type:     ", Type_Frac(Ifrac)
         WRITE(91,'(A,i2)')   "Box:                 ", Box_Frac(Ifrac)
         WRITE(91,'(A,f9.6)') "Lambda:              ", Lambda_Frac(Ifrac)
         WRITE(91,'(A,i2,A)') "Reaction-Step        ", ReactionStep_Frac(Ifrac), " (only used if Type = 3 (RxMC))"
         WRITE(91,*)

         DO I=1,N_MolInFrac(Ifrac)
            Imol = I_MolInFrac(Ifrac,I)
            Tm   = TypeMol(Imol)
            WRITE(91,'(A,i2)') "Moleculetype ", Tm
            WRITE(91,'(A)') "Atomtype         X                  Y                   Z"
            DO Iatom=1,N_AtomInMolType(Tm)
               WRITE(91,'(i3,3f20.10)')
     &            TypeAtom(Tm,Iatom), X(Imol,Iatom), Y(Imol,Iatom), Z(Imol,Iatom)
            END DO
            WRITE(91,*)
         END DO
         WRITE(91,'(A)') bars(1)
         WRITE(91,*)
      END DO

      CLOSE(91)

      RETURN
      END
