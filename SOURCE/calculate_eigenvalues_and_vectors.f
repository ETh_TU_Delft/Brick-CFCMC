      SUBROUTINE Calculate_Eigenvalues_and_Vectors(M,r,s,t,U,V,W)
      implicit none

      include "global_variables.inc"
      include "output.inc"

      integer I,Imax,Imid,Imin,Jmax,ISweep,MaxSweep

      double precision M(3,3),M1(3,3),M2(3,3),M3(3,3)
      double precision E(3,3),E1(3,3)
      double precision G(3,3),GT(3,3)
      double precision U(3),V(3),W(3)
      double precision r,s,t,det
      double precision MaxValue,MaxEigenValue,MinEigenValue,theta

      Parameter (MaxSweep = 15)

      E(1,:) = (/ 1.0d0, 0.0d0, 0.0d0 /)
      E(2,:) = (/ 0.0d0, 1.0d0, 0.0d0 /)
      E(3,:) = (/ 0.0d0, 0.0d0, 1.0d0 /)
     
      M1 = M

      DO Isweep=1,MaxSweep
         IF(dabs(M1(1,2)+dabs(M1(1,3))+dabs(M1(2,3))).GT.1.0D-8) THEN
C     Look for the largest absolute value off the diagonal
            MaxValue = dabs(M1(1,2))
            Imax = 1
            Jmax = 2
            IF(dabs(M1(1,3)).GT.MaxValue) THEN
               MaxValue = dabs(M1(1,3))
               Imax = 1
               Jmax = 3
            END IF
            IF(dabs(M1(2,3)).GT.MaxValue) THEN
               MaxValue = dabs(M1(2,3))
               Imax = 2
               Jmax = 3
            END IF

            IF(dabs(M1(Jmax,Jmax)-M1(Imax,Imax)).LT.1.0d-8) THEN
               theta = 0.25d0*OnePi
            ELSE  
               theta = 0.5d0*datan(2.0d0*M1(Imax,Jmax)/(M1(Jmax,Jmax)-M1(Imax,Imax)))
            END IF

            CALL Givens_Rotation_Matrix(Imax,Jmax,theta,G)
            CALL Transpose_Matrix(G,GT)
            CALL Multiply_Matrix(GT,M1,M2)
            CALL Multiply_Matrix(M2,G,M3)

            M1=M3

            CALL Multiply_Matrix(E,G,E1)

            E = E1

         ELSE
            EXIT
         END IF

      END DO

      IF(ISweep.GE.MaxSweep) THEN
         WRITE(6,*) "WARNING, MaxSweeps reached in Jacobi algorithm for Eigenvalues and Vectors."
      END IF

C     Sort the eigenvalues from large to small

      MaxEigenValue = M1(1,1)
      MinEigenValue = M1(1,1)
      Imax = 1
      Imin = 1

      DO I=2,3
         IF(M2(I,I).GT.MaxEigenValue) THEN
            MaxEigenValue=M1(I,I)
            Imax = I 
         ELSEIF(M2(I,I).LT.MinEigenValue) THEN
            MinEigenValue=M1(I,I)
            Imin = I 
         END IF
      END DO

      IF(Imax.EQ.Imin) THEN
         WRITE(6,*) ERROR, "Imax = Imin, calculate eigenvalues_and_vectors"
         STOP
      END IF

      DO I=1,3
         IF((I.NE.Imax).AND.(I.NE.Imin)) THEN
            Imid = I 
         END IF
      END DO

      r = M1(Imax,Imax)
      U = E(:,Imax)

      s = M1(Imid,Imid)
      V = E(:,Imid)

      t = M1(Imin,Imin)
      W = E(:,Imin)
      CALL Determinant(U,V,W,det)

      IF(det<0.0d0) THEN
         U(:)=-U(:)
       END IF

      RETURN
      END



      SUBROUTINE Print_Matrix(M)
      implicit none

      integer I,J
      double precision M(3,3)
      
      DO I=1,3
         WRITE(6,'(3(f10.5,1x))') (M(I,J), J=1,3)
      END DO

      RETURN
      END



      SUBROUTINE Multiply_Matrix(P,Q,R)
      implicit none

      integer I,J,K
      double precision P(3,3),Q(3,3),R(3,3)

      DO I=1,3
         DO J=1,3
            R(I,J) = 0.0d0
            DO K=1,3
               R(I,J) = R(I,J) + P(I,K)*Q(K,J)
            END DO
         END DO
      END DO

      RETURN
      END SUBROUTINE



      SUBROUTINE Transpose_Matrix(M,MT)
      implicit none

      integer I,J
      double precision M(3,3),MT(3,3)
   
      DO I=1,3
         DO J=1,3
            MT(I,J) = M(J,I)
         END DO
      END DO
   
      RETURN
      END



      SUBROUTINE Givens_Rotation_Matrix(I,J,theta,G)
      implicit none

      integer I,J
      double precision theta,G(3,3)

      IF(I.eq.J) THEN
         WRITE(6,'(A,A)') "ERROR, Givens Rotation Matrix p=q"
         STOP
      END IF

      G(1,:) = (/ 1, 0, 0 /)
      G(2,:) = (/ 0, 1, 0 /)
      G(3,:) = (/ 0, 0, 1 /)

      G(I,I) = dcos(theta)
      G(J,J) = G(I,I)
      G(I,J) = dsin(theta)
      G(J,I) = -G(I,J)
         
      RETURN
      END


      SUBROUTINE Determinant(U,V,W,det)
      implicit none
      double precision U(3),V(3),W(3),det

      det=U(1)*(V(2)*W(3)-W(2)*V(3))-V(1)*(U(2)*W(3)-W(2)*U(3))+
     &    W(1)*(U(2)*V(3)-U(3)*V(2))

      RETURN
      END      





