      SUBROUTINE Place_molecule_back_in_box(Imol)
      implicit none

C     Put a molecule with label Imol back in its box

      include "global_variables.inc"

      integer Imol,Ib,Tm,I
      double precision dX,dY,dZ,XCMold,YCMold,ZCMold

      Ib = Ibox(Imol)
      Tm = TypeMol(Imol)

      XCMold = XCM(Imol)
      YCMold = YCM(Imol)
      ZCMold = ZCM(Imol)

      dX = XCM(Imol)
      dY = YCM(Imol)
      dZ = ZCM(Imol)

      dX = dX - BoxSize(Ib)*Dnint(dX*InvBoxSize(Ib))
      dY = dY - BoxSize(Ib)*Dnint(dY*InvBoxSize(Ib))
      dZ = dZ - BoxSize(Ib)*Dnint(dZ*InvBoxSize(Ib))

      IF(dX.LT.0.0d0) dX = dX + BoxSize(Ib)
      IF(dY.LT.0.0d0) dY = dY + BoxSize(Ib)
      IF(dZ.LT.0.0d0) dZ = dZ + BoxSize(Ib)

      XCM(Imol) = dX
      YCM(Imol) = dY
      ZCM(Imol) = dZ

      dX = XCM(Imol) - XCMold
      dY = YCM(Imol) - YCMold
      dZ = ZCM(Imol) - ZCMold

      DO I=1,N_AtomInMolType(Tm)
         X(Imol,I) = X(Imol,I) + dX
         Y(Imol,I) = Y(Imol,I) + dY
         Z(Imol,I) = Z(Imol,I) + dZ
      END DO



      RETURN
      END
