      FUNCTION Dderfc(x)
      implicit none

      double precision dderfc,x,t

      t=1.0d0/(1.0d0+0.5d0*x)
      dderfc=t*dexp(-x*x-1.26551223d0+t*(1.00002368d0+t*(0.37409196d0+t
     &       *(0.09678418d0+t*(-0.18628806d0+t*(.27886807d0+t*(-1.13520398d0+t
     &       *(1.48851587d0+t*(-0.82215223d0+t*0.17087277d0)))))))))

      return
      END
