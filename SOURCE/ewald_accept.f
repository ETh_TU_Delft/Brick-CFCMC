      SUBROUTINE Ewald_Accept(Ib)
      implicit none

      include "global_variables.inc"
      include "ewald.inc"

C     Update CKC_old and CKS_old if the move is accepted

      integer Ivec,Ib

      DO Ivec = 1,N_Kvec(Ib)
        CKC_old(Ivec,Ib) = CKC_new(Ivec,Ib)
        CKS_old(Ivec,Ib) = CKS_new(Ivec,Ib)

        IF(L_dUdl) THEN
            CKC_dUdl_old(Ivec,1) = CKC_dUdl_new(Ivec,1)
            CKS_dUdl_old(Ivec,1) = CKS_dUdl_new(Ivec,1)
        END IF

      ENDDO

      RETURN

      END
