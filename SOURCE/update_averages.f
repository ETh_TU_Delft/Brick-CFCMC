      SUBROUTINE Update_Averages
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "output.inc"
      include "settings.inc"

      integer Ifrac,Ibin,Rs,Tf,Ib,J,Ireac,Tm,Imol
      double precision Myweight,Lf,Density,Delta,E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,
     &  E_EL_Excl,E_Bending,E_Torsion,dE_LJ_Inter_dLambda,dE_LJ_Intra_dLambda,dE_EL_Real_dLambda,
     &  dE_EL_Intra_dLambda,dE_EL_Excl_dLambda,dE_Bending_dLambda,dE_Torsion_dLambda,dE_Total_dlambda,
     &  LambdaOld,E_LJ_InterRight,E_LJ_IntraRight,E_EL_RealRight,E_EL_IntraRight,E_EL_ExclRight,
     &  E_BendingRight,E_TorsionRight,E_LJ_InterLeft,E_LJ_IntraLeft,E_EL_RealLeft,E_EL_IntraLeft,
     &  E_EL_ExclLeft,E_BendingLeft,E_TorsionLeft
      logical L_Overlap_Inter,L_Overlap_Intra
      Parameter (Delta = 0.005d0)

C     Calculate weight of the averages
      Myweight = 0.0d0

      DO Ifrac=1,N_Frac
         Tf = Type_Frac(Ifrac)
         Lf = Lambda_Frac(Ifrac)
         Ib = Box_Frac(Ifrac)

         IF(Tf.EQ.3) THEN
            Rs = ReactionStep_Frac(Ifrac)
         ELSE
            Rs = 1
         END IF

         Ibin = 1 + int(dble(N_LambdaBin(Ifrac))*Lf)

         IF(Ibin.GT.N_LambdaBin(Ifrac)) THEN
            WRITE(6,'(A,A)') ERROR, "Problem with lambda value and bin in update_averages"
            STOP
         END IF

         Myweight = Myweight + Weight(Ibin,Ib,Rs,Ifrac)

      END DO

      Myweight = dexp(-Myweight)

      AvR = AvR + Myweight
      AvB = AvB + 1.0d0

C     Calculate average (number) densities
      DO Ib=1,N_Box
         DO Tm=1,N_MolType
            AvR_Densityptpb(Ib,Tm) = AvR_Densityptpb(Ib,Tm) + Myweight*dble(Nmptpb(Ib,Tm))/(Volume(Ib))
            AvR_Nmptpb(Ib,Tm)      = AvR_Nmptpb(Ib,Tm)      + Myweight*dble(Nmptpb(Ib,Tm))

            AvB_Densityptpb(Ib,Tm) = AvB_Densityptpb(Ib,Tm) + dble(Nmptpb(Ib,Tm))/(Volume(Ib))
            AvB_Nmptpb(Ib,Tm)      = AvB_Nmptpb(Ib,Tm)      + dble(Nmptpb(Ib,Tm))
         END DO
      END DO

C     Update average energies
      DO Ib=1,N_Box
         AvR_U_LJ_Inter(Ib) = AvR_U_LJ_Inter(Ib) + Myweight*U_LJ_Inter(Ib)
         AvR_U_LJ_Intra(Ib) = AvR_U_LJ_Intra(Ib) + Myweight*U_LJ_Intra(Ib)
         AvR_U_LJ_Tail(Ib)  = AvR_U_LJ_Tail(Ib)  + Myweight*U_LJ_Tail(Ib)

         AvR_U_EL_Real(Ib)  = AvR_U_EL_Real(Ib)  + Myweight*U_EL_Real(Ib)
         AvR_U_EL_Intra(Ib) = AvR_U_EL_Intra(Ib) + Myweight*U_EL_Intra(Ib)
         AvR_U_EL_Excl(Ib)  = AvR_U_EL_Excl(Ib)  + Myweight*U_EL_Excl(Ib)
         AvR_U_EL_Self(Ib)  = AvR_U_EL_Self(Ib)  + Myweight*U_EL_Self(Ib)
         AvR_U_EL_Four(Ib)  = AvR_U_EL_Four(Ib)  + Myweight*U_EL_Four(Ib)

         AvR_U_Bending_Total(Ib) = AvR_U_Bending_Total(Ib) + Myweight*U_Bending_Total(Ib)
         AvR_U_Torsion_Total(Ib) = AvR_U_Torsion_Total(Ib) + Myweight*U_Torsion_Total(Ib)

         AvB_U_LJ_Inter(Ib) = AvB_U_LJ_Inter(Ib) + U_LJ_Inter(Ib)
         AvB_U_LJ_Intra(Ib) = AvB_U_LJ_Intra(Ib) + U_LJ_Intra(Ib)
         AvB_U_LJ_Tail(Ib)  = AvB_U_LJ_Tail(Ib)  + U_LJ_Tail(Ib)

         AvB_U_EL_Real(Ib)  = AvB_U_EL_Real(Ib)  + U_EL_Real(Ib)
         AvB_U_EL_Intra(Ib) = AvB_U_EL_Intra(Ib) + U_EL_Intra(Ib)
         AvB_U_EL_Excl(Ib)  = AvB_U_EL_Excl(Ib)  + U_EL_Excl(Ib)
         AvB_U_EL_Self(Ib)  = AvB_U_EL_Self(Ib)  + U_EL_Self(Ib)
         AvB_U_EL_Four(Ib)  = AvB_U_EL_Four(Ib)  + U_EL_Four(Ib)

         AvB_U_Bending_Total(Ib) = AvB_U_Bending_Total(Ib) + U_Bending_Total(Ib)
         AvB_U_Torsion_Total(Ib) = AvB_U_Torsion_Total(Ib) + U_Torsion_Total(Ib)
      END DO

C     Update the average boxsize
      DO Ib=1,N_Box
         AvR_Volume(Ib) = AvR_Volume(Ib) + Myweight*Volume(Ib)
         AvB_Volume(Ib) = AvB_Volume(Ib) + Volume(Ib)
      END DO

C     Update the average densities using Umbrella Sampling
      DO Ib=1,N_Box
         DO Tm=1,N_MolType
            Density = dble(Nmptpb(Ib,Tm))/(Volume(Ib))
            DO J=1,N_UmbrellaBinPressure(Ib)
               AvR_Densityptpb_UmbrellaPressure(J,Ib,Tm) = AvR_Densityptpb_UmbrellaPressure(J,Ib,Tm)
     &            + Density*MyWeight*dexp(beta*Volume(Ib)*(Pressure-P_Umbrella(J,Ib)))
               AvR_Norm_UmbrellaPressure(J,Ib,Tm) = AvR_Norm_UmbrellaPressure(J,Ib,Tm)
     &            +  MyWeight*dexp(beta*Volume(Ib)*(Pressure-P_Umbrella(J,Ib)))
               AvB_Densityptpb_UmbrellaPressure(J,Ib,Tm) = AvB_Densityptpb_UmbrellaPressure(J,Ib,Tm)
     &            + Density*dexp(beta*Volume(Ib)*(Pressure-P_Umbrella(J,Ib)))
               AvB_Norm_UmbrellaPressure(J,Ib,Tm) = AvB_Norm_UmbrellaPressure(J,Ib,Tm)
     &            + dexp(beta*Volume(Ib)*(Pressure-P_Umbrella(J,Ib)))
            END DO
            DO J=1,N_UmbrellaBinBeta(Ib)
               AvR_Densityptpb_UmbrellaBeta(J,Ib,Tm) = AvR_Densityptpb_UmbrellaBeta(J,Ib,Tm)
     &            + Density*Myweight*dexp((beta-Beta_Umbrella(J,Ib))*(U_Total(Ib)+Pressure*Volume(Ib)))
               AvR_Norm_UmbrellaBeta(J,Ib,Tm) = AvR_Norm_UmbrellaBeta(J,Ib,Tm)
     &            + Myweight*dexp((beta-Beta_Umbrella(J,Ib))*(U_Total(Ib)+Pressure*Volume(Ib)))
               AvB_Densityptpb_UmbrellaBeta(J,Ib,Tm) = AvB_Densityptpb_UmbrellaBeta(J,Ib,Tm)
     &            + Density*dexp((beta-Beta_Umbrella(J,Ib))*(U_Total(Ib)+Pressure*Volume(Ib)))
               AvB_Norm_UmbrellaBeta(J,Ib,Tm) = AvB_Norm_UmbrellaBeta(J,Ib,Tm)
     &            + dexp((beta-Beta_Umbrella(J,Ib))*(U_Total(Ib)+Pressure*Volume(Ib)))
            END DO
         END DO
      END DO

C     Update lambda averages every cycle instead of every step to prevent precision loss
      DO Ifrac=1,N_Frac
         Tf = Type_Frac(Ifrac)

         IF(Tf.EQ.1) THEN
            Ib = Box_Frac(Ifrac)
            Rs = 1
            DO Ibin=1,N_LambdaBin(Ifrac)
               LambdaCounter(Ibin,Ib,Rs,Ifrac)     = LambdaCounter(Ibin,Ib,Rs,Ifrac)     + LambdaCounter_T(Ibin,Ib,Rs,Ifrac)

               AvR_EnthalpyvsLambda(Ibin,Ifrac)      = AvR_EnthalpyvsLambda(Ibin,Ifrac)      + AvT_EnthalpyvsLambda(Ibin,Ifrac)
               AvR_HoverVvsLambda(Ibin,Ifrac)        = AvR_HoverVvsLambda(Ibin,Ifrac)        + AvT_HoverVvsLambda(Ibin,Ifrac)
               AvR_1overVvsLambda(Ibin,Ifrac)        = AvR_1overVvsLambda(Ibin,Ifrac)        + AvT_1overVvsLambda(Ibin,Ifrac)
               AvR_U_TotalvsLambda(Ibin,Ifrac)       = AvR_U_TotalvsLambda(Ibin,Ifrac)       + AvT_U_TotalvsLambda(Ibin,Ifrac)
               AvR_VolumevsLambda(Ibin,Ifrac)        = AvR_VolumevsLambda(Ibin,Ifrac)        + AvT_VolumevsLambda(Ibin,Ifrac)
               AvR_Nmolecinvpfpb(Ibin,Ifrac)         = AvR_Nmolecinvpfpb(Ibin,Ifrac)         + AvT_Nmolecinvpfpb(Ibin,Ifrac)
               AvR_HsquaredvsLambda(Ibin,Ifrac)      = AvR_HsquaredvsLambda(Ibin,Ifrac)      + AvT_HsquaredvsLambda(Ibin,Ifrac)
               AvR_HsquaredoverVvsLambda(Ibin,Ifrac) = AvR_HsquaredoverVvsLambda(Ibin,Ifrac) + AvT_HsquaredoverVvsLambda(Ibin,Ifrac)

            END DO

            DO J=1,N_UmbrellaBinPressure(Ib)
               Lambda0Counter_UmbrellaPressure(J,Ifrac) = Lambda0Counter_UmbrellaPressure(J,Ifrac)
     &                                                  + Lambda0Counter_UmbrellaPressure_T(J,Ifrac)
               Lambda1Counter_UmbrellaPressure(J,Ifrac) = Lambda1Counter_UmbrellaPressure(J,Ifrac)
     &                                                  + Lambda1Counter_UmbrellaPressure_T(J,Ifrac)
            END DO
            DO J=1,N_UmbrellaBinBeta(Ib)
               Lambda0Counter_UmbrellaBeta(J,Ifrac)     = Lambda0Counter_UmbrellaBeta(J,Ifrac)
     &                                                  + Lambda0Counter_UmbrellaBeta_T(J,Ifrac)
               Lambda1Counter_UmbrellaBeta(J,Ifrac)     = Lambda1Counter_UmbrellaBeta(J,Ifrac)
     &                                                  + Lambda1Counter_UmbrellaBeta_T(J,Ifrac)
            END DO

         ELSEIF(Tf.EQ.2) THEN
            Rs = 1
            DO Ib=1,N_Box
               DO Ibin=1,N_LambdaBin(Ifrac)
                  LambdaCounter(Ibin,Ib,Rs,Ifrac) = LambdaCounter(Ibin,Ib,Rs,Ifrac) + LambdaCounter_T(Ibin,Ib,Rs,Ifrac)
               END DO
            END DO
         ELSEIF(Tf.EQ.3) THEN
            Ib = Box_Frac(Ifrac)
            Ireac = Reaction_Frac(Ifrac)
            DO Rs=1,N_ReactionStep(Ireac)
               DO Ibin=1,N_LambdaBin(Ifrac)
                  LambdaCounter(Ibin,Ib,Rs,Ifrac) = LambdaCounter(Ibin,Ib,Rs,Ifrac) + LambdaCounter_T(Ibin,Ib,Rs,Ifrac)
               END DO
            END DO
         ELSEIF(Tf.EQ.4) THEN
            Ib = Box_Frac(Ifrac)
            Rs = 1
            DO Ibin=1,N_LambdaBin(Ifrac)
               LambdaCounter(Ibin,Ib,Rs,Ifrac) = LambdaCounter(Ibin,Ib,Rs,Ifrac) + LambdaCounter_T(Ibin,Ib,Rs,Ifrac)
            END DO
         END IF

#ifdef variance
         IF(Tf.EQ.1) THEN
            LambdaOld = Lambda_Frac(Ifrac)

            IF(Lambdaold-Delta.GT.0.0d0.AND.Lambdaold+Delta.LT.1.0d0) THEN

               Lambda_Frac(Ifrac) = LambdaOld + Delta
               Imol = I_MolInFrac(Ifrac,1)
               CALL Energy_molecule(Imol,E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_EL_Excl,
     &                                   E_Bending,E_Torsion,L_Overlap_Inter,L_Overlap_Intra)
               IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
                  WRITE(6,'(A,A)') ERROR, "Energy Overlap (Variance)"
                  STOP
               END IF

               E_LJ_InterRight = E_LJ_Inter
               E_LJ_IntraRight = E_LJ_Intra
               E_EL_RealRight  = E_EL_Real
               E_EL_IntraRight = E_EL_Intra
               E_EL_ExclRight  = E_EL_Excl
               E_BendingRight  = E_Bending
               E_TorsionRight  = E_Torsion

               Lambda_Frac(Ifrac) = LambdaOld - Delta
               CALL Energy_molecule(Imol,E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_EL_Excl,
     &                                   E_Bending,E_Torsion,L_Overlap_Inter,L_Overlap_Intra)
               IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
                  WRITE(6,'(A,A)') ERROR, "Energy Overlap (Variance)"
                  STOP
               END IF     
               E_LJ_InterLeft = E_LJ_Inter
               E_LJ_IntraLeft = E_LJ_Intra
               E_EL_RealLeft  = E_EL_Real
               E_EL_IntraLeft = E_EL_Intra
               E_EL_ExclLeft  = E_EL_Excl
               E_BendingLeft  = E_Bending
               E_TorsionLeft  = E_Torsion

               dE_LJ_Inter_dLambda = (E_LJ_InterRight - E_LJ_InterLeft)/(2.0d0*Delta)
               dE_LJ_Intra_dLambda = (E_LJ_IntraRight - E_LJ_IntraLeft)/(2.0d0*Delta)
               dE_EL_Real_dLambda  = (E_EL_RealRight  - E_EL_RealLeft) /(2.0d0*Delta)
               dE_EL_Intra_dLambda = (E_EL_IntraRight - E_EL_IntraLeft)/(2.0d0*Delta)
               dE_EL_Excl_dLambda  = (E_EL_ExclRight  - E_EL_ExclLeft) /(2.0d0*Delta)
               dE_Bending_dLambda  = (E_BendingRight  - E_BendingLeft) /(2.0d0*Delta)
               dE_Torsion_dLambda  = (E_TorsionRight  - E_TorsionLeft) /(2.0d0*Delta)
               dE_Total_dlambda    = dE_LJ_Inter_dLambda + dE_LJ_Intra_dLambda + dE_EL_Real_dLambda
     &         + dE_EL_Intra_dLambda + dE_EL_Excl_dLambda + dE_Bending_dLambda + dE_Torsion_dLambda

               Ibin = 1 + Int(Dble(N_LambdaBin(Ifrac))*Lambda_Frac(Ifrac))

               LambdaCounterVariance(Ibin,Ifrac) = LambdaCounterVariance(Ibin,Ifrac) + 1.0d0

               Ib = Ibox(Box_Frac(Ifrac))

               AvR_dE_LJ_Inter_dLambda(Ibin,Ifrac) = AvR_dE_LJ_Inter_dLambda(Ibin,Ifrac) + dE_LJ_Inter_dLambda
               AvR_dE_LJ_Intra_dLambda(Ibin,Ifrac) = AvR_dE_LJ_Intra_dLambda(Ibin,Ifrac) + dE_LJ_Intra_dLambda
               AvR_dE_EL_Real_dLambda(Ibin,Ifrac)  = AvR_dE_EL_Real_dLambda(Ibin,Ifrac)  + dE_EL_Real_dLambda
               AvR_dE_EL_Intra_dLambda(Ibin,Ifrac) = AvR_dE_EL_Intra_dLambda(Ibin,Ifrac) + dE_EL_Intra_dLambda
               AvR_dE_EL_Excl_dLambda(Ibin,Ifrac)  = AvR_dE_EL_Excl_dLambda(Ibin,Ifrac)  + dE_EL_Excl_dLambda
               AvR_dE_Bending_dLambda(Ibin,Ifrac)  = AvR_dE_Bending_dLambda(Ibin,Ifrac)  + dE_Bending_dLambda
               AvR_dE_Torsion_dLambda(Ibin,Ifrac)  = AvR_dE_Torsion_dLambda(Ibin,Ifrac)  + dE_Torsion_dLambda
               AvR_dE_Total_dLambda(Ibin,Ifrac)    = AvR_dE_Total_dLambda(Ibin,Ifrac)    + dE_Total_dLambda

               AvR_dE_LJ_Inter_dLambda_Squared(Ibin,Ifrac) = AvR_dE_LJ_Inter_dLambda_Squared(Ibin,Ifrac)
     &                                                     + dE_LJ_Inter_dLambda*dE_LJ_Inter_dLambda
               AvR_dE_LJ_Intra_dLambda_Squared(Ibin,Ifrac) = AvR_dE_LJ_Intra_dLambda_Squared(Ibin,Ifrac)
     &                                                     + dE_LJ_Intra_dLambda*dE_LJ_Intra_dLambda
               AvR_dE_EL_Real_dLambda_Squared(Ibin,Ifrac)  = AvR_dE_EL_Real_dLambda_Squared(Ibin,Ifrac)
     &                                                     + dE_EL_Real_dLambda*dE_EL_Real_dLambda
               AvR_dE_EL_Intra_dLambda_Squared(Ibin,Ifrac) = AvR_dE_EL_Intra_dLambda_Squared(Ibin,Ifrac)
     &                                                     + dE_EL_Intra_dLambda*dE_EL_Intra_dLambda
               AvR_dE_EL_Excl_dLambda_Squared(Ibin,Ifrac)  = AvR_dE_EL_Excl_dLambda_Squared(Ibin,Ifrac)
     &                                                     + dE_EL_Excl_dLambda*dE_EL_Excl_dLambda
               AvR_dE_Bending_dLambda_Squared(Ibin,Ifrac)  = AvR_dE_Bending_dLambda_Squared(Ibin,Ifrac)
     &                                                     + dE_Bending_dLambda*dE_Bending_dLambda
               AvR_dE_Torsion_dLambda_Squared(Ibin,Ifrac)  = AvR_dE_Torsion_dLambda_Squared(Ibin,Ifrac)
     &                                                     + dE_Torsion_dLambda*dE_Torsion_dLambda
               AvR_dE_Total_dLambda_Squared(Ibin,Ifrac)    = AvR_dE_Total_dLambda_Squared(Ibin,Ifrac)
     &                                                     + dE_Total_dLambda*dE_Total_dLambda

               Lambda_Frac(Ifrac) = LambdaOld
            END IF
         END IF
#endif

C     Keep track of average dU/dlambda of the bins
      IF(L_dUdl) THEN
         Ibin = 1 + int((dble(N_LambdaBin(1)))*Lambda_Frac(1))
         dUdlCounter(Ibin) = dUdlCounter(Ibin) + 1.0d0
         AvR_dU_dl(Ibin) = AvR_dU_dl(Ibin) + dU_dlTotal
         sq_AvR_dU_dl(Ibin) = sq_AvR_dU_dl(Ibin) + (dU_dlTotal**2.0d0)
      END IF

      END DO

      RETURN
      END



      SUBROUTINE Update_Temporary_Averages
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "output.inc"
      include "settings.inc"

      integer Ib,Ibin,Tm,Rs,Ifrac,Tf,J,Nwhole(2)

      double precision Lf

      DO Ib=1,N_Box
         Nwhole(Ib) = 0
         DO Tm=1,N_MolType
            Nwhole(Ib) = Nwhole(Ib) + Nmptpb(Ib,Tm)
         END DO
      END DO

      DO Ifrac=1,N_Frac

         Tf = Type_Frac(Ifrac)

         IF(Tf.EQ.3) THEN
            Rs = ReactionStep_Frac(Ifrac)
         ELSE
            Rs = 1
         END IF

         Lf = Lambda_Frac(Ifrac)
         Ib = Box_Frac(Ifrac)

         Ibin = 1 + int(dble(N_LambdaBin(Ifrac))*Lf)

         LambdaCounter_T(Ibin,Ib,Rs,Ifrac) = LambdaCounter_T(Ibin,Ib,Rs,Ifrac)  + 1.0d0

         IF(Tf.EQ.1) THEN
            AvT_EnthalpyvsLambda(Ibin,Ifrac) = AvT_EnthalpyvsLambda(Ibin,Ifrac) + (U_Total(Ib)+Pressure*Volume(Ib))
            AvT_HoverVvsLambda(Ibin,Ifrac)   = AvT_HoverVvsLambda(Ibin,Ifrac)   + U_Total(Ib)/Volume(Ib)+Pressure
            AvT_1overVvsLambda(Ibin,Ifrac)   = AvT_1overVvsLambda(Ibin,Ifrac)   + 1.0d0/Volume(Ib)
            AvT_U_TotalvsLambda(Ibin,Ifrac)  = AvT_U_TotalvsLambda(Ibin,Ifrac)  + U_Total(Ib)
            AvT_VolumevsLambda(Ibin,Ifrac)   = AvT_VolumevsLambda(Ibin,Ifrac)   + Volume(Ib)
            AvT_Nmolecinvpfpb(Ibin,Ifrac)    = AvT_Nmolecinvpfpb(Ibin,Ifrac)    + 1.0d0/dble(Nwhole(Ib)+N_MolInFrac(Ifrac))

            AvT_HsquaredvsLambda(Ibin,Ifrac) = AvT_HsquaredvsLambda(Ibin,Ifrac)
     &      + (U_Total(Ib)+Pressure*Volume(Ib))*(U_Total(Ib)+Pressure*Volume(Ib))
            AvT_HsquaredoverVvsLambda(Ibin,Ifrac) = AvT_HsquaredoverVvsLambda(Ibin,Ifrac)
     &      + (U_Total(Ib)+Pressure*Volume(Ib))*(U_Total(Ib)+Pressure*Volume(Ib))/Volume(Ib)


            IF(Ibin.EQ.1) THEN
               DO J=1,N_UmbrellaBinPressure(Ib)
                  Lambda0Counter_UmbrellaPressure_T(J,Ifrac) = Lambda0Counter_UmbrellaPressure_T(J,Ifrac)
     &                                                       + dexp(beta*Volume(Ib)*(Pressure-P_Umbrella(J,Ib)))
               END DO
               DO J=1,N_UmbrellaBinBeta(Ib)
                  Lambda0Counter_UmbrellaBeta_T(J,Ifrac) = Lambda0Counter_UmbrellaBeta_T(J,Ifrac)
     &                                                   + dexp((Beta-Beta_Umbrella(J,Ib))*(U_Total(Ib)+Pressure*Volume(Ib)))
               END DO
            ELSEIF(Ibin.EQ.N_LambdaBin(Ifrac)) THEN
               DO J=1,N_UmbrellaBinPressure(Ib)
                  Lambda1Counter_UmbrellaPressure_T(J,Ifrac) = Lambda1Counter_UmbrellaPressure_T(J,Ifrac)
     &                                                       + dexp(beta*Volume(Ib)*(Pressure-P_Umbrella(J,Ib)))
               END DO
               DO J=1,N_UmbrellaBinBeta(Ib)
                  Lambda1Counter_UmbrellaBeta_T(J,Ifrac) = Lambda1Counter_UmbrellaBeta_T(J,Ifrac)
     &                                                   + dexp((Beta-Beta_Umbrella(J,Ib))*(U_Total(Ib)+Pressure*Volume(Ib)))
               END DO
            END IF

         END IF

      END DO

      RETURN
      END
