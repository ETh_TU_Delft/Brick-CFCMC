      SUBROUTINE Chemicalpotential_Insertion(Ichoice)
      implicit none

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC   Test particle insertion method by Widom for chemical potential   CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      include "global_variables.inc"
      include "output.inc" 

      integer Ninsertions
      Parameter (Ninsertions = 10)

      integer Ichoice,I,J,Ib,Tm,Imol
      double precision E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_Bending,E_Torsion,E_EL_Excl,dU
      double precision dX,dY,dZ,Ran_Uniform
      double precision U_test(2,MaxMolType),Counter
      double precision dU_dl
      logical L_Overlap_Inter,L_Overlap_Intra

      save U_test,Counter           

CCC   INITIALIZE
      IF(Ichoice.EQ.0) THEN

         DO Ib=1,N_Box
            DO Tm=1,N_MolType
               U_test(Ib,Tm) = 0.0d0
            END DO
         END DO

         Counter = 0.0d0



CCC   SAMPLE
      ELSEIF(Ichoice.EQ.1) THEN

         DO J=1,Ninsertions
            DO Ib=1,N_Box
               DO Tm=1,N_MolType

                  Imol = N_MolInBox(Ib) + 1

                  Ibox(Imol) = Ib
                  TypeMol(Imol) = Tm
                  L_Frac(Imol) = .false.

                  XCM(Imol) = 0.0d0
                  YCM(Imol) = 0.0d0
                  ZCM(Imol) = 0.0d0

                  dX = Ran_Uniform()*BoxSize(Ib)
                  dY = Ran_Uniform()*BoxSize(Ib)
                  dZ = Ran_Uniform()*BoxSize(Ib)

                  DO I=1,N_AtomInMolType(Tm)
                     X(Imol,I) = Xin(Tm,I) + dX
                     Y(Imol,I) = Yin(Tm,I) + dY
                     Z(Imol,I) = Zin(Tm,I) + dZ

                     XCM(Imol) = XCM(Imol) + X(Imol,I)
                     YCM(Imol) = YCM(Imol) + Y(Imol,I)
                     ZCM(Imol) = ZCM(Imol) + Z(Imol,I)
                  END DO

                  XCM(Imol) = XCM(Imol)/dble(N_AtomInMolType(Tm))
                  YCM(Imol) = YCM(Imol)/dble(N_AtomInMolType(Tm))
                  ZCM(Imol) = ZCM(Imol)/dble(N_AtomInMolType(Tm))

                  CALL Random_Orientation(Imol)

                  CALL Place_molecule_back_in_box(Imol)

                  CALL Energy_Molecule(Imol,E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_EL_Excl,
     &                                      E_Bending,E_Torsion,L_Overlap_Inter,L_Overlap_Intra,dU_dl)

                  IF(.NOT.(L_Overlap_Inter.OR.L_Overlap_Intra)) THEN
                     dU=E_LJ_Inter+E_LJ_Intra+E_EL_Real+E_EL_Intra+E_EL_Excl+E_Bending+E_Torsion
                     U_test(Ib,Tm)=U_test(Ib,Tm)+dexp(-beta*dU)
                  END IF

               END DO
            END DO
         END DO

         Counter = Counter + dble(Ninsertions)



CCC   WRITE
      ELSEIF(Ichoice.EQ.2) THEN

         WRITE(6,'(A)') "Chemical potential"

         DO Tm=1,N_MolType
            WRITE(6,'(1x,A19,18x,2e20.10e3)') C_MolType(Tm), (-dlog(U_test(Ib,Tm)/Counter)/beta, Ib=1,N_Box)
         END DO
         WRITE(6,*)

      END IF

      RETURN
      END
