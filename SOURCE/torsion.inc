C     Variables for torsion
C
C     A0-A5         = Torsion parameters per torsion type
C     dphi          = Delta phi (rotation angle) per torsion type
C     Natomtorslist = Number of atoms in Iatomtorslist
C     Iatomtorslist = Labels of atoms that should be rotated for each torsion and molecule type

      integer   Natomtorslist(MaxMolType,MaxTorsionInMolType,2),
     &          Iatomtorslist(MaxMolType,MaxTorsionInMolType,2,MaxAtom)

      double precision A0(MaxTorsionType), A1(MaxTorsionType), A2(MaxTorsionType),
     &           A3(MaxTorsionType), A4(MaxTorsionType), A5(MaxTorsionType),
     &           dphi(MaxTorsionType)

      Common /TorsionInteger/ Natomtorslist,Iatomtorslist

      Common /TorsionDouble/ A0,A1,A2,A3,A4,A5,dphi
