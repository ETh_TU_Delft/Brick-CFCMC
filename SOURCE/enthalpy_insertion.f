      SUBROUTINE Enthalpy_insertion(Ichoice)
      implicit none

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC   Test particle insertion method by Ciccotti and Frenkel   CCC
CCC   for partial molar enthalpy and partial molar volume      CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      include "global_variables.inc"
      include "output.inc"   

      integer Ninsertions
      Parameter (Ninsertions = 10)

      integer Ichoice,I,J,Ib,Tm,Imol
      double precision E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_Bending,E_Torsion,E_EL_Excl,dU,U

      double precision dX,dY,dZ,Ran_Uniform

      double precision H_test_1(2,MaxMolType),H_test_2(2,MaxMolType),H_test_3(2,MaxMolType),
     &                 V_test_1(2,MaxMolType),V_test_3(2,MaxMolType),Counter,
     &                 H2_test_1(2,MaxMolType),H2_test_3(2,MaxMolType)
      double precision dU_dl
      logical L_Overlap_Inter,L_Overlap_Intra

      save H_test_1,H_test_2,H_test_3,V_test_1,V_test_3,H2_test_1,H2_test_3,Counter     

CCC   INITIALIZE
      IF(Ichoice.EQ.0) THEN

         DO Ib=1,N_Box
            DO Tm=1,N_MolType
               H_test_1(Ib,Tm) = 0.0d0
               H_test_2(Ib,Tm) = 0.0d0
               H_test_3(Ib,Tm) = 0.0d0

               V_test_1(Ib,Tm) = 0.0d0
               V_test_3(Ib,Tm) = 0.0d0

               H2_test_1(Ib,Tm) = 0.0d0
               H2_test_3(Ib,Tm) = 0.0d0
            END DO
         END DO

         Counter = 0.0d0



CCC   SAMPLE
      ELSEIF(Ichoice.EQ.1) THEN

         DO J=1,Ninsertions
            DO Ib=1,N_Box
               DO Tm=1,N_MolType

                  Imol = N_MolInBox(Ib) + 1

                  Ibox(Imol) = Ib
                  TypeMol(Imol) = Tm
                  L_Frac(Imol) = .false.

                  XCM(Imol) = 0.0d0
                  YCM(Imol) = 0.0d0
                  ZCM(Imol) = 0.0d0

                  dX = Ran_Uniform()*BoxSize(Ib)
                  dY = Ran_Uniform()*BoxSize(Ib)
                  dZ = Ran_Uniform()*BoxSize(Ib)

                  DO I=1,N_AtomInMolType(Tm)
                     X(Imol,I) = Xin(Tm,I) + dX
                     Y(Imol,I) = Yin(Tm,I) + dY
                     Z(Imol,I) = Zin(Tm,I) + dZ

                     XCM(Imol) = XCM(Imol) + X(Imol,I)
                     YCM(Imol) = YCM(Imol) + Y(Imol,I)
                     ZCM(Imol) = ZCM(Imol) + Z(Imol,I)
                  END DO

                  XCM(Imol) = XCM(Imol)/dble(N_AtomInMolType(Tm))
                  YCM(Imol) = YCM(Imol)/dble(N_AtomInMolType(Tm))
                  ZCM(Imol) = ZCM(Imol)/dble(N_AtomInMolType(Tm))

                  CALL Random_Orientation(Imol)

                  CALL Place_molecule_back_in_box(Imol)

                  CALL Energy_Molecule(Imol,E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_EL_Excl,
     &                                      E_Bending,E_Torsion,L_Overlap_Inter,L_Overlap_Intra,dU_dl)

                  U = U_LJ_Inter(Ib) + U_LJ_Tail(Ib)
     &                 + U_EL_Real(Ib)  + U_EL_Excl(Ib) + U_EL_Self(Ib)  + U_EL_Four(Ib)
     &                 + U_LJ_Intra(Ib) + U_EL_Intra(Ib) + U_Bending_Total(Ib) + U_Torsion_Total(Ib)
                  
                  IF(.NOT.(L_Overlap_Inter.OR.L_Overlap_Intra)) THEN
                     
                     dU=E_LJ_Inter+E_LJ_Intra+E_EL_Real+E_EL_Intra+E_EL_Excl+E_Bending+E_Torsion
                     
                     H_test_1(Ib,Tm) = H_test_1(Ib,Tm)
     &                + (U + dU + Pressure*Volume(Ib))*Volume(Ib)*dexp(-beta*dU)
                     H_test_2(Ib,Tm) = H_test_2(Ib,Tm) + Volume(Ib)*dexp(-beta*dU)

                     V_test_1(Ib,Tm) = V_test_1(Ib,Tm) + Volume(Ib)*Volume(Ib)*dexp(-beta*dU)

                     H2_test_1(Ib,Tm) = H2_test_1(Ib,Tm) + (U + dU + Pressure*Volume(Ib))*
     &                (U + dU + Pressure*Volume(Ib))*Volume(Ib)*dexp(-beta*dU)

                  END IF
                  H_test_3(Ib,Tm) = H_test_3(Ib,Tm) + (U + Pressure*Volume(Ib))
                  V_test_3(Ib,Tm) = V_test_3(Ib,Tm) + Volume(Ib)
                  H2_test_3(Ib,Tm) = H2_test_3(Ib,Tm)
     &              + (U + Pressure*Volume(Ib))*(U + Pressure*Volume(Ib))

               END DO
            END DO
         END DO

         Counter = Counter + dble(Ninsertions)



CCC   WRITE
      ELSEIF(Ichoice.EQ.2) THEN

         WRITE(6,'(A)') "Enthalpy"

         DO Tm=1,N_MolType
            WRITE(6,'(1x,A19,18x,2e20.10e3)') C_MolType(Tm),
     &   (-1.0d0/beta + H_test_1(Ib,Tm)/H_test_2(Ib,Tm) - H_test_3(Ib,Tm)/Counter, Ib=1,N_Box)
         END DO

         WRITE(6,*)
         WRITE(6,'(A)') "Partial volume"

         DO Tm=1,N_MolType
            WRITE(6,'(1x,A19,18x,2e20.10e3)') C_MolType(Tm),
     &   (V_test_1(Ib,Tm)/H_test_2(Ib,Tm) - V_test_3(Ib,Tm)/Counter, Ib=1,N_Box)
         END DO

         WRITE(6,*)
         WRITE(6,'(A)') "Heat Capacity"

         DO Tm=1,N_MolType
            WRITE(6,'(1x,A19,18x,2e20.10e3)') C_MolType(Tm),
     &   (1.0d0/(beta*beta) + (H_test_1(Ib,Tm)/H_test_2(Ib,Tm))*(H_test_1(Ib,Tm)/H_test_2(Ib,Tm))
     &    - H2_test_1(Ib,Tm)/H_test_2(Ib,Tm) - (H_test_3(Ib,Tm)/Counter)*(H_test_3(Ib,Tm)/Counter)
     &    + H2_test_3(Ib,Tm)/Counter , Ib=1,N_Box)
         END DO

      END IF

      RETURN
      END
