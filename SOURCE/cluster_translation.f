      SUBROUTINE Cluster_Translation
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "energy.inc"
      include "ewald.inc"
      include "output.inc"

C     Translate a cluster of molecules (in vapor phase)

      integer Imol,Jmol,Iatom,Ib,Tm,I,J,It,Select_Random_Integer,Icluster,Nclusterold,Nclusternew,
     &   Nmolecinclusterold(MaxCluster),Imolecinclusterold(MaxCluster,MaxMolInCluster),
     &   Nmolecinclusternew(MaxCluster),Imolecinclusternew(MaxCluster,MaxMolInCluster)
      double precision Ran_Uniform,dummy1,dummy2,dummy3,dummy4,dummy5,dE,Myl,Myc,Myi,Rm,
     &   Xold(MaxMolInCluster,MaxAtom),Yold(MaxMolInCluster,MaxAtom),
     &   Zold(MaxMolInCluster,MaxAtom),XCMold(MaxMolInCluster),Delta_ClusterTranslation,
     &   YCMold(MaxMolInCluster),ZCMold(MaxMolInCluster),dR,dE_EL_Four,
     &   E_LJ_Inter,E_EL_Real,E_LJ_InterNew,E_EL_RealNew,E_LJ_InterOld,E_EL_RealOld,
     &   dlLJ_dlf,dlEL_dlf,dU_dl,ddUELFour_dl
      logical L_Overlap_Inter,L_Overlap_Intra,Laccept,LEwald

      Ib = Ibvapor

C     Find clusters and select one of them at random
      CALL Find_Clusters(Nclusterold,Nmolecinclusterold,Imolecinclusterold)

C     If there are no clusters, or there is only one cluster (the whole box) than there is nothing to do
      IF(Nclusterold.EQ.0) RETURN
      IF(Nclusterold.EQ.1) RETURN

      Icluster = Select_Random_Integer(Nclusterold)

      LEwald = .false.
      DO I=1,Nmolecinclusterold(Icluster)
         Imol = Imolecinclusterold(Icluster,I)
         IF(L_ChargeInMolType(TypeMol(Imol)).AND.L_Ewald(Ib)) THEN
            LEwald = .true.
            EXIT
         END IF
      END DO

      IF(LEwald) CALL Ewald_Init

      E_LJ_InterOld = 0.0d0
      E_EL_RealOld  = 0.0d0

      Delta_ClusterTranslation = 0.0d0

      IF(LEwald) THEN
         DO I=1,Nmolecinclusterold(Icluster)
            Imol = Imolecinclusterold(Icluster,I)
            Tm   = TypeMol(Imol)
            IF(.NOT.L_ChargeInMolType(Tm)) CYCLE

            IF(L_frac(Imol)) CALL interactionlambda(Imol,Myl,Myc,Myi,dlLJ_dlf,dlEL_dlf)

            DO Iatom=1,N_AtomInMolType(Tm)
               It=TypeAtom(Tm,Iatom)
               IF(L_Charge(It)) THEN
                  NKSPACE(Ib,1) = NKSPACE(Ib,1) + 1
                  J = NKSPACE(Ib,1)
                  XKSPACE(J,Ib,1) = X(Imol,Iatom)
                  YKSPACE(J,Ib,1) = Y(Imol,Iatom)
                  ZKSPACE(J,Ib,1) = Z(Imol,Iatom)
                  IF(L_frac(Imol)) THEN
                     QKSPACE(J,Ib,1) = Myc*Q(It)
                  ELSE
                     QKSPACE(J,Ib,1) = Q(It)
                  END IF
               END IF
            END DO
         END DO
      END IF

C     Store old configuration and calculate energy
      DO I=1,Nmolecinclusterold(Icluster)
         Imol = Imolecinclusterold(Icluster,I)
         Tm   = TypeMol(Imol)

         DO Iatom=1,N_AtomInMolType(Tm)
            Xold(I,Iatom) = X(Imol,Iatom)
            Yold(I,Iatom) = Y(Imol,Iatom)
            Zold(I,Iatom) = Z(Imol,Iatom)
         END DO

         XCMold(I) = XCM(Imol)
         YCMold(I) = YCM(Imol)
         ZCMold(I) = ZCM(Imol)

         CALL Energy_Molecule(Imol,E_LJ_Inter,dummy1,E_EL_Real,dummy2,dummy3,dummy4,dummy5,L_Overlap_Inter,L_Overlap_Intra,dU_dl)

         IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
            WRITE(6,'(A,A)') ERROR, "Energy Overlap (Cluster Translation)"
            STOP
         END IF
         E_LJ_InterOld = E_LJ_InterOld + E_LJ_Inter
         E_EL_RealOld  = E_EL_RealOld  + E_EL_Real

         Delta_ClusterTranslation = Delta_ClusterTranslation + Delta_Translation(Ib,Tm)
      END DO

      Delta_ClusterTranslation = Delta_ClusterTranslation/dble(Nmolecinclusterold(Icluster))

C     Correct for counting LJ and Electrostatic energy double
      DO I=1,Nmolecinclusterold(Icluster)-1
         Imol = Imolecinclusterold(Icluster,I)
         DO J=I+1,Nmolecinclusterold(Icluster)
            Jmol = Imolecinclusterold(Icluster,J)

            CALL Energy_Intermolecular(Imol,Jmol,E_LJ_Inter,E_EL_Real,L_Overlap_Inter,dU_dl)

            IF(L_Overlap_Inter) THEN
               WRITE(6,'(A,A)') ERROR, "Energy Overlap (Cluster Translation)"
               STOP
            END IF
            E_LJ_InterOld = E_LJ_InterOld - E_LJ_Inter
            E_EL_RealOld  = E_EL_RealOld  - E_EL_Real
         END DO
      END DO

C     Generate new configuration
      dR = 2.0d0*(Ran_Uniform()-0.5d0)*Delta_ClusterTranslation

      Rm = 3.0d0*Ran_Uniform()

      IF(Rm.LT.1.0d0) THEN
         DO I=1,Nmolecinclusterold(Icluster)
            Imol = Imolecinclusterold(Icluster,I)
            Tm   = TypeMol(Imol)

            DO Iatom=1,N_AtomInMolType(Tm)
               X(Imol,Iatom) = X(Imol,Iatom) + dR
            END DO

            XCM(Imol) = XCM(Imol) + dR

            CALL Place_molecule_back_in_box(Imol)
         END DO
      ELSEIF(Rm.LT.2.0d0) THEN
         DO I=1,Nmolecinclusterold(Icluster)
            Imol = Imolecinclusterold(Icluster,I)
            Tm   = TypeMol(Imol)

            DO Iatom=1,N_AtomInMolType(Tm)
               Y(Imol,Iatom) = Y(Imol,Iatom) + dR
            END DO

            YCM(Imol) = YCM(Imol) + dR

            CALL Place_molecule_back_in_box(Imol)
         END DO
      ELSE
         DO I=1,Nmolecinclusterold(Icluster)
            Imol = Imolecinclusterold(Icluster,I)
            Tm   = TypeMol(Imol)

            DO Iatom=1,N_AtomInMolType(Tm)
               Z(Imol,Iatom) = Z(Imol,Iatom) + dR
            END DO

            ZCM(Imol) = ZCM(Imol) + dR

            CALL Place_molecule_back_in_box(Imol)
         END DO
      ENDIF

C     Check if clusters changed
      CALL Find_Clusters(Nclusternew,Nmolecinclusternew,Imolecinclusternew)

C     If clusters changed than reject the move (because this would violate detailed balance)
      IF(Nclusternew.NE.Nclusterold) THEN
         Laccept=.false.
         GO TO 1
      END IF

      IF(LEwald) THEN
         DO I=1,Nmolecinclusterold(Icluster)
            Imol = Imolecinclusterold(Icluster,I)
            Tm   = TypeMol(Imol)
            IF(.NOT.L_ChargeInMolType(Tm)) CYCLE

            IF(L_frac(Imol)) CALL interactionlambda(Imol,Myl,Myc,Myi,dlLJ_dlf,dlEL_dlf)

            DO Iatom=1,N_AtomInMolType(Tm)
               It=TypeAtom(Tm,Iatom)
               IF(L_Charge(It)) THEN
                  NKSPACE(Ib,2) = NKSPACE(Ib,2) + 1
                  J = NKSPACE(Ib,2)
                  XKSPACE(J,Ib,2) = X(Imol,Iatom)
                  YKSPACE(J,Ib,2) = Y(Imol,Iatom)
                  ZKSPACE(J,Ib,2) = Z(Imol,Iatom)
                  IF(L_frac(Imol)) THEN
                     QKSPACE(J,Ib,2) = Myc*Q(It)
                  ELSE
                     QKSPACE(J,Ib,2) = Q(It)
                  END IF
               END IF
            END DO
         END DO
      END IF

C     Calculate energy of new configuration
      E_LJ_InterNew = 0.0d0
      E_EL_RealNew  = 0.0d0

      DO I=1,Nmolecinclusterold(Icluster)
         Imol = Imolecinclusterold(Icluster,I)

         CALL Energy_Molecule(Imol,E_LJ_Inter,dummy1,E_EL_Real,dummy2,dummy3,dummy4,dummy5,L_Overlap_Inter,L_Overlap_Intra,dU_dl)

         IF(L_Overlap_Intra) THEN
            WRITE(6,'(A,A)') ERROR, "Intramolecular Energy Overlap (Cluster Translation)"
            STOP
         ELSEIF(L_Overlap_Inter) THEN
            Laccept=.false.
            GO TO 1
         END IF
         E_LJ_InterNew = E_LJ_InterNew + E_LJ_Inter
         E_EL_RealNew  = E_EL_RealNew  + E_EL_Real
      END DO

C     Correct for counting LJ and Electrostatic energy double
      DO I=1,Nmolecinclusterold(Icluster)-1
         Imol = Imolecinclusterold(Icluster,I)
         DO J=I+1,Nmolecinclusterold(Icluster)
            Jmol = Imolecinclusterold(Icluster,J)

            CALL Energy_Intermolecular(Imol,Jmol,E_LJ_Inter,E_EL_Real,L_Overlap_Inter,dU_dl)

            IF(L_Overlap_Inter) THEN
               Laccept=.false.
               GO TO 1
            END IF
            E_LJ_InterNew = E_LJ_InterNew - E_LJ_Inter
            E_EL_RealNew  = E_EL_RealNew  - E_EL_Real
         END DO
      END DO

      dE_EL_Four = 0.0d0

      IF(LEwald) CALL Ewald_Move(Ib,dE_EL_Four,ddUELFour_dl)

      dE = E_LJ_InterNew + E_EL_RealNew - E_LJ_InterOld - E_EL_RealOld + dE_EL_Four

      CALL Accept_or_Reject(dexp(-beta*dE),Laccept)

   1  CONTINUE

      TrialClusterTranslation(Ib) = TrialClusterTranslation(Ib) + 1.0d0
      TrialClusterTranslationvsClusterSize(Ib,Nmolecinclusterold(Icluster)) =
     &   TrialClusterTranslationvsClusterSize(Ib,Nmolecinclusterold(Icluster)) + 1.0d0

      IF(Laccept) THEN

         AcceptClusterTranslation(Ib) = AcceptClusterTranslation(Ib) + 1.0d0
         AcceptClusterTranslationvsClusterSize(Ib,Nmolecinclusterold(Icluster)) =
     &      AcceptClusterTranslationvsClusterSize(Ib,Nmolecinclusterold(Icluster)) + 1.0d0

         U_LJ_Inter(Ib) = U_LJ_Inter(Ib) + E_LJ_InterNew - E_LJ_InterOld
         U_EL_Real(Ib)  = U_EL_Real(Ib)  + E_EL_RealNew  - E_EL_RealOld
         U_EL_Four(Ib)  = U_EL_Four(Ib)  + dE_EL_Four

         U_Total(Ib) = U_Total(Ib) + dE

         IF(LEwald) CALL Ewald_Accept(Ib)

      ELSE

         DO I=1,Nmolecinclusterold(Icluster)
            Imol = Imolecinclusterold(Icluster,I)
            Tm   = TypeMol(Imol)

            DO Iatom=1,N_AtomInMolType(Tm)
               X(Imol,Iatom) = Xold(I,Iatom)
               Y(Imol,Iatom) = Yold(I,Iatom)
               Z(Imol,Iatom) = Zold(I,Iatom)
            END DO

            XCM(Imol) = XCMold(I)
            YCM(Imol) = YCMold(I)
            ZCM(Imol) = ZCMold(I)
         END DO

      END IF

      RETURN
      END
