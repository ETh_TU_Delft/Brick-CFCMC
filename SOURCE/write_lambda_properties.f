      SUBROUTINE Write_Lambda_Properties(Ichoice)
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "settings.inc"

      integer Ifrac,Ibin,Ib,Ireac,Rs,Tf,Ichoice,unit_base(MaxFrac)
      double precision NormBias(2,MaxReactionStep,MaxFrac),NormObs(2,MaxReactionStep,MaxFrac)
      character*100 plambdafile,HvsLfile,VvsLfile,HoverVfile,InvVfile,UvsLfile,
     &              InvNfile,HsqvLfile,HsqoVfile,Varfile

      save  unit_base
      
C     Start modification darshan 22/6/23
      
      do ib=1,2
         do Ireac = 1,MaxReactionStep
            do ifrac=1,MaxFrac
               Normbias(ib,Ireac,ifrac) = 0.0d0
               NormObs(ib,Ireac,ifrac) = 0.0d0
            enddo
         enddo
      enddo
      
C     End modification darshan 22/6/23      
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                Open and initialize files                 C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      IF(Ichoice.EQ.0) THEN

         CALL system('mkdir OUTPUT/CFC')

         DO Ifrac=1,N_Frac
            unit_base(Ifrac) = 300 + 10*Ifrac
            IF(Ifrac.LE.9) THEN
               WRITE(plambdafile,'(A,i1,A)') "./OUTPUT/CFC/plambda-", Ifrac, ".dat"
               IF(Type_Frac(Ifrac).EQ.1) THEN
                  WRITE(HvsLfile,   '(A,i1,A)') "./OUTPUT/CFC/H_vs_Lambda-", Ifrac, ".dat"
                  WRITE(VvsLfile,   '(A,i1,A)') "./OUTPUT/CFC/V_vs_Lambda-", Ifrac, ".dat"
                  WRITE(HoverVfile, '(A,i1,A)') "./OUTPUT/CFC/HoverV_vs_Lambda-", Ifrac, ".dat"
                  WRITE(InvVfile,   '(A,i1,A)') "./OUTPUT/CFC/1overV_vs_Lambda-", Ifrac, ".dat"
                  WRITE(UvsLfile,   '(A,i1,A)') "./OUTPUT/CFC/U_Total_vs_Lambda-", Ifrac, ".dat"
                  WRITE(InvNfile,   '(A,i1,A)') "./OUTPUT/CFC/1overN_vs_Lambda-", Ifrac, ".dat"
                  WRITE(HsqvLfile,  '(A,i1,A)') "./OUTPUT/CFC/Hsquared_vs_Lambda-", Ifrac, ".dat"
                  WRITE(HsqoVfile,  '(A,i1,A)') "./OUTPUT/CFC/HsquaredoverV_vs_Lambda-", Ifrac, ".dat"
                  WRITE(Varfile,    '(A,i1,A)') "./OUTPUT/CFC/Variance_dUdLambda_vs_Lambda-", Ifrac, ".dat"
               END IF
            ELSE
               WRITE(plambdafile,'(A,i2,A)') "./OUTPUT/CFC/plambda-", Ifrac, ".dat"
               IF(Type_Frac(Ifrac).EQ.1) THEN
                  WRITE(HvsLfile,   '(A,i2,A)') "./OUTPUT/CFC/H_vs_Lambda-", Ifrac, ".dat"
                  WRITE(VvsLfile,   '(A,i2,A)') "./OUTPUT/CFC/V_vs_Lambda-", Ifrac, ".dat"
                  WRITE(HoverVfile, '(A,i2,A)') "./OUTPUT/CFC/HoverV_vs_Lambda-", Ifrac, ".dat"
                  WRITE(InvVfile,   '(A,i2,A)') "./OUTPUT/CFC/1overV_vs_Lambda-", Ifrac, ".dat"
                  WRITE(UvsLfile,   '(A,i2,A)') "./OUTPUT/CFC/U_Total_vs_Lambda-", Ifrac, ".dat"
                  WRITE(InvNfile,   '(A,i2,A)') "./OUTPUT/CFC/1overN_vs_Lambda-", Ifrac, ".dat"
                  WRITE(HsqvLfile,  '(A,i2,A)') "./OUTPUT/CFC/Hsquared_vs_Lambda-", Ifrac, ".dat"
                  WRITE(HsqoVfile,  '(A,i2,A)') "./OUTPUT/CFC/HsquaredoverV_vs_Lambda-", Ifrac, ".dat"
                  WRITE(Varfile,    '(A,i2,A)') "./OUTPUT/CFC/Variance_dUdLambda_vs_Lambda-", Ifrac, ".dat"
               END IF
            END IF

            OPEN(unit=unit_base(Ifrac)+0,file=plambdafile)
            WRITE(unit_base(Ifrac)+0,'(A)') "# Lambda, (Probability, Probability_biased(/observed)) per Fractional"

            IF(Type_Frac(Ifrac).EQ.1) THEN
               OPEN(unit=unit_base(Ifrac)+1,file=HvsLfile)
               OPEN(unit=unit_base(Ifrac)+2,file=VvsLfile)
               OPEN(unit=unit_base(Ifrac)+3,file=HoverVfile)
               OPEN(unit=unit_base(Ifrac)+4,file=InvVfile)
               OPEN(unit=unit_base(Ifrac)+5,file=UvsLfile)
               OPEN(unit=unit_base(Ifrac)+6,file=InvNfile)
               OPEN(unit=unit_base(Ifrac)+7,file=HsqvLfile)
               OPEN(unit=unit_base(Ifrac)+8,file=HsqoVfile)

               WRITE(unit_base(Ifrac)+1,'(A)') "# Lambda, Enthalpy"
               WRITE(unit_base(Ifrac)+2,'(A)') "# Lambda, Volume"
               WRITE(unit_base(Ifrac)+3,'(A)') "# Lambda, Enthalpy/Volume"
               WRITE(unit_base(Ifrac)+4,'(A)') "# Lambda, 1/Volume"
               WRITE(unit_base(Ifrac)+5,'(A)') "# Lambda, Total Energy"
               WRITE(unit_base(Ifrac)+6,'(A)') "# Lambda, 1/N_molecules"
               WRITE(unit_base(Ifrac)+7,'(A)') "# Lambda, Enthalpy^2"
               WRITE(unit_base(Ifrac)+8,'(A)') "# Lambda, Enthalpy^2/Volume"

#ifdef variance
               OPEN(unit=unit_base(Ifrac)+9,file=Varfile)
               WRITE(unit_base(Ifrac)+9,'(A)') "# Lambda, dU/dLambda"
#endif
            END IF

         END DO

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                     Write to files                       C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      ELSEIF(Ichoice.EQ.1) THEN
CCC   Write lambda probability distribution to file
         DO Ifrac=1,N_Frac
            Tf=Type_Frac(Ifrac)

            IF(Tf.EQ.1) THEN
               Ib = Box_Frac(Ifrac)
               Rs = 1
               DO Ibin=1,N_LambdaBin(Ifrac)
                  NormBias(Ib,Rs,Ifrac) = NormBias(Ib,Rs,Ifrac) + LambdaCounter(Ibin,Ib,Rs,Ifrac)*dexp(-Weight(Ibin,Ib,Rs,Ifrac))
                  NormObs(Ib,Rs,Ifrac)  = NormObs(Ib,Rs,Ifrac)  + LambdaCounter(Ibin,Ib,Rs,Ifrac)
               END DO
               NormBias(Ib,Rs,Ifrac) = NormBias(Ib,Rs,Ifrac)/dble(N_LambdaBin(Ifrac))
               NormObs(Ib,Rs,Ifrac)  = NormObs(Ib,Rs,Ifrac)/dble(N_LambdaBin(Ifrac))
            ELSEIF(Tf.EQ.2) THEN
               Rs = 1
               DO Ib=1,N_Box
                  DO Ibin=1,N_LambdaBin(Ifrac)
                     NormBias(Ib,Rs,Ifrac) = NormBias(Ib,Rs,Ifrac) + LambdaCounter(Ibin,Ib,Rs,Ifrac)*dexp(-Weight(Ibin,Ib,Rs,Ifrac))
                     NormObs(Ib,Rs,Ifrac)  = NormObs(Ib,Rs,Ifrac)  + LambdaCounter(Ibin,Ib,Rs,Ifrac)
                  END DO
                  NormBias(Ib,Rs,Ifrac) = NormBias(Ib,Rs,Ifrac)/dble(N_LambdaBin(Ifrac))
                  NormObs(Ib,Rs,Ifrac)  = NormObs(Ib,Rs,Ifrac)/dble(N_LambdaBin(Ifrac))
               END DO
            ELSEIF(Tf.EQ.3) THEN
               Ib = Box_Frac(Ifrac)
               Ireac = Reaction_Frac(Ifrac)
               DO Rs=1,N_ReactionStep(Ireac)
                  DO Ibin=1,N_LambdaBin(Ifrac)
                     NormBias(Ib,Rs,Ifrac) = NormBias(Ib,Rs,Ifrac) + LambdaCounter(Ibin,Ib,Rs,Ifrac)*dexp(-Weight(Ibin,Ib,Rs,Ifrac))
                     NormObs(Ib,Rs,Ifrac)  = NormObs(Ib,Rs,Ifrac)  + LambdaCounter(Ibin,Ib,Rs,Ifrac)
                  END DO
                  NormBias(Ib,Rs,Ifrac) = NormBias(Ib,Rs,Ifrac)/dble(N_LambdaBin(Ifrac))
                  NormObs(Ib,Rs,Ifrac)  = NormObs(Ib,Rs,Ifrac)/dble(N_LambdaBin(Ifrac))
               END DO
            ELSEIF(Tf.EQ.4) THEN
               Ib = Box_Frac(Ifrac)
               Rs = 1
               DO Ibin=1,N_LambdaBin(Ifrac)
                  NormBias(Ib,Rs,Ifrac) = NormBias(Ib,Rs,Ifrac) + LambdaCounter(Ibin,Ib,Rs,Ifrac)*dexp(-Weight(Ibin,Ib,Rs,Ifrac))
                  NormObs(Ib,Rs,Ifrac)  = NormObs(Ib,Rs,Ifrac)  + LambdaCounter(Ibin,Ib,Rs,Ifrac)
               END DO
               NormBias(Ib,Rs,Ifrac) = NormBias(Ib,Rs,Ifrac)/dble(N_LambdaBin(Ifrac))
               NormObs(Ib,Rs,Ifrac)  = NormObs(Ib,Rs,Ifrac)/dble(N_LambdaBin(Ifrac))
            END IF
         END DO

         DO Ifrac=1,N_Frac
            Tf=Type_Frac(Ifrac)
            DO Ibin=1,N_LambdaBin(Ifrac)
               WRITE(unit_base(Ifrac)+0,'(f10.7,$)') dble(Ibin-1)/dble(N_Lambdabin(Ifrac)-1)
               IF(Tf.EQ.1) THEN
                  Ib = Box_Frac(Ifrac)
                  Rs = 1
                  WRITE(unit_base(Ifrac)+0,'(2e20.10e3)')
     &               LambdaCounter(Ibin,Ib,Rs,Ifrac)*dexp(-Weight(Ibin,Ib,Rs,Ifrac))/NormBias(Ib,Rs,Ifrac),
     &               LambdaCounter(Ibin,Ib,Rs,Ifrac)/NormObs(Ib,Rs,Ifrac)
               ELSEIF(Tf.EQ.2) THEN
                  Rs = 1
                  WRITE(unit_base(Ifrac)+0,'(4e20.10e3)')
     &              (LambdaCounter(Ibin,Ib,Rs,Ifrac)*dexp(-Weight(Ibin,Ib,Rs,Ifrac))/NormBias(Ib,Rs,Ifrac),
     &               LambdaCounter(Ibin,Ib,Rs,Ifrac)/NormObs(Ib,Rs,Ifrac), Ib=1,N_Box)
               ELSEIF(Tf.EQ.3) THEN
                  Ib = Box_Frac(Ifrac)
                  Ireac = Reaction_Frac(Ifrac)
                  WRITE(unit_base(Ifrac)+0,'(100e20.10e3)')
     &              (LambdaCounter(Ibin,Ib,Rs,Ifrac)*dexp(-Weight(Ibin,Ib,Rs,Ifrac))/NormBias(Ib,Rs,Ifrac),
     &               LambdaCounter(Ibin,Ib,Rs,Ifrac)/NormObs(Ib,Rs,Ifrac), Rs=1,N_ReactionStep(Ireac))
               ELSEIF(Tf.EQ.4) THEN
                  Ib = Box_Frac(Ifrac)
                  Rs = 1
                  WRITE(unit_base(Ifrac)+0,'(2e20.10e3)')
     &               LambdaCounter(Ibin,Ib,Rs,Ifrac)*dexp(-Weight(Ibin,Ib,Rs,Ifrac))/NormBias(Ib,Rs,Ifrac),
     &               LambdaCounter(Ibin,Ib,Rs,Ifrac)/NormObs(Ib,Rs,Ifrac)
               END IF

               IF(Tf.EQ.1) THEN
                  WRITE(unit_base(Ifrac)+1,'(f10.7,e20.10e3)') dble(Ibin-1)/dble(N_Lambdabin(Ifrac)-1),
     &              AvR_EnthalpyvsLambda(Ibin,Ifrac)/LambdaCounter(Ibin,Ib,Rs,Ifrac)
                  WRITE(unit_base(Ifrac)+2,'(f10.7,e20.10e3)') dble(Ibin-1)/dble(N_Lambdabin(Ifrac)-1),
     &              AvR_VolumevsLambda(Ibin,Ifrac)/LambdaCounter(Ibin,Ib,Rs,Ifrac)
                  WRITE(unit_base(Ifrac)+3,'(f10.7,e20.10e3)') dble(Ibin-1)/dble(N_Lambdabin(Ifrac)-1),
     &              AvR_HoverVvsLambda(Ibin,Ifrac)/LambdaCounter(Ibin,Ib,Rs,Ifrac)
                  WRITE(unit_base(Ifrac)+4,'(f10.7,e20.10e3)') dble(Ibin-1)/dble(N_Lambdabin(Ifrac)-1),
     &              AvR_1overVvsLambda(Ibin,Ifrac)/LambdaCounter(Ibin,Ib,Rs,Ifrac)
                  WRITE(unit_base(Ifrac)+5,'(f10.7,e20.10e3)') dble(Ibin-1)/dble(N_Lambdabin(Ifrac)-1),
     &              AvR_U_TotalvsLambda(Ibin,Ifrac)/LambdaCounter(Ibin,Ib,Rs,Ifrac)
                  WRITE(unit_base(Ifrac)+6,'(f10.7,e20.10e3)') dble(Ibin-1)/dble(N_Lambdabin(Ifrac)-1),
     &              AvR_Nmolecinvpfpb(Ibin,Ifrac)/LambdaCounter(Ibin,Ib,Rs,Ifrac)
                  WRITE(unit_base(Ifrac)+7,'(f10.7,e20.10e3)') dble(Ibin-1)/dble(N_Lambdabin(Ifrac)-1),
     &              AvR_HsquaredvsLambda(Ibin,Ifrac)/LambdaCounter(Ibin,Ib,Rs,Ifrac)
                  WRITE(unit_base(Ifrac)+8,'(f10.7,e20.10e3)') dble(Ibin-1)/dble(N_Lambdabin(Ifrac)-1),
     &              AvR_HsquaredoverVvsLambda(Ibin,Ifrac)/LambdaCounter(Ibin,Ib,Rs,Ifrac)
#ifdef variance
                  WRITE(unit_base(Ifrac)+9,'(f10.7,8e20.10e3)') dble(Ibin-1)/dble(N_Lambdabin(Ifrac)-1),
     &             AvR_dE_LJ_Inter_dLambda_Squared(Ibin,Ifrac)/LambdaCounterVariance(Ibin,Ifrac) -
     &              (AvR_dE_LJ_Inter_dLambda(Ibin,Ifrac)*AvR_dE_LJ_Inter_dLambda(Ibin,Ifrac))/
     &              (LambdaCounterVariance(Ibin,Ifrac)*LambdaCounterVariance(Ibin,Ifrac)),
     &             AvR_dE_LJ_Intra_dLambda_Squared(Ibin,Ifrac)/LambdaCounterVariance(Ibin,Ifrac) -
     &              (AvR_dE_LJ_Intra_dLambda(Ibin,Ifrac)*AvR_dE_LJ_Intra_dLambda(Ibin,Ifrac))/
     &              (LambdaCounterVariance(Ibin,Ifrac)*LambdaCounterVariance(Ibin,Ifrac)),
     &             AvR_dE_EL_Real_dLambda_Squared(Ibin,Ifrac)/LambdaCounterVariance(Ibin,Ifrac) -
     &              (AvR_dE_EL_Real_dLambda(Ibin,Ifrac)*AvR_dE_EL_Real_dLambda(Ibin,Ifrac))/
     &              (LambdaCounterVariance(Ibin,Ifrac)*LambdaCounterVariance(Ibin,Ifrac)),
     &             AvR_dE_EL_Intra_dLambda_Squared(Ibin,Ifrac)/LambdaCounterVariance(Ibin,Ifrac) -
     &              (AvR_dE_EL_Intra_dLambda(Ibin,Ifrac)*AvR_dE_EL_Intra_dLambda(Ibin,Ifrac))/
     &              (LambdaCounterVariance(Ibin,Ifrac)*LambdaCounterVariance(Ibin,Ifrac)),
     &             AvR_dE_EL_Excl_dLambda_Squared(Ibin,Ifrac)/LambdaCounterVariance(Ibin,Ifrac) -
     &              (AvR_dE_EL_Excl_dLambda(Ibin,Ifrac)*AvR_dE_EL_Excl_dLambda(Ibin,Ifrac))/
     &              (LambdaCounterVariance(Ibin,Ifrac)*LambdaCounterVariance(Ibin,Ifrac)),
     &             AvR_dE_Bending_dLambda_Squared(Ibin,Ifrac)/LambdaCounterVariance(Ibin,Ifrac) -
     &              (AvR_dE_Bending_dLambda(Ibin,Ifrac)*AvR_dE_Bending_dLambda(Ibin,Ifrac))/
     &              (LambdaCounterVariance(Ibin,Ifrac)*LambdaCounterVariance(Ibin,Ifrac)),
     &             AvR_dE_Torsion_dLambda_Squared(Ibin,Ifrac)/LambdaCounterVariance(Ibin,Ifrac) -
     &              (AvR_dE_Torsion_dLambda(Ibin,Ifrac)*AvR_dE_Torsion_dLambda(Ibin,Ifrac))/
     &              (LambdaCounterVariance(Ibin,Ifrac)*LambdaCounterVariance(Ibin,Ifrac)),
     &             AvR_dE_Total_dLambda_Squared(Ibin,Ifrac)/LambdaCounterVariance(Ibin,Ifrac) -
     &              (AvR_dE_Total_dLambda(Ibin,Ifrac)*AvR_dE_Total_dLambda(Ibin,Ifrac))/
     &              (LambdaCounterVariance(Ibin,Ifrac)*LambdaCounterVariance(Ibin,Ifrac))
#endif
               END IF
            END DO

            WRITE(unit_base(Ifrac)+0,*)
            WRITE(unit_base(Ifrac)+0,*)

            IF(Tf.EQ.1) THEN
               WRITE(unit_base(Ifrac)+1,*)
               WRITE(unit_base(Ifrac)+1,*)
               WRITE(unit_base(Ifrac)+2,*)
               WRITE(unit_base(Ifrac)+2,*)
               WRITE(unit_base(Ifrac)+3,*)
               WRITE(unit_base(Ifrac)+3,*)
               WRITE(unit_base(Ifrac)+4,*)
               WRITE(unit_base(Ifrac)+4,*)
               WRITE(unit_base(Ifrac)+5,*)
               WRITE(unit_base(Ifrac)+5,*)
               WRITE(unit_base(Ifrac)+6,*)
               WRITE(unit_base(Ifrac)+6,*)
               WRITE(unit_base(Ifrac)+7,*)
               WRITE(unit_base(Ifrac)+7,*)
               WRITE(unit_base(Ifrac)+8,*)
               WRITE(unit_base(Ifrac)+8,*)
#ifdef variance
               WRITE(unit_base(Ifrac)+9,*)
               WRITE(unit_base(Ifrac)+9,*)
#endif
            END IF

         END DO

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C  Write final lambda distribution to file and close files C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      ELSEIF(Ichoice.EQ.2) THEN
      
         OPEN(300,file="./OUTPUT/CFC/plambda.out")
         DO Ifrac=1,N_Frac
            Tf=Type_Frac(Ifrac)

            IF(Tf.EQ.1) THEN
               Ib = Box_Frac(Ifrac)
               Rs = 1
               DO Ibin=1,N_LambdaBin(Ifrac)
                  NormBias(Ib,Rs,Ifrac) = NormBias(Ib,Rs,Ifrac) + LambdaCounter(Ibin,Ib,Rs,Ifrac)*dexp(-Weight(Ibin,Ib,Rs,Ifrac))
                  NormObs(Ib,Rs,Ifrac)  = NormObs(Ib,Rs,Ifrac)  + LambdaCounter(Ibin,Ib,Rs,Ifrac)
               END DO
               NormBias(Ib,Rs,Ifrac) = NormBias(Ib,Rs,Ifrac)/dble(N_LambdaBin(Ifrac))
               NormObs(Ib,Rs,Ifrac)  = NormObs(Ib,Rs,Ifrac)/dble(N_LambdaBin(Ifrac))
            ELSEIF(Tf.EQ.2) THEN
               Rs = 1
               DO Ib=1,N_Box
                  DO Ibin=1,N_LambdaBin(Ifrac)
                     NormBias(Ib,Rs,Ifrac) = NormBias(Ib,Rs,Ifrac) + LambdaCounter(Ibin,Ib,Rs,Ifrac)*dexp(-Weight(Ibin,Ib,Rs,Ifrac))
                     NormObs(Ib,Rs,Ifrac)  = NormObs(Ib,Rs,Ifrac)  + LambdaCounter(Ibin,Ib,Rs,Ifrac)
                  END DO
                  NormBias(Ib,Rs,Ifrac) = NormBias(Ib,Rs,Ifrac)/dble(N_LambdaBin(Ifrac))
                  NormObs(Ib,Rs,Ifrac)  = NormObs(Ib,Rs,Ifrac)/dble(N_LambdaBin(Ifrac))
               END DO
            ELSEIF(Tf.EQ.3) THEN
               Ib = Box_Frac(Ifrac)
               Ireac = Reaction_Frac(Ifrac)
               DO Rs=1,N_ReactionStep(Ireac)
                  DO Ibin=1,N_LambdaBin(Ifrac)
                     NormBias(Ib,Rs,Ifrac) = NormBias(Ib,Rs,Ifrac) + LambdaCounter(Ibin,Ib,Rs,Ifrac)*dexp(-Weight(Ibin,Ib,Rs,Ifrac))
                     NormObs(Ib,Rs,Ifrac)  = NormObs(Ib,Rs,Ifrac)  + LambdaCounter(Ibin,Ib,Rs,Ifrac)
                  END DO
                  NormBias(Ib,Rs,Ifrac) = NormBias(Ib,Rs,Ifrac)/dble(N_LambdaBin(Ifrac))
                  NormObs(Ib,Rs,Ifrac)  = NormObs(Ib,Rs,Ifrac)/dble(N_LambdaBin(Ifrac))
               END DO
            ELSEIF(Tf.EQ.4) THEN
               Ib = Box_Frac(Ifrac)
               Rs = 1
               DO Ibin=1,N_LambdaBin(Ifrac)
                  NormBias(Ib,Rs,Ifrac) = NormBias(Ib,Rs,Ifrac) + LambdaCounter(Ibin,Ib,Rs,Ifrac)*dexp(-Weight(Ibin,Ib,Rs,Ifrac))
                  NormObs(Ib,Rs,Ifrac)  = NormObs(Ib,Rs,Ifrac)  + LambdaCounter(Ibin,Ib,Rs,Ifrac)
               END DO
               NormBias(Ib,Rs,Ifrac) = NormBias(Ib,Rs,Ifrac)/dble(N_LambdaBin(Ifrac))
               NormObs(Ib,Rs,Ifrac)  = NormObs(Ib,Rs,Ifrac)/dble(N_LambdaBin(Ifrac))
            END IF
         END DO

         DO Ifrac=1,N_Frac
            Tf=Type_Frac(Ifrac)
            DO Ibin=1,N_LambdaBin(Ifrac)
               WRITE(300,'(f10.7,$)') dble(Ibin-1)/dble(N_Lambdabin(Ifrac)-1)
               IF(Tf.EQ.1) THEN
                  Ib = Box_Frac(Ifrac)
                  Rs = 1
                  WRITE(300,'(2e20.10e3)')
     &               LambdaCounter(Ibin,Ib,Rs,Ifrac)*dexp(-Weight(Ibin,Ib,Rs,Ifrac))/NormBias(Ib,Rs,Ifrac),
     &               LambdaCounter(Ibin,Ib,Rs,Ifrac)/NormObs(Ib,Rs,Ifrac)
               ELSEIF(Tf.EQ.2) THEN
                  Rs = 1
                  WRITE(300,'(4e20.10e3)')
     &              (LambdaCounter(Ibin,Ib,Rs,Ifrac)*dexp(-Weight(Ibin,Ib,Rs,Ifrac))/NormBias(Ib,Rs,Ifrac),
     &               LambdaCounter(Ibin,Ib,Rs,Ifrac)/NormObs(Ib,Rs,Ifrac), Ib=1,N_Box)
               ELSEIF(Tf.EQ.3) THEN
                  Ib = Box_Frac(Ifrac)
                  Ireac = Reaction_Frac(Ifrac)
                  WRITE(300,'(100e20.10e3)')
     &              (LambdaCounter(Ibin,Ib,Rs,Ifrac)*dexp(-Weight(Ibin,Ib,Rs,Ifrac))/NormBias(Ib,Rs,Ifrac),
     &               LambdaCounter(Ibin,Ib,Rs,Ifrac)/NormObs(Ib,Rs,Ifrac), Rs=1,N_ReactionStep(Ireac))
               ELSEIF(Tf.EQ.4) THEN
                  Ib = Box_Frac(Ifrac)
                  Rs = 1
                  WRITE(300,'(2e20.10e3)')
     &               LambdaCounter(Ibin,Ib,Rs,Ifrac)*dexp(-Weight(Ibin,Ib,Rs,Ifrac))/NormBias(Ib,Rs,Ifrac),
     &               LambdaCounter(Ibin,Ib,Rs,Ifrac)/NormObs(Ib,Rs,Ifrac)
               END IF

            END DO
            WRITE(300,*)
            WRITE(300,*)
         END DO

         IF(L_dUdl) THEN
            OPEN(400,file="./OUTPUT/CFC/dU_dlambda.out")
            WRITE(400,'(A,i5,1x,A,A24)') "#", N_LambdaBin(1), "bins of average dU/dlambda of ", Name_Frac(1)
            DO Ibin=1,N_LambdaBin(1)
               WRITE(400,'(f10.7,1x,e20.10e3,e20.10e3)') (dble(Ibin)-0.5d0)/dble(N_LambdaBin(1)),
     &         AvR_dU_dl(Ibin)/dUdlCounter(Ibin),dsqrt(sq_AvR_dU_dl(Ibin)/dUdlCounter(Ibin)-
     &         ((AvR_dU_dl(Ibin)/dUdlCounter(Ibin))**2.0d0))
            END DO
            CLOSE(400)
         END IF

         DO Ifrac=1,N_Frac
            CLOSE(unit_base(Ifrac)+0)
            CLOSE(unit_base(Ifrac)+1)
            CLOSE(unit_base(Ifrac)+2)
            CLOSE(unit_base(Ifrac)+3)
            CLOSE(unit_base(Ifrac)+4)
            CLOSE(unit_base(Ifrac)+5)
            CLOSE(unit_base(Ifrac)+6)
            CLOSE(unit_base(Ifrac)+7)
            CLOSE(unit_base(Ifrac)+8)
#ifdef variance
            CLOSE(unit_base(Ifrac)+9)
#endif
         END DO

      END IF

      RETURN
      END
