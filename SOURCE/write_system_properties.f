      SUBROUTINE Write_System_Properties(Ichoice,Icycle)
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"

      integer Ichoice,Icycle,Ib,Tm,Ibin

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                Open and initialize files                 C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      IF(Ichoice.EQ.0) THEN
         OPEN(1,file="OUTPUT/density_vs_cycle.dat")
         OPEN(2,file="OUTPUT/number_of_molecules_vs_cycle.dat")
         OPEN(3,file="OUTPUT/volume_vs_cycle.dat")
         OPEN(4,file="OUTPUT/energy_vs_cycle.dat")
         WRITE(*,*) L_dUdl
CCC      open the file and print the header MERT 12/07/21
         IF(L_dUdl) THEN
            OPEN(90,file="OUTPUT/dUdl_vs_cycle_for_alchemlyb.dat")
            WRITE(90,'(A)') "#Cycle, Lambda, dU_dl"
         END IF
CCC      open the file and print the header MERT 12/07/21

         WRITE(1,'(A)') "# Cycle, ((density(Ib,Tm), Tm=1,N_MolType), Ib=1,N_Box)"
         WRITE(2,'(A)') "# Cycle, ((number_of_molecules(Ib,Tm), Tm=1,N_MolType), Ib=1,N_Box)"
         WRITE(3,'(A)') "# Cycle, (volume(Ib), Ib=1,N_Box)"
         WRITE(4,'(A)') "# Energies per box"
         WRITE(4,'(A)') "# Cycle, LJ_Inter, LJ_tail, LJ_Inter_Total, EL_Real, EL_Excl, EL_Self, EL_Four,
     &                    EL_Inter, LJ_Intra, EL_Intra, E_Bending, E_Torsion, E_Total"

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                     Write to files                       C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      ELSEIF(Ichoice.EQ.1) THEN


         WRITE(1,'(i10,100e20.10e3)') Icycle, ((dble(Nmptpb(Ib,Tm))/Volume(Ib)*MolarMass(Tm)*Mconv, Tm=1,N_MolType), Ib=1,N_Box)

         WRITE(2,'(i10,100i5)')     Icycle, ((Nmptpb(Ib,Tm), Tm=1,N_MolType), Ib=1,N_Box)

         WRITE(3,'(i10,2e20.10e3)')   Icycle, (Volume(Ib), Ib=1,N_Box)

         WRITE(4,'(i10,$)') Icycle
         DO Ib=1,N_Box
            WRITE(4,'(3e20.10e3,$)') U_LJ_Inter(Ib), U_LJ_Tail(Ib), U_LJ_Inter(Ib)+U_LJ_Tail(Ib)
            WRITE(4,'(4e20.10e3,$)') U_EL_Real(Ib), U_EL_Excl(Ib), U_EL_Self(Ib),U_EL_Four(Ib)
            WRITE(4,'(e20.10e3,$)')  U_EL_Real(Ib)+U_EL_Excl(Ib)+U_EL_Self(Ib)+U_EL_Four(Ib)
            WRITE(4,'(2e20.10e3,$)') U_LJ_Intra(Ib), U_EL_Intra(Ib)
            WRITE(4,'(2e20.10e3,$)') U_Bending_Total(Ib), U_Torsion_Total(Ib)
            WRITE(4,'(e20.10e3,$)')  U_Total(Ib)
         END DO
         WRITE(4,*)
CCC      print MERT 12/07/21
         IF(L_dUdl) THEN
            Ibin = 1 + int(dble(N_LambdaBin(1))*Lambda_Frac(1))
            WRITE(90,'(i10,f10.7,100e20.10e3)') Icycle, (dble(Ibin)-0.5d0)/dble(N_LambdaBin(1)), dU_dlTotal
         END IF
CCC      print MERT 12/07/21

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                      Close files                         C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      ELSEIF(Ichoice.EQ.2) THEN

         CLOSE(1)
         CLOSE(2)
         CLOSE(3)
         CLOSE(4)
CCC      close the file MERT 12/07/21
         IF(L_dUdl) THEN
            CLOSE(90)
         END IF
CCC      close the file MERT 12/07/21
      END IF

      RETURN
      END
