      SUBROUTINE Energy_Correction(Ib)
      implicit none

      include "global_variables.inc"
      include "energy.inc"
      include "settings.inc"

      integer  Ib,It,Jt,Tm,Tmi,Tmj,I,J,Imol,Ifrac,Iff(2)
      double precision Cljtail(MaxMolType,MaxMolType,2),Myl,Myc,Myi,dderfc,SelfTerm_Factor(2),
     &   R2,R3,R6,R3i,S6,N_FracmolLJ(MaxMolType),N_FracmolEL(MaxMolType),
     &   N_FracmolELd(MaxMolType),dlLJ_dlf,dlEL_dlf

      Save Iff,Cljtail,SelfTerm_Factor
      Data Iff /0,0/

      IF(Iff(Ib).EQ.0) THEN
         DO It=1,N_AtomType
            DO Jt=1,N_AtomType
               U_LJ_Shift(It,Jt,Ib) = 0.0d0
            END DO
         END DO
         DO Tmi=1,N_MolType
            DO Tmj=1,N_MolType
               Cljtail(Tmi,Tmj,Ib) = 0.0d0
            END DO
         END DO

         IF(L_LJ_Shift) THEN
C     Calculate energy shifts for LJ (factor 4 is in routine that calculates the energy)
            DO It=1,N_AtomType
               DO Jt=1,N_AtomType
                  IF(L_LJ(It,Jt)) THEN
                     R2  = Sigma_2(It,Jt)/R_Cut_LJ_2(Ib)
                     R6  = R2*R2*R2
                     U_LJ_Shift(It,Jt,Ib) = -Epsilon(It,Jt)*(R6*(R6-1.0d0))
                  END IF
               END DO
            END DO
         ELSEIF(L_LJ_Tail) THEN
C     Calculate the constant factors for LJ tailcorrections
            DO Tmi=1,N_MolType
               DO Tmj=1,N_MolType
                  DO I=1,N_AtomInMolType(Tmi)
                     DO J=1,N_AtomInMolType(Tmj)
                        It=TypeAtom(Tmi,I)
                        Jt=TypeAtom(Tmj,J)
                        IF(L_LJ(It,Jt)) THEN
                           S6  = Sigma_2(It,Jt)*Sigma_2(It,Jt)*Sigma_2(It,Jt)
                           R3  = R_Cut_LJ(Ib)*R_Cut_LJ(Ib)*R_Cut_LJ(Ib)
                           R3i = 1.0d0/R3
                           Cljtail(Tmi,Tmj,Ib) = Cljtail(Tmi,Tmj,Ib)
     &                      + Epsilon(It,Jt)*(S6*S6*R3i*R3i*R3i/9.0d0 - S6*R3i/3.0d0)
                        END IF
                     END DO
                  END DO
                  Cljtail(Tmi,Tmj,Ib) = 8.0d0*OnePi*Cljtail(Tmi,Tmj,Ib)
               END DO
            END DO
         END IF

C     Calculate constant factors for Electrostatic interactions in different methods
         IF(L_Ewald(Ib)) THEN
            ErfcAlphaRc(Ib)     = 0.0d0
            U_EL_Shift(Ib)      = 0.0d0
            FG_Factor(Ib)       = 0.0d0
            SelfTerm_Factor(Ib) = -R4pie*(Alpha_EL(Ib)/dsqrt(OnePi))
         ELSEIF(L_Wolf(Ib)) THEN
            ErfcAlphaRc(Ib)     = dderfc(Alpha_EL(Ib)*R_Cut_EL(Ib))
            U_EL_Shift(Ib)      = ErfcAlphaRc(Ib)/R_Cut_EL(Ib)
            FG_Factor(Ib)       = 0.0d0
            SelfTerm_Factor(Ib) = -R4pie*(0.5d0*U_EL_Shift(Ib)+Alpha_EL(Ib)/dsqrt(OnePi))
         ELSEIF(L_WolfFG(Ib)) THEN
            ErfcAlphaRc(Ib)     = dderfc(Alpha_EL(Ib)*R_Cut_EL(Ib))
            U_EL_Shift(Ib)      = ErfcAlphaRc(Ib)/R_Cut_EL(Ib)
            FG_Factor(Ib)       = dderfc(Alpha_EL(Ib)*R_Cut_EL(Ib))/(R_Cut_EL(Ib)*R_Cut_EL(Ib))
     & + (2.0d0*Alpha_EL(Ib)/dsqrt(OnePi))*dexp(-Alpha_EL(Ib)*Alpha_EL(Ib)*R_Cut_EL(Ib)*R_Cut_EL(Ib))/R_Cut_EL(Ib)
            SelfTerm_Factor(Ib) = -R4pie*(0.5d0*U_EL_Shift(Ib)+Alpha_EL(Ib)/dsqrt(OnePi))
         ELSE
            ErfcAlphaRc(Ib)     = 0.0d0
            U_EL_Shift(Ib)      = 0.0d0
            FG_Factor(Ib)       = 0.0d0
            SelfTerm_Factor(Ib) = 0.0d0
         END IF

C     Calculate constant factor for force calculation using Damped Shifted Force potential
         DSF_Factor(Ib) = dderfc(Alpha_DSF(Ib)*R_Cut_DSF(Ib))/(R_Cut_DSF(Ib)*R_Cut_DSF(Ib))

C     In a normal simulation the quantities calculated above are constants
C     and do not need updating in contrast with making Wolfplots.
         IF(L_WolfPlot) THEN
            Iff(Ib) = 0
         ELSE
            Iff(Ib) = 1
         END IF

      END IF

      U_LJ_Tail(Ib) = 0.0d0
      U_EL_Self(Ib) = 0.0d0

      dUTail_dl     = 0.0d0
      dUELSelf_dl   = 0.0d0

      IF(L_IdealGas(Ib)) RETURN

C     All energy corrections scale linearly with lambda: the number
C     of particles is set as N+sum_lambda. Here we calculate the sum
C     of the lambdas (N_Fracmol) in box Ib.
      DO Tm=1,N_MolType
         N_FracmolLJ(Tm) = 0.0d0
         N_FracmolEL(Tm) = 0.0d0
         N_FracmolELd(Tm)= 0.0d0
      END DO

      DO Ifrac=1,N_Frac
         IF(Box_Frac(Ifrac).NE.Ib) CYCLE
         DO I=1,N_MolInFrac(Ifrac)
            Imol = I_MolInFrac(Ifrac,I)
            CALL interactionlambda(Imol,Myl,Myc,Myi,dlLJ_dlf,dlEL_dlf)

            Tm  = TypeMol(Imol)
            
            N_FracmolLJ(Tm) = N_FracmolLJ(Tm) + Myl
            N_FracmolEL(Tm) = N_FracmolEL(Tm) + Myc*Myc
            N_FracmolELd(Tm)= N_FracmolELd(Tm)+ Myc
         END DO
      END DO

C     Calculate LJ long-range tailcorrections
      IF(L_LJ_Tail) THEN
         DO Tmi=1,N_MolType
            DO Tmj=1,N_MolType
               U_LJ_Tail(Ib) = U_LJ_Tail(Ib) + (dble(Nmptpb(Ib,Tmi))+N_FracmolLJ(Tmi))*
     &              (dble(Nmptpb(Ib,Tmj))+N_FracmolLJ(Tmj))*Cljtail(Tmi,Tmj,Ib)
               IF(L_dUdl) THEN
                  dUTail_dl = dUTail_dl + dlLJ_dlf*(dble(Nmptpb(1,Tmi))*N_MolOfMolTypeInFrac(Tmj,1)+dble(Nmptpb(1,Tmj))*
     &                        N_MolOfMolTypeInFrac(Tmi,1)+2.0d0*N_FracmolLJ(Tmj)*N_MolOfMolTypeInFrac(Tmj,1))*Cljtail(Tmi,Tmj,1)
               END IF
            END DO
         END DO
         U_LJ_Tail(Ib) = U_LJ_Tail(Ib)*InvBoxSize(Ib)*InvBoxSize(Ib)*InvBoxSize(Ib)
         dUTail_dl = dUTail_dl*InvBoxSize(1)*InvBoxSize(1)*InvBoxSize(1)
      END IF

C     Calculate the Selfterm in electrostatics
      DO Tm=1,N_MolType
         U_EL_Self(Ib) = U_EL_Self(Ib) + (dble(Nmptpb(Ib,Tm))+N_FracmolEL(Tm))*Qsum_2(Tm)
         IF(L_dUdl) THEN
            dUELSelf_dl   = dUELSelf_dl + dlEL_dlf*2.0d0*N_FracmolELd(Tm)*Qsum_2(Tm)
         END IF
      END DO

      U_EL_Self(Ib) = SelfTerm_Factor(Ib)*U_EL_Self(Ib)
      dUELSelf_dl   = SelfTerm_Factor(1)*dUELSelf_dl

      RETURN
      END
