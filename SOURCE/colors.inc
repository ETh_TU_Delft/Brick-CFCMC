CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC   Include this file to use colors in the output written to the        CCC
CCC   terminal/screen. Please note that the file should be included       CCC
CCC   using #include because directives are being used in this file.      CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      character*10 color_red,color_yellow,color_green,color_white
      character*24 OK
      character*24 ERROR
      character*24 WARNING

#IFDEF colors
      Parameter (color_red = "\033[1;31m")
      Parameter (color_yellow = "\033[1;33m")
      Parameter (color_green = "\033[1;92m")
      Parameter (color_white = "\033[0m")

      Parameter (OK      = "\033[1;92m OK\033[0m")
      Parameter (WARNING = "\033[1;33m WARNING\033[0m" )
      Parameter (ERROR   = "\033[1;31m ERROR\033[0m")
#ELSE
      Parameter (color_red = "")
      Parameter (color_yellow = "")
      Parameter (color_green = "")
      Parameter (color_white = "")

      Parameter (OK      = " OK")
      Parameter (WARNING = " WARNING" )
      Parameter (ERROR   = " ERROR")
#ENDIF
