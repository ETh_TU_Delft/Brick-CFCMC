      SUBROUTINE Cluster_Rotation
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "energy.inc"
      include "ewald.inc"
      include "output.inc"

C     Rotate a cluster of molecules (in vapor phase)

      integer Imol,Jmol,Iatom,Ib,Tm,I,J,It,Select_Random_Integer,Icluster,Nclusternew,Nclusterold,
     &   Nmolecinclusterold(MaxCluster),Imolecinclusterold(MaxCluster,MaxMolInCluster),
     &   Nmolecinclusternew(MaxCluster),Imolecinclusternew(MaxCluster,MaxMolInCluster)
      double precision Ran_Uniform,Rm,dummy1,dummy2,dummy3,dummy4,dummy5,dE,Myl,Myc,Myi,
     &   Xold(MaxMolInCluster,MaxAtom),Yold(MaxMolInCluster,MaxAtom),dE_EL_Four,
     &   Zold(MaxMolInCluster,MaxAtom),XCMold(MaxMolInCluster),Delta_ClusterRotation,
     &   YCMold(MaxMolInCluster),ZCMold(MaxMolInCluster),zetax,zetay,zetaz,xix,xiy,xiz,
     &   Cmx,Cmy,Cmz,Rrx,Rry,Rrz,Ryynew,Rxxnew,Rzznew,Dgamma,Cosdg,Sindg,
     &   E_LJ_Inter,E_EL_Real,E_LJ_InterNew,E_EL_RealNew,E_LJ_InterOld,E_EL_RealOld,
     &   dlLJ_dlf,dlEL_dlf,dU_dl,ddUELFour_dl
      logical L_Overlap_Inter,L_Overlap_Intra,Laccept,LEwald

      Ib = Ibvapor

C     Find clusters and select one of them at random
      CALL Find_Clusters(Nclusterold,Nmolecinclusterold,Imolecinclusterold)

C     If there are no clusters, or there is only one cluster (the whole box) than there is nothing to do
      IF(Nclusterold.EQ.0) RETURN
      IF(Nclusterold.EQ.1) RETURN

      Icluster = Select_Random_Integer(Nclusterold)

      LEwald = .false.
      DO I=1,Nmolecinclusterold(Icluster)
         Imol = Imolecinclusterold(Icluster,I)
         IF(L_ChargeInMolType(TypeMol(Imol)).AND.L_Ewald(Ib)) THEN
            LEwald = .true.
            EXIT
         END IF
      END DO

      IF(LEwald) CALL Ewald_Init

      E_LJ_InterOld = 0.0d0
      E_EL_RealOld  = 0.0d0

      Delta_ClusterRotation = 0.0d0

      IF(LEwald) THEN
         DO I=1,Nmolecinclusterold(Icluster)
            Imol = Imolecinclusterold(Icluster,I)
            Tm   = TypeMol(Imol)
            
            IF(.NOT.L_ChargeInMolType(Tm)) CYCLE
            
            IF(L_frac(Imol)) CALL interactionlambda(Imol,Myl,Myc,Myi,dlLJ_dlf,dlEL_dlf)

            DO Iatom=1,N_AtomInMolType(Tm)
               It=TypeAtom(Tm,Iatom)
               IF(L_Charge(It)) THEN
                  NKSPACE(Ib,1) = NKSPACE(Ib,1) + 1
                  J = NKSPACE(Ib,1)
                  XKSPACE(J,Ib,1) = X(Imol,Iatom)
                  YKSPACE(J,Ib,1) = Y(Imol,Iatom)
                  ZKSPACE(J,Ib,1) = Z(Imol,Iatom)
                  IF(L_frac(Imol)) THEN
                     QKSPACE(J,Ib,1) = Myc*Q(It)
                  ELSE
                     QKSPACE(J,Ib,1) = Q(It)
                  END IF
               END IF
            END DO
         END DO
      END IF

C     Store old configuration and calculate energy
      DO I=1,Nmolecinclusterold(Icluster)
         Imol = Imolecinclusterold(Icluster,I)
         Tm   = TypeMol(Imol)

         DO Iatom=1,N_AtomInMolType(Tm)
            Xold(I,Iatom) = X(Imol,Iatom)
            Yold(I,Iatom) = Y(Imol,Iatom)
            Zold(I,Iatom) = Z(Imol,Iatom)
         END DO

         XCMold(I) = XCM(Imol)
         YCMold(I) = YCM(Imol)
         ZCMold(I) = ZCM(Imol)

         CALL Energy_Molecule(Imol,E_LJ_Inter,dummy1,E_EL_Real,dummy2,dummy3,dummy4,dummy5,L_Overlap_Inter,L_Overlap_Intra,dU_dl)

         IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
            WRITE(6,'(A,A)') ERROR, "Energy Overlap (Cluster Rotation)"
            STOP
         END IF
         E_LJ_InterOld = E_LJ_InterOld + E_LJ_Inter
         E_EL_RealOld  = E_EL_RealOld  + E_EL_Real

         Delta_ClusterRotation = Delta_ClusterRotation + Delta_Rotation(Ib,Tm)
      END DO

      Delta_ClusterRotation = Delta_ClusterRotation/dble(Nmolecinclusterold(Icluster))

C     Correct for counting LJ and Electrostatic energy double
      DO I=1,Nmolecinclusterold(Icluster)-1
         Imol = Imolecinclusterold(Icluster,I)
         DO J=I+1,Nmolecinclusterold(Icluster)
            Jmol = Imolecinclusterold(Icluster,J)

            CALL Energy_Intermolecular(Imol,Jmol,E_LJ_Inter,E_EL_Real,L_Overlap_Inter,dU_dl)

            IF(L_Overlap_Inter) THEN
               WRITE(6,'(A,A)') ERROR, "Intermolecular Energy Overlap (Cluster Rotation)"
               STOP
            END IF
            E_LJ_InterOld = E_LJ_InterOld - E_LJ_Inter
            E_EL_RealOld = E_EL_RealOld - E_EL_Real
         END DO
      END DO

C     Generate new configuration
      Dgamma = Delta_ClusterRotation*(2.0d0*Ran_Uniform() - 1.0d0)
      Cosdg  = dcos(Dgamma)
      Sindg  = dsin(Dgamma)

      xix = 0.0d0
      xiy = 0.0d0
      xiz = 0.0d0

      zetax = 0.0d0
      zetay = 0.0d0
      zetaz = 0.0d0

      DO I=1,Nmolecinclusterold(Icluster)
         Imol = Imolecinclusterold(Icluster,I)
         Tm   = TypeMol(Imol)

         xix = xix + dcos(XCM(Imol)*InvBoxSize(Ib)*TwoPi)*N_AtomInMolType(Tm)
         xiy = xiy + dcos(YCM(Imol)*InvBoxSize(Ib)*TwoPi)*N_AtomInMolType(Tm)
         xiz = xiz + dcos(ZCM(Imol)*InvBoxSize(Ib)*TwoPi)*N_AtomInMolType(Tm)

         zetax = zetax + dsin(XCM(Imol)*InvBoxSize(Ib)*TwoPi)*N_AtomInMolType(Tm)
         zetay = zetay + dsin(YCM(Imol)*InvBoxSize(Ib)*TwoPi)*N_AtomInMolType(Tm)
         zetaz = zetaz + dsin(ZCM(Imol)*InvBoxSize(Ib)*TwoPi)*N_AtomInMolType(Tm)
      END DO

      cmx = (datan2(-zetax,-xix) + OnePi)*BoxSize(Ib)/TwoPi
      cmy = (datan2(-zetay,-xiy) + OnePi)*BoxSize(Ib)/TwoPi
      cmz = (datan2(-zetaz,-xiz) + OnePi)*BoxSize(Ib)/TwoPi

      Rm = 3.0d0*Ran_Uniform()

      IF(Rm.LT.1.0d0) THEN

         DO I=1,Nmolecinclusterold(Icluster)
            Imol = Imolecinclusterold(Icluster,I)
            Tm   = TypeMol(Imol)

            DO Iatom=1,N_AtomInMolType(Tm)
               Rry    = Y(Imol,Iatom) - Cmy
               Rrz    = Z(Imol,Iatom) - Cmz
               Ryynew = Cosdg*Rry + Sindg*Rrz
               Rzznew = Cosdg*Rrz - Sindg*Rry
               Y(Imol,Iatom) = Cmy + Ryynew
               Z(Imol,Iatom) = Cmz + Rzznew
            END DO

            Rry    = YCM(Imol) - Cmy
            Rrz    = ZCM(Imol) - Cmz
            Ryynew = Cosdg*Rry + Sindg*Rrz
            Rzznew = Cosdg*Rrz - Sindg*Rry
            YCM(Imol) = Cmy + Ryynew
            ZCM(Imol) = Cmz + Rzznew

            CALL Place_molecule_back_in_box(Imol)

         END DO

      ELSEIF(Rm.LT.2.0d0) THEN

         DO I=1,Nmolecinclusterold(Icluster)
            Imol = Imolecinclusterold(Icluster,I)
            Tm   = TypeMol(Imol)

            DO Iatom=1,N_AtomInMolType(Tm)
               Rrx    = X(Imol,Iatom) - Cmx
               Rrz    = Z(Imol,Iatom) - Cmz
               Rxxnew = Cosdg*Rrx - Sindg*Rrz
               Rzznew = Cosdg*Rrz + Sindg*Rrx
               X(Imol,Iatom) = Cmx + Rxxnew
               Z(Imol,Iatom) = Cmz + Rzznew
            END DO

            Rrx    = XCM(Imol) - Cmx
            Rrz    = ZCM(Imol) - Cmz
            Rxxnew = Cosdg*Rrx - Sindg*Rrz
            Rzznew = Cosdg*Rrz + Sindg*Rrx
            XCM(Imol) = Cmx + Rxxnew
            ZCM(Imol) = Cmz + Rzznew

            CALL Place_molecule_back_in_box(Imol)

         END DO

      ELSE

         DO I=1,Nmolecinclusterold(Icluster)
            Imol = Imolecinclusterold(Icluster,I)
            Tm   = TypeMol(Imol)

            DO Iatom=1,N_AtomInMolType(Tm)
               Rrx    = X(Imol,Iatom) - Cmx
               Rry    = Y(Imol,Iatom) - Cmy
               Rxxnew = Cosdg*Rrx + Sindg*Rry
               Ryynew = Cosdg*Rry - Sindg*Rrx
               X(Imol,Iatom) = Cmx + Rxxnew
               Y(Imol,Iatom) = Cmy + Ryynew
            END DO

            Rrx    = XCM(Imol) - Cmx
            Rry    = YCM(Imol) - Cmy
            Rxxnew = Cosdg*Rrx + Sindg*Rry
            Ryynew = Cosdg*Rry - Sindg*Rrx
            XCM(Imol) = Cmx + Rxxnew
            YCM(Imol) = Cmy + Ryynew

            CALL Place_molecule_back_in_box(Imol)

         END DO

      END IF

C     Check if clusters changed
      CALL Find_Clusters(Nclusternew,Nmolecinclusternew,Imolecinclusternew)

C     If clusters changed than reject the move (because this would violate detailed balance)
      IF(Nclusternew.NE.Nclusterold) THEN
         Laccept=.false.
         GO TO 1
      END IF

      IF(LEwald) THEN
         DO I=1,Nmolecinclusterold(Icluster)
            Imol = Imolecinclusterold(Icluster,I)
            Tm   = TypeMol(Imol)
            IF(.NOT.L_ChargeInMolType(Tm)) CYCLE

            IF(L_frac(Imol)) CALL interactionlambda(Imol,Myl,Myc,Myi,dlLJ_dlf,dlEL_dlf)

            DO Iatom=1,N_AtomInMolType(Tm)
               It=TypeAtom(Tm,Iatom)
               IF(L_Charge(It)) THEN
                  NKSPACE(Ib,2) = NKSPACE(Ib,2) + 1
                  J = NKSPACE(Ib,2)
                  XKSPACE(J,Ib,2) = X(Imol,Iatom)
                  YKSPACE(J,Ib,2) = Y(Imol,Iatom)
                  ZKSPACE(J,Ib,2) = Z(Imol,Iatom)
                  IF(L_frac(Imol)) THEN
                     QKSPACE(J,Ib,2) = Myc*Q(It)
                  ELSE
                     QKSPACE(J,Ib,2) = Q(It)
                  END IF
               END IF
            END DO
         END DO
      END IF

C     Calculate energy of new configuration
      E_LJ_InterNew = 0.0d0
      E_EL_RealNew  = 0.0d0

      DO I=1,Nmolecinclusterold(Icluster)
         Imol = Imolecinclusterold(Icluster,I)

         CALL Energy_Molecule(Imol,E_LJ_Inter,dummy1,E_EL_Real,dummy2,dummy3,dummy4,dummy5,L_Overlap_Inter,L_Overlap_Intra,dU_dl)

         IF(L_Overlap_Intra) THEN
            WRITE(6,'(A,A)') ERROR, "Intramolecular Energy Overlap (Cluster Rotation)"
            STOP
         ELSEIF(L_Overlap_Inter) THEN
            Laccept=.false.
            GO TO 1
         END IF
         E_LJ_InterNew = E_LJ_InterNew + E_LJ_Inter
         E_EL_RealNew  = E_EL_RealNew  + E_EL_Real
      END DO

C     Correct for counting LJ and Electrostatic energy double
      DO I=1,Nmolecinclusterold(Icluster)-1
         Imol = Imolecinclusterold(Icluster,I)
         DO J=I+1,Nmolecinclusterold(Icluster)
            Jmol = Imolecinclusterold(Icluster,J)

            CALL Energy_Intermolecular(Imol,Jmol,E_LJ_Inter,E_EL_Real,L_Overlap_Inter,dU_dl)

            IF(L_Overlap_Inter) THEN
               Laccept=.false.
               GO TO 1
            END IF
            E_LJ_InterNew = E_LJ_InterNew - E_LJ_Inter
            E_EL_RealNew  = E_EL_RealNew  - E_EL_Real
         END DO
      END DO

      dE_EL_Four = 0.0d0

      IF(LEwald) CALL Ewald_Move(Ib,dE_EL_Four,ddUELFour_dl)

      dE = E_LJ_InterNew + E_EL_RealNew - E_LJ_InterOld - E_EL_RealOld + dE_EL_Four

      CALL Accept_or_Reject(dexp(-beta*dE),Laccept)

   1  CONTINUE

      TrialClusterRotation(Ib) = TrialClusterRotation(Ib) + 1.0d0
      TrialClusterRotationvsClusterSize(Ib,Nmolecinclusterold(Icluster)) =
     &   TrialClusterRotationvsClusterSize(Ib,Nmolecinclusterold(Icluster)) + 1.0d0

      IF(Laccept) THEN

         AcceptClusterRotation(Ib) = AcceptClusterRotation(Ib) + 1.0d0
         AcceptClusterRotationvsClusterSize(Ib,Nmolecinclusterold(Icluster)) =
     &      AcceptClusterRotationvsClusterSize(Ib,Nmolecinclusterold(Icluster)) + 1.0d0

         U_LJ_Inter(Ib) = U_LJ_Inter(Ib) + E_LJ_InterNew - E_LJ_InterOld
         U_EL_Real(Ib)  = U_EL_Real(Ib)  + E_EL_RealNew  - E_EL_RealOld
         U_EL_Four(Ib)  = U_EL_Four(Ib)  + dE_EL_Four

         U_Total(Ib) = U_Total(Ib) + dE

         IF(LEwald) CALL Ewald_Accept(Ib)

      ELSE

         DO I=1,Nmolecinclusterold(Icluster)
            Imol = Imolecinclusterold(Icluster,I)
            Tm   = TypeMol(Imol)

            DO Iatom=1,N_AtomInMolType(Tm)
               X(Imol,Iatom) = Xold(I,Iatom)
               Y(Imol,Iatom) = Yold(I,Iatom)
               Z(Imol,Iatom) = Zold(I,Iatom)
            END DO

            XCM(Imol) = XCMold(I)
            YCM(Imol) = YCMold(I)
            ZCM(Imol) = ZCMold(I)
         END DO

      END IF

      RETURN
      END
