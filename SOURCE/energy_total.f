      SUBROUTINE Energy_Total
     & (Ib,E_LJ_Inter,E_LJ_Intra,E_EL_Real,E_EL_Intra,E_EL_Excl,E_Bending,E_Torsion,L_Overlap_Inter,L_Overlap_Intra,dU_dl)
      implicit none

      include "global_variables.inc"
      include "energy.inc"
      include "output.inc"

C     Calculate the total energy in box Ib

      integer I,J,Imol,Jmol,Ib,Tmi,Tmj,ii,jj,It,Jt
      double precision E_LJ_Inter,E_EL_Real,E_EL_Intra,E_Torsion,E_LJ_Intra,E_EL_Excl,E_Bending,
     &   dX,dY,dZ,R2,R,R0,R0c,R6,Is,Lambda_LJi,Lambda_LJj,Lambda_ELi,Lambda_ELj,
     &   Myl,Myc,Myi,Off,Ofc,dderfc,Ubend,Utors,Xb(3),Yb(3),Zb(3),Xt(4),Yt(4),Zt(4)
      double precision dlLJ_dlf,dlEL_dlf,dU_dl,dULJ_dl,dULJ_dlc,
     &   dUELReal_dl,XR,XRcut,dUELExcl_dl,Mycd
      logical L_Overlap_Inter,L_Overlap_Intra

      E_LJ_Inter = 0.0d0
      E_EL_Real  = 0.0d0
      E_EL_Excl  = 0.0d0
      E_LJ_Intra = 0.0d0
      E_EL_Intra = 0.0d0
      E_Bending  = 0.0d0
      E_Torsion  = 0.0d0
      dULJ_dl    = 0.0d0
      dUELReal_dl= 0.0d0 
      dUELExcl_dl= 0.0d0

      L_Overlap_Inter = .false.
      L_Overlap_Intra = .false.

C     In an ideal gas skip the intermolecular interactions
      IF(.NOT.L_IdealGas(Ib)) THEN

C     Calculate total intermolecular energy
C     Loop over all molecule pairs in the box
         DO I=1,N_MolInBox(Ib)-1
            Imol = I_MolInBox(Ib,I)

            Tmi = TypeMol(Imol)

            DO J=I+1,N_MolInBox(Ib)
               Jmol = I_MolInBox(Ib,J)

               IF(Imol.EQ.Jmol) THEN
                  WRITE(6,'(A,A)') ERROR, "Imol = Jmol, this should not happen! (energy_total.f)"
                  STOP
               END IF

               Tmj = TypeMol(Jmol)

               IF(L_Frac(Imol).AND.L_Frac(Jmol)) THEN
                  CALL interactionlambda(Imol,Lambda_LJi,Lambda_ELi,Myi,dlLJ_dlf,dlEL_dlf)
                  CALL interactionlambda(Jmol,Lambda_LJj,Lambda_ELj,Myi,dlLJ_dlf,dlEL_dlf)

                  Myl = Lambda_LJi*Lambda_LJj
                  Off = Alpha_Offset_LJ*(1.0d0 - Myl)

                  Myc = Lambda_ELi*Lambda_ELj
                  Ofc = Alpha_Offset_EL*(1.0d0 - Myc)
               ELSEIF(L_Frac(Imol)) THEN
                  CALL interactionlambda(Imol,Myl,Myc,Myi,dlLJ_dlf,dlEL_dlf)

                  Off = Alpha_Offset_LJ*(1.0d0 - Myl)
                  Ofc = Alpha_Offset_EL*(1.0d0 - Myc)
               ELSEIF(L_Frac(Jmol)) THEN
                  CALL interactionlambda(Jmol,Myl,Myc,Myi,dlLJ_dlf,dlEL_dlf)

                  Off = Alpha_Offset_LJ*(1.0d0 - Myl)
                  Ofc = Alpha_Offset_EL*(1.0d0 - Myc)
               END IF

               DO ii = 1,N_AtomInMolType(Tmi)
                  DO jj = 1,N_AtomInMolType(Tmj)

                     dX = X(Imol,ii) - X(Jmol,jj)
                     dY = Y(Imol,ii) - Y(Jmol,jj)
                     dZ = Z(Imol,ii) - Z(Jmol,jj)

                     dX = dX - BoxSize(Ib)*Dnint(dX*InvBoxSize(Ib))
                     dY = dY - BoxSize(Ib)*Dnint(dY*InvBoxSize(Ib))
                     dZ = dZ - BoxSize(Ib)*Dnint(dZ*InvBoxSize(Ib))

                     R2 = dX*dX + dY*dY + dZ*dZ

                     IF(R2.LT.R_Cut_Max_2(Ib)) THEN

                        R  = dsqrt(R2)
                        It = TypeAtom(Tmi,ii)
                        Jt = TypeAtom(Tmj,jj)

                        IF(L_Frac(Imol).OR.L_Frac(Jmol)) THEN
                           IF(L_LJ(It,Jt)) THEN
                              IF(R2.LT.R_Cut_LJ_2(Ib)) THEN
                                 Is  = 1.0d0/Sigma_2(It,Jt)
                                 R0 = (1.0d0/((R2*Is)**(C_LJ/2.0d0) + Off))**(6.0d0/C_LJ)
                                 IF(L_LJ_Shift) THEN
                                    R0c = (1.0d0/((R_Cut_LJ_2(Ib)*Is)**(C_LJ/2.0d0) + Off))**(6.0d0/C_LJ)
                                    E_LJ_Inter = E_LJ_Inter + Myl*Epsilon(It,Jt)*(R0*(R0-1.0d0)-R0c*(R0c-1.0d0))

                                    IF(L_dUdl) THEN
                                       IF(L_Frac(Imol).AND.L_Frac(Jmol)) THEN
                                          dULJ_dl  = dULJ_dl + dlLJ_dlf*8.0d0*Epsilon(It,Jt)*Lambda_LJj*R0*(R0-1.0d0+
     &                                       ((6.0d0*Myl*Alpha_Offset_LJ)/(((R2*Is)**(C_LJ*0.5d0)+Off)*C_LJ))*(2.0d0*R0-1.0d0))
                                          dULJ_dlc = dlLJ_dlf*8.0d0*Epsilon(It,Jt)*Lambda_LJj*R0c*(R0c-1.0d0+((6.0d0*Myl*
     &                                       Alpha_Offset_LJ)/(((R_Cut_LJ_2(1)*Is)**(C_LJ*0.5d0)+Off)*C_LJ))*(2.0d0*R0c-1.0d0))
                                          dULJ_dl  = dULJ_dl - dULJ_dlc
                                       ELSE
                                          dULJ_dl  = dULJ_dl + dlLJ_dlf*4.0d0*Epsilon(It,Jt)*R0*(R0-1.0d0+((6.0d0*Myl*
     &                                       Alpha_Offset_LJ)/(((R2*Is)**(C_LJ*0.5d0)+Off)*C_LJ))*(2.0d0*R0-1.0d0))
                                          dULJ_dlc = dlLJ_dlf*4.0d0*Epsilon(It,Jt)*R0c*(R0c-1.0d0+((6.0d0*Myl*Alpha_Offset_LJ)/
     &                                       (((R_Cut_LJ_2(1)*Is)**(C_LJ*0.5d0)+Off)*C_LJ))*(2.0d0*R0c-1.0d0))
                                          dULJ_dl  = dULJ_dl - dULJ_dlc
                                       END IF
                                    END IF

                                 ELSE
                                    E_LJ_Inter = E_LJ_Inter + Myl*Epsilon(It,Jt)*(R0*(R0-1.0d0))

                                    IF(L_dUdl) THEN
                                       IF(L_Frac(Imol).AND.L_Frac(Jmol)) THEN
                                          dULJ_dl  = dULJ_dl + dlLJ_dlf*8.0d0*Epsilon(It,Jt)*Lambda_LJj*R0*(R0-1.0d0+
     &                                       ((6.0d0*Myl*Alpha_Offset_LJ)/(((R2*Is)**(C_LJ*0.5d0)+Off)*C_LJ))*(2.0d0*R0-1.0d0))
                                       ELSE
                                          dULJ_dl  = dULJ_dl + dlLJ_dlf*4.0d0*Epsilon(It,Jt)*R0*(R0-1.0d0+((6.0d0*Myl*
     &                                       Alpha_Offset_LJ)/(((R2*Is)**(C_LJ*0.5d0)+Off)*C_LJ))*(2.0d0*R0-1.0d0))
                                       END IF
                                    END IF

                                 END IF
                              END IF
                           END IF

                           IF(L_EL(It,Jt)) THEN
                              IF(R2.LT.R_Cut_EL_2(Ib)) THEN
                                 IF(L_Ewald(Ib)) THEN
                                    E_EL_Real = E_EL_Real + Myc*Q(It)*Q(Jt)*(Dderfc(Alpha_EL(Ib)*(R + Ofc))/(R + Ofc))

                                    IF(L_dUdl) THEN
                                       XR    = (Alpha_Offset_EL/((R + Ofc)**2.0d0))*(((R + Ofc)*(2.0d0*Alpha_EL(1)/dsqrt(OnePi))*
     &                                    dexp(-1.0d0*(Alpha_EL(1)**2.0d0)*((R+Ofc)**2.0d0)))+Dderfc(Alpha_EL(1)*(R + Ofc)))
                                       IF(L_Frac(Imol).AND.L_Frac(Jmol)) THEN
                                          dUELReal_dl = dUELReal_dl + dlEL_dlf*2.0d0*Lambda_ELj*Q(It)*Q(Jt)*
     &                                       ((Dderfc(Alpha_EL(Ib)*(R + Ofc))/(R + Ofc))+(Myc*XR))
                                       ELSE
                                          dUELReal_dl = dUELReal_dl + dlEL_dlf*Q(It)*Q(Jt)*
     &                                       ((Dderfc(Alpha_EL(Ib)*(R + Ofc))/(R + Ofc))+(Myc*XR))
                                       END IF
                                    END IF

                                 ELSEIF(L_Wolf(Ib)) THEN
                                    E_EL_Real = E_EL_Real + Myc*Q(It)*Q(Jt)*(Dderfc(Alpha_EL(Ib)*(R + Ofc))/(R + Ofc)
     &                                         - (Dderfc(Alpha_EL(Ib)*(R_Cut_EL(Ib) + Ofc))/(R_Cut_EL(Ib) + Ofc)))

                                    IF(L_dUdl) THEN
                                       XR    = (Alpha_Offset_EL/((R + Ofc)**2.0d0))*(((R + Ofc)*(2.0d0*Alpha_EL(1)/dsqrt(OnePi))*
     &                                    dexp(-1.0d0*(Alpha_EL(1)**2.0d0)*((R+Ofc)**2.0d0)))+Dderfc(Alpha_EL(1)*(R + Ofc)))
                                       XRcut = (Alpha_Offset_EL/((R_Cut_EL(1) + Ofc)**2.0d0))*(((R_Cut_EL(1) + Ofc)*
     &                                    (2.0d0*Alpha_EL(1)/dsqrt(OnePi))*dexp(-1.0d0*(Alpha_EL(1)**2.0d0)*
     &                                    ((R_Cut_EL(1) + Ofc)**2.0d0)))+Dderfc(Alpha_EL(1)*(R_Cut_EL(1)+Ofc)))
                                       IF(L_Frac(Imol).AND.L_Frac(Jmol)) THEN
                                          dUELReal_dl = dUELReal_dl + dlEL_dlf*2.0d0*Lambda_ELj*Q(It)*Q(Jt)*((Dderfc(Alpha_EL(1)*
     &                                       (R+Ofc))/(R+Ofc))-(Dderfc(Alpha_EL(1)*(R_Cut_EL(1)+Ofc))/
     &                                       (R_Cut_EL(1)+Ofc))+(Myc*(XR-XRcut)))
                                       ELSE
                                          dUELReal_dl = dUELReal_dl + dlEL_dlf*Q(It)*Q(Jt)*((Dderfc(Alpha_EL(1)*(R+Ofc))/(R+Ofc))-
     &                                       (Dderfc(Alpha_EL(1)*(R_Cut_EL(1)+Ofc))/(R_Cut_EL(1)+Ofc))+(Myc*(XR-XRcut)))
                                       END IF
                                    END IF

                                 ELSEIF(L_WolfFG(Ib)) THEN
                                    E_EL_Real = E_EL_Real + Myc*Q(It)*Q(Jt)*(Dderfc(Alpha_EL(Ib)*(R + Ofc))/(R + Ofc)
     &                                         - (Dderfc(Alpha_EL(Ib)*(R_Cut_EL(Ib) + Ofc))/(R_Cut_EL(Ib) + Ofc))
     &                                         + FG_Factor(Ib)*(R - R_Cut_EL(Ib)))

                                    IF(L_dUdl) THEN
                                       XR    = (Alpha_Offset_EL/((R + Ofc)**2.0d0))*(((R + Ofc)*(2.0d0*Alpha_EL(1)/dsqrt(OnePi))*
     &                                    dexp(-1.0d0*(Alpha_EL(1)**2.0d0)*((R+Ofc)**2.0d0)))+Dderfc(Alpha_EL(1)*(R + Ofc)))
                                       XRcut = (Alpha_Offset_EL/((R_Cut_EL(1) + Ofc)**2.0d0))*(((R_Cut_EL(1) + Ofc)*
     &                                    (2.0d0*Alpha_EL(1)/dsqrt(OnePi))*dexp(-1.0d0*(Alpha_EL(1)**2.0d0)*
     &                                    ((R_Cut_EL(1) + Ofc)**2.0d0)))+Dderfc(Alpha_EL(1)*(R_Cut_EL(1)+Ofc)))
                                       IF(L_Frac(Imol).AND.L_Frac(Jmol)) THEN
                                          dUELReal_dl = dUELReal_dl + dlEL_dlf*2.0d0*Lambda_ELj*Q(It)*Q(Jt)*((Dderfc(Alpha_EL(1)*
     &                                       (R+Ofc))/(R+Ofc))-(Dderfc(Alpha_EL(1)*(R_Cut_EL(1)+Ofc))/
     &                                       (R_Cut_EL(1)+Ofc))+(Myc*(XR-XRcut))+(FG_Factor(1)*(R - R_Cut_EL(1))))
                                       ELSE
                                          dUELReal_dl = dUELReal_dl + dlEL_dlf*Q(It)*Q(Jt)*((Dderfc(Alpha_EL(1)*(R+Ofc))/(R+Ofc))-
     &                                       (Dderfc(Alpha_EL(1)*(R_Cut_EL(1)+Ofc))/(R_Cut_EL(1)+Ofc))+(Myc*(XR-XRcut))+
     &                                       (FG_Factor(1)*(R - R_Cut_EL(1))))
                                       END IF
                                    END IF

                                 ELSE !none
                                    E_EL_Real = E_EL_Real + Myc*Q(It)*Q(Jt)/(R + Ofc)
                                 END IF
                              END IF
                           END IF

                        ELSE

                           IF(R2.LT.R_Min_2(It,Jt)) THEN
                              L_Overlap_Inter = .true.
                              RETURN
                           END IF

                           IF(L_LJ(It,Jt)) THEN
                              IF(R2.LT.R_Cut_LJ_2(Ib)) THEN
                                 R6 = Sigma_2(It,Jt)/R2
                                 R6 = R6*R6*R6
                                 E_LJ_Inter = E_LJ_Inter + Epsilon(It,Jt)*(R6*(R6-1.0d0)) + U_LJ_Shift(It,Jt,Ib)
                              END IF
                           END IF

                           IF(L_EL(It,Jt)) THEN
                              IF(R2.LT.R_Cut_EL_2(Ib)) THEN
                                 IF(L_Ewald(Ib)) THEN
                                    E_EL_Real = E_EL_Real + Q(It)*Q(Jt)*(dderfc(Alpha_EL(Ib)*R)/R)
                                 ELSEIF(L_Wolf(Ib)) THEN
                                    E_EL_Real = E_EL_Real + Q(It)*Q(Jt)*(dderfc(Alpha_EL(Ib)*R)/R
     &                                         - U_EL_Shift(Ib))
                                 ELSEIF(L_WolfFG(Ib)) THEN
                                    E_EL_Real = E_EL_Real + Q(It)*Q(Jt)*(dderfc(Alpha_EL(Ib)*R)/R
     &                                         - U_EL_Shift(Ib)
     &                                         + FG_Factor(Ib)*(R - R_Cut_EL(Ib)))
                                 ELSE !none
                                    E_EL_Real = E_EL_Real + Q(It)*Q(Jt)/R
                                 END IF
                              END IF
                           END IF
                        END IF
                     END IF
                  END DO
               END DO
            END DO
         END DO
      END IF

      DO I=1,N_MolInBox(Ib)
         Imol = I_MolInBox(Ib,I)
         Tmi  = TypeMol(Imol)

         IF(L_Frac(Imol)) THEN
            CALL interactionlambda(Imol,Myl,Myc,Myi,dlLJ_dlf,dlEL_dlf)
            Mycd= Myc
            Myc = Myc*Myc
            Off = Alpha_Offset_LJ*(1.0d0 - Myl)
            Ofc = Alpha_Offset_EL*(1.0d0 - Myc)
         END IF

         DO ii=1,N_AtomInMolType(Tmi)-1
            DO jj=ii+1,N_AtomInMolType(Tmi)

               dX = X(Imol,ii)-X(Imol,jj)
               dY = Y(Imol,ii)-Y(Imol,jj)
               dZ = Z(Imol,ii)-Z(Imol,jj)

               R2 = dX*dX + dY*dY + dZ*dZ
               R  = dsqrt(R2)

               It = TypeAtom(Tmi,ii)
               Jt = TypeAtom(Tmi,jj)

               IF(.NOT.L_IdealGas(Ib)) THEN
                  IF(L_EL(It,Jt)) THEN

                     IF(L_Frac(Imol)) THEN
                        IF(R2.LT.R_Cut_EL_2(Ib)) THEN
                           IF(L_Ewald(Ib)) THEN
                              E_EL_Excl = E_EL_Excl + Myc*Q(It)*Q(Jt)*((Dderfc(Alpha_EL(Ib)*(R + Ofc))-1.0d0)/(R + Ofc))

                              IF(L_dUdl) THEN
                                 XR    = (Alpha_Offset_EL/((R + Ofc)**2.0d0))*(((R + Ofc)*(2.0d0*Alpha_EL(1)/dsqrt(OnePi))*
     &                              dexp(-1.0d0*(Alpha_EL(1)**2.0d0)*((R+Ofc)**2.0d0)))+Dderfc(Alpha_EL(1)*(R + Ofc)))
                                 dUELExcl_dl = dUELExcl_dl + dlEL_dlf*2.0d0*Mycd*Q(It)*Q(Jt)*((Dderfc(Alpha_EL(1)*(R+Ofc))/
     &                              (R + Ofc))+(Myc*XR)-((R + Ofc + Alpha_Offset_EL*Myc)/((R + Ofc)**2.0d0)))
                              END IF

                           ELSEIF(L_Wolf(Ib)) THEN
                              E_EL_Excl = E_EL_Excl + Myc*Q(It)*Q(Jt)*((Dderfc(Alpha_EL(Ib)*(R + Ofc))-1.0d0)/(R + Ofc)
     &                                  - (Dderfc(Alpha_EL(Ib)*(R_Cut_EL(Ib) + Ofc))/(R_Cut_EL(Ib) + Ofc)))

                              IF(L_dUdl) THEN
                                 XR    = (Alpha_Offset_EL/((R + Ofc)**2.0d0))*(((R + Ofc)*(2.0d0*Alpha_EL(1)/dsqrt(OnePi))*
     &                              dexp(-1.0d0*(Alpha_EL(1)**2.0d0)*((R+Ofc)**2.0d0)))+Dderfc(Alpha_EL(1)*(R + Ofc)))
                                 XRcut = (Alpha_Offset_EL/((R_Cut_EL(1) + Ofc)**2.0d0))*(((R_Cut_EL(1) + Ofc)*(2.0d0*Alpha_EL(1)/
     &                              dsqrt(OnePi))*dexp(-1.0d0*(Alpha_EL(1)**2.0d0)*((R_Cut_EL(1) + Ofc)**2.0d0)))+
     &                              Dderfc(Alpha_EL(1)*(R_Cut_EL(1)+Ofc)))
                                 dUELExcl_dl = dUELExcl_dl + dlEL_dlf*2.0d0*Mycd*Q(It)*Q(Jt)*((Dderfc(Alpha_EL(1)*(R + Ofc))/
     &                              (R + Ofc))-(Dderfc(Alpha_EL(1)*(R_Cut_EL(1) + Ofc))/(R_Cut_EL(1) + Ofc))+(Myc*(XR-XRcut))-
     &                              ((R + Ofc + Alpha_Offset_EL*Myc)/((R+Ofc)**2.0d0)))
                              END IF

                           ELSEIF(L_WolfFG(Ib)) THEN
                              E_EL_Excl = E_EL_Excl + Myc*Q(It)*Q(Jt)*((Dderfc(Alpha_EL(Ib)*(R + Ofc))-1.0d0)/(R + Ofc)
     &                                  - (Dderfc(Alpha_EL(Ib)*(R_Cut_EL(Ib) + Ofc))/(R_Cut_EL(Ib) + Ofc)))

                              IF(L_dUdl) THEN
                                 XR    = (Alpha_Offset_EL/((R + Ofc)**2.0d0))*(((R + Ofc)*(2.0d0*Alpha_EL(1)/dsqrt(OnePi))*
     &                              dexp(-1.0d0*(Alpha_EL(1)**2.0d0)*((R+Ofc)**2.0d0)))+Dderfc(Alpha_EL(1)*(R + Ofc)))
                                 XRcut = (Alpha_Offset_EL/((R_Cut_EL(1) + Ofc)**2.0d0))*(((R_Cut_EL(1) + Ofc)*(2.0d0*Alpha_EL(1)/
     &                              dsqrt(OnePi))*dexp(-1.0d0*(Alpha_EL(1)**2.0d0)*((R_Cut_EL(1) + Ofc)**2.0d0)))+
     &                              Dderfc(Alpha_EL(1)*(R_Cut_EL(1)+Ofc)))
                                 dUELExcl_dl = dUELExcl_dl + dlEL_dlf*2.0d0*Mycd*Q(It)*Q(Jt)*((Dderfc(Alpha_EL(1)*(R + Ofc))/
     &                              (R + Ofc))-(Dderfc(Alpha_EL(1)*(R_Cut_EL(1) + Ofc))/(R_Cut_EL(1) + Ofc))+(Myc*(XR-XRcut))-
     &                              ((R + Ofc + Alpha_Offset_EL*Myc)/((R+Ofc)**2.0d0)))
                              END IF

                           END IF
                        END IF
                     ELSE
                        IF(R2.LT.R_Cut_EL_2(Ib)) THEN
                           IF(L_Ewald(Ib)) THEN
                              E_EL_Excl = E_EL_Excl + Q(It)*Q(Jt)*((Dderfc(Alpha_EL(Ib)*R)-1.0d0)/R)
                           ELSEIF(L_Wolf(Ib)) THEN
                              E_EL_Excl = E_EL_Excl + Q(It)*Q(Jt)*((Dderfc(Alpha_EL(Ib)*R)-1.0d0)/R
     &                                  - U_EL_Shift(Ib))
                           ELSEIF(L_WolfFG(Ib)) THEN
                              E_EL_Excl = E_EL_Excl + Q(It)*Q(Jt)*((Dderfc(Alpha_EL(Ib)*R)-1.0d0)/R
     &                                  - U_EL_Shift(Ib))
                           END IF
                        END IF
                     END IF
                  END IF
               END IF



               IF(L_Intra(Tmi,ii,jj)) THEN
C                  IF(L_Frac(Imol)) THEN
C
C                     IF(L_Frac_Intra_LJ(Tmi,ii,jj)) THEN
C                        Is = 1.0d0/Sigma_2(It,Jt)
C                        R0 = (1.0d0/((R2*Is)**(C_LJ/2.0d0) + Off))**(6.0d0/C_LJ)
C                        E_LJ_Intra = E_LJ_Intra + Myl*Epsilon(It,Jt)*(Scale_LJ_12(Tmi,ii,jj)*R0*R0 - Scale_LJ_6(Tmi,ii,jj)*R0)
C                     ELSE
C                        R6 = Sigma_2(It,Jt)/R2
C                        R6 = R6*R6*R6
C                        E_LJ_Intra = E_LJ_Intra + Epsilon(It,Jt)*(Scale_LJ_12(Tmi,ii,jj)*R6*R6 - Scale_LJ_6(Tmi,ii,jj)*R6)
C                     END IF
C
C                     IF(L_EL(It,Jt)) THEN
C                        IF(L_Frac_Intra_EL(Tmi,ii,jj)) THEN
C                           E_EL_Intra = E_EL_Intra + Myc*Scale_EL(Tmi,ii,jj)*Q(It)*Q(Jt)/(R + Ofc)
C                        ELSE
C                           E_EL_Intra = E_EL_Intra + Scale_EL(Tmi,ii,jj)*Q(It)*Q(Jt)/R
C                        END IF
C                     END IF

C                  ELSE

                     IF(R2.LT.R_Min_2(It,Jt)) THEN
                        L_Overlap_Intra = .true.
                        RETURN
                     END IF

                     R6 = Sigma_2(It,Jt)/R2
                     R6 = R6*R6*R6
                     E_LJ_Intra = E_LJ_Intra + Epsilon(It,Jt)*(Scale_LJ_12(Tmi,ii,jj)*R6*R6 - Scale_LJ_6(Tmi,ii,jj)*R6)

                     IF(L_EL(It,Jt)) THEN
                        E_EL_Intra = E_EL_Intra + Scale_EL(Tmi,ii,jj)*Q(It)*Q(Jt)/R
                     END IF

C                  END IF

               END IF

            END DO
         END DO


CCC   Bending Energy
         DO J=1,N_BendingInMolType(Tmi)
            DO jj=1,3
               Xb(jj) = X(Imol,BendingList(Tmi,J,jj))
               Yb(jj) = Y(Imol,BendingList(Tmi,J,jj))
               Zb(jj) = Z(Imol,BendingList(Tmi,J,jj))
            END DO

            CALL Calculate_Bending(TypeBending(Tmi,J),Ubend,Xb,Yb,Zb)

CCC   Comment out if intramolecular interactions should be scaled.
C            IF(L_Frac(Imol).AND.L_Frac_Bending(Tmi,J)) THEN
C               CALL interactionlambda(Imol,Myl,Myc,Myi,dlLJ_dlf,dlEL_dlf)
C               Ubend = Myi*Ubend
C            END IF

            E_Bending = E_Bending + Ubend

         END DO


CCC   Torsion Energy
         DO J=1,N_TorsionInMolType(Tmi)
            DO jj=1,4
               Xt(jj)=X(Imol,TorsionList(Tmi,J,jj))
               Yt(jj)=Y(Imol,TorsionList(Tmi,J,jj))
               Zt(jj)=Z(Imol,TorsionList(Tmi,J,jj))
            END DO

            CALL Calculate_Torsion(TypeTorsion(Tmi,J),Utors,Xt,Yt,Zt)
            
CCC   Comment out if intramolecular interactions should be scaled.
C            IF(L_Frac(Imol).AND.L_Frac_Torsion(Tmi,J)) THEN
C               CALL interactionlambda(Imol,Myl,Myc,Myi,dlLJ_dlf,dlEL_dlf)
C               Utors = Myi*Utors
C            END IF

            E_Torsion = E_Torsion + Utors

         END DO

      END DO

      E_LJ_Inter = 4.0d0*E_LJ_Inter
      E_LJ_Intra = 4.0d0*E_LJ_Intra
      E_EL_Real  = R4pie*E_EL_Real
      E_EL_Intra = R4pie*E_EL_Intra
      E_EL_Excl  = R4pie*E_EL_Excl
      dU_dl      = dULJ_dl + R4pie*dUELReal_dl + R4pie*dUELExcl_dl

      RETURN

      END
