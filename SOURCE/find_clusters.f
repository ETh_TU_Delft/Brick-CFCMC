      SUBROUTINE Find_Clusters(Ncluster,Nmolecincluster,Imolecincluster)
      implicit none

      include "global_variables.inc"

      integer Ib,I,J,Imol,Jmol,Ncluster,Inewincluster(MaxMolInCluster),
     &  Nmolecincluster(MaxCluster),Imolecincluster(MaxCluster,MaxMolInCluster),
     &  Nnewincluster,Kmol,K,M,Ilist(MaxMolInCluster),Nlist,Inewinclusternew(MaxMolInCluster)
      logical Lincluster(MaxMol)

      Ib=Ibvapor

      Ncluster=0

      DO I=1,MaxMol
         Lincluster(I) = .false.
      END DO

      DO I=1,MaxCluster
         DO J=1,MaxMolInCluster
            Imolecincluster(I,J) = -10
         END DO
         Nmolecincluster(I) = 0
      END DO

      DO I=1,N_MolInBox(Ib)

         Imol = I_MolInBox(Ib,I)

         IF(Lincluster(Imol)) CYCLE

         Ncluster = Ncluster + 1

         Nnewincluster = 1
         Inewincluster(1) = Imol

         Nmolecincluster(Ncluster) = 1
         Imolecincluster(Ncluster,1) = Imol

         Lincluster(Imol) = .true.

  1      CONTINUE
         IF(Nnewincluster.NE.0) THEN

            M = 0

            DO J=1,Nnewincluster
               Jmol = Inewincluster(J)
               CALL Find_Neighbors(Jmol,Nlist,Ilist)

               DO K=1,Nlist
                  Kmol = Ilist(K)
                  IF(.NOT.Lincluster(Kmol)) THEN
                     Nmolecincluster(Ncluster) = Nmolecincluster(Ncluster) + 1
                     Imolecincluster(Ncluster,Nmolecincluster(Ncluster)) = Kmol
                     Lincluster(Kmol) = .true.

                     M = M + 1
                     Inewinclusternew(M) = Kmol
                  END IF
               END DO
            END DO

            Nnewincluster = M
            DO J=1,M
               Inewincluster(J)=Inewinclusternew(J)
            END DO

            GO TO 1

         END IF

      END DO

      RETURN
      END




      SUBROUTINE Find_Neighbors(Imol,Nlist,Ilist)
      implicit none

C     Check if two particles have a center of mass distance smaller than Rcluster
      include "global_variables.inc"

      integer           Imol,Jmol,Nlist,Ilist(MaxMolInCluster),Ib,I
      double precision  R2,dX,dY,dZ

      Ib    = Ibvapor
      Nlist = 0

      DO I=1,MaxMolInCluster
         Ilist(I) = 0
      END DO

      DO I=1,N_MolInBox(Ib)
         Jmol = I_MolInBox(Ib,I)
         IF(Jmol.EQ.Imol) CYCLE

         dX = XCM(Imol) - XCM(Jmol)
         dY = YCM(Imol) - YCM(Jmol)
         dZ = ZCM(Imol) - ZCM(Jmol)

         dX = dX - BoxSize(Ib)*Dnint(dX*InvBoxSize(Ib))
         dY = dY - BoxSize(Ib)*Dnint(dY*InvBoxSize(Ib))
         dZ = dZ - BoxSize(Ib)*Dnint(dZ*InvBoxSize(Ib))

         R2 = dX*dX + dY*dY + dZ*dZ

         IF(R2.LT.R_Cluster_2) THEN
            Nlist = Nlist + 1
            Ilist(Nlist) = Jmol
         END IF

      END DO

      RETURN
      END
