      SUBROUTINE Write_Averages(Ichoice,Icycle)
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"

      integer Ichoice,Icycle,Ib,Tm
      double precision InvAvR

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                Open and initialize files                 C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      IF(Ichoice.EQ.0) THEN
         OPEN(71,file="OUTPUT/av_density.dat")
         OPEN(72,file="OUTPUT/av_number_of_molecules.dat")
         OPEN(73,file="OUTPUT/av_volume.dat")
         OPEN(74,file="OUTPUT/av_energy.dat")

         WRITE(71,'(A)') "# Cycle, ((Average density(Ib,Tm), Tm=1,N_MolType), Ib=1,N_Box)"
         WRITE(72,'(A)') "# Cycle, ((Average number_of_molecules(Ib,Tm), Tm=1,N_MolType), Ib=1,N_Box)"
         WRITE(73,'(A)') "# Cycle, (Average volume(Ib), Ib=1,N_Box)"
         WRITE(74,'(A)') "# Average energies per box"
         WRITE(74,'(A)') "# Cycle, LJ_Inter, LJ_tail, LJ_Inter_Total, EL_Real, EL_Excl, EL_Self, EL_Four,
     &                    EL_Inter, LJ_Intra, EL_Intra, E_Bending, E_Torsion, E_Total"

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                     Write to files                       C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      ELSEIF(Ichoice.EQ.1) THEN

         InvAvR = 1.0d0/AvR

         WRITE(71,'(i10,100e20.10e3)') Icycle, ((AvR_Densityptpb(Ib,Tm)*MolarMass(Tm)*Mconv*InvAvR, Tm=1,N_MolType), Ib=1,N_Box)

         WRITE(72,'(i10,100e20.10e3)') Icycle, ((AvR_Nmptpb(Ib,Tm)*InvAvR, Tm=1,N_MolType), Ib=1,N_Box)

         WRITE(73,'(i10,2e20.10e3)') Icycle, (AvR_Volume(Ib)*InvAvR, Ib=1,N_Box)

         WRITE(74,'(i10,$)') Icycle
         DO Ib=1,N_Box
            WRITE(74,'(2e20.10e3,$)')  AvR_U_LJ_Inter(Ib)*InvAvR,AvR_U_LJ_Tail(Ib)*InvAvR
            WRITE(74,'(1e20.10e3,$)') (AvR_U_LJ_Inter(Ib) + AvR_U_LJ_Tail(Ib))*InvAvR
            WRITE(74,'(2e20.10e3,$)')  AvR_U_EL_Real(Ib)*InvAvR, AvR_U_EL_Excl(Ib)*InvAvR
            WRITE(74,'(2e20.10e3,$)')  AvR_U_EL_Self(Ib)*InvAvR, AvR_U_EL_Four(Ib)*InvAvR
            WRITE(74,'(1e20.10e3,$)') (AvR_U_EL_Real(Ib)+AvR_U_EL_Excl(Ib)+AvR_U_EL_Self(Ib)+AvR_U_EL_Four(Ib))*InvAvR
            WRITE(74,'(2e20.10e3,$)')  AvR_U_LJ_Intra(Ib)*InvAvR, AvR_U_EL_Intra(Ib)*InvAvR
            WRITE(74,'(2e20.10e3,$)')  AvR_U_Bending_Total(Ib)*InvAvR, AvR_U_Torsion_Total(Ib)*InvAvR
            WRITE(74,'(1e20.10e3,$)') (AvR_U_LJ_Inter(Ib)+AvR_U_LJ_Tail(Ib)+AvR_U_EL_Real(Ib)+AvR_U_EL_Excl(Ib)+AvR_U_EL_Self(Ib)
     &       +AvR_U_EL_Four(Ib)+AvR_U_LJ_Intra(Ib)+AvR_U_EL_Intra(Ib)+AvR_U_Bending_Total(Ib)+AvR_U_Torsion_Total(Ib))*InvAvR
         END DO
         WRITE(74,*)

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                      Close files                         C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      ELSEIF(Ichoice.EQ.2) THEN

         CLOSE(71)
         CLOSE(72)
         CLOSE(73)
         CLOSE(74)

      END IF

      RETURN
      END
