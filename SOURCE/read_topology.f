      SUBROUTINE Read_Topology
      implicit none

      include "global_variables.inc"
      include "energy.inc"
      include "output.inc"
      include "settings.inc"
      include "trial_moves.inc"

      integer           Tm,I,J,Ib,Tf,N_MolTypeInReaction,ii,jj,Ireac,N_stoiprev,io,Ipair,
     &                  Tmi,Tmj,Nintra,Ifrac,Rs,N_stoi,K,M_MolInFrac,StartInBox
      double precision  Qtotal,Rcluster
      character*24      Catom,Ctors,Cbending,Cwhat
      character*24      Cmol,Cmoli,Cmolj,Cmolarray(9)
      character*100     Cstring,Cstring_temp
      character*1       Ccheck
      character*10      C_FracType
      character         dummy
      logical           Lmatch,Lfile_exist,L_LJ_Inter,L_EL_Inter,L_No_Warnings,
     &                  L_Print_Warning,L_Check_CFC_Trial_Move(4)

      WRITE(6,'(A)') "Topology"
      L_No_Warnings = .true.

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                       Initialize                         C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      DO Ib=1,2
         DO Tm=1,MaxMolType
            Nmptpb(Ib,Tm) = 0
         END DO
      END DO

      DO Tm=1,MaxMolType
         DO ii=1,MaxAtom
            DO jj=1,MaxAtom
               L_Intra(Tm,ii,jj)     = .false.
               Scale_LJ_6(Tm,ii,jj)  = 0.0d0
               Scale_LJ_12(Tm,ii,jj) = 0.0d0
               Scale_EL(Tm,ii,jj)    = 0.0d0

               L_Frac_Intra_LJ(Tm,ii,jj) = .false.
               L_Frac_Intra_EL(Tm,ii,jj) = .false.
            END DO
         END DO

         DO I=1,MaxBendingInMolType
            L_Frac_Bending(Tm,I) = .false.
         END DO

         DO I=1,MaxTorsionInMolType
            L_Frac_Torsion(Tm,I) = .false.
         END DO

         Qpartition(Tm)     = 0.0d0
         Fugacity_Coeff(Tm) = 1.0d0
      END DO

      DO Ipair=1,MaxMolTypePair
         CounterType(Ipair,1) = 0
         CounterType(Ipair,2) = 0
      END DO

      DO Ifrac=1,MaxFrac
         Box_Frac(Ifrac)          = 0
         N_MolInFrac(Ifrac)       = 0
         Reaction_Frac(Ifrac)     = 0
         ReactionStep_Frac(Ifrac) = 1
         Name_Frac(Ifrac)         = " "
         DO Tm=1,MaxMolType
            N_MolOfMolTypeInFrac(Tm,Ifrac) = 0
         END DO
      END DO

      DO Ireac=1,MaxReaction
         N_ReactionStep(Ireac) = 0
         DO Rs=1,MaxReactionStep
            N_MolInReactionStep(Ireac,Rs) = 0
            DO I=1,MaxMolInFrac
               I_MolTypeInReactionStep(Ireac,Rs,I) = 0
            END DO
            DO Tm=1,MaxMolType
               N_MolOfMolTypeInReactionStep(Ireac,Rs,Tm) = 0
            END DO
            Name_ReactionStep(Ireac,Rs) = ""
         END DO
      END DO

      L_LJ_Inter = .false.
      L_EL_Inter = .false.

      L_NVPT = .false.
      L_GE   = .false.
      L_GC   = .false.
      L_RXMC = .false.

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                   Read Topology file                     C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      OPEN(7,file="./INPUT/topology.in")
      READ(7,*)

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                     Read Molecules                       C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      N_MolType = 0
      DO
         READ(7,*,IOSTAT=io,err=999) Ccheck
         IF(Ccheck.EQ."#") EXIT

         N_MolType = N_MolType + 1
         IF(N_MolType.GT.MaxMolType) THEN
            WRITE(6,'(A,A)') ERROR, "N_MolType > MaxMolType"
            STOP
         END IF

         BACKSPACE(7)
         READ(7,*,IOSTAT=io,err=10) C_MolType(N_MolType), (Nmptpb(Ib,N_MolType), Ib=1,N_Box)

         DO I=1,N_MolType-1
            IF(C_MolType(I).EQ.C_MolType(N_MolType)) THEN
               WRITE(6,'(A,A)') ERROR, "Each molecule type should have a unique name in topology.in"
               STOP
            END IF
         END DO

      END DO

      IF(N_MolType.LE.0) THEN
         WRITE(6,'(A,A)') ERROR, "N_MolType <= 0"
         STOP
      END IF

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC                                                      CCC
CCC                 Read Molecule Files                  CCC
CCC                                                      CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      DO Tm=1,N_MolType
         INQUIRE(file="./INPUT/"//TRIM(C_MolType(Tm)),EXIST=Lfile_exist)
         IF(.NOT.Lfile_exist) THEN
            WRITE(6,'(A,A,A)') ERROR, "Molecule file not present in INPUT folder: ", C_MolType(Tm)
            STOP
         END IF

         OPEN(8,file="./INPUT/"//TRIM(C_MolType(Tm)))

         READ(8,*,iostat=io,err=20) MolarMass(Tm)
         READ(8,*)

         READ(8,*,iostat=io,err=21) N_AtomInMolType(Tm)
         IF(N_AtomInMolType(Tm).LE.0.OR.N_AtomInMolType(Tm).GT.MaxAtom) THEN
            WRITE(6,'(A,A,A)') ERROR, "N_AtomInMolType <= 0 or N_AtomInMolType > MaxAtom. Molecule Type: ", C_MolType(Tm)
            STOP
         END IF

         Qsum(Tm)   = 0.0d0
         Qsum_2(Tm) = 0.0d0
         L_ChargeInMolType(Tm) = .false.

C     Set masses of molecules for Smart Monte Carlo routine         
         IF(Lreducedunits) THEN
            MassOneMolecule(Tm) = N_AtomInMolType(Tm)
         ELSE
            MassOneMolecule(Tm) = 10.0d0*MolarMass(Tm)/R_GasConstant
         END IF

C     Read atoms (types and positions), assign the correct number type and charge
         DO I=1,N_AtomInMolType(Tm)
            READ(8,*,iostat=io,err=22) Catom, Xin(Tm,I), Yin(Tm,I), Zin(Tm,I)
            Lmatch=.false.
            DO J=1,N_AtomType
               IF(Catom.EQ.C_AtomType(J)) THEN
                  TypeAtom(Tm,I)=J
                  Lmatch=.true.
                  IF(L_LJ_in(J)) L_LJ_Inter = .true.
                  IF(L_EL_in(J)) L_EL_Inter = .true.
                  EXIT
               END IF
            END DO
            IF(.NOT.Lmatch) THEN
               WRITE(6,'(A,A,A)') ERROR, "Atomtype not defined in forcefield: ", Catom
               STOP
            END IF
            Qsum(Tm)   = Qsum(Tm)   + Q(TypeAtom(Tm,I))
            Qsum_2(Tm) = Qsum_2(Tm) + Q(TypeAtom(Tm,I))*Q(TypeAtom(Tm,I))
         END DO
         IF(Qsum_2(Tm).GT.zero) THEN
            L_ChargeInMolType(Tm) = .true.
         END IF

         READ(8,'(A1)',iostat=io,err=999) Ccheck
         IF(Ccheck.NE."-") THEN
            WRITE(6,'(A,A,A)') ERROR, "Number of atoms is wrong in molecule ", C_MolType(Tm)
            STOP
         END IF

C     Read bonds between atoms
         READ(8,*,iostat=io,err=23) N_BondInMolType(Tm)
         IF(N_BondInMolType(Tm).LT.0.OR.N_BondInMolType(Tm).GT.MaxBond) THEN
            WRITE(6,'(A,A,A)') ERROR, "N_BondInMolType < 0 or N_BondInMolType > MaxBond. Molecule Type: ", C_MolType(Tm)
            STOP
         END IF
         DO I=1,N_BondInMolType(Tm)
            READ(8,*,iostat=io,err=24) (Bondlist(Tm,I,J), J=1,2)
         END DO
         READ(8,'(A1)',iostat=io,err=999) Ccheck
         IF(Ccheck.NE."-") THEN
            WRITE(6,'(A,A,A)') ERROR, "Number of bonds is wrong in molecule ", C_MolType(Tm)
            STOP
         END IF

C     Read bond bendings in molecule
         READ(8,*,iostat=io,err=25) N_BendingInMolType(Tm)
         IF(N_BendingInMolType(Tm).LT.0.OR.N_BendingInMolType(Tm).GT.MaxBendingInMolType) THEN
            WRITE(6,'(A,A,A)') ERROR,
     &        "N_BendingInMolType < 0 or N_BendingInMolType > MaxBendingInMolType. Molecule Type: ", C_MolType(Tm)
            STOP
         END IF

         DO I=1,N_BendingInMolType(Tm)
            READ(8,*,iostat=io,err=26) Cbending, (BendingList(Tm,I,J), J=1,3)!, L_Frac_Bending(Tm,I)
            Lmatch=.false.
            DO J=1,Nbendingtype
               IF(Cbending.EQ.C_BendingType(J)) THEN
                  TypeBending(Tm,I)=J
                  Lmatch=.true.
                  EXIT
               END IF
            END DO
            IF(.NOT.Lmatch) THEN
               WRITE(6,'(A,A,A)') ERROR, "Bending Type not defined in forcefield: ", Cbending
               STOP
            END IF
         END DO

         READ(8,'(A1)',iostat=io,err=999) Ccheck
         IF(Ccheck.NE."-") THEN
            WRITE(6,'(A,A,A)') ERROR, "Number of Bendings is wrong in molecule ", C_MolType(Tm)
            STOP
         END IF


C     Read possible torsions in molecule
         READ(8,*,iostat=io,err=27) N_TorsionInMolType(Tm)
         IF(N_TorsionInMolType(Tm).LT.0.OR.N_TorsionInMolType(Tm).GT.MaxTorsionInMolType) THEN
            WRITE(6,'(A,A,A)') ERROR, "N_TorsionInMolType < 0 or Ntors > MaxTorsionInMolType. Molecule Type: ", C_MolType(Tm)
            STOP
         END IF

         DO I=1,N_TorsionInMolType(Tm)
            READ(8,*,iostat=io,err=28) Ctors,  (TorsionList(Tm,I,J),  J=1,4)!, L_Frac_Torsion(Tm,I)
            Lmatch=.false.
            DO J=1,Ntorsiontype
               IF(Ctors.EQ.C_TorsionType(J)) THEN
                  TypeTorsion(Tm,I)=J
                  Lmatch=.true.
                  EXIT
               END IF
            END DO
            IF(.NOT.Lmatch) THEN
               WRITE(6,'(A,A,A)') ERROR, "Torsiontype not defined in forcefield: ", Ctors
               STOP
            END IF
         END DO

         READ(8,'(A1)',iostat=io,err=999) Ccheck
         IF(Ccheck.NE."-") THEN
            WRITE(6,'(A,A,A)') ERROR, "Number of torsions is wrong in molecule ", C_MolType(Tm)
            STOP
         END IF

C     Read intramolecular interactions and scaling factors
         READ(8,*,iostat=io,err=29) Nintra

         DO I=1,Nintra
            READ(8,*,iostat=io,err=30) ii, jj, Scale_LJ_12(Tm,ii,jj), Scale_LJ_6(Tm,ii,jj), Scale_EL(Tm,ii,jj)!,
     &                                         !L_Frac_Intra_LJ(Tm,ii,jj), L_Frac_Intra_EL(Tm,ii,jj)
            Scale_LJ_6(Tm,jj,ii)  =  Scale_LJ_6(Tm,ii,jj)
            Scale_LJ_12(Tm,jj,ii) =  Scale_LJ_12(Tm,ii,jj)
            Scale_EL(Tm,jj,ii)    =  Scale_EL(Tm,ii,jj)

CCC   Comment out if intramolecular interactions should be scaled.
C            L_Frac_Intra_LJ(Tm,jj,ii) = L_Frac_Intra_LJ(Tm,ii,jj)
C            L_Frac_Intra_EL(Tm,jj,ii) = L_Frac_Intra_EL(Tm,ii,jj)

            L_Intra(Tm,ii,jj)     =  .true.
            L_Intra(Tm,jj,ii)     =  .true.
         END DO

         CLOSE(8)

      END DO

C     Construct a list of atoms that are connected on 'one' side of a torsion bond.
C     This is used for rotating atoms in a torsion-move.
      CALL Construct_List_for_Bending
      CALL Construct_List_for_Torsion

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC                                                      CCC
CCC     Read Partition Functions and Fugacities          CCC
CCC                                                      CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
  99  READ(7,'(A)',iostat=io,ADVANCE="NO",err=40) CWhat
      BACKSPACE(7)

      IF((Cwhat(1:9).EQ."Partition").OR.(Cwhat(1:9).EQ."partition")) THEN
         READ(7,*)
         DO I=1,N_MolType
            READ(7,*,iostat=io,err=41) Cmol
            IF(Cmol(1:1).EQ."#") EXIT
            CALL Lookup_MoleculeLabel(Cmol,Tm)
            BACKSPACE(7)
            READ(7,*,iostat=io,err=42) Cmol, Qpartition(Tm)
         END DO
         READ(7,*)
         GO TO 99
      ELSEIF((Cwhat(1:8).EQ."Fugacity").OR.(Cwhat(1:8).EQ."fugacity")) THEN
         READ(7,*)
         DO I=1,N_MolType
            READ(7,*,iostat=io,err=43) Cmol
            IF(Cmol(1:1).EQ."#") EXIT
            CALL Lookup_MoleculeLabel(Cmol,Tm)
            BACKSPACE(7)
            READ(7,*,iostat=io,err=44) Cmol, Fugacity_Coeff(Tm)
         END DO
         READ(7,*)
         GO TO 99
      END IF

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC                                                      CCC
CCC               Read Fractional Types                  CCC
CCC                                                      CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      READ(7,*,iostat=io,err=50) N_Frac
      IF((N_Frac.LT.0).OR.(N_Frac.GT.MaxFrac)) THEN
         WRITE(6,'(A,A)') ERROR, "N_Frac < 0 or N_Frac > MaxFrac"
         STOP
      ELSEIF((N_Frac.GT.1).AND.L_dUdl) THEN
         WRITE(6,'(A,A)') ERROR, "dU/dl can only be calculated when there is exactly 1 fractional. 
     &                            Switch off dU/dl or run with only 1 fractional."
      END IF


      READ(7,*)

      DO Ifrac=1,N_Frac

         READ(7,*,iostat=io,err=51) C_FracType, StartInBox, M_MolInFrac
         IF(M_MolInFrac.GT.MaxMolInFrac) THEN
            WRITE(6,'(A,A)') ERROR, "N_MolInFrac > MaxMolInFrac"
            STOP
         END IF
         IF((StartInBox.LE.0).OR.(StartInBox.GE.3)) THEN
            WRITE(6,'(A,A,i2,A)') ERROR, "Initial Box of Fractional number ", Ifrac, "is not 1 or 2"
            STOP
         END IF
         BACKSPACE(7)

         N_MolInFrac(Ifrac) = M_MolInFrac
         Box_Frac(Ifrac)    = StartInBox

         READ(7,*,iostat=io,err=51) C_FracType, StartInBox, M_MolInFrac, (Cmolarray(I), I=1,N_MolInFrac(Ifrac)), Name_Frac(Ifrac)

         DO I=1,N_MolInFrac(Ifrac)
            CALL Lookup_MoleculeLabel(Cmolarray(I),Tm)
            I_MolTypeInFrac(Ifrac,I) = Tm
            N_MolOfMolTypeInFrac(Tm,Ifrac) = N_MolOfMolTypeInFrac(Tm,Ifrac) + 1
         END DO

         IF(C_FracType(1:1).EQ."N") THEN
            Type_Frac(Ifrac) = 1
            L_NVPT = .true.
         ELSEIF(C_FracType(1:2).EQ."GE") THEN
            Type_Frac(Ifrac) = 2
            L_GE = .true.
            IF(N_Box.NE.2) THEN
               WRITE(6,'(A,A)') ERROR, "Fractional of Type GE defined but only 1 Simulation Box defined."
               STOP
            END IF
         ELSEIF(C_FracType(1:2).EQ."GC") THEN
            Type_Frac(Ifrac) = 4
            L_GC = .true.
            IF(N_MolInFrac(Ifrac).NE.1) THEN
               WRITE(6,'(A,A,i2)') ERROR,
     &           "Number of Molecules in Fractional of Type GC should be 1. Please check Fractional ", Ifrac
               STOP
            END IF
         ELSE
            WRITE(6,'(A,i2,A,A)') ERROR, "Fractional Type of Fractional number ", Ifrac, " not defined: ", C_FracType
            STOP
         END IF
      END DO
      
CCC   For du/dlambda anything other than NVPT and Nfrac=1 is not accepted
      IF(L_dUdl.AND.(.NOT.(L_NVPT.AND.N_Frac.EQ.1))) THEN
         WRITE(6,'(A)') ERROR, 
     &   "To calculate dU/dlambda, only one fractional is allowed and that fractional should be of type NVT/NPT"
         STOP
      END IF

      READ(7,*)

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC                                                      CCC
CCC                   Read Reactions                     CCC
CCC                                                      CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      READ(7,*,iostat=io,err=60) N_Reaction

      IF(N_Reaction.LT.0.OR.N_Reaction.GT.MaxReaction) THEN
         WRITE(6,'(A,A)') ERROR, "N_Reaction < 0 or N_Reaction > MaxReaction"
         STOP
      ELSEIF(N_Reaction.GT.0) THEN
         L_RXMC = .true.
      END IF

      DO Ireac=1,N_Reaction

         N_Frac = N_Frac + 1
         Ifrac  = N_Frac

         IF(N_Frac.GT.MaxFrac) THEN
            WRITE(6,'(A,A)') ERROR, "N_Frac > MaxFrac"
            STOP
         END IF

         READ(7,*)
         READ(7,'(i2,A100)',iostat=io,err=61) N_MolTypeInReaction, Cstring
         Cstring_temp=TRIM(Cstring)
         READ(Cstring_temp(LEN(TRIM(Cstring)):LEN(TRIM(Cstring))),*) StartInBox
         READ(7,*)

         IF((StartInBox.LE.0).OR.(StartInBox.GE.3)) THEN
            WRITE(6,'(A,A,i2,A)') ERROR, "Initial Box of Fractional number ", Ifrac, " is not 1 or 2"
            STOP
         END IF

         Type_Frac(Ifrac) = 3
         Box_Frac(Ifrac)  = StartInBox
         Reaction_Frac(Ifrac) = Ireac

         Rs = 0
         N_stoiprev = 0
         Name_Reaction(Ireac) = ""

         DO J=1,N_MolTypeInReaction
            READ(7,*,iostat=io,err=62) Cmol
            CALL Lookup_MoleculeLabel(Cmol,Tm)
            BACKSPACE(7)
            READ(7,*,iostat=io,err=63) Cmol, N_stoi

C      If the sign of the stoichiometric coefficient is changing then a new reaction step is set.
C      If Nprev=0 it means a new reaction is considered and also a new step should be defined.
            IF(N_stoi*N_stoiprev.LE.0) THEN
               N_ReactionStep(Ireac) = N_ReactionStep(Ireac) + 1
               Rs = Rs + 1
               K  = 0
            END IF

            N_MolOfMolTypeInReactionStep(Ireac,Rs,Tm) = abs(N_stoi)
            N_MolInReactionStep(Ireac,Rs) = N_MolInReactionStep(Ireac,Rs) + abs(N_stoi)

            IF(N_MolInReactionStep(Ireac,Rs).GT.MaxMolInFrac) THEN
               WRITE(6,'(A,A)') ERROR, "N_MolInReactionStep > MaxMolInFrac"
               STOP
            END IF

            DO I=1,N_MolOfMolTypeInReactionStep(Ireac,Rs,Tm)
               K = K + 1
               I_MolTypeInReactionStep(Ireac,Rs,K) = Tm
            END DO

            IF(abs(N_stoi).NE.1) THEN
               Name_ReactionStep(Ireac,Rs) = TRIM(Name_ReactionStep(Ireac,Rs)) // Cnum(abs(N_stoi)) // TRIM(C_MolType(Tm)) // "+"
            ELSE
               Name_ReactionStep(Ireac,Rs) = TRIM(Name_ReactionStep(Ireac,Rs)) // TRIM(C_MolType(Tm)) // "+"
            END IF

            N_stoiprev = N_stoi

         END DO

      END DO

C     Remove final "+" in each fractional reaction step name
      DO Ireac=1,N_Reaction
         DO Rs=1,N_ReactionStep(Ireac)
            I=LEN(TRIM(Name_ReactionStep(Ireac,Rs)))
            Name_ReactionStep(Ireac,Rs)(I:I)=""
         END DO
      END DO

C     Create strings with reactions
      DO Ireac=1,N_Reaction
         DO Rs=1,N_ReactionStep(Ireac)
            Name_Reaction(Ireac) = TRIM(Name_Reaction(Ireac)) // " " // TRIM(Name_ReactionStep(Ireac,Rs)) // " <-->"
         END DO
      END DO

      DO Ireac=1,N_Reaction
         I=LEN(TRIM(Name_Reaction(Ireac)))
         Name_Reaction(Ireac)(I-4:I) = "    "
         Name_Reaction(Ireac) = adjustl(Name_Reaction(Ireac))
      END DO

      READ(7,*)
      READ(7,*)
      
CCC   Comment out if intramolecular interactions should be scaled.
C      L_Print_Warning = .false.
C      DO Ifrac=1,N_Frac
C         Tf=Type_Frac(Ifrac)
C         IF(Tf.EQ.3) THEN
C            Ireac=Reaction_Frac(Ifrac)
C            DO Rs=1,N_ReactionStep(Ireac)
C              DO I=1,N_MolInReactionStep(Ireac,Rs)
C                  Tm = I_MolTypeInReactionStep(Ireac,Rs,I)
C                  DO J=1,N_BendingInMolType(Tm)
C                     IF(L_Frac_Bending(Tm,J)) L_Print_Warning = .true.
C                     L_Frac_Bending(Tm,J) = .false.
C                  END DO
C                  DO J=1,N_TorsionInMolType(Tm)
C                     IF(L_Frac_Torsion(Tm,J)) L_Print_Warning = .true.
C                     L_Frac_Torsion(Tm,J) = .false.
C                  END DO
C                  DO ii=1,N_AtomInMolType(Tm)
C                     DO jj=ii,N_AtomInMolType(Tm)
C                        IF(L_Frac_Intra_LJ(Tm,ii,jj)) L_Print_Warning = .true.
C                        IF(L_Frac_Intra_LJ(Tm,jj,ii)) L_Print_Warning = .true.
C                        IF(L_Frac_Intra_EL(Tm,ii,jj)) L_Print_Warning = .true.
C                        IF(L_Frac_Intra_EL(Tm,jj,ii)) L_Print_Warning = .true.
C                        L_Frac_Intra_LJ(Tm,ii,jj) = .false.
C                        L_Frac_Intra_LJ(Tm,jj,ii) = .false.
C                        L_Frac_Intra_EL(Tm,ii,jj) = .false.
C                        L_Frac_Intra_EL(Tm,jj,ii) = .false.
C                     END DO
C                  END DO
C               END DO
C            END DO
C         ELSEIF(Tf.EQ.4) THEN
C            DO I=1,N_MolInFrac(Ifrac)
C               Tm = I_MolTypeInFrac(Ifrac,I)
C               DO J=1,N_BendingInMolType(Tm)
C                  IF(L_Frac_Bending(Tm,J)) L_Print_Warning = .true.
C                  L_Frac_Bending(Tm,J) = .false.
C               END DO
C               DO J=1,N_TorsionInMolType(Tm)
C                  IF(L_Frac_Torsion(Tm,J)) L_Print_Warning = .true.
C                  L_Frac_Torsion(Tm,J) = .false.
C               END DO
C               DO ii=1,N_AtomInMolType(Tm)
C                  DO jj=ii,N_AtomInMolType(Tm)
C                     IF(L_Frac_Intra_LJ(Tm,ii,jj)) L_Print_Warning = .true.
C                     IF(L_Frac_Intra_LJ(Tm,jj,ii)) L_Print_Warning = .true.
C                     IF(L_Frac_Intra_EL(Tm,ii,jj)) L_Print_Warning = .true.
C                     IF(L_Frac_Intra_EL(Tm,jj,ii)) L_Print_Warning = .true.
C                     L_Frac_Intra_LJ(Tm,ii,jj) = .false.
C                     L_Frac_Intra_LJ(Tm,jj,ii) = .false.
C                     L_Frac_Intra_EL(Tm,ii,jj) = .false.
C                     L_Frac_Intra_EL(Tm,jj,ii) = .false.
C                  END DO
C               END DO
C            END DO
C         END IF
C      END DO
C      IF(L_Print_Warning) THEN
C         WRITE(6,'(A,A,$)') WARNING, "Scaling of intramolecular interactions (bending, torsion, LJ and EL)"
C         WRITE(6,'(A)')              " has been switched off for all Molecule Types in Fractional Groups of Type RxMC and GCMC."
C         L_No_Warnings = .false.
C      END IF



CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC                                                      CCC
CCC      Read BoxSize and delta for Volume Change        CCC
CCC        and time step for smart trial moves           CCC
CCC                                                      CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      DO Ib=1,N_Box
         READ(7,*,iostat=io,err=70) dummy, BoxSize(Ib), Delta_Volume(Ib)
         IF(Linit) THEN
            IF(BoxSize(Ib).LT.-zero) THEN
               WRITE(6,'(A,A,i1)') ERROR, "Boxsize is negative of Box ", Ib
               STOP
            END IF
            IF(Delta_Volume(Ib).LT.-zero) THEN
               WRITE(6,'(A,A,i1)') ERROR, "Delta Volume is negative for Box ", Ib
               STOP
            END IF
         END IF

         Volume(Ib) = BoxSize(Ib)*BoxSize(Ib)*BoxSize(Ib)
         InvBoxSize(Ib) = 1.0d0/BoxSize(Ib)

         Delta_ClusterVolume(Ib) = Delta_Volume(Ib)

         IF((Delta_Volume(Ib).GT.0.1d0*Volume(Ib)).AND.Linit) THEN
            L_No_Warnings=.false.
            WRITE(6,'(A,A,i1)') WARNING, "Delta Volume is large compared to the Box size of Box ", Ib
         END IF

      END DO

      READ(7,*)
      READ(7,*)
      READ(7,*)

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CC                                                        CC
CC  Read deltas for translation and rotation trial move   CC
CC                                                        CC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      DO I=1,N_MolType
         READ(7,*,iostat=io,err=80) Cmol
         CALL Lookup_MoleculeLabel(Cmol,Tm)
         BACKSPACE(7)
         READ(7,*,iostat=io,err=81) Cmol, (Delta_Translation(Ib,Tm), Delta_Rotation(Ib,Tm), Ib=1,N_Box)
      END DO

      DO Ib=1,N_Box
         DO Tm=1,N_MolType
            Delta_Rotation(Ib,Tm) = Delta_Rotation(Ib,Tm)*OnePi/180.0d0

            IF(Linit) THEN
               IF(Delta_Translation(Ib,Tm).LT.zero) THEN
                  WRITE(6,'(A,A,i1,A,i2)') ERROR, "Delta Translation is negative in Box ", Ib, "for Molecule Type ", Tm
                  STOP
               ELSEIF(Delta_Translation(Ib,Tm).GT.0.5d0*BoxSize(Ib)) THEN
                  WRITE(6,'(A,A,i1,A,i2)') ERROR,
     &              "Delta Translation is larger than half the Box size of Box ", Ib, "for Molecule Type ", Tm
                  STOP
               END IF
               IF(Delta_Rotation(Ib,Tm).LT.zero) THEN
                  WRITE(6,'(A,A,i1,A,i2)') ERROR, "Delta Rotation is negative in Box ", Ib, "for Molecule Type ", Tm
                  STOP
               END IF
            END IF

         END DO
      END DO

      READ(7,*)
      READ(7,*)
      READ(7,*)

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CC                                                        CC
CC          Read deltas for lambda trial move             CC
CC                                                        CC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      DO Ifrac=1,N_Frac
         READ(7,*,iostat=io,err=82) dummy, (Delta_Lambda(Ib,Ifrac),  Ib=1,N_Box),
     &                              N_LambdaBin(Ifrac), LambdaSwitch(Ifrac)

         DO Ib=1,N_Box
            IF(Delta_Lambda(Ib,Ifrac).LT.-zero) THEN
               WRITE(6,'(A,A,i1,A,i3)') WARNING, "Delta Lambda is negative in Box ", Ib, "for Fractional", Ifrac
            ELSEIF(Delta_Lambda(Ib,Ifrac).GT.1.0d0+zero) THEN
               WRITE(6,'(A,A,i1,A,i3)') WARNING, "Delta Lambda is larger than 1 in Box ", Ib, "for Fractional", Ifrac
            END IF
         END DO

         IF(N_LambdaBin(Ifrac).LE.2) THEN
            WRITE(6,'(A,A,i3)') ERROR, "Number of LambdaBins is 2 or less for Fractional Group", Ifrac
            STOP
         ELSEIF(N_LambdaBin(Ifrac).GT.MaxLambdaBin) THEN
            WRITE(6,'(A,A,i3)') ERROR, "Number of LambdaBins is larger than MaxLambdaBin for Fractional Group", Ifrac
            STOP
         END IF

         IF(LambdaSwitch(Ifrac).LT.-zero) THEN
            WRITE(6,'(A,A,i3)') ERROR, "LambdaSwitch is negative for Fractional Group", Ifrac
            STOP
         ELSEIF(LambdaSwitch(Ifrac).GT.1.0d0+zero) THEN
            WRITE(6,'(A,A,i3)') ERROR, "LambdaSwitch is larger than 1 for Fractional Group", Ifrac
            STOP
         END IF

         IF(Type_Frac(Ifrac).NE.3) THEN
            DO I=1,N_MolInFrac(Ifrac)
               Tm = I_MolTypeInFrac(Ifrac,I)
               IF(L_ChargeInMolType(Tm)) THEN
                  IF(LambdaSwitch(Ifrac).GT.0.9d0) THEN
                     WRITE(6,'(A,A,i3)') WARNING, "LambdaSwitch is large for charged Fractional Group", Ifrac
                     L_No_Warnings=.false.
                     EXIT
                  END IF
               END IF
            END DO
         ELSEIF(Type_Frac(Ifrac).EQ.3) THEN
            Ireac = Reaction_Frac(Ifrac)
            DO Rs=1,N_ReactionStep(Ireac)
               DO I=1,N_MolInReactionStep(Ireac,Rs)
                  Tm = I_MolTypeInReactionStep(Ireac,Rs,I)
                  IF(L_ChargeInMolType(Tm)) THEN
                     IF(LambdaSwitch(Ifrac).GT.0.9d0) THEN
                        WRITE(6,'(A,A,i2,A,i2,A)') WARNING, "LambdaSwitch is large for Fractional Group ",
     &                                              Ifrac, ", Reaction Step ", Rs, " has a charge."
                        L_No_Warnings=.false.
                        EXIT
                     END IF
                  END IF
               END DO
            END DO
         END IF

         IF(LambdaSwitch(Ifrac).LT.1.0d0/dble(N_LambdaBin(Ifrac))) THEN
            LambdaSwitch(Ifrac) = 1.0d0/dble(N_LambdaBin(Ifrac))
         ELSEIF(LambdaSwitch(Ifrac).GT.dble(N_LambdaBin(Ifrac)-1)/dble(N_LambdaBin(Ifrac))) THEN
            LambdaSwitch(Ifrac) = dble(N_LambdaBin(Ifrac)-1)/dble(N_LambdaBin(Ifrac))
         END IF

      END DO

      READ(7,*)


CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC                                                      CCC
CCC     Read Molecule Type Pairs and Cluster Radius      CCC
CCC                                                      CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      READ(7,*,iostat=io,err=90) N_MolTypePair

      IF(N_MolTypePair.LT.0.OR.N_MolTypePair.GT.MaxMolTypePair) THEN
         WRITE(6,'(A,A)') ERROR, "N_MolTypePair < 0 or N_MolTypePair > MaxMolTypePair"
         STOP
      END IF

      DO I=1,N_MolTypePair
         READ(7,*,iostat=io,err=91) Cmoli, Cmolj
         CALL Lookup_MoleculeLabel(Cmoli,Tmi)
         CALL Lookup_MoleculeLabel(Cmolj,Tmj)
         Countertype(I,1) = Tmi
         Countertype(I,2) = Tmj
         C_MolTypePair(I) = TRIM(C_MolType(Tmi)) // "+" // TRIM(C_MolType(Tmj))
      END DO

      READ(7,*)

      READ(7,*,iostat=io,err=92) dummy, dummy, dummy, Rcluster

      R_Cluster_2 = Rcluster*Rcluster


      CLOSE(7)


CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C                                                          C
C Check for charge neutrality of Box and Fractional Types  C
C                                                          C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      DO Ib=1,N_Box
         Qtotal = 0.0d0
         DO Tm=1,N_MolType
            Qtotal = Qtotal + Nmptpb(Ib,Tm)*Qsum(Tm)
         END DO
         IF(abs(Qtotal).GT.zero) THEN
            WRITE(6,'(A,A,i1,A)') ERROR, "Box ", Ib, " is not charge neutral."
            STOP
         END IF
      END DO

C     Check if Fractional Types are charge neutral
      DO Ifrac=1,N_Frac
         Tf = Type_Frac(Ifrac)
         IF(Tf.NE.3) THEN
            Qtotal = 0.0d0
            DO I=1,N_MolInFrac(Ifrac)
               Qtotal = Qtotal + Qsum(I_MolTypeInFrac(Ifrac,I))
            END DO
            IF(abs(Qtotal).GT.zero) THEN
               WRITE(6,'(A,A,i3,A)') ERROR, "Fractional ", Ifrac, "is not charge neutral."
               STOP
            END IF
         ELSEIF(Tf.EQ.3) THEN
            Ireac = Reaction_Frac(Ifrac)
            DO Rs=1,N_ReactionStep(Ireac)
               Qtotal = 0.0d0
               DO I=1,N_MolInReactionStep(Ireac,Rs)
                  Qtotal = Qtotal + Qsum(I_MolTypeInReactionStep(Ireac,Rs,I))
               END DO
               IF(abs(Qtotal).GT.zero) THEN
                  WRITE(6,'(A,A,i2,A,i2,A)') ERROR, "Reaction Step ", Rs, " of Reaction ", Ireac, " is not charge neutral."
                  STOP
               END IF
            END DO
         END IF
      END DO

      IF(L_LJ_Inter.AND.L_EL_Inter) THEN
         DO Ib=1,N_Box
            R_Cut_Max_2(Ib) = max(R_Cut_LJ_2(Ib),R_Cut_EL_2(Ib))
         END DO
      ELSEIF(L_LJ_Inter) THEN
         DO Ib=1,N_Box
            R_Cut_Max_2(Ib) = R_Cut_LJ_2(Ib)
         END DO
      ELSEIF(L_EL_Inter) THEN
         DO Ib=1,N_Box
            R_Cut_Max_2(Ib) = R_Cut_EL_2(Ib)
         END DO
      ELSE
         DO Ib=1,N_Box
            R_Cut_Max_2(Ib) = 0.0d0
         END DO
      END IF

C     Check if box size is large enough for Rcuts
      DO Ib=1,N_Box
         IF(.NOT.L_Idealgas(Ib)) THEN
            IF(0.5d0*BoxSize(Ib).LT.R_Cut_LJ(Ib)) THEN
               WRITE(6,'(A,A,i1,1x,A)') ERROR, "Boxsize not large enough for Rcut LJ in box: ", Ib,
     &           " change the boxsize or Rcut."
               STOP
            END IF
            IF(0.5d0*BoxSize(Ib).LT.R_Cut_EL(Ib)) THEN
               WRITE(6,'(A,A,i1,1x,A)') ERROR, "Boxsize not large enough for Rcut Electrostatic in box: ", Ib,
     &           " change the boxsize or Rcut."
               STOP
            END IF
         END IF
      END DO

C     Check if trial moves have been set for the fractional groups
      L_Check_CFC_Trial_Move(1) = .false.
      L_Check_CFC_Trial_Move(2) = .false.
      L_Check_CFC_Trial_Move(3) = .false.
      L_Check_CFC_Trial_Move(4) = .false.

      DO Ifrac=1,N_Frac
         L_Check_CFC_Trial_Move(Type_Frac(Ifrac)) = .true.
      END DO

      IF((.NOT.L_Check_CFC_Trial_Move(1)).AND.L_NVPTHybrid) THEN
         WRITE(6,'(A,A)') WARNING, "No Fractionals of type NVT/NPT defined, switch off the CFC trial moves for this type."
         L_No_Warnings = .false.
      END IF
      IF((.NOT.L_Check_CFC_Trial_Move(2)).AND.L_GEHybrid) THEN
         WRITE(6,'(A,A)') WARNING, "No Fractionals of type GE defined, switch off the CFC trial moves for this type."
         L_No_Warnings = .false.
      END IF
      IF((.NOT.L_Check_CFC_Trial_Move(3)).AND.L_RXMCHybrid) THEN
         WRITE(6,'(A,A)') WARNING, "No Reactions defined, switch off the CFC trial moves for this type."
         L_No_Warnings = .false.
      END IF
      IF((.NOT.L_Check_CFC_Trial_Move(4)).AND.L_GCMCLambdaMove) THEN
         WRITE(6,'(A,A)') WARNING, "No Fractionals of type GCMC defined, switch off the CFC trial moves for this type."
         L_No_Warnings = .false.
      END IF      

      IF(L_No_Warnings) WRITE(6,'(A)') OK
      WRITE(6,*)

      RETURN


CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC                                                      CCC
CCC                     END OF ROUTINE                   CCC
CCC                                                      CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC


CCC   ERROR HANDLING READING INPUT
 10   IF(io.NE.0) THEN
         WRITE(6,'(A,A,i2)') ERROR, "Reading Molecule or Number of Molecules, number", N_MolType
         STOP
      END IF
 20   IF(io.NE.0) THEN
         WRITE(6,'(A,A,A)') ERROR, "Reading Molar Mass of Molecule: ", C_MolType(Tm)
         STOP
      END IF
 21   IF(io.NE.0) THEN
         WRITE(6,'(A,A,A)') ERROR, "Reading Number of Atoms in Molecule: ", C_MolType(Tm)
         STOP
      END IF
 22   IF(io.NE.0) THEN
         WRITE(6,'(A,A,A)') ERROR, "Reading Atom Positions in Molecule: ", C_MolType(Tm)
         STOP
      END IF
 23   IF(io.NE.0) THEN
         WRITE(6,'(A,A,A)') ERROR, "Reading Number of Bonds in Molecule: ", C_MolType(Tm)
         STOP
      END IF
 24   IF(io.NE.0) THEN
         WRITE(6,'(A,A,A)') ERROR, "Reading Bonds in Molecule: ", C_MolType(Tm)
         STOP
      END IF
 25   IF(io.NE.0) THEN
         WRITE(6,'(A,A,A)') ERROR, "Reading Number of Bendings in Molecule: ", C_MolType(Tm)
         STOP
      END IF
 26   IF(io.NE.0) THEN
         WRITE(6,'(A,A,A)') ERROR, "Reading Bendings in Molecule: ", C_MolType(Tm)
         STOP
      END IF
 27   IF(io.NE.0) THEN
         WRITE(6,'(A,A,A)') ERROR, "Reading Number of Torsions in Molecule: ", C_MolType(Tm)
         STOP
      END IF
 28   IF(io.NE.0) THEN
         WRITE(6,'(A,A,A)') ERROR, "Reading Torsions in Molecule: ", C_MolType(Tm)
         STOP
      END IF
 29   IF(io.NE.0) THEN
          WRITE(6,'(A,A,A)') ERROR, "Reading Number of Intramolecular interactions in Molecule: ", C_MolType(Tm)
          STOP
       END IF
 30   IF(io.NE.0) THEN
          WRITE(6,'(A,A,A)') ERROR, "Reading Intramolecular Interactions in Molecule: ", C_MolType(Tm)
          STOP
      END IF
 40   IF(io.NE.0) THEN
          WRITE(6,'(A,A)') ERROR, "Reading Partition Functions and/or Fugacity coefficients"
          STOP
       END IF
 41   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Partition Functions"
         STOP
      END IF
 42   IF(io.NE.0) THEN
         WRITE(6,'(A,A,A)') ERROR, "Reading Partition Functions of Molecule: ", Cmol
         STOP
      END IF
 43   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Fugacity Coefficients"
         STOP
      END IF
 44   IF(io.NE.0) THEN
         WRITE(6,'(A,A,A)') ERROR, "Reading Fugacity Coefficient of Molecule:", Cmol
         STOP
      END IF
 50   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Number of Fractionals"
         STOP
      END IF
 51   IF(io.NE.0) THEN
         WRITE(6,'(A,A,i2)') ERROR, "Reading Molecules in Fractional: ", Ifrac
         STOP
      END IF
 60   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Number of Reactions"
         STOP
      END IF
 61   IF(io.NE.0) THEN
         WRITE(6,'(A,A,i2)') ERROR, "Reading Reaction number: ", Ireac
         STOP
      END IF
 62   IF(io.NE.0) THEN
         WRITE(6,'(A,A,i2)') ERROR, "Reading Molecules in Reaction number: ", Ifrac
         STOP
      END IF
 63   IF(io.NE.0) THEN
         WRITE(6,'(A,A,A)') ERROR, "Reading Stoichiometric coefficient of Molecule: ", Cmol
         STOP
      END IF
 70   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading BoxSize and Delta Volume"
         STOP
      END IF
 80   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "while reading topology"
         STOP
      END IF
 81   IF(io.NE.0) THEN
         WRITE(6,'(A,A,A)') ERROR, "Reading deltas for Molecule: ", Cmol
         STOP
      END IF
 82   IF(io.NE.0) THEN
         WRITE(6,'(A,A,i2)') ERROR, "Reading deltas for Fractional Group: ", Ifrac
         STOP
      END IF
 90   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Number of Molecule Type Pairs"
         STOP
      END IF
 91   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Molecule Type Pairs"
         STOP
      END IF
 92   IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Reading Cluster Radius"
         STOP
      END IF
 999  IF(io.NE.0) THEN
         WRITE(6,'(A,A)') ERROR, "Empty line encountered while reading. Please use '-' and '#' between blocks of input"
         STOP
      END IF


      STOP

      CONTAINS

      SUBROUTINE Lookup_MoleculeLabel(MoleculeName,MoleculeIndex)
      implicit none

      character*24  MoleculeName
      integer       MoleculeIndex,I

      DO I=1,N_MolType

         IF(MoleculeName.EQ.C_MolType(I)) THEN
            MoleculeIndex = I
            RETURN
         END IF

      END DO

      WRITE(6,'(A,A,A)') ERROR, "Molecule name not found in list ", MoleculeName
      STOP

      END SUBROUTINE

      END
