CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC                                                                         CCC
CCC                           Linear interpolation                          CCC
CCC                                                                         CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC     
      SUBROUTINE Weightfunction_Linear_Interpolation(Ifrac,Ib,Rs,Lambda,W_Interpolated)
      IMPLICIT none

      include "global_variables.inc"

      INTEGER I,Ibin,Ifrac,Ib,Rs
      DOUBLE PRECISION Lambda,RevN,Rev2N,LambdaNew,W_Interpolated
      CHARACTER dummy
      
      RevN = 1.0d0/N_LambdaBin(Ifrac)
      Rev2N = 1.0d0/(2*N_LambdaBin(Ifrac))
      
      Ibin = 1 + int(dble(N_LambdaBin(Ifrac))*Lambda)
      
      IF(Lambda.LT.RevN) THEN
         W_Interpolated = Weight(Ibin,Ib,Rs,Ifrac)
      ELSEIF(Ibin.EQ.2.AND.Lambda.LT.((2*Ibin-1)*Rev2N)) THEN
         W_Interpolated = Weight(Ibin,Ib,Rs,Ifrac)+(Lambda-((2*Ibin-1)*Rev2N))*
     &   ((Weight(Ibin-1,Ib,Rs,Ifrac)-Weight(Ibin,Ib,Rs,Ifrac))/(RevN-((2*Ibin-1)*Rev2N)))
      ELSEIF(Lambda.GT.((N_LambdaBin(Ifrac)-1)*RevN)) THEN
         W_Interpolated = Weight(Ibin,Ib,Rs,Ifrac)
      ELSEIF(Ibin.EQ.(N_LambdaBin(Ifrac)-1).AND.Lambda.GT.((2*Ibin-1)*Rev2N)) THEN
         W_Interpolated = Weight(Ibin,Ib,Rs,Ifrac)+(Lambda-((2*Ibin-1)*Rev2N))*
     &   ((Weight(Ibin+1,Ib,Rs,Ifrac)-Weight(Ibin,Ib,Rs,Ifrac))/((((N_LambdaBin(Ifrac))-1)*RevN)-((2*Ibin-1)*Rev2N)))
      ELSEIF(Lambda.LT.((2*Ibin-1)*Rev2N)) THEN
         W_Interpolated = Weight(Ibin,Ib,Rs,Ifrac)+(Lambda-((2*Ibin-1)*Rev2N))*
     &   ((Weight(Ibin-1,Ib,Rs,Ifrac)-Weight(Ibin,Ib,Rs,Ifrac))/(((2*(Ibin-1)-1)*Rev2N)-((2*Ibin-1)*Rev2N)))
      ELSE
         W_Interpolated = Weight(Ibin,Ib,Rs,Ifrac)+(Lambda-((2*Ibin-1)*Rev2N))*
     &   ((Weight(Ibin+1,Ib,Rs,Ifrac)-Weight(Ibin,Ib,Rs,Ifrac))/(((2*(Ibin+1)-1)*Rev2N)-((2*Ibin-1)*Rev2N)))
      END IF

      RETURN
      END      
      
      

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC                                                                         CCC
CCC                      Interpolation using a spline                       CCC
CCC                                                                         CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC      
      SUBROUTINE Weightfunction_Spline_Interpolation(Ifrac,Ib,Rs,Lambda,W_Interpolated)
      IMPLICIT none

      include "global_variables.inc"
      
      INTEGER Ifrac,Ib,Rs,N_LambdaBins,K,Khi,Klo,Ibin
      DOUBLE PRECISION Lambda,W_Interpolated,Xa(MaxLambdaBin),Y2a(MaxLambdaBin),Ya(MaxLambdaBin),A,B,H,RevN,Rev2N

      DO Ibin=1,N_LambdaBin(Ifrac)
         Xa(Ibin)  = MidPointBins(Ibin,Ifrac)
         Ya(Ibin)  = Weight(Ibin,Ib,Rs,Ifrac)
         Y2a(Ibin) = d2Wdl2(Ibin,Ib,Rs,Ifrac)
      END DO      

      N_LambdaBins = N_LambdaBin(Ifrac)

      RevN = 1.0d0/N_LambdaBins
      Rev2N = 1.0d0/(2*N_LambdaBins)
      Ibin = 1 + int(dble(N_LambdaBins)*Lambda)

      IF(Lambda.LT.RevN) THEN
         W_Interpolated = Ya(1)
      ELSEIF(IBin.EQ.2.AND.Lambda.LT.((2*IBin-1)*Rev2N)) THEN
         Klo=1
         Khi=2
         H=Xa(Khi)-RevN
         A=(Xa(Khi)-Lambda)/H
         B=(Lambda-RevN)/H
         W_Interpolated=A*Ya(Klo)+B*Ya(Khi)+((A**3-A)*Y2a(Klo)+(B**3-B)*Y2a(Khi))
     &     *(H**2)/6.0d0
      ELSEIF(IBin.EQ.(N_LambdaBins-1).AND.Lambda.GT.((2*IBin-1)*Rev2N)) THEN
         Klo=N_LambdaBins-1
         Khi=N_LambdaBins
         H=((N_LambdaBins-1)*RevN)-Xa(Klo)
         A=(((N_LambdaBins-1)*RevN)-Lambda)/H
         B=(Lambda-Xa(Klo))/H
         W_Interpolated=A*Ya(Klo)+B*Ya(Khi)+((A**3-A)*Y2a(Klo)+(B**3-B)*Y2a(Khi))
     &     *(H**2)/6.0d0
      ELSEIF(Lambda.GT.((N_LambdaBins-1)*RevN)) THEN
         W_Interpolated = Ya(N_LambdaBins)
      ELSE
         Klo=1
         Khi=N_LambdaBins
  3      CONTINUE
         IF (Khi-Klo.Gt.1) THEN
           K=(Khi+Klo)/2
            IF(Xa(K).Gt.Lambda) THEN
               Khi=K
            ELSE
               Klo=K
            END IF
            GO TO 3
         END IF
         H=Xa(Khi)-Xa(Klo)
         A=(Xa(Khi)-Lambda)/H
         B=(Lambda-Xa(Klo))/H
         W_Interpolated=A*Ya(Klo)+B*Ya(Khi)+((A**3-A)*Y2a(Klo)+(B**3-B)*Y2a(Khi))
     &     *(H**2)/6.0d0
     
      END IF
      
      END



CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCC                                                                         CCC
CCC    Calculate second derivatives for interpolation using a spline        CCC
CCC                                                                         CCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      SUBROUTINE Weightfunction_Spline_Derivatives
      IMPLICIT none

      include "global_variables.inc"
           
      DOUBLE PRECISION P,Qn,Sig,Un,
     &                 U(MaxLambdaBin,2,MaxReactionStep,MaxFrac)
      INTEGER Ifrac,Ibin,Ib,Rs,Tf,Iff

      SAVE Iff
      DATA Iff /0/
      
C     Calculate the midpoints of bins only once (the first time this routine is called)      
      IF(Iff.EQ.0) THEN
         DO Ifrac=1,MaxFrac
            DO Ibin=1,MaxLambdaBin
               MidPointBins(Ibin,Ifrac)=((2.0d0*Ibin)-1.0d0)/(2.0d0*N_LambdaBin(Ifrac))
            END DO
         END DO
         Iff = 1
      END IF

      DO Ifrac=1,N_Frac
         Tf = Type_Frac(Ifrac)

         IF(Tf.EQ.1) THEN ! NVT/NPT
            Ib = Box_Frac(Ifrac)
            Rs = 1

            d2Wdl2(1,Ib,Rs,Ifrac) = 0.0d0
            U(1,Ib,Rs,Ifrac)      = 0.0d0

            d2Wdl2(N_LambdaBin(Ifrac),Ib,Rs,Ifrac) = 0.0d0


         ELSEIF(Tf.EQ.2) THEN ! Gibbs Ensemble
            Rs = 1

            DO Ib=1,2
               d2Wdl2(1,Ib,Rs,Ifrac) = 0.0d0
               U(1,Ib,Rs,Ifrac)      = 0.0d0

               d2Wdl2(N_LambdaBin(Ifrac),Ib,Rs,Ifrac) = 0.0d0
            END DO

         ELSEIF(Tf.EQ.3) THEN ! Reaction Ensemble
            Ib = Box_Frac(Ifrac)

            DO Rs=1,N_ReactionStep(Reaction_Frac(Ifrac))
               d2Wdl2(1,Ib,Rs,Ifrac) = 0.0d0
               U(1,Ib,Rs,Ifrac)      = 0.0d0

               d2Wdl2(N_LambdaBin(Ifrac),Ib,Rs,Ifrac) = 0.0d0
            END DO  

         ELSEIF(Tf.EQ.4) THEN ! Grand Canonical Ensemble
            Ib = Box_Frac(Ifrac)
            Rs = 1 

            d2Wdl2(1,Ib,Rs,Ifrac) = 0.0d0
            U(1,Ib,Rs,Ifrac)      = 0.0d0

            d2Wdl2(N_LambdaBin(Ifrac),Ib,Rs,Ifrac) = 0.0d0
         END IF

      END DO


      DO Ifrac=1,N_Frac
         Tf = Type_Frac(Ifrac)

         IF(Tf.EQ.1) THEN ! NVT/NPT
            Ib = Box_Frac(Ifrac)
            Rs = 1

            DO Ibin=2,N_LambdaBin(Ifrac)-1
               Sig = (MidPointBins(Ibin,Ifrac) - MidPointBins(Ibin-1,Ifrac))
     &              /(MidPointBins(Ibin+1,Ifrac) - MidPointBins(Ibin-1,Ifrac))
               P = Sig*d2Wdl2(Ibin-1,Ib,Rs,Ifrac)+2.0d0
               d2Wdl2(Ibin,Ib,Rs,Ifrac) = (Sig-1.0d0)/P
               U(Ibin,Ib,Rs,Ifrac) = (6.0d0*((Weight(Ibin+1,Ib,Rs,Ifrac)-Weight(Ibin,Ib,Rs,Ifrac))
     &                                      /(MidPointBins(Ibin+1,Ifrac)-MidPointBins(Ibin,Ifrac)) 
     &                                      -(Weight(Ibin,Ib,Rs,Ifrac)-Weight(Ibin-1,Ib,Rs,Ifrac))
     &                                      /(MidPointBins(Ibin,Ifrac)-MidPointBins(Ibin-1,Ifrac))) 
     &                                      /(MidPointBins(Ibin+1,Ifrac)-MidPointBins(Ibin-1,Ifrac)) 
     &                                      - Sig*U(Ibin-1,Ib,Rs,Ifrac))/P
            END DO

         ELSEIF(Tf.EQ.2) THEN ! Gibbs Ensemble
            Rs = 1

            DO Ib=1,N_Box
               DO Ibin=2,N_LambdaBin(Ifrac)-1
                  Sig = (MidPointBins(Ibin,Ifrac) - MidPointBins(Ibin-1,Ifrac))
     &                 /(MidPointBins(Ibin+1,Ifrac) - MidPointBins(Ibin-1,Ifrac))
                  P = Sig*d2Wdl2(Ibin-1,Ib,Rs,Ifrac)+2.0d0
                  d2Wdl2(Ibin,Ib,Rs,Ifrac) = (Sig-1.0d0)/P
                  U(Ibin,Ib,Rs,Ifrac) = (6.0d0*((Weight(Ibin+1,Ib,Rs,Ifrac)-Weight(Ibin,Ib,Rs,Ifrac))
     &                                         /(MidPointBins(Ibin+1,Ifrac)-MidPointBins(Ibin,Ifrac)) 
     &                                         -(Weight(Ibin,Ib,Rs,Ifrac)-Weight(Ibin-1,Ib,Rs,Ifrac))
     &                                         /(MidPointBins(Ibin,Ifrac)-MidPointBins(Ibin-1,Ifrac))) 
     &                                         /(MidPointBins(Ibin+1,Ifrac)-MidPointBins(Ibin-1,Ifrac)) 
     &                                         - Sig*U(Ibin-1,Ib,Rs,Ifrac))/P
               END DO
            END DO

         ELSEIF(Tf.EQ.3) THEN ! Reaction Ensemble
            Ib = Box_Frac(Ifrac)

            DO Rs=1,N_ReactionStep(Reaction_Frac(Ifrac))
               DO Ibin=2,N_LambdaBin(Ifrac)-1
                  Sig = (MidPointBins(Ibin,Ifrac) - MidPointBins(Ibin-1,Ifrac))
     &                 /(MidPointBins(Ibin+1,Ifrac) - MidPointBins(Ibin-1,Ifrac))
                  P = Sig*d2Wdl2(Ibin-1,Ib,Rs,Ifrac)+2.0d0
                  d2Wdl2(Ibin,Ib,Rs,Ifrac) = (Sig-1.0d0)/P
                  U(Ibin,Ib,Rs,Ifrac) = (6.0d0*((Weight(Ibin+1,Ib,Rs,Ifrac)-Weight(Ibin,Ib,Rs,Ifrac))
     &                                         /(MidPointBins(Ibin+1,Ifrac)-MidPointBins(Ibin,Ifrac)) 
     &                                         -(Weight(Ibin,Ib,Rs,Ifrac)-Weight(Ibin-1,Ib,Rs,Ifrac))
     &                                         /(MidPointBins(Ibin,Ifrac)-MidPointBins(Ibin-1,Ifrac))) 
     &                                         /(MidPointBins(Ibin+1,Ifrac)-MidPointBins(Ibin-1,Ifrac)) 
     &                                         - Sig*U(Ibin-1,Ib,Rs,Ifrac))/P
               END DO
            END DO

         ELSEIF(Tf.EQ.4) THEN ! Grand Canonical Ensemble
            Ib = Box_Frac(Ifrac)
            Rs = 1 

            DO Ibin=2,N_LambdaBin(Ifrac)-1
               Sig = (MidPointBins(Ibin,Ifrac) - MidPointBins(Ibin-1,Ifrac))
     &              /(MidPointBins(Ibin+1,Ifrac) - MidPointBins(Ibin-1,Ifrac))
               P = Sig*d2Wdl2(Ibin-1,Ib,Rs,Ifrac)+2.0d0
               d2Wdl2(Ibin,Ib,Rs,Ifrac) = (Sig-1.0d0)/P
               U(Ibin,Ib,Rs,Ifrac) = (6.0d0*((Weight(Ibin+1,Ib,Rs,Ifrac)-Weight(Ibin,Ib,Rs,Ifrac))
     &                                      /(MidPointBins(Ibin+1,Ifrac)-MidPointBins(Ibin,Ifrac)) 
     &                                      -(Weight(Ibin,Ib,Rs,Ifrac)-Weight(Ibin-1,Ib,Rs,Ifrac))
     &                                      /(MidPointBins(Ibin,Ifrac)-MidPointBins(Ibin-1,Ifrac))) 
     &                                      /(MidPointBins(Ibin+1,Ifrac)-MidPointBins(Ibin-1,Ifrac)) 
     &                                      - Sig*U(Ibin-1,Ib,Rs,Ifrac))/P
            END DO

         END IF         

      END DO


      DO Ifrac=1,N_Frac
         Tf = Type_Frac(Ifrac)

         IF(Tf.EQ.1) THEN ! NVT/NPT
            Ib = Box_Frac(Ifrac)
            Rs = 1

            DO Ibin=N_LambdaBin(Ifrac)-1,1,-1
               d2Wdl2(Ibin,Ib,Rs,Ifrac) = d2Wdl2(Ibin,Ib,Rs,Ifrac)*d2Wdl2(Ibin+1,Ib,Rs,Ifrac) + U(Ibin,Ib,Rs,Ifrac)
            END DO


         ELSEIF(Tf.EQ.2) THEN ! Gibbs Ensemble
            Rs = 1

            DO Ib=1,2
               DO Ibin=N_LambdaBin(Ifrac)-1,1,-1
                  d2Wdl2(Ibin,Ib,Rs,Ifrac) = d2Wdl2(Ibin,Ib,Rs,Ifrac)*d2Wdl2(Ibin+1,Ib,Rs,Ifrac) + U(Ibin,Ib,Rs,Ifrac)
               END DO
            END DO

         ELSEIF(Tf.EQ.3) THEN ! Reaction Ensemble
            Ib = Box_Frac(Ifrac)

            DO Rs=1,N_ReactionStep(Reaction_Frac(Ifrac))
               DO Ibin=N_LambdaBin(Ifrac)-1,1,-1
                  d2Wdl2(Ibin,Ib,Rs,Ifrac) = d2Wdl2(Ibin,Ib,Rs,Ifrac)*d2Wdl2(Ibin+1,Ib,Rs,Ifrac) + U(Ibin,Ib,Rs,Ifrac)
               END DO
            END DO  

         ELSEIF(Tf.EQ.4) THEN ! Grand Canonical Ensemble
            Ib = Box_Frac(Ifrac)
            Rs = 1 

            DO Ibin=N_LambdaBin(Ifrac)-1,1,-1
               d2Wdl2(Ibin,Ib,Rs,Ifrac) = d2Wdl2(Ibin,Ib,Rs,Ifrac)*d2Wdl2(Ibin+1,Ib,Rs,Ifrac) + U(Ibin,Ib,Rs,Ifrac)
            END DO
         END IF

      END DO

      RETURN
      END 
