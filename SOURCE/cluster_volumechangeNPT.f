      SUBROUTINE Cluster_VolumeChangeNPT
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "energy.inc"
      include "output.inc"

      integer Imol,Ib,I,Iatom,Select_Random_Integer,Tm,Npart,Icluster,Nclusterold,Nclusternew,
     &   Nmolecincluster(MaxCluster),Imolecincluster(MaxCluster,MaxMolInCluster)
      double precision Boxold,Volold,Volnew,Df,dX,dY,dZ,xix,xiy,xiz,
     &   zetax,zetay,zetaz,cmxold,cmyold,cmzold,cmxnew,cmynew,cmznew,
     &   E_LJ_InterNew,E_LJ_IntraNew,E_EL_RealNew,E_EL_IntraNew,E_TorsionNew,
     &   Eold,Enew,dE,XCMold(MaxMol),YCMold(MaxMol),
     &   ZCMold(MaxMol),Ran_Uniform,Xold(MaxMol,MaxAtom),E_EL_FourNew,
     &   Yold(MaxMol,MaxAtom),Zold(MaxMol,MaxAtom),E_LJ_TailOld,
     &   E_EL_SelfOld,E_EL_ExclNew,E_LJ_TailNew,E_EL_SelfNew,E_BendingNew
      double precision dU_dl,dUELFour_dl
      logical L_Overlap_Inter,L_Overlap_Intra,Laccept

C     Volume change in the NPT ensemble (with and without a fractional particle)
      Ib = Select_Random_Integer(N_Box)

      Boxold = BoxSize(Ib)
      Volold = Boxold*Boxold*Boxold
      Volnew = Volold + (2.0d0*Ran_Uniform()-1.0d0)*Delta_ClusterVolume(Ib)

      IF(Volnew.LE.0.0d0) RETURN

C     Save old configuration
      E_LJ_TailOld = U_LJ_Tail(Ib)
      E_EL_SelfOld = U_EL_Self(Ib)

      Eold = U_Total(Ib)

      IF(L_Ewald(Ib)) CALL Ewald_Store(Ib,1)      

      DO I=1,N_MolInBox(Ib)
         Imol = I_MolInBox(Ib,I)
         Tm   = TypeMol(Imol)

         DO Iatom=1,N_AtomInMolType(Tm)
            Xold(Imol,Iatom) = X(Imol,Iatom)
            Yold(Imol,Iatom) = Y(Imol,Iatom)
            Zold(Imol,Iatom) = Z(Imol,Iatom)
         END DO

         XCMold(Imol) = XCM(Imol)
         YCMold(Imol) = YCM(Imol)
         ZCMold(Imol) = ZCM(Imol)
      END DO

C     Scale box size
      Df             = (Volnew/Volold)**(1.0d0/3.0d0)
      BoxSize(Ib)    = Volnew**(1.0d0/3.0d0)
      InvBoxSize(Ib) = 1.0d0/BoxSize(Ib)

      IF(.NOT.L_IdealGas(Ib)) THEN
         IF(dsqrt(R_Cut_Max_2(Ib)).Gt.0.5d0*BoxSize(Ib)) THEN
            WRITE(6,'(A,A,i1)') ERROR, "Volume; Rcut is too large in box ", Ib
            STOP
         END IF
      END IF

      IF(Ib.EQ.Ibvapor) THEN
C     Calculate the center of mass of the cluster and scale coordinates accordingly
         CALL Find_Clusters(Nclusterold,Nmolecincluster,Imolecincluster)

         DO Icluster=1,Nclusterold
            xix = 0.0d0
            xiy = 0.0d0
            xiz = 0.0d0

            zetax = 0.0d0
            zetay = 0.0d0
            zetaz = 0.0d0

            DO I=1,Nmolecincluster(Icluster)
               Imol = Imolecincluster(Icluster,I)
               Tm   = TypeMol(Imol)

               xix = xix + dcos(XCM(Imol)*InvBoxSize(Ib)*TwoPi)*N_AtomInMolType(Tm)
               xiy = xiy + dcos(YCM(Imol)*InvBoxSize(Ib)*TwoPi)*N_AtomInMolType(Tm)
               xiz = xiz + dcos(ZCM(Imol)*InvBoxSize(Ib)*TwoPi)*N_AtomInMolType(Tm)

               zetax = zetax + dsin(XCM(Imol)*InvBoxSize(Ib)*TwoPi)*N_AtomInMolType(Tm)
               zetay = zetay + dsin(YCM(Imol)*InvBoxSize(Ib)*TwoPi)*N_AtomInMolType(Tm)
               zetaz = zetaz + dsin(ZCM(Imol)*InvBoxSize(Ib)*TwoPi)*N_AtomInMolType(Tm)
            END DO

            cmxold = (datan2(-zetax,-xix) + OnePi)*BoxSize(Ib)/TwoPi
            cmyold = (datan2(-zetay,-xiy) + OnePi)*BoxSize(Ib)/TwoPi
            cmzold = (datan2(-zetaz,-xiz) + OnePi)*BoxSize(Ib)/TwoPi

            cmxnew = cmxold*Df
            cmynew = cmyold*Df
            cmznew = cmzold*Df

            dX = cmxnew - cmxold
            dY = cmynew - cmyold
            dZ = cmznew - cmzold

            DO I=1,Nmolecincluster(Icluster)
               Imol = Imolecincluster(Icluster,I)
               Tm = TypeMol(Imol)

               DO Iatom=1,N_AtomInMolType(Tm)
                  X(Imol,Iatom) = X(Imol,Iatom) + dX
                  Y(Imol,Iatom) = Y(Imol,Iatom) + dY
                  Z(Imol,Iatom) = Z(Imol,Iatom) + dZ
               END DO

               XCM(Imol) = XCM(Imol) + dX
               YCM(Imol) = YCM(Imol) + dY
               ZCM(Imol) = ZCM(Imol) + dZ
            END DO

         END DO

C     Check if clusters changed
         CALL Find_Clusters(Nclusternew,Nmolecincluster,Imolecincluster)

C     If clusters changed than reject the move (because this would violate detailed balance)
         IF(Nclusternew.NE.Nclusterold) THEN
            Laccept=.false.
            GO TO 1
         END IF

         Npart = Nclusterold

      ELSE
C     Conventional scaling of all particles in the box
         DO I=1,N_MolInBox(Ib)
            Imol = I_MolInBox(Ib,I)
            Tm   = TypeMol(Imol)

            XCM(Imol) = XCM(Imol)*Df
            YCM(Imol) = YCM(Imol)*Df
            ZCM(Imol) = ZCM(Imol)*Df

            dX = XCM(Imol) - XCMold(Imol)
            dY = YCM(Imol) - YCMold(Imol)
            dZ = ZCM(Imol) - ZCMold(Imol)

            DO Iatom=1,N_AtomInMolType(Tm)
               X(Imol,Iatom) = XCM(Imol) + dX
               Y(Imol,Iatom) = YCM(Imol) + dY
               Z(Imol,Iatom) = ZCM(Imol) + dZ
            END DO

         END DO

         Npart = N_MolInBox(Ib)

      END IF

C     Calculate energy of the new configuration
      CALL Energy_Total(Ib,E_LJ_InterNew,E_LJ_IntraNew,E_EL_RealNew,E_EL_IntraNew,E_EL_ExclNew,
     &                     E_BendingNew,E_TorsionNew,L_Overlap_Inter,L_Overlap_Intra,dU_dl)

      IF(L_Overlap_Intra) THEN
         WRITE(6,'(A,A)') ERROR, "Intramolecular Energy Overlap (Cluster Volume Change NPT)"
         STOP
      ELSEIF(L_Overlap_Inter) THEN
         Laccept=.false.
         GO TO 1
      END IF

      E_EL_FourNew = 0.0d0

      IF(L_Ewald(Ib)) CALL Ewald_Total(Ib,E_EL_FourNew,dUELFour_dl)

      CALL Energy_Correction(Ib)

      E_LJ_TailNew = U_LJ_Tail(Ib)
      E_EL_SelfNew = U_EL_Self(Ib)

      Enew = E_LJ_InterNew + E_LJ_IntraNew + E_EL_RealNew + E_EL_IntraNew + E_BendingNew
     &     + E_TorsionNew  + E_LJ_TailNew  + E_EL_SelfNew + E_EL_ExclNew  + E_EL_FourNew

      dE = Enew - Eold

      CALL Accept_or_Reject(dexp(-beta*(dE + Pressure*(Volnew - Volold)) +
     &     dble(Npart)*dlog(Volnew/Volold)),Laccept)


   1  CONTINUE

      TrialClusterVolume(Ib) = TrialClusterVolume(Ib) + 1.0d0

      IF(Laccept) THEN

         AcceptClusterVolume(Ib) = AcceptClusterVolume(Ib) + 1.0d0

         U_LJ_Inter(Ib) = E_LJ_InterNew
         U_LJ_Intra(Ib) = E_LJ_IntraNew

         U_EL_Real(Ib)  = E_EL_RealNew
         U_EL_Intra(Ib) = E_EL_IntraNew
         U_EL_Excl(Ib)  = E_EL_ExclNew
         U_EL_Four(Ib)  = E_EL_FourNew

         U_Bending_Total(Ib) = E_BendingNew
         U_Torsion_Total(Ib) = E_TorsionNew

         U_Total(Ib) = U_Total(Ib) + dE

         Volume(Ib) = Volnew

      ELSE

         BoxSize(Ib)       = Boxold
         InvBoxSize(Ib)    = 1.0d0/BoxSize(Ib)
         U_LJ_Tail(Ib)     = E_LJ_TailOld
         U_EL_Self(Ib) = E_EL_SelfOld

         DO I=1,N_MolInBox(Ib)
            Imol = I_MolInBox(Ib,I)
            Tm   = TypeMol(Imol)

            DO Iatom=1,N_AtomInMolType(Tm)
               X(Imol,Iatom) = Xold(Imol,Iatom)
               Y(Imol,Iatom) = Yold(Imol,Iatom)
               Z(Imol,Iatom) = Zold(Imol,Iatom)
            END DO

            XCM(Imol) = XCMold(Imol)
            YCM(Imol) = YCMold(Imol)
            ZCM(Imol) = ZCMold(Imol)
         END DO

         IF(L_Ewald(Ib)) CALL Ewald_Store(Ib,2)

      END IF

      RETURN
      END
