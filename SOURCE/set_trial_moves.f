      SUBROUTINE Set_Trial_Moves
      implicit none

      include "global_variables.inc"
      include "output.inc"
      include "trial_moves.inc"

      double precision Psum
      
C     If probabilities are negative we set them to 0, this way the MC-moves won't be selected
      IF(p_Translation.LT.0.0d0)        p_Translation = 0.0d0
      IF(p_PairTranslation.LT.0.0d0)    p_PairTranslation = 0.0d0
      IF(p_ClusterTranslation.LT.0.0d0) p_ClusterTranslation = 0.0d0
      IF(p_SmartTranslation.LT.0.0d0)   p_SmartTranslation = 0.0d0
      IF(p_Rotation.LT.0.0d0)           p_Rotation = 0.0d0
      IF(p_PairRotation.LT.0.0d0)       p_PairRotation = 0.0d0
      IF(p_ClusterRotation.LT.0.0d0)    p_ClusterRotation = 0.0d0
      IF(p_SmartRotation.LT.0.0d0)      p_SmartRotation = 0.0d0
      IF(p_Volume.LT.0.0d0)             p_Volume = 0.0d0
      IF(p_ClusterVolume.LT.0.0d0)      p_ClusterVolume = 0.0d0
      IF(p_LambdaMove.LT.0.0d0)         p_LambdaMove = 0.0d0
      IF(p_GCMCLambdaMove.LT.0.0d0)     p_GCMCLambdaMove = 0.0d0
      IF(p_Bending.LT.0.0d0)            p_Bending = 0.0d0
      IF(p_Torsion.LT.0.0d0)            p_Torsion = 0.0d0
      IF(p_NVPTHybrid.LT.0.0d0)         p_NVPTHybrid = 0.0d0
      IF(p_GEHybrid.LT.0.0d0)           p_GEHybrid = 0.0d0
      IF(p_RXMCHybrid.LT.0.0d0)         p_RXMCHybrid = 0.0d0

C     Check which trial moves are used
      L_Translation = .false.
      L_PairTranslation = .false.
      L_ClusterTranslation = .false.
      L_SmartTranslation = .false.
      L_Rotation = .false.
      L_PairRotation = .false.
      L_ClusterRotation = .false.
      L_SmartRotation = .false.
      L_Volume = .false.
      L_ClusterVolume = .false.
      L_NVPTHybrid = .false.
      L_GEHybrid = .false.
      L_RXMCHybrid = .false.
      L_LambdaMove = .false.
      L_GCMCLambdaMove = .false.
      L_Bending = .false.
      L_Torsion = .false.

      IF(p_Translation.GT.zero)        L_Translation = .true.
      IF(p_PairTranslation.GT.zero)    L_PairTranslation = .true.
      IF(p_ClusterTranslation.GT.zero) L_ClusterTranslation = .true.
      IF(p_SmartTranslation.GT.zero)   L_SmartTranslation = .true.
      IF(p_Rotation.GT.zero)           L_Rotation = .true.
      IF(p_PairRotation.GT.zero)       L_PairRotation = .true.
      IF(p_ClusterRotation.GT.zero)    L_ClusterRotation = .true.
      IF(p_SmartRotation.GT.zero)      L_SmartRotation = .true.
      IF(p_Volume.GT.zero)             L_Volume = .true.
      IF(p_ClusterVolume.GT.zero)      L_ClusterVolume = .true.
      IF(p_NVPTHybrid.GT.zero)         L_NVPTHybrid = .true.
      IF(p_GEHybrid.GT.zero)           L_GEHybrid = .true.
      IF(p_RXMCHybrid.GT.zero)         L_RXMCHybrid = .true.
      IF(p_LambdaMove.GT.zero)         L_LambdaMove = .true.
      IF(p_GCMCLambdaMove.GT.zero)     L_GCMCLambdaMove = .true.
      IF(p_Bending.GT.zero)            L_Bending = .true.
      IF(p_Torsion.GT.zero)            L_Torsion = .true.

C     Normalize probabilities
      Psum = p_Translation + p_PairTranslation + p_ClusterTranslation + p_SmartTranslation
     &     + p_Rotation + p_PairRotation + p_ClusterRotation + p_SmartRotation + p_Volume
     &     + p_ClusterVolume + p_LambdaMove + p_GCMCLambdaMove + p_Bending + p_Torsion
     &     + p_NVPTHybrid + p_GEHybrid + p_RXMCHybrid

      L_No_MC_Moves = .false.
      IF(Psum.LT.zero) THEN
         L_No_MC_Moves = .true.
         RETURN
      END IF

      p_Translation        = p_Translation/Psum
      p_PairTranslation    = p_PairTranslation/Psum
      p_ClusterTranslation = p_ClusterTranslation/Psum
      p_SmartTranslation   = p_SmartTranslation/Psum
      p_Rotation           = p_Rotation/Psum
      p_PairRotation       = p_PairRotation/Psum
      p_ClusterRotation    = p_ClusterRotation/Psum
      p_SmartRotation      = p_SmartRotation/Psum
      p_Bending            = p_Bending/PSum
      p_Torsion            = p_Torsion/Psum
      p_LambdaMove         = p_LambdaMove/Psum
      p_GCMCLambdaMove     = p_GCMCLambdaMove/Psum
      p_NVPTHybrid         = p_NVPTHybrid/Psum
      p_GEHybrid           = p_GEHybrid/Psum
      p_RXMCHybrid         = p_RXMCHybrid/Psum
      p_Volume             = p_Volume/Psum
      p_ClusterVolume      = p_ClusterVolume/Psum

C     Add probabilities (create "CDF")
      p_PairTranslation    = p_PairTranslation + p_Translation
      p_ClusterTranslation = p_ClusterTranslation + p_PairTranslation
      p_SmartTranslation   = p_SmartTranslation + p_ClusterTranslation
      p_Rotation           = p_Rotation + p_SmartTranslation
      p_PairRotation       = p_PairRotation + p_Rotation
      p_ClusterRotation    = p_ClusterRotation + p_PairRotation
      p_SmartRotation      = p_SmartRotation + p_ClusterRotation
      p_Bending            = p_Bending + p_SmartRotation
      p_Torsion            = p_Torsion + p_Bending
      p_LambdaMove         = p_LambdaMove + p_Torsion
      p_GCMCLambdaMove     = p_GCMCLambdaMove + p_LambdaMove
      p_NVPTHybrid         = p_NVPTHybrid + p_GCMCLambdaMove
      p_GEHybrid           = p_GEHybrid + p_NVPTHybrid
      p_RXMCHybrid         = p_RXMCHybrid + p_GEHybrid
      p_Volume             = p_Volume + p_RXMCHybrid
      p_ClusterVolume      = p_ClusterVolume + p_Volume

C     The last probability should be equal to one if all probabilities are properly normalized
      IF(dabs(p_ClusterVolume-1.0d0).GT.zero) THEN
         WRITE(6,'(A,A)') ERROR,
     & "Something went wrong normalizing the probabilities for selecting trial moves (set_trial_moves.f)"
         STOP
      END IF

      RETURN
      END
