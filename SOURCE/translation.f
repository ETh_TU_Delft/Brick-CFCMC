      SUBROUTINE Translation
      implicit none

      include "global_variables.inc"
      include "averages_and_counters.inc"
      include "energy.inc"
      include "ewald.inc"
      include "output.inc"

C     Give random molecule a random displacement
      integer Imol,Ib,Tm,I,J,Select_Random_Integer,It,Ifrac
      double precision Ran_Uniform,Xold(MaxAtom),Yold(MaxAtom),Zold(MaxAtom),dE,dE_EL_Four,
     &      dummy1,dummy2,dummy3,dummy4,dummy5,E_LJ_InterNew,E_EL_RealNew,E_LJ_InterOld,
     &      E_EL_RealOld,XCMold,YCMold,ZCMold,Myl,Myc,Myi,Rm,dR,
     &      dlLJ_dlf,dlEL_dlf,dU_dlNew,dU_dlOld,ddU_dl,ddUELFour_dl
      logical L_Overlap_Inter,L_Overlap_Intra,Laccept,LEwald

      Rm = Ran_Uniform()

      IF(Rm.LT.0.95d0) THEN
         Imol = Select_Random_Integer(N_MolTotal)
      ELSE
         IF(N_Frac.EQ.0) RETURN
         Ifrac = Select_Random_Integer(N_Frac)
         I     = Select_Random_Integer(N_MolInFrac(Ifrac))
         Imol  = I_MolInFrac(Ifrac,I)
      END IF

      Imol = Select_Random_Integer(N_MolTotal)

      Ib   = Ibox(Imol)
      Tm   = TypeMol(Imol)

      LEwald = .false.
      IF(L_ChargeInMolType(Tm).AND.L_Ewald(Ib)) LEwald = .true.

      IF(LEwald) CALL Ewald_Init

      TrialTranslation(Ib,Tm) = TrialTranslation(Ib,Tm) + 1.0d0

      CALL Energy_Molecule(Imol,E_LJ_InterOld,dummy1,E_EL_RealOld,dummy2,dummy3,dummy4,dummy5,
     &                                                  L_Overlap_Inter,L_Overlap_Intra,dU_dlOld)

      IF(L_Overlap_Inter.OR.L_Overlap_Intra) THEN
         WRITE(6,'(A,A)') ERROR, "Energy Overlap (Translation)"
         STOP
      END IF

      DO I=1,N_AtomInMolType(Tm)
         Xold(I) = X(Imol,I)
         Yold(I) = Y(Imol,I)
         Zold(I) = Z(Imol,I)
      END DO

      XCMold = XCM(Imol)
      YCMold = YCM(Imol)
      ZCMold = ZCM(Imol)

C     Store positions and charges for Ewald summation
      IF(LEwald) THEN

         IF(L_frac(Imol)) THEN
            CALL interactionlambda(Imol,Myl,Myc,Myi,dlLJ_dlf,dlEL_dlf)
            LKSPACE(1) = dlEL_dlf
         END IF

         DO I=1,N_AtomInMolType(Tm)
            It=TypeAtom(Tm,I)
            IF(L_Charge(It)) THEN
               NKSPACE(Ib,1) = NKSPACE(Ib,1) + 1
               J = NKSPACE(Ib,1)
               XKSPACE(J,Ib,1) = X(Imol,I)
               YKSPACE(J,Ib,1) = Y(Imol,I)
               ZKSPACE(J,Ib,1) = Z(Imol,I)
               QKSPACE_Unscaled(J,Ib,1) = Q(It)

               IF(L_frac(Imol)) THEN
                  QKSPACE(J,Ib,1) = Myc*Q(It)
                  L_KSPACE_Scale(J,Ib,1)   = .true.
               ELSE
                  QKSPACE(J,Ib,1) = Q(It)
                  L_KSPACE_Scale(J,Ib,1)   = .false.
               END IF
            END IF
         END DO
      END IF

      dR = 2.0d0*(Ran_Uniform()-0.5d0)*Delta_Translation(Ib,Tm)

      Rm = 3.0d0*Ran_Uniform()

      IF(Rm.LT.1.0d0) THEN
         DO I=1,N_AtomInMolType(Tm)
            X(Imol,I) = X(Imol,I) + dR
         END DO
         XCM(Imol) = XCM(Imol) + dR
      ELSEIF(Rm.LT.2.0d0) THEN
         DO I=1,N_AtomInMolType(Tm)
            Y(Imol,I) = Y(Imol,I) + dR
         END DO
         YCM(Imol) = YCM(Imol) + dR
      ELSE
         DO I=1,N_AtomInMolType(Tm)
            Z(Imol,I) = Z(Imol,I) + dR
         END DO
         ZCM(Imol) = ZCM(Imol) + dR
      END IF

      CALL Place_molecule_back_in_box(Imol)

C     Store positions and charges for Ewald summation
      IF(LEwald) THEN

         IF(L_frac(Imol)) THEN
            CALL interactionlambda(Imol,Myl,Myc,Myi,dlLJ_dlf,dlEL_dlf)
            LKSPACE(2) = dlEL_dlf
         END IF

         DO I=1,N_AtomInMolType(Tm)
            It=TypeAtom(Tm,I)
            IF(L_Charge(It)) THEN
               NKSPACE(Ib,2) = NKSPACE(Ib,2) + 1
               J = NKSPACE(Ib,2)
               XKSPACE(J,Ib,2) = X(Imol,I)
               YKSPACE(J,Ib,2) = Y(Imol,I)
               ZKSPACE(J,Ib,2) = Z(Imol,I)
               QKSPACE_Unscaled(J,Ib,2) = Q(It)

               IF(L_frac(Imol)) THEN
                  QKSPACE(J,Ib,2) = Myc*Q(It)
                  L_KSPACE_Scale(J,Ib,2)   = .true.
               ELSE
                  QKSPACE(J,Ib,2) = Q(It)
                  L_KSPACE_Scale(J,Ib,2)   = .false.
               END IF
            END IF
         END DO
      END IF

      CALL Energy_Molecule(Imol,E_LJ_InterNew,dummy1,E_EL_RealNew,dummy2,dummy3,dummy4,dummy5,
     &                                                  L_Overlap_Inter,L_Overlap_Intra,dU_dlNew)
      IF(L_Overlap_Intra) THEN
         WRITE(6,'(A,A)') ERROR, "Intramolecular Energy Overlap (Translation)"
         STOP
      ELSEIF(L_Overlap_Inter) THEN
         Laccept=.false.
         GO TO 1
      END IF

      dE_EL_Four = 0.0d0
      ddUELFour_dl = 0.0d0
      IF(LEwald) CALL Ewald_Move(Ib,dE_EL_Four,ddUELFour_dl)

      dE = E_LJ_InterNew + E_EL_RealNew - E_LJ_InterOld - E_EL_RealOld + dE_EL_Four

      CALL Accept_or_Reject(dexp(-beta*dE),Laccept)

   1  CONTINUE

      IF(Laccept) THEN
         AcceptTranslation(Ib,Tm) = AcceptTranslation(Ib,Tm) + 1.0d0

         U_LJ_Inter(Ib) = U_LJ_Inter(Ib) + E_LJ_InterNew - E_LJ_InterOld
         U_EL_Real(Ib)  = U_EL_Real(Ib)  + E_EL_RealNew  - E_EL_RealOld
         U_EL_Four(Ib)  = U_EL_Four(Ib)  + dE_EL_Four

         U_Total(Ib) = U_Total(Ib) + dE

         ddU_dl     = dU_dlNew - dU_dlOld + ddUELFour_dl
         dU_dlTotal = dU_dlTotal + ddU_dl

         IF(LEwald) CALL Ewald_Accept(Ib)

      ELSE

         DO I=1,N_AtomInMolType(Tm)
            X(Imol,I) = Xold(I)
            Y(Imol,I) = Yold(I)
            Z(Imol,I) = Zold(I)
         END DO

         XCM(Imol) = XCMold
         YCM(Imol) = YCMold
         ZCM(Imol) = ZCMold

      END IF

      RETURN
      END
