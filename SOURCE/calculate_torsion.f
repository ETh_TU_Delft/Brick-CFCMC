      SUBROUTINE Calculate_Torsion(Ttype,Utors,RX,RY,RZ)
      implicit none

      include "global_variables.inc"
      include "torsion.inc"

C     Calculate torsion energy

      integer          Ttype
      double precision RX(4),RY(4),RZ(4),xab,yab,zab,xbc,ybc,zbc,xcd,ycd,zcd,
     &     pbx,pby,pbz,pcx,pcy,pcz,pc2,pb2,Utors,rpb1,rpc1,
     &     cost,sint,theta,pbpc,rrbc,cos1,cos2,cos3,cos4,cos5

      xab = RX(1) - RX(2)
      yab = RY(1) - RY(2)
      zab = RZ(1) - RZ(2)

      xbc = RX(2) - RX(3)
      ybc = RY(2) - RY(3)
      zbc = RZ(2) - RZ(3)

      xcd = RX(3) - RX(4)
      ycd = RY(3) - RY(4)
      zcd = RZ(3) - RZ(4)

      pbx = yab*zbc - zab*ybc
      pby = zab*xbc - xab*zbc
      pbz = xab*ybc - yab*xbc

      pcx = ybc*zcd - zbc*ycd
      pcy = zbc*xcd - xbc*zcd
      pcz = xbc*ycd - ybc*xcd

      pc2 = pcx*pcx + pcy*pcy + pcz*pcz
      pb2 = pbx*pbx + pby*pby + pbz*pbz

      pbpc = pbx*pcx + pby*pcy + pbz*pcz

      rpb1 = 1.0D0/DSQRT(pb2)
      rpc1 = 1.0D0/DSQRT(pc2)

      rrbc = 1.0D0/DSQRT(xbc*xbc + ybc*ybc + zbc*zbc)

      cost = pbpc*rpb1*rpc1

      sint = (xbc*(pcy*pbz-pcz*pby) + ybc*(pbx*pcz-pbz*pcx) + zbc*(pcx*pby-pcy*pbx))*(rpb1*rpc1*rrbc)

      theta = DATAN2(sint,cost)

      cos1 = dcos(theta)
      cos2 = cos1*cos1
      cos3 = cos2*cos1
      cos4 = cos3*cos1
      cos5 = cos4*cos1

      Utors = A0(Ttype)      + A1(Ttype)*cos1 + A2(Ttype)*cos2
     &      + A3(Ttype)*cos3 + A4(Ttype)*cos4 + A5(Ttype)*cos5


      RETURN
      END
