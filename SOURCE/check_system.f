      SUBROUTINE Check_System(Lresult)
      implicit None

      include "global_variables.inc"
      include "output.inc"

      integer           Nmol,Zmptpb(2,MaxMolType),Imol,Ib,Tm,Ifrac,I,Iatom
      double precision  Myl,bondlen(MaxMolType,MaxBond),dX,dY,dZ,Nwhole(2),Numdens(2),XCM0,YCM0,ZCM0
      logical           Llist,Lresult

      Lresult=.true.

C     Check total number of molecules
      IF(N_MolTotal.NE.(N_MolInBox(1)+N_MolInBox(2))) THEN
         WRITE(6,'(A,A)') ERROR, "Sum of number of molecules in boxes is not equal to total number of molecules"
         Lresult=.false.
      END IF

      Nmol = 0
      DO Ib=1,N_Box
         DO Tm=1,N_MolType
            Nmol = Nmol + Nmptpb(Ib,Tm)
         END DO
      END DO

      DO Ifrac=1,N_Frac
         Nmol = Nmol + N_MolInFrac(Ifrac)
      END DO

      IF(N_MolTotal.NE.Nmol) THEN
         WRITE(6,'(A,A)') ERROR, "Sum of number of molecules and fractionals is not equal to total number of molecules"
         Lresult=.false.
      END IF

C     Check total number of whole molecules per type
      DO Ib=1,N_Box
         DO Tm=1,N_MolType
            Zmptpb(Ib,Tm) = 0
         END DO
      END DO

      DO Imol=1,N_MolTotal
         IF(.NOT.L_Frac(Imol)) THEN
            Zmptpb(Ibox(Imol),TypeMol(Imol)) = Zmptpb(Ibox(Imol),TypeMol(Imol)) + 1
         END IF
      END DO

      DO Ib=1,N_Box
         DO Tm=1,N_MolType
            IF(Zmptpb(Ib,Tm).NE.Nmptpb(Ib,Tm)) THEN
               WRITE(6,'(A,A)') ERROR, "Difference in number of whole molecules per type"
               WRITE(6,'(A,1x,i1,1x,i2,1x,i4,1x,i4)') "Box, TypeMol, Counted, Simulation", Ib, Tm, Zmptpb(Ib,Tm), Nmptpb(Ib,Tm)
               Lresult=.false.
            END IF
         END DO
      END DO

C     Check if every particle is in the right list
      DO Imol=1,N_MolTotal

         Ib    = Ibox(Imol)
         Llist = .false.

         DO I=1,N_MolInBox(Ib)
            IF(I_MolInBox(Ib,I).EQ.Imol) THEN
               IF(Llist) THEN
                  WRITE(6,'(A,A,i5)') ERROR, "Molecule is more than one time in a list", Imol
                  Lresult=.false.
               ELSE
                  Llist=.true.
               END IF
            END IF

         END DO

         IF(.NOT.Llist) THEN
            WRITE(6,'(A,A,i5)') ERROR, "Molecule is not in list of molecules", Imol
            Lresult=.false.
         END IF

      END DO

      DO Ib=1,N_Box
         DO Tm=1,N_MolType
            DO I=1,Nmptpb(Ib,Tm)
               Imol = Imptpb(Ib,Tm,I)
               IF(TypeMol(Imol).NE.Tm) THEN
                  WRITE(6,'(A,A)') ERROR, "Molecule in list if of wrong type"
                  WRITE(6,'(A,1x,i1,1x,i2,1x,i5)') "Box, TypeMol, Imol", Ib, Tm, Imol
                  Lresult=.false.
               END IF
               IF(Ibox(Imol).NE.Ib) THEN
                  WRITE(6,'(A,A)') ERROR, "Molecule in list is in wrong box"
                  WRITE(6,'(A,1x,i1,1x,i2,1x,i5)') "Box, TypeMol, Imol", Ib, Tm, Imol
                  Lresult=.false.
               END IF
            END DO
         END DO
      END DO

C     Check fractionals (box, label and lambda)
      DO Ifrac=1,N_Frac
         Ib  = Box_Frac(Ifrac)
         Myl = Lambda_Frac(Ifrac)

         IF(Myl.LT.0.0d0.OR.Myl.GT.1.0d0) THEN
            WRITE(6,'(A,A)') ERROR, "Value of lambda"
            WRITE(6,'(A,1x,f7.4,1x,i2)') "Lambda, Fractional", Myl, Ifrac
            Lresult=.false.
         END IF

         DO I=1,N_MolInFrac(Ifrac)
            Imol = I_MolInFrac(Ifrac,I)
            IF(Ibox(Imol).NE.Ib) THEN
               WRITE(6,'(A,A)') ERROR, "Component of fractional not in right box"
               WRITE(6,'(A,1x,i2,1x,i5,1x,i1,1x,i1)') "Ifrac, Imol, Box, Box(Imol)", IFrac, Imol, Ib, Ibox(Imol)
               Lresult=.false.
            END IF
            IF(.NOT.L_Frac(Imol)) THEN
               WRITE(6,'(A,A,1x,i5)') ERROR, "Molecule is not labeled as fractional", Imol
               Lresult=.false.
            END IF
         END DO

      END DO

C     Check centers of mass
      DO Imol=1,N_MolTotal
         XCM0 = 0.0d0
         YCM0 = 0.0d0
         ZCM0 = 0.0d0

         DO Iatom=1,N_AtomInMolType(TypeMol(Imol))
            XCM0 = XCM0 + X(Imol,Iatom)
            YCM0 = YCM0 + Y(Imol,Iatom)
            ZCM0 = ZCM0 + Z(Imol,Iatom)
         END DO

         XCM0 = XCM0/dble(N_AtomInMolType(TypeMol(Imol)))
         YCM0 = YCM0/dble(N_AtomInMolType(TypeMol(Imol)))
         ZCM0 = ZCM0/dble(N_AtomInMolType(TypeMol(Imol)))

         IF((abs(XCM0-XCM(Imol)).GT.1.0D-6).OR.(abs(YCM0-YCM(Imol)).GT.1.0D-6).OR.(abs(ZCM0-ZCM(Imol)).GT.1.0D-6)) THEN
            WRITE(6,'(A,i5)') ERROR, "Wrong center of mass for molecule", Imol
            Lresult=.false.
         END IF

      END DO

C     Check bondlengths
      DO Tm=1,N_MolType
         DO I=1,N_BondInMolType(Tm)
            dX = Xin(Tm,Bondlist(Tm,I,1)) - Xin(Tm,Bondlist(Tm,I,2))
            dY = Yin(Tm,Bondlist(Tm,I,1)) - Yin(Tm,Bondlist(Tm,I,2))
            dZ = Zin(Tm,Bondlist(Tm,I,1)) - Zin(Tm,Bondlist(Tm,I,2))
            bondlen(Tm,I) = dX*dX + dY*dY + dZ*dZ
         END DO
      END DO

      DO Imol=1,N_MolTotal
         Tm = TypeMol(Imol)
         DO I=1,N_BondInMolType(Tm)
            dX = X(Imol,Bondlist(Tm,I,1)) - X(Imol,Bondlist(Tm,I,2))
            dY = Y(Imol,Bondlist(Tm,I,1)) - Y(Imol,Bondlist(Tm,I,2))
            dZ = Z(Imol,Bondlist(Tm,I,1)) - Z(Imol,Bondlist(Tm,I,2))
            IF(abs(dsqrt(dX*dX+dY*dY+dZ*dZ)-dsqrt(bondlen(Tm,I))).GT.1.0D-4) THEN
               WRITE(6,'(A,A,i3,A,i5,A)') ERROR, "Bond ", I, " in molecule ", Imol, " has changed length"
               Lresult=.false.
            END IF
         END DO
      END DO

C     Check if vapor phase box at the end of the simulation is still the same
      IF(N_Box.EQ.2) THEN
         DO Ib=1,N_Box
            Nwhole(Ib) = 0.0d0
            DO Tm=1,N_MolType
               Nwhole(Ib) = Nwhole(Ib) + Nmptpb(Ib,Tm)
            END DO
            Numdens(Ib) = Nwhole(Ib)/Volume(Ib)
         END DO

         IF(Numdens(1).LT.Numdens(2)) THEN
            IF(Ibvapor.NE.1) THEN
               WRITE(6,'(A,A)') WARNING, "Vapor phase box has changed during simulation."
               Lresult=.false.
            END IF
         ELSE
            IF(Ibvapor.NE.2) THEN
               WRITE(6,'(A,A)') WARNING, "Vapor phase box has changed during simulation."
               Lresult=.false.
            END IF
         END IF
      END IF

      Return
      End
