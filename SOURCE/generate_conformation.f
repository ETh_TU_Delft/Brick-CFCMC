      SUBROUTINE Generate_conformation(Imol)
      implicit none

      include "global_variables.inc"
      include "output.inc"

      integer Imol,Tm,Iatom,Nmove,Imove,Ib
      double precision Pbend,dE,Rm,Ran_Uniform,dX,dY,dZ,
     &      E_LJ_IntraOld,E_EL_IntraOld,E_BendingOld,Etorsionold,Eold,
     &      E_LJ_IntraNew,E_EL_IntraNew,E_BendingNew,Etorsionnew,Enew,
     &      Xold(MaxAtom),Yold(MaxAtom),Zold(MaxAtom),XCMold,YCMold,ZCMold
      logical L_Overlap_Intra,Laccept

      Tm = TypeMol(Imol)
      Ib = Ibox(Imol)

      dX = Ran_Uniform()*BoxSize(Ib)
      dY = Ran_Uniform()*BoxSize(Ib)
      dZ = Ran_Uniform()*BoxSize(Ib)

      XCM(Imol) = 0.0d0
      YCM(Imol) = 0.0d0
      ZCM(Imol) = 0.0d0

      DO Iatom=1,N_AtomInMolType(Tm)
         Y(Imol,Iatom) = Yin(Tm,Iatom) + dX
         X(Imol,Iatom) = Xin(Tm,Iatom) + dY
         Z(Imol,Iatom) = Zin(Tm,Iatom) + dZ

         XCM(Imol) = XCM(Imol) + X(Imol,Iatom)
         YCM(Imol) = YCM(Imol) + Y(Imol,Iatom)
         ZCM(Imol) = ZCM(Imol) + Z(Imol,Iatom)
      END DO

      XCM(Imol) = XCM(Imol)/dble(N_AtomInMolType(Tm))
      YCM(Imol) = YCM(Imol)/dble(N_AtomInMolType(Tm))
      ZCM(Imol) = ZCM(Imol)/dble(N_AtomInMolType(Tm))

      CALL Place_molecule_back_in_box(Imol)
      CALL Random_Orientation(Imol)

      Nmove = 50*(N_BendingInMolType(Tm) + N_TorsionInMolType(Tm))

      IF(Nmove.EQ.0) RETURN

      Pbend = dble(N_BendingInMolType(Tm))/dble(N_BendingInMolType(Tm) + N_TorsionInMolType(Tm))

      CALL Energy_Intramolecular(Imol,E_LJ_IntraOld,E_EL_IntraOld,E_BendingOld,Etorsionold,L_Overlap_Intra)
      IF(L_Overlap_Intra) THEN
         WRITE(6,'(A,A)') ERROR, "Energy overlap (generate_conformation.f)"
         STOP
      END IF

      Eold = E_LJ_IntraOld + E_EL_IntraOld + E_BendingOld + Etorsionold

      DO Imove=1,Nmove

         DO Iatom=1,N_AtomInMolType(Tm)
            Yold(Iatom) = Y(Imol,Iatom)
            Xold(Iatom) = X(Imol,Iatom)
            Zold(Iatom) = Z(Imol,Iatom)
         END DO

         XCMold = XCM(Imol)
         YCMold = YCM(Imol)
         ZCMold = ZCM(Imol)

         Rm = Ran_Uniform()

         IF(Rm.LT.Pbend) THEN
            CALL Random_Bending(Imol)
         ELSE
            CALL Random_Torsion(Imol)
         END IF

         XCM(Imol) = 0.0d0
         YCM(Imol) = 0.0d0
         ZCM(Imol) = 0.0d0

         DO Iatom=1,N_AtomInMolType(Tm)
            XCM(Imol) = XCM(Imol) + X(Imol,Iatom)
            YCM(Imol) = YCM(Imol) + Y(Imol,Iatom)
            ZCM(Imol) = ZCM(Imol) + Z(Imol,Iatom)
         END DO

         XCM(Imol) = XCM(Imol)/dble(N_AtomInMolType(Tm))
         YCM(Imol) = YCM(Imol)/dble(N_AtomInMolType(Tm))
         ZCM(Imol) = ZCM(Imol)/dble(N_AtomInMolType(Tm))

         CALL Place_molecule_back_in_box(Imol)

         CALL Energy_Intramolecular(Imol,E_LJ_IntraNew,E_EL_IntraNew,E_BendingNew,Etorsionnew,L_Overlap_Intra)
         IF(L_Overlap_Intra) THEN
            Laccept = .false.
            GO TO 1
         END IF

         Enew = E_LJ_IntraNew + E_EL_IntraNew + E_BendingNew + Etorsionnew

         dE = Enew - Eold

         CALL Accept_or_Reject(dexp(-beta*dE),Laccept)

   1     CONTINUE

         IF(Laccept) THEN

            Eold = Enew

         ELSE

            DO Iatom=1,N_AtomInMolType(Tm)
               Y(Imol,Iatom) = Yold(Iatom)
               X(Imol,Iatom) = Xold(Iatom)
               Z(Imol,Iatom) = Zold(Iatom)
            END DO

            XCM(Imol) = XCMold
            YCM(Imol) = YCMold
            ZCM(Imol) = ZCMold

         END IF


      END DO

      RETURN
      END



      SUBROUTINE Random_Bending(Imol)
      implicit none

      include "global_variables.inc"
      include "bending.inc"

      integer Imol,Iatom,Iatom1,Iatom2,Iatom3,I,Ibend,Il,Tm,Select_Random_Integer
      double precision phi,s,c,u1,u2,u3,v1,v2,v3,n1,n2,n3,nnorm,Ox,Oy,Oz,Xo,Yo,Zo,Ran_Uniform

      Tm = TypeMol(Imol)

      IF(N_BendingInMolType(Tm).EQ.0) RETURN
      Ibend = Select_Random_Integer(N_BendingInMolType(Tm))
      Il = Select_Random_Integer(2)

      phi = TwoPi*(Ran_Uniform()-0.5d0)

      s = dsin(0.5d0*phi)
      c = dcos(0.5d0*phi)

      Iatom1 = BendingList(Tm,Ibend,1)
      Iatom2 = BendingList(Tm,Ibend,2)
      Iatom3 = BendingList(Tm,Ibend,3)

      u1 = X(Imol,Iatom1) - X(Imol,Iatom2)
      u2 = Y(Imol,Iatom1) - Y(Imol,Iatom2)
      u3 = Z(Imol,Iatom1) - Z(Imol,Iatom2)
      v1 = X(Imol,Iatom3) - X(Imol,Iatom2)
      v2 = Y(Imol,Iatom3) - Y(Imol,Iatom2)
      v3 = Z(Imol,Iatom3) - Z(Imol,Iatom2)

      n1 = u2*v3 - u3*v2
      n2 = u3*v1 - u1*v3
      n3 = u1*v2 - u2*v1

      nnorm = 1.0d0/dsqrt(n1*n1 + n2*n2 + n3*n3)

      n1 = n1*nnorm
      n2 = n2*nnorm
      n3 = n3*nnorm

      Ox = X(Imol,Iatom2)
      Oy = Y(Imol,Iatom2)
      Oz = Z(Imol,Iatom2)

      DO I=1,Natombendlist(Tm,Ibend,Il)
         Iatom=Iatombendlist(Tm,Ibend,Il,I)

         Xo = X(Imol,Iatom) - Ox
         Yo = Y(Imol,Iatom) - Oy
         Zo = Z(Imol,Iatom) - Oz

         X(Imol,Iatom) = s*s*(Xo*(n1*n1-n2*n2-n3*n3) + 2.0d0*n1*(Yo*n2+Zo*n3))
     &        + 2.0d0*s*c*(Zo*n2-Yo*n3) + c*c*Xo + Ox
         Y(Imol,Iatom) = s*s*(Yo*(n2*n2-n3*n3-n1*n1) + 2.0d0*n2*(Xo*n1+Zo*n3))
     &        + 2.0d0*s*c*(Xo*n3-Zo*n1) + c*c*Yo + Oy
         Z(Imol,Iatom) = s*s*(Zo*(n3*n3-n1*n1-n2*n2) + 2.0d0*n3*(Xo*n1+Yo*n2))
     &        + 2.0d0*s*c*(Yo*n1-Xo*n2) + c*c*Zo + Oz

      END DO

      RETURN
      END


      SUBROUTINE Random_Torsion(Imol)
      implicit none

      include "global_variables.inc"
      include "torsion.inc"

      integer Imol,Iatom,Iatom1,Iatom2,I,Tm,Itors,Il,Select_Random_Integer
      double precision phi,s,c,Ox,Oy,Oz,Rx,Ry,Rz,R,Ri,Xo,Yo,Zo,Ran_Uniform

      Tm = TypeMol(Imol)

      IF(N_TorsionInMolType(Tm).EQ.0) RETURN
      Itors = Select_Random_Integer(N_TorsionInMolType(Tm))
      Il = Select_Random_Integer(2)

      phi = TwoPi*(Ran_Uniform()-0.5d0)

      s = dsin(0.5d0*phi)
      c = dcos(0.5d0*phi)

      Iatom1 = TorsionList(Tm,Itors,2)
      Iatom2 = TorsionList(Tm,Itors,3)

      Ox = 0.5d0*(X(Imol,Iatom1)+X(Imol,Iatom2))
      Oy = 0.5d0*(Y(Imol,Iatom1)+Y(Imol,Iatom2))
      Oz = 0.5d0*(Z(Imol,Iatom1)+Z(Imol,Iatom2))

      Rx = X(Imol,Iatom1) - X(Imol,Iatom2)
      Ry = Y(Imol,Iatom1) - Y(Imol,Iatom2)
      Rz = Z(Imol,Iatom1) - Z(Imol,Iatom2)

      R  = dsqrt(Rx*Rx+Ry*Ry+Rz*Rz)
      Ri = 1.0d0/R

      Rx = Rx*Ri
      Ry = Ry*Ri
      Rz = Rz*Ri

      DO I=1,Natomtorslist(Tm,Itors,Il)
         Iatom = Iatomtorslist(Tm,Itors,Il,I)

         Xo = X(Imol,Iatom) - Ox
         Yo = Y(Imol,Iatom) - Oy
         Zo = Z(Imol,Iatom) - Oz

         X(Imol,Iatom) = s*s*(Xo*(Rx*Rx-Ry*Ry-Rz*Rz) + 2.0d0*Rx*(Yo*Ry+Zo*Rz))
     &        + 2.0d0*s*c*(Zo*Ry-Yo*Rz) + c*c*Xo + Ox
         Y(Imol,Iatom) = s*s*(Yo*(Ry*Ry-Rz*Rz-Rx*Rx) + 2.0d0*Ry*(Xo*Rx+Zo*Rz))
     &        + 2.0d0*s*c*(Xo*Rz-Zo*Rx) + c*c*Yo + Oy
         Z(Imol,Iatom) = s*s*(Zo*(Rz*Rz-Rx*Rx-Ry*Ry) + 2.0d0*Rz*(Xo*Rx+Yo*Ry))
     &        + 2.0d0*s*c*(Yo*Rx-Xo*Ry) + c*c*Zo + Oz

      END DO
      RETURN

      END
