# Brick-CFCMC
This software package is designed for performing force field-based Molecular
Simulations of single- and multicomponent fluids using state of the
art Continuous Fractional Component Monte Carlo techniques. Various
ensembles can be combined: *NVT*, *NPT*, the Gibbs Ensemble, the
Reaction Ensemble, the Grand-Canonical Ensemble, and the Osmotic
Ensemble. Properties such as excess chemical potentials, fugacity
coefficients, partial molar enthalpies, and partial molar volumes can be
directly obtained from single simulations. Brick-CFCMC can also
perform thermodynamic integration to compute free energies (including
ionic systems). For documentation, we would like to refer to the following:

- Brick-CFCMC manual, available from [Gitlab][gitlabeth], or from [our website][ethmanual]
- [Main publication of Brick-CFCMC][paper1] and its Supporting Information (open
access)
- [Review paper on the CFCMC technique][paper2] (open access)
- [Thermodynamic Integrations and Collective Trial Moves in Brick-CFCMC][paper3] (open access)
- [PhD thesis of Remco Hens][thesis1] (open access) [(alternative website)][thesis1a]
- [PhD thesis of Ahmadreza Rahbari][thesis2] (open access) [(alternative website)][thesis2a]
- [Website][web1] of the Engineering Thermodynamics group at Delft University of Technology

[gitlabeth]: <https://gitlab.com/ETh_TU_Delft/Brick-CFCMC>
[ethmanual]: <https://thijsvlugt.github.io/website/Brick-CFCMC/Brick-CFCMC.pdf>
[paper1]: <https://doi.org/10.1021/acs.jcim.0c00334>
[paper2]: <https://doi.org/10.1080/08927022.2020.1828585>
[paper3]: <https://doi.org/10.1021/acs.jcim.1c00652>
[thesis1]: <https://doi.org/10.4233/uuid:41c32a8f-2db7-4091-abb5-4d6a5e596345>
[thesis1a]: <https://homepage.tudelft.nl/v9k6y/thesis-Hens.pdf>
[thesis2]: <https://doi.org/10.4233/uuid:eb04d860-281a-4c6b-8c5b-263f526d0bd9>
[thesis2a]: <https://homepage.tudelft.nl/v9k6y/thesis-Rahbari.pdf>
[web1]: <https://www.tudelft.nl/3me/over/afdelingen/process-energy/chairs/engineering-thermodynamics/people>

If you use Brick-CFCMC in your publication, please cite the following papers:

- [J. Chem. Inf. Model., 2020, 60, 2678-2682][paper1]
- [Molecular Simulation, 2021, 47, 804-823][paper2]
- [J. Chem. Inf. Model., 2021, 61, 3752-3757][paper3]

# Quick Start
We assume that you are working on a Linux/Unix system, and that you
have installed the [Intel Fortran Compiler][intel]. If you do not have this
compiler installed, you can compile the Brick-CFCMC using the [GNU
Fortran Compiler][gfortran].
To obtain Brick-CFCMC make a clone of this repository on your local machine:
```sh
git clone https://gitlab.com/ETh_TU_Delft/Brick-CFCMC.git brick
```
or, if you have set up a GitLab account:
```sh
git clone git@gitlab.com:ETh_TU_Delft/Brick-CFCMC.git brick
```

[intel]: <https://software.intel.com/en-us/fortran-compilers>
[gfortran]: <https://gcc.gnu.org/fortran>

Brick-CFCMC comes with commands that makes it easier to use. In order to use
these commands, add the following three lines to your *.bashrc*: (we
assume that ``bash`` is your default shell)
```vim
export BRICK_DIR=${HOME}/brick
. ${BRICK_DIR}/.brick.sh
. ${BRICK_DIR}/.autocompletion
```
and source your *.bashrc*:
```sh
source ~/.bashrc
```
The whole software package can now easily be compiled by running:
```sh
brick compile
```
which compiles the main source code as well as all tools that come
with Brick-CFCMC with both the [Intel Fortran Compiler][intel] and the
[GNU Fortran Compiler][gfortran]. If you do not have the [Intel Fortran Compiler][intel], please type
the following instead:
```sh
brick compile -g
```

If you are a new user to Brick-CFCMC, we advise you to run one of the
examples, provided in the directory ``${BRICK_DIR}/EXAMPLES``. These
examples are documented in detail in the Brick-CFCMC manual. For example, to
compute the vapor-liquid equilibrium of methanol using the Gibbs
Ensemble technique, please type

```sh
cd ${BRICK_DIR}/EXAMPLES/Gibbs_Ensemble/Example_2_VLE_Methanol
```

This directory will shown a directory ``INPUT `` and an executable
``run ``. To start this simulation, please type

```sh
./run --terminal
```

which will run Brick-CFCMC and write output to the terminal. Please have a
careful look at the input and output files.

# Creating your own simulation

To set up your first simulation run yourself, please type first:
```sh
brick new MyFirstSimulation
```
and next, to create input files for the simulation:
```sh
brick input
```
This will execute the tool that helps you create the input files. When
you have created the input files for your simulation you can run your
simulation by typing:

```sh
./run --terminal
```

which will run Brick-CFCMC and write output to the terminal.

# To conclude..

This introduced the most basic commands in Brick-CFCMC. For more
information, other commands,  and options see the references listed at
the beginning of this document.

Good luck!

The BRICK-CFCMC team

# License for using Brick-CFCMC

Copyright (c) 2021 R. Hens, A. Rahbari, S. Caro-Ortiz, N. Dawass,
M. Erdos, H.M. Polat, A. Poursaeidesfahani, H.S. Salehi, A.T. Celebi,
M. Ramdin, O.A. Moultos, D. Dubbeldam, and T.J.H. Vlugt

Permission is hereby granted, free of charge, to any person
obtaining a copy  of this software and associated documentation
files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

-The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

-THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

